import React, { Component } from "react";
import {
  BrowserRouter,
  HashRouter,
  Redirect,
  Route,
  Switch,
} from "react-router-dom";
import "./scss/style.scss";
import Timeout from "./views/timeout";
import AdBack from "./views/adback";

// Login
const Login = React.lazy(() => import("./views/login/Login"));
const Dashboard = React.lazy(() => import("./views/dashboard/Dashboard_Login"));

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
);

// Containers
const TheLayout = React.lazy(() => import("./containers/TheLayout"));

class App extends Component {
  constructor(props) {
    super(props);
  }
  componentWillMount() {
    console.log("willmount::", this.props);
  }
  render() {
    return (
      <BrowserRouter>
        <React.Suspense fallback={loading}>
          <Switch>
            <Route
              path="/home"
              name="Home"
              render={(props) => <TheLayout {...props} />}
            />
            <Route
              exact
              path="/login"
              name="Login Page"
              render={(props) => <Login {...props} />}
            />
            <Route
              exact
              path="/timeout"
              name="Timeout"
              render={(props) => <Timeout {...props} />}
            />
            <Route
              exact
              path="/"
              name="AdBack"
              render={(props) => <AdBack {...props} />}
            />

            <Route
              exact
              path="*"
              name="notfound"
              render={(props) => <AdBack {...props} />}
            />

            {/* <Route exact path="/" render={() => <Redirect to="/login" />} /> */}
            <Route
              exact
              path="/dashboard"
              name="Dashboard Page"
              render={(props) => <TheLayout {...props} />}
            />
            {/* <Route exact path="/" render={() => <Redirect to="/dashboard" />} /> */}
          </Switch>
        </React.Suspense>
      </BrowserRouter>
    );
  }
}

export default App;
