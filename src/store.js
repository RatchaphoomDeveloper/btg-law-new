import { createStore, combineReducers, applyMiddleware } from "redux";
import mainLawReducer from "./redux/mainLaw/mainLawReducer";
import mainLawSubReducer from "./redux/mainLawSub/mainLawSubReducer";
import priorityOptionReducer from "./redux/priorityOption/priorityOptionReducer";
import buassignReducer from "./redux/buassigndata/buassignReducer";
import buassignSelectorReducer from "./redux/buassigndata/buassignSelectorReducer";
import lawinfoReducer from "./redux/lawinfoDetail/lawinfoReducer";
import saveBuReducer from "./redux/saveBu/saveBuReducer";
import savetranbrsfileReducer from "./redux/savetranbrcsfiledb/savetranbrsfileReducer";
import savetransbrsbuReducer from "./redux/savetransbrsbu/savetransbrsbuReducer";
import gettranbrcsbuReducer from "./redux/gettranbrcsbu/gettranbrcsbuReducer";
import getBrcsAssessmentReducer from "./redux/getbrcaassessment/getbrcaassessmentReducer";

import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import thunk from "redux-thunk";
import logger from "redux-logger";
import { composeWithDevTools } from "redux-devtools-extension";

const initialState = {
  sidebarShow: "responsive",
  asideShow: false,
  darkMode: false,
  user: {},
  token: "",
};

const changeState = (state = initialState, { type, ...rest }) => {
  switch (type) {
    case "set":
      return { ...state, ...rest };
    case "set_user":
      return { ...state, ...rest };
    default:
      return state;
  }
};

const persistConfig = {
  key: "root",
  storage,
};

const rootReducer = combineReducers({
  changeState,
  mainLaws: mainLawReducer,
  mainLawsSub: mainLawSubReducer,
  priorityOption: priorityOptionReducer,
  saveBu: saveBuReducer,
  buassignReducer,
  buassignSelectorReducer,
  lawinfoReducer,
  savetranbrsfileDB: savetranbrsfileReducer,
  savetransbrsbu: savetransbrsbuReducer,
  gettranbrcsbu: gettranbrcsbuReducer,
  getbrcaassessment: getBrcsAssessmentReducer,
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

export default () => {
  let store = createStore(
    persistedReducer,
    //composeWithDevTools(applyMiddleware(logger, thunk)),
    composeWithDevTools(applyMiddleware(thunk))
  );
  let persistor = persistStore(store);
  return { store, persistor };
};
