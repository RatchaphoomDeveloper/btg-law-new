
const initialState = {
  sidebarShow: "responsive",
  asideShow: false,
  darkMode: false,
  user: {},
};
const changeState = (state = initialState, { type, ...rest }) => {
  switch (type) {
    case "set":
      return { ...state, ...rest };
    case "set_user":
      return { ...state, ...rest };
    default:
      return state;
  }
};

export default changeState

