const hidetable = (state = "", action) => {
  switch (action.type) {
    case "SET_HIDE_TABLE":
      return action.status;
    default:
      return state;
  }
};

export {hidetable}
