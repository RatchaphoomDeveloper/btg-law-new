export const VERSION_WEB = "1.0.6";

// export const WEB_FILE_URL = "https://localhost:44378/";
// export const WEB_API = "https://localhost:44378/api/";
// export const REPORT_API = "https://localhost:44327/api/";

export const WEB_FILE_URL = "http://122.155.0.74:3991";
export const WEB_API = "http://122.155.0.74:3991/api/";
export const REPORT_API = "https://122.155.0.74:3992/api/";

// export const WEB_FILE_URL = "https://np-brcsdevapi.betagro.com/";
// export const WEB_API = "https://np-brcsdevapi.betagro.com/api/";
// export const REPORT_API = "https://np-brcsdevrpt.betagro.com/api/";

// export const WEB_FILE_URL = "https://np-brcsprdapi.betagro.com/";
// export const WEB_API = "https://np-brcsprdapi.betagro.com/api/";
// export const REPORT_API = "https://np-brcsprdrpt.betagro.com/api/";

// export const adfs_redirect_url = 'http://localhost:3990/';
export const adfs_redirect_url = 'http://178.128.61.203:3990/';
// export const adfs_redirect_url = 'https://np-brcsdev.betagro.com/';
// export const adfs_redirect_url = 'https://np-brcsprd.betagro.com/';
export const adfs_client_id = '7197d1ad-7dc9-4163-9179-f3ad748817f4';