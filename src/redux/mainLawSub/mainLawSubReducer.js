import {
  FETCH_MAINLAWSUB_ERROR,
  FETCH_MAINLAWSUB_REQUEST,
  FETCH_MAINLAWSUB_SUCCESS,
} from "./mainLawSubType";

const initialState = {
  loading: false,
  loaded: false,
  mainlawsSub: [],
  error: "",
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_MAINLAWSUB_REQUEST:
      return {
        ...state,
        loading: true,
        loading: false,
        mainlawsSub: [],
        error: "",
      };
    case FETCH_MAINLAWSUB_SUCCESS:
      return {
        loaded: true,
        loading: false,
        mainlawsSub: action.data,
      };
    case FETCH_MAINLAWSUB_ERROR:
      return {
        loading: false,
        mainlawsSub: [],
        error: action.payload,
      };
    default:
      return state;
  }
};

export default reducer;
