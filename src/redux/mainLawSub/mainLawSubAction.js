import {
  FETCH_MAINLAWSUB_ERROR,
  FETCH_MAINLAWSUB_REQUEST,
  FETCH_MAINLAWSUB_SUCCESS,
} from "./mainLawSubType";

import axios from "axios";

export const mainlawsubRequest = () => ({
  type: FETCH_MAINLAWSUB_REQUEST,
});

export const mainlawsubSuccess = (data) => ({
  type: FETCH_MAINLAWSUB_SUCCESS,
  data,
});

export const mainlawSubError = (error) => ({
  type: FETCH_MAINLAWSUB_ERROR,
  error,
});

export const fetchgetMainLawSub = (mainlawId, bu_id) => {
  return (dispatch) => {
    dispatch(mainlawsubRequest);
    axios
      .get(
        `Master/GetBrcsMainLawSub?id=${mainlawId}&bu_id=${bu_id ? bu_id : 0}`
      )
      .then((res) => {
        if (res.data.status === 200) {
          let prepare = res.data.response;
          let result = [];
          prepare.length !== 0 &&
            prepare.forEach((data) => {
              if (data.monthList !== " ") {
                let monthParse = data && JSON.parse(data["monthList"]);
                result.push({
                  id: data.id,
                  seq: data.seq,
                  description: data.description,
                  descriptionlink: data.descriptionLink,
                  mainlawsub_buid: data.mainlawsubbuid,
                  month01: monthParse.month01,
                  month02: monthParse.month02,
                  month03: monthParse.month03,
                  month04: monthParse.month04,
                  month05: monthParse.month05,
                  month06: monthParse.month06,
                  month07: monthParse.month07,
                  month08: monthParse.month08,
                  month09: monthParse.month09,
                  month10: monthParse.month10,
                  month11: monthParse.month11,
                  month12: monthParse.month12,
                });
              } else {
                result.push({
                  id: data.id,
                  seq: data.seq,
                  description: data.description,
                  descriptionlink: data.descriptionLink,
                  mainlawsub_buid: data.mainlawsubbuid,
                  month01: false,
                  month02: false,
                  month03: false,
                  month04: false,
                  month05: false,
                  month06: false,
                  month07: false,
                  month08: false,
                  month09: false,
                  month10: false,
                  month11: false,
                  month12: false,
                });
              }
            });
          dispatch(mainlawsubSuccess(result));
        }
      })
      .catch((err) => {
        const errMessage = err.errMessage;
        dispatch(mainlawSubError(errMessage));
      });
  };
};
