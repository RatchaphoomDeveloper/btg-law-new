import { GET_TRANSBRCSASSESSMENT_FAILURE, GET_TRANSBRCSASSESSMENT_REQUEST, GET_TRANSBRCSASSESSMENT_SUCCESS } from "./getbrcaassessmentType";

  const InitialState = {
    loading: false,
    loaded: false,
    data: [],
    error: "",
  };
  const reducer = (state = InitialState, action) => {
    switch (action.type) {
      case GET_TRANSBRCSASSESSMENT_REQUEST:
        return {
          ...state,
          loaded: false,
          loading: true,
          data: [],
          error: "",
        };
      case GET_TRANSBRCSASSESSMENT_SUCCESS:
        return {
          ...state,
          loaded: false,
          loading: true,
          data: action.payload,
          error: "",
        };
      case GET_TRANSBRCSASSESSMENT_FAILURE:
        return {
          ...state,
          loaded: false,
          loading: true,
          data: [],
          error: action.payload,
        };
      default:
        return state;
    }
  };
  
  export default reducer;
  