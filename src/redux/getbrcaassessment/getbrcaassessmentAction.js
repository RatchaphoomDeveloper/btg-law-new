import axios from "axios";
import {
  GET_TRANSBRCSASSESSMENT_FAILURE,
  GET_TRANSBRCSASSESSMENT_REQUEST,
  GET_TRANSBRCSASSESSMENT_SUCCESS,
} from "./getbrcaassessmentType";
export const getBrcsAssessmentRequest = () => ({
  type: GET_TRANSBRCSASSESSMENT_REQUEST,
});
export const getBrcsAssessmentSuccess = (payload) => ({
  type: GET_TRANSBRCSASSESSMENT_SUCCESS,
  payload,
});
export const getBrcsAssessmentFailure = (payload) => ({
  type: GET_TRANSBRCSASSESSMENT_FAILURE,
  payload,
});

export const getBrcsAssessmentDetail = (data) => {
  return (dispatch) => {
    dispatch(getBrcsAssessmentRequest);
    axios
      .post(`Master/GetTranBrcsAssessmentDetail`, data)
      .then((res) => {
        dispatch(getBrcsAssessmentSuccess(res.data));
      })
      .catch((err) => {
        dispatch(getBrcsAssessmentFailure(err.message));
      });
  };
};
