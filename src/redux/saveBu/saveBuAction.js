import axios from "axios";
import { SAVE_BU_ERROR, SAVE_BU_REQUEST, SAVE_BU_SUCCESS } from "./saveBuType";
import Swal from "sweetalert2/src/sweetalert2.js";

export const savebuRequest = () => ({
  type: SAVE_BU_REQUEST,
});

export const savebuSuccess = (payload) => ({
  type: SAVE_BU_SUCCESS,
  payload,
});

export const savebuError = (payload) => ({
  type: SAVE_BU_ERROR,
  payload,
});

export const saveButoDB = (data) => {
  return (dispatch) => {
    dispatch(savebuRequest);
    axios
      .post("Master/SaveTranBrcsAssesment",  data)
      .then((res) => {
        // if(res.data.response === 200){
        //   Swal.fire({
        //     icon: "success",
        //     title: "Success",
        //   });
        // }
      })
      .catch((err) => {
        console.log("err", err);
        if (err.response.data.statusCode && err.response.data.statusCode == 550){
          Swal.fire({
            icon: "error",
            title: "ERROR",
            text: `Send Email Error ${err.response.data.message}`
          });
        }else{
          Swal.fire({
            icon: "error",
            title: "Process Error!",
            text: err.response.data
          });
        }
      });
  };
};
