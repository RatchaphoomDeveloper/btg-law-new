import { SAVE_BU_ERROR, SAVE_BU_REQUEST, SAVE_BU_SUCCESS } from "./saveBuType";

const reducerInitialState = {
  loading: false,
  loaded: false,
  response: [],
  error: "",
};
const reducer = (state = reducerInitialState, action) => {
  switch (action.type) {
    case SAVE_BU_REQUEST:
      return { ...state, loading: true, response: [], error: "" };
    case SAVE_BU_SUCCESS:
      return {
        loading: false,
        loaded: true,
        response: action.payload,
        error: "",
      };
    case SAVE_BU_ERROR:
      return {
        loading: false,
        loaded: true,
        error: action.payload,
        response: [],
      };
    default:
      return state;
  }
};

export default reducer;
