import {
  ADD_LAWINFO_ERROR,
  ADD_LAWINFO_REQUEST,
  ADD_LAWINFO_SUCCESS,
} from "./lawinfoType";

export const addlawinfoAction = () => ({
  type: ADD_LAWINFO_REQUEST,
});

export const addlawinfoSuccessAction = (payload) => ({
  type: ADD_LAWINFO_SUCCESS,
  payload,
});

export const addlawinfoErrorAction = (payload) => ({
  type: ADD_LAWINFO_ERROR,
  payload,
});

export const setaddLawinfo = (data) => {
  return (dispatch) => {
    dispatch(addlawinfoAction);
    dispatch(addlawinfoSuccessAction(data));
  };
};

export const setrefreshLawinfo = (data) => {
  return (dispatch) => {
    dispatch(addlawinfoSuccessAction({}));
  };
};
