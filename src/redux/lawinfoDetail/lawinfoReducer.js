import { ADD_LAWINFO_REQUEST, ADD_LAWINFO_SUCCESS } from "./lawinfoType";

const InitialState = {
  loading: false,
  loaded: false,
  data: [],
  error: "",
};
export const reducer = (state = InitialState, action) => {
  switch (action.type) {
    case ADD_LAWINFO_REQUEST:
      return {
        ...state,
        loading: true,
        loaded: false,
        data: [],
        error: "",
      };
    case ADD_LAWINFO_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.payload,
        error: "",
      };
    default:
      return state;
  }
};

export default reducer;
