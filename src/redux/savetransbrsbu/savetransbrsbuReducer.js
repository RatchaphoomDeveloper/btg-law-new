import {
  SAVE_TRANSBRCSBU_FAILURE,
  SAVE_TRANSBRCSBU_REQUEST,
  SAVE_TRANSBRCSBU_SUCCESS,
} from "./savetransbrsbuType";

const InitialState = {
  loading: false,
  loaded: false,
  data: [],
  error: "",
};
const reducer = (state = InitialState, action) => {
  switch (action.type) {
    case SAVE_TRANSBRCSBU_REQUEST:
      return {
        ...state,
        loaded: false,
        loading: true,
        data: [],
        error: "",
      };
    case SAVE_TRANSBRCSBU_SUCCESS:
      return {
        ...state,
        loaded: false,
        loading: true,
        data: action.payload,
        error: "",
      };
    case SAVE_TRANSBRCSBU_FAILURE:
      return {
        ...state,
        loaded: false,
        loading: true,
        data: [],
        error: action.payload,
      };
    default:
      return state;
  }
};

export default reducer;
