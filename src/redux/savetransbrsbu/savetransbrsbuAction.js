import {
  SAVE_TRANSBRCSBU_FAILURE,
  SAVE_TRANSBRCSBU_REQUEST,
  SAVE_TRANSBRCSBU_SUCCESS,
} from "./savetransbrsbuType";
import axios from "axios";
export const saveTransBrcsBuRequest = () => ({
  type: SAVE_TRANSBRCSBU_REQUEST,
});
export const saveTransBrcsBuSuccess = (payload) => ({
  type: SAVE_TRANSBRCSBU_SUCCESS,
  payload,
});
export const saveTransBrcsBuFailure = (payload) => ({
  type: SAVE_TRANSBRCSBU_FAILURE,
  payload,
});

export const saveTranBrcsBu = (data) => {
  return (dispatch) => {
    dispatch(saveTransBrcsBuRequest);
    axios
      .post(`Master/SaveTransBrsBu`, data)
      .then((res) => {
        if (res.data.response === 200) {
          dispatch(saveTransBrcsBuSuccess(res.data));
        }
      })
      .catch((err) => {
        dispatch(saveTransBrcsBuFailure(err.message));
      });
  };
};
