import {
  FETCH_PRIORITY_ERROR,
  FETCH_PRIORITY_REQUEST,
  FETCH_PRIORITY_SUCCESS,
} from "./priorityOptionType";

const initialState = {
  loading: false,
  loaded: false,
  priorityData: [],
  error: "",
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PRIORITY_REQUEST:
      return {
        ...state,
        loading: true,
        loading: false,
        priorityData: [],
        error: "",
      };
    case FETCH_PRIORITY_SUCCESS:
      return {
        loaded: true,
        loading: false,
        priorityData: action.payload,
      };
    case FETCH_PRIORITY_ERROR:
      return {
        loading: false,
        priorityData: [],
        error: action.payload,
      };
    default:
      return state;
  }
};

export default reducer;
