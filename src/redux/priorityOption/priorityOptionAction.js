import axios from "axios";
import { setHeaderAuth } from "../../utils";
import {
  FETCH_PRIORITY_ERROR,
  FETCH_PRIORITY_REQUEST,
  FETCH_PRIORITY_SUCCESS,
} from "./priorityOptionType";

export const priorityoptionListRequest = () => ({
  type: FETCH_PRIORITY_REQUEST,
});

export const priorityoptionListSuccess = (payload) => ({
  type: FETCH_PRIORITY_SUCCESS,
  payload,
});

export const priorityoptionListError = (payload) => ({
  type: FETCH_PRIORITY_ERROR,
  payload,
});

export const fetchPriority = () => {
  return async (dispatch) => {
    dispatch(priorityoptionListRequest);
    await setHeaderAuth();
    axios({
      url: `Master/GetMasterPriority`,
      method: "get",
    })
      .then((res) => {
        const result = res.data;
        dispatch(priorityoptionListSuccess({ payload: result }));
      })
      .catch((err) => {
        const errMessage = err.message;
        dispatch(priorityoptionListError({ payload: errMessage }));
      });
  };
};
