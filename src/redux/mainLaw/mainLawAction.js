import {
  FETCH_MAINLAW_ERROR,
  FETCH_MAINLAW_REQUEST,
  FETCH_MAINLAW_SUCCESS,
} from "./mainLawType";
import axios from "axios";
import { setHeaderAuth } from "../../utils";
export const mainLawListRequest = () => {
  return {
    type: FETCH_MAINLAW_REQUEST,
  };
};

export const mainLawListSuccess = (mainlaws) => {
  return {
    type: FETCH_MAINLAW_SUCCESS,
    payload: mainlaws,
  };
};

export const mainLawListError = (error) => {
  return {
    type: FETCH_MAINLAW_ERROR,
    payload: error,
  };
};

export const fetchgetMainLaw = (id) => {
  return async (dispatch) => {
    dispatch(mainLawListRequest);
    if (id > 0) {
      await setHeaderAuth();
      axios
        .get(`Master/GetBrcsMainLaw?id=${id}`)
        .then((response) => {
          const mainlaws = response.data;

          dispatch(mainLawListSuccess(mainlaws));
        })
        .catch((error) => {
          const errorMsg = error.message;
          dispatch(mainLawListError(errorMsg));
        });
    }
  };
};
