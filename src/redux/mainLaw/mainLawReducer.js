import {
  FETCH_MAINLAW_ERROR,
  FETCH_MAINLAW_REQUEST,
  FETCH_MAINLAW_SUCCESS,
} from "./mainLawType";

const reducerInitialState = {
  loading: false,
  loaded: false,
  mainlaws: [],
  error: "",
};
const reducer = (state = reducerInitialState, action) => {
  switch (action.type) {
    case FETCH_MAINLAW_REQUEST:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: "",
        mainlaws: [],
      };
    case FETCH_MAINLAW_SUCCESS:
      return {
        loaded: true,
        loading: false,
        mainlaws: action.payload,
        error: "",
      };
    case FETCH_MAINLAW_ERROR:
      return {
        loading: false,
        mainlaws: [],
        error: action.payload,
      };
    default:
      return state;
  }
};


export default reducer;