import axios from "axios";
import {
  GET_TRANSBRCSBU_FAILURE,
  GET_TRANSBRCSBU_REQUEST,
  GET_TRANSBRCSBU_SUCCESS,
} from "./gettranbrcsbuType";
export const getTransBrcsBuRequest = () => ({
  type: GET_TRANSBRCSBU_REQUEST,
});
export const getTransBrcsBuSuccess = (payload) => ({
  type: GET_TRANSBRCSBU_SUCCESS,
  payload,
});
export const getTransBrcsBuFailure = (payload) => ({
  type: GET_TRANSBRCSBU_FAILURE,
  payload,
});

export const getTranBrcsBu = (data) => {
  console.log("GetTransBrsBuSel::");
  return  (dispatch) => {
    const response =   axios.post(`Master/GetTransBrsBuSel`, data);
    console.log("GetTransBrsBuSel::",response);
    // dispatch(getTransBrcsBuRequest);
    // axios
    //   .post(`Master/GetTransBrsBuSel`, data)
    //   .then((res) => {
    //     if (res.data.status === 200) {
    //       dispatch(getTransBrcsBuSuccess(res.data.response));
    //     }
    //   })
    //   .catch((err) => {
    //     dispatch(getTransBrcsBuFailure(err.message));
    //   });
  };
};
