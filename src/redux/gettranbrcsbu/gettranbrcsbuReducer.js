import { GET_TRANSBRCSBU_FAILURE, GET_TRANSBRCSBU_REQUEST, GET_TRANSBRCSBU_SUCCESS } from "./gettranbrcsbuType";

  const InitialState = {
    loading: false,
    loaded: false,
    data: [],
    error: "",
  };
  const reducer = (state = InitialState, action) => {
    switch (action.type) {
      case GET_TRANSBRCSBU_REQUEST:
        return {
          ...state,
          loaded: false,
          loading: true,
          data: [],
          error: "",
        };
      case GET_TRANSBRCSBU_SUCCESS:
        return {
          ...state,
          loaded: false,
          loading: true,
          data: action.payload,
          error: "",
        };
      case GET_TRANSBRCSBU_FAILURE:
        return {
          ...state,
          loaded: false,
          loading: true,
          data: [],
          error: action.payload,
        };
      default:
        return state;
    }
  };
  
  export default reducer;
  