import { ADD_BUSELECTOR_REQUEST, ADD_BUSELECTOR_SUCCESS } from "./buassignType";

const InitialState = {
  loading: false,
  loaded: false,
  data: [],
};
const reducer = (state = InitialState, action) => {
  switch (action.type) {
    case ADD_BUSELECTOR_REQUEST:
      return { ...state, loading: true, loading: false, data: [], error: "" };
    case ADD_BUSELECTOR_SUCCESS:
      return {
        loading: false,
        loading: true,
        data: action.payload,
        error: "",
      };
    default:
      return state;
  }
};

export default reducer;
