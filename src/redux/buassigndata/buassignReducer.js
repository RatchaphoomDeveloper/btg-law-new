import { ADD_BU_REQUEST, ADD_BU_SUCCESS,ADD_BU_ERROR } from "./buassignType";

const nameInitialState = {
  loading: false,
  loaded: false,
  data: [],
  error: "",
};
const reducer = (state = nameInitialState, action) => {
  switch (action.type) {
    case ADD_BU_REQUEST:
      return {
        ...state,
        loaded: false,
        loading: true,
        data: action.payload,
        error: "",
      };
    case ADD_BU_SUCCESS:
      return  {
        loaded: true,
        loading: false,
        data: action.payload,
        error: "",
      };
    default:
      return state;
  }
};

export default reducer