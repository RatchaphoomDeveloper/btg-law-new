import {
  ADD_BU_REQUEST,
  ADD_BU_SUCCESS,
  ADD_BU_ERROR,
  ADD_BUSELECTOR_REQUEST,
  ADD_BUSELECTOR_SUCCESS,
} from "./buassignType";

export const addbuassignaction = () => ({
  type: ADD_BU_REQUEST,
});

export const addbuassignactionsuccess = (payload) => ({
  type: ADD_BU_SUCCESS,
  payload,
});

export const addbuassignactionerror = (payload) => ({
  type: ADD_BU_ERROR,
  payload,
});

export const addbuselectorAction = () => ({
  type: ADD_BUSELECTOR_REQUEST,
});

export const addbuselectorSuccessAction = (payload) => ({
  type: ADD_BUSELECTOR_SUCCESS,
  payload,
});

export const setBuassign = (data) => {
  return (dispatch) => {
    dispatch(addbuassignaction);
    dispatch(addbuassignactionsuccess(data));
  };
};

export const setBuSelector = (data) => {
  return (dispatch) => {
    console.log("dispatchData", data);
    dispatch(addbuselectorAction);
    dispatch(addbuselectorSuccessAction(data));
  };
};
