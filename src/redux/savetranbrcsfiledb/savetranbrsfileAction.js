import {
  SAVE_TRANBRCSFILES_ERROR,
  SAVE_TRANBRCSFILES_REQUEST,
  SAVE_TRANBRCSFILES_SUCCESS,
} from "./savetranbrsfileType";

import axios from "axios";
import Swal from "sweetalert2/src/sweetalert2.js";
export const saveTranBrsFileReQuestAction = () => ({
  type: SAVE_TRANBRCSFILES_REQUEST,
});

export const saveTranBrsFileSuccessAction = (payload) => ({
  type: SAVE_TRANBRCSFILES_SUCCESS,
  payload,
});

export const saveTranBrsFileErrorAction = (payload) => ({
  type: SAVE_TRANBRCSFILES_ERROR,
  payload,
});

export const saveDataToTransBrcsFileDB = (data) => {
  return (dispatch) => {
    dispatch(saveTranBrsFileReQuestAction);
    axios
      .post(`/Master/SaveTranBrcsFileDB`, data)
      .then((res) => {
        if (res.data.response === 200) {
          dispatch(saveTranBrsFileSuccessAction(res.data));
          Swal.fire({
            icon: "success",
            title: "Success",
          });
        }
      })
      .catch((error) => {
        dispatch(saveTranBrsFileErrorAction(error.message));
        Swal.fire({
          icon: "error",
          title: error.message,
        });
      });
  };
};
