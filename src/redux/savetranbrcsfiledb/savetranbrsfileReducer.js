import {
  SAVE_TRANBRCSFILES_ERROR,
  SAVE_TRANBRCSFILES_REQUEST,
  SAVE_TRANBRCSFILES_SUCCESS,
} from "./savetranbrsfileType";

const InitialState = {
  loading: false,
  loaded: true,
  data: [],
  error: "",
};
const reducer = (state = InitialState, action) => {
  switch (action.type) {
    case SAVE_TRANBRCSFILES_REQUEST:
      return {
        ...state,
        loading: true,
        loaded: false,
        data: [],
        error: "",
      };
    case SAVE_TRANBRCSFILES_SUCCESS:
      return {
        loading: false,
        loaded: true,
        data: action.payload,
        error: "",
      };
    case SAVE_TRANBRCSFILES_ERROR:
      return {
        loading: false,
        loaded: true,
        data: [],
        error: action.payload,
      };
    default:
      return state;
  }
};

export default reducer
