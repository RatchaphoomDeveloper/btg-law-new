import axios from "axios";
import { WEB_API } from "../env";
import { useDispatch, useSelector } from "react-redux";
import { removeLocalStorage } from "../utils/localStorage";
axios.defaults.baseURL = WEB_API;

axios.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error.response.status === 401) {
      console.log('error401::')
      removeLocalStorage("token");
      window.location.href = "/timeout";
    }
    throw error;
  }
);
