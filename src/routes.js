import React from "react";

const DashboardLogin = React.lazy(() =>
  import("./views/dashboard/Dashboard_Login")
);
const TypeofLaw = React.lazy(() => import("./views/typeoflaw"));
const LawType = React.lazy(() => import("./views/lawtype"));
const LawAgencies = React.lazy(() => import("./views/lawagencies"));
const LawAgencieSub = React.lazy(() => import("./views/lawagenciesub"));
const LawGroup = React.lazy(() => import("./views/lawgroup"));
const LawGroupSub = React.lazy(() => import("./views/lawgroupsub"));
const Country = React.lazy(() => import("./views/country"));
const Devision = React.lazy(() => import("./views/devision"));
const Plant = React.lazy(() => import("./views/plant"));
const BU = React.lazy(() => import("./views/bu"));
const UserManagement = React.lazy(() => import("./views/usermanagement"));
const UserManagementSub = React.lazy(() => import("./views/usermanagesub"));
const Site = React.lazy(() => import("./views/site"));
const Standart = React.lazy(() => import("./views/standart"));
const Role = React.lazy(() => import("./views/role"));
const CountryType = React.lazy(() => import("./views/countrytype"));
const SystemType = React.lazy(() => import("./views/systemtype"));
const RegularLawDept = React.lazy(() => import("./views/regularLawDept"));
const RegularLawType = React.lazy(() =>
  import("./views/regularLawDept/lawType")
);
const RegularLaw = React.lazy(() =>
  import("./views/regularLawDept/lawType/document")
);
const CountryAssign = React.lazy(() => import("./views/countryassign"));
const LawInfo = React.lazy(() => import("./views/lawinfo"));
const CreateLawInfo = React.lazy(() => import("./views/lawinfo/create"));
const SearchAddFreequency = React.lazy(() =>
  import("./views/lawinfo/addfrequency")
);
const CreateAddFrequency = React.lazy(() =>
  import("./views/lawinfo/addfrequency/create")
);
// const SearchAssessConsistency = React.lazy(() => import('./views/lawinfo/assessConsistency'));
const SearchAssessConsistency = React.lazy(() =>
  import("./views/lawinfo/assessConsistency/indexAssess")
);
const CreateAssessConsistency = React.lazy(() =>
  import("./views/lawinfo/assessConsistency/create/indexAssess")
);
const SearchApprove1 = React.lazy(() =>
  import("./views/lawinfo/approve1/indexAssess")
);
const CreateApprove1 = React.lazy(() =>
  import("./views/lawinfo/approve1/create/indexAssess")
);
const SearchApprove2 = React.lazy(() =>
  import("./views/lawinfo/approve2/indexAssess")
);
const CreateApprove2 = React.lazy(() =>
  import("./views/lawinfo/approve2/create/indexAssess")
);
const SearchFrequency = React.lazy(() =>
  import("./views/lawinfo/searchFrequency")
);
const ViewFrequency = React.lazy(() =>
  import("./views/lawinfo/searchFrequency/create")
);
const SearchOnly = React.lazy(() => import("./views/lawinfo/searchOnly"));
const ViewOnly = React.lazy(() => import("./views/lawinfo/searchOnly/create"));
const SearchAssess = React.lazy(() => import("./views/lawinfo/searchAssess"));
const ViewAssess = React.lazy(() =>
  import("./views/lawinfo/searchAssess/create")
);
const ReviewDocument = React.lazy(() =>
  import("./views/lawinfo/reviewDocument")
);
const Review = React.lazy(() =>
  import("./views/lawinfo/reviewDocument/create")
);
const Report1 = React.lazy(() => import("./views/report/report1"));
const Report2 = React.lazy(() => import("./views/report/report2"));
const Report3 = React.lazy(() => import("./views/report/report3"));
const Report4 = React.lazy(() => import("./views/report/report4"));
const Report5 = React.lazy(() => import("./views/report/report5"));
const ReportGraph1 = React.lazy(() => import("./views/reportGraph/report1"));
const ReportGraph2 = React.lazy(() => import("./views/reportGraph/report2"));
const ReportGraph3 = React.lazy(() => import("./views/reportGraph/report3"));
const EmailTemplate = React.lazy(() => import("./views/emailtemplate"));
const BuAssign = React.lazy(() => import("./views/buassign"));
const BuAssignUser = React.lazy(() => import("./views/buassign/buAssignUser"));
const AssignMoreBu = React.lazy(() => import("./views/lawinfo/assignMoreBu"));
const AssignMoreBuDetail = React.lazy(() =>
  import("./views/lawinfo/assignMoreBu/create")
);
const AssignMorePlant = React.lazy(() =>
  import("./views/lawinfo/assignMorePlant")
);
const AssignMorePlantDetail = React.lazy(() =>
  import("./views/lawinfo/assignMorePlant/create")
);
const ReEvaluate = React.lazy(() => import("./views/lawinfo/reEvaluate"));
const ReEvaluateDetail = React.lazy(() =>
  import("./views/lawinfo/reEvaluate/create")
);
const ManageActionPlan = React.lazy(() =>
  import("./views/lawinfo/manageActionPlan/indexAssess")
);
const ManageActionPlanDetail = React.lazy(() =>
  import("./views/lawinfo/manageActionPlan/create/indexAssess")
);
const MasterConfig = React.lazy(() => import("./views/masterConfig"));
const ImportBrcs = React.lazy(() => import("./views/lawinfo/import"));
const Timeout = React.lazy(() => import("./views/timeout"));

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: "/home", exact: true, name: "หน้าหลัก" },
  { path: "/home/dashboard", name: "แดชบอร์ด", component: DashboardLogin },
  { path: "/dashboard", name: "แดชบอร์ด", component: DashboardLogin },
  { path: "/home/lawsetting", name: "กฎหมาย", exact: true },
  {
    path: "/home/lawsetting/lawagencies",
    name: "หน่วยงานผู้ออกกฏหมาย",
    component: LawAgencies,
  },
  {
    path: "/home/lawsetting/lawagenciesub",
    name: "หน่วยงานผู้ออกกฏหมายย่อย",
    component: LawAgencieSub,
  },
  {
    path: "/home/lawsetting/lawtype",
    name: "ประเภทกฏหมาย",
    component: LawType,
  },
  { path: "/home/master", name: "ข้อมูลพื้นฐานระบบ", exact: true },
  {
    path: "/home/master/typeoflaw",
    name: "ประเภทของกฏหมาย",
    component: TypeofLaw,
  },
  {
    path: "/home/master/lawgroup",
    name: "กลุ่มของกฏหมาย Type",
    component: LawGroup,
  },
  {
    path: "/home/master/lawgroupsub",
    name: "กลุ่มของกฏหมาย SubType",
    component: LawGroupSub,
  },
  { path: "/home/master/country", name: "รหัสประเทศ", component: Country },
  {
    path: "/home/master/countryassign",
    name: "ประเทศปลายทางที่ออกกฏหมาย",
    component: CountryAssign,
  },
  {
    path: "/home/master/devision",
    name: "หน่วยงาน (Department)",
    component: Devision,
  },
  { path: "/home/master/plant", name: "โรงงาน (Plant)", component: Plant },
  {
    path: "/home/master/bu",
    name: "กลุ่มธุรกิจ (Business Unit)",
    component: BU,
  },
  { path: "/home/master/site", name: "Site", component: Site },
  { path: "/home/master/standart", name: "Standart", component: Standart },
  {
    path: "/home/master/countrytype",
    name: "CountryType",
    component: CountryType,
  },
  {
    path: "/home/master/systemtype",
    name: "ประเภทของระบบ",
    component: SystemType,
  },
  {
    path: "/home/master/regularlawdept",
    name: "RegularLawDept",
    component: RegularLawDept,
  },
  {
    path: "/home/master/regularlawtype",
    name: "RegularLawType",
    component: RegularLawType,
  },
  {
    path: "/home/master/regularlaw",
    name: "RegularLaw",
    component: RegularLaw,
  },
  {
    path: "/home/master/emailtemplate",
    name: "EmailTemplate",
    component: EmailTemplate,
  },
  { path: "/home/master/buAssign", name: "Plant Assign", component: BuAssign },
  {
    path: "/home/master/buAssignUser",
    name: "Plant Assign User",
    component: BuAssignUser,
  },
  { path: "/home/usersetting", name: "ข้อมูลพื้นฐานผู้ใช้ระบบ", exact: true },
  { path: "/home/usersetting/role", name: "ประเภทผู้ใช้งาน", component: Role },
  {
    path: "/home/usersetting/usermanagement",
    name: "จัดการข้อมูลผู้ใช้งาน",
    component: UserManagement,
  },
  {
    path: "/home/usersetting/usermanagesub",
    name: "จัดการประเภทผู้ใช้งาน",
    component: UserManagementSub,
  },
  { path: "/home/lawinfo", name: "กฎหมาย", exact: true },
  { path: "/home/lawinfo/search", name: "ค้นหา", component: LawInfo },
  { path: "/home/lawinfo/create", name: "สร้าง", component: CreateLawInfo },
  {
    path: "/home/lawinfo/addfrequency/search",
    name: "ค้นหา",
    component: SearchAddFreequency,
  },
  {
    path: "/home/lawinfo/addfrequency/create",
    name: "ดูรายละเอียด",
    component: CreateAddFrequency,
  },
  {
    path: "/home/lawinfo/assessConsistency/search",
    name: "ค้นหา",
    component: SearchAssessConsistency,
  },
  {
    path: "/home/lawinfo/assessConsistency/create",
    name: "ดูรายละเอียด",
    component: CreateAssessConsistency,
  },
  {
    path: "/home/lawinfo/approve1/search",
    name: "ค้นหา",
    component: SearchApprove1,
  },
  {
    path: "/home/lawinfo/approve1/create",
    name: "ดูรายละเอียด",
    component: CreateApprove1,
  },
  {
    path: "/home/lawinfo/approve2/search",
    name: "ค้นหา",
    component: SearchApprove2,
  },
  {
    path: "/home/lawinfo/approve2/create",
    name: "ดูรายละเอียด",
    component: CreateApprove2,
  },
  {
    path: "/home/lawinfo/searchOnly/search",
    name: "ค้นหา",
    component: SearchOnly,
  },
  {
    path: "/home/lawinfo/searchOnly/create",
    name: "ดูรายละเอียด",
    component: ViewOnly,
  },
  {
    path: "/home/lawinfo/searchAssess/search",
    name: "ค้นหา",
    component: SearchAssess,
  },
  {
    path: "/home/lawinfo/searchAssess/create",
    name: "ดูรายละเอียด",
    component: ViewAssess,
  },
  {
    path: "/home/lawinfo/reviewDocument/search",
    name: "ค้นหา",
    component: ReviewDocument,
  },
  {
    path: "/home/lawinfo/reviewDocument/create",
    name: "ดูรายละเอียด",
    component: Review,
  },
  {
    path: "/home/lawinfo/searchFrequency/search",
    name: "ค้นหา",
    component: SearchFrequency,
  },
  {
    path: "/home/lawinfo/searchFrequency/create",
    name: "ดูรายละเอียด",
    component: ViewFrequency,
  },
  {
    path: "/home/lawinfo/assignMoreBu/search",
    name: "ค้นหา",
    component: AssignMoreBu,
  },
  {
    path: "/home/lawinfo/assignMoreBu/create",
    name: "ดูรายละเอียด",
    component: AssignMoreBuDetail,
  },
  {
    path: "/home/lawinfo/assignMorePlant/search",
    name: "ค้นหา",
    component: AssignMorePlant,
  },
  {
    path: "/home/lawinfo/assignMorePlant/create",
    name: "ดูรายละเอียด",
    component: AssignMorePlantDetail,
  },
  {
    path: "/home/lawinfo/reEvaluate/search",
    name: "ค้นหา",
    component: ReEvaluate,
  },
  {
    path: "/home/lawinfo/reEvaluate/create",
    name: "ดูรายละเอียด",
    component: ReEvaluateDetail,
  },
  {
    path: "/home/lawinfo/manageActionPlan/search",
    name: "ค้นหา",
    component: ManageActionPlan,
  },
  {
    path: "/home/lawinfo/manageActionPlan/create",
    name: "ดูรายละเอียด",
    component: ManageActionPlanDetail,
  },
  { path: "/home/lawinfo/import", name: "ดูรายละเอียด", component: ImportBrcs },

  { path: "/home/report", name: "รายงาน", exact: true },
  {
    path: "/home/report/report1",
    name: "ผลการประเมินระดับแผนก",
    component: Report1,
  },
  {
    path: "/home/report/report2",
    name: "ผลการประเมินระดับโรงงาน",
    component: Report2,
  },
  {
    path: "/home/report/report3",
    name: "ผลการทบทวนการประเมินระดับโรงงาน",
    component: Report3,
  },
  {
    path: "/home/report/report4",
    name: "ผลการแก้ไขป้องกัน",
    component: Report4,
  },
  {
    path: "/home/report/report5",
    name: "รายงานจัดการข้อมูลกฎหมาย",
    component: Report5,
  },

  { path: "/home/graph", name: "รายงาน รูปแบบกราฟ", exact: true },
  {
    path: "/home/graph/report1",
    name: "รายงาน Regulation Compliance",
    component: ReportGraph1,
  },
  {
    path: "/home/graph/report2",
    name: "รายงาน Regulation Risk",
    component: ReportGraph2,
  },
  {
    path: "/home/graph/report3",
    name: "รายงาน Overdue Action Plan",
    component: ReportGraph3,
  },
  {
    path: "/home/masterconfig",
    name: "การตั้งค่าระบบ",
    component: MasterConfig,
  },
];

export default routes;
