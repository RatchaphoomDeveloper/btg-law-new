import { sygnet } from "./sygnet";
import { logo } from "./logo";
import { logoNegative } from "./logo-negative";

import {
  cilPen,
  cilTrash,
  cilApplicationsSettings,
  cilSpeedometer,
  cilSun,
  cilMoon,
  cilSettings,
  cilPlus,
  cilSearch,
  cilCloudUpload,
  cilCheckAlt,
  cilChevronCircleDownAlt,
  cilLockLocked,
  cilNotes,
  cilUser,
  cilCheckCircle,
  cilPenNib,
  cilBalanceScale,
  cilDescription,
  cilPaperclip,
  cilWarning,
  cilHistory,
  cilTags,
  cilCalendar,
  cilFile,
} from "@coreui/icons";

export const icons = Object.assign(
  {},
  {
    sygnet,
    logo,
    logoNegative,
  },
  {
    cilApplicationsSettings,
    cilSpeedometer,
    cilSun,
    cilMoon,
    cilSettings,
    cilPlus,
    cilPen,
    cilTrash,
    cilSearch,
    cilCloudUpload,
    cilCheckAlt,
    cilChevronCircleDownAlt,
    cilLockLocked,
    cilNotes,
    cilUser,
    cilCheckCircle,
    cilPenNib,
    cilBalanceScale,
    cilDescription,
    cilPaperclip,
    cilWarning,
    cilHistory,
    cilTags,
    cilCalendar,
    cilFile,
  }
);
