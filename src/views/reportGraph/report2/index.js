import React, { useEffect, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CNav,
  CForm,
  CLabel,
  CSelect,
  CInput,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import Api from "../../../api/ApiReportGraph";
// import Tables from './Tables';
import SearchPanel from "../component/searchpanel";
import { Doughnut, Bar } from "react-chartjs-2";
import { Divider } from "antd";
import { useDispatch, useSelector } from "react-redux";

const ReportGraph2 = () => {
  const [dataTable, setDataTable] = useState([]);
  const userState = useSelector((state) => state.changeState.user);
  const [lawsub_id, setlawsub_id] = useState(0);
  const [datasetCharts, setdatasetCharts] = useState({
    labels_list: [],
    count_list: [],
    background: [],
  });

  const [lastSearchParams, setlastSearchParams] = useState({});

  const [datasetLawSubCharts, setdatasetLawSubCharts] = useState({
    labels_list: [],
    count_list: [],
    background: [],
  });

  const [datasetLawSubPlantCharts, setdatasetLawSubPlantCharts] = useState({
    labels_list: [],
    count_list: [],
    background: [],
  });

  const CHART_COLORS = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)'
  };

  useEffect(() => {
    getData();
    return () => {};
  }, []);

  const getData = async (data) => {
    try {
      if (data == undefined) {
        data = {
          role: userState.role
        };
      }else{
        data.role = userState.role
      }
      
      setlastSearchParams(data);
      setlawsub_id(0);
      const result = await Api.Report2(data);
      if (result.status == 200) {
        setDataTable(result.data);
        setdatasetCharts({
          labels_list: result.data.map((x) => x.name),
          count_list: result.data.map((x) => x.cnt),
          background: result.data.map((x) => {
            var letters = "BCDEF".split("");
            var color = "#";
            for (var i = 0; i < 6; i++) {
              color += letters[Math.floor(Math.random() * letters.length)];
            }
            return color;
          }),
        });
        console.log("data::", result.data);
      }
      // setDataTable(tableData);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (lawsub_id > 0) {
      getLawSubData();
      getLawSubPlantData();
    }
    return () => {};
  }, [lawsub_id]);

  const getLawSubData = async () => {
    try {
      const result = await Api.Report2SubLaw({
        ...lastSearchParams,
        lawGroup: lawsub_id,
      });
      if (result.status == 200) {
        setdatasetLawSubCharts({
          labels_list: result.data.map((x) => x.name),
          count_list: result.data.map((x) => x.cnt),
          background: result.data.map((x) => {
            var letters = "BCDEF".split("");
            var color = "#";
            for (var i = 0; i < 6; i++) {
              color += letters[Math.floor(Math.random() * letters.length)];
            }
            return color;
          }),
        });
        console.log("lawSub::", result.data);
        // setDataTable(result.data);
      }
      // setDataTable(tableData);
    } catch (error) {
      console.log(error);
    }
  };
  const getLawSubPlantData = async () => {
    try {
      const result = await Api.Report2SubLawPlant({
        ...lastSearchParams,
        lawGroup: lawsub_id,
      });
      if (result.status == 200) {
        setdatasetLawSubPlantCharts({
          labels_list: result.data.map((x) => x.sitecode),
          count_list: result.data.map((x) => x.cnt),
          background: result.data.map((x) => {
            var letters = "BCDEF".split("");
            var color = "#";
            for (var i = 0; i < 6; i++) {
              color += letters[Math.floor(Math.random() * letters.length)];
            }
            return color;
          }),
        });
        console.log("lawSubplant::", result.data);
        // setDataTable(result.data);
      }
      // setDataTable(tableData);
    } catch (error) {
      console.log(error);
    }
  };

  const options = {
    indexAxis: "y",
    // Elements options apply to all of the options unless overridden in a dataset
    // In this case, we are setting the border of each horizontal bar to be 2px wide
    elements: {
      bar: {
        borderWidth: 2,
      },
    },
    responsive: true,
    plugins: {
      legend: {
        position: "right",
      },
      title: {
        display: true,
        text: "Graph Regulation Risk",
      },
    },
  };

  const getElementAtEvent = (element) => {
    if (!element.length) return;
    const { datasetIndex, index } = element[0];
    setlawsub_id(dataTable[index].id);
  };

  return (
    <>
      <CRow>
        <CCol xs="12" lg="12">
          <CCard>
            <CCardHeader>
              <b> รายงาน Regulation Risk ({dataTable.length}) </b>
              <div style={{ fontSize: "12px" }}>
                <SearchPanel getData={getData} />
              </div>
            </CCardHeader>
            <CCardBody>
              {/* <Tables refreshData={getData} data={dataTable} /> */}
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
      <CRow>
        <CCol sm={12}>
          <CCard>
            <CCardBody>
              <CRow className="justify-content-center">
                <CCol sm={8}>
                  {dataTable.length > 0 && (
                    <Bar
                      key={"chart1"}
                      data={{
                        labels: datasetCharts.labels_list,
                        datasets: [
                          {
                            label: "กลุ่มกฏหมายหลัก",
                            data: datasetCharts.count_list,
                            backgroundColor: Object.values(CHART_COLORS),
                          },
                        ],
                      }}
                      getElementAtEvent={getElementAtEvent}
                      options={options}
                    />
                  )}
                </CCol>
              </CRow>
              <Divider
                style={{
                  marginTop: 10,
                  marginBottom: 10,
                  border: "0.5px solid #efefef",
                }}
              />
              <CRow className="mt-3">
                <CCol sm={6}>
                  {datasetLawSubCharts.labels_list.length > 0 && lawsub_id > 0 && (
                    <Bar
                      key={"chart2"}
                      data={{
                        labels: datasetLawSubCharts.labels_list,
                        datasets: [
                          {
                            label: "กลุ่มกฏหมายย่อย",
                            data: datasetLawSubCharts.count_list,
                            backgroundColor: Object.values(CHART_COLORS),
                          },
                        ],
                      }}
                    />
                  )}
                </CCol>
                <CCol sm={6}>
                  {datasetLawSubPlantCharts.labels_list.length > 0 &&
                    lawsub_id > 0 && (
                      <Bar
                      key={"chart3"}
                        data={{
                          labels: datasetLawSubPlantCharts.labels_list,
                          datasets: [
                            {
                              label: "กลุ่มกฏหมายย่อย ตาม Plant",
                              data: datasetLawSubPlantCharts.count_list,
                              backgroundColor: Object.values(CHART_COLORS),
                            },
                          ],
                        }}
                      />
                    )}
                </CCol>
              </CRow>
           
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default ReportGraph2;
