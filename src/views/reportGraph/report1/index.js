import React, { useEffect, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CNav,
  CForm,
  CLabel,
  CSelect,
  CInput,
  CListGroup,
  CListGroupItem,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import Api from "../../../api/ApiReportGraph";
// import Tables from './Tables';
import SearchPanel from "../component/searchpanel";
import { useDispatch, useSelector } from "react-redux";
// import { Gauge } from "chartjs-gauge";
import { Doughnut, Bar, Pie, PolarArea } from "react-chartjs-2";
const section1Type = ["all_document", "estimated_document", "wait_document"];
const section2Type = [
  "estimate_1",
  "estimate_2",
  "estimate_3_4",
  "very_high",
  "high",
  "medium",
  "low",
];

const CHART_COLORS = {
  red: 'rgb(255, 99, 132)',
  orange: 'rgb(255, 159, 64)',
  yellow: 'rgb(255, 205, 86)',
  green: 'rgb(75, 192, 192)',
  blue: 'rgb(54, 162, 235)',
  purple: 'rgb(153, 102, 255)',
  grey: 'rgb(201, 203, 207)'
};

const ReportGraph1 = () => {
  const [dataTable, setDataTable] = useState([]);
  const userState = useSelector((state) => state.changeState.user);
  const [datasetGauge1, setdatasetGauge1] = useState([]);
  const [datasetGauge2, setdatasetGauge2] = useState([]);
  const [totalGauge1, setTotalGauge1] = useState([]);
  const [totalGauge2, setTotalGauge2] = useState([]);

  useEffect(() => {
    getData();
    return () => {};
  }, []);

  const getData = async (data) => {
    try {
      if (data == undefined) {
        data = {
          role: userState.role
        };
      }else{
        data.role = userState.role
      }
      const result = await Api.Report1(data);
      if (result.status == 200) {
        console.log("data::", result.data);
        setDataTable(result.data);
        var gauge1 = [];
        var gauge2 = [];
        var total1 = 0;
        var total2 = 0;
        section1Type.map((type) => {
          var res = result.data.find((x) => x.dbtype == type);
          // console.log("res::", res);
          gauge1.push(res);
          // console.log("cnt1::", res.cnt);
          if (type != "all_document"){
            total1 += res.cnt;
          }
        });
        section2Type.map((type) => {
          var res = result.data.find((x) => x.dbtype == type);
          // console.log("res2::", res);
          gauge2.push(res);
          // console.log("cnt2::", res.cnt);
          total2 += res.cnt;
        });
        console.log("gauge1::", gauge1);
        setdatasetGauge1(gauge1);
        console.log("total1::", total1);
        setTotalGauge1(total1);
        console.log("gauge2::", gauge2);
        setdatasetGauge2(gauge2);
        console.log("total2::", total2);
        setTotalGauge2(total2);
      }
      // setDataTable(tableData);
    } catch (error) {
      console.log(error);
    }
  };

  const option = {
    tooltip: {
      callbacks: {
        label: function (tooltipItem, data) {
          alert("");
          var dataset = data.datasets[tooltipItem.datasetIndex];
          var meta = dataset._meta[Object.keys(dataset._meta)[0]];
          var total = meta.total;
          var currentValue = dataset.data[tooltipItem.index];
          var percentage = parseFloat(
            ((currentValue / total) * 100).toFixed(1)
          );
          return currentValue + " (" + percentage + "%)";
        },
        title: function (tooltipItem, data) {
          alert("");
          return data.labels[tooltipItem[0].index];
        },
      },
    }
  };

  return (
    <>
      <CRow>
        <CCol xs="12" lg="12">
          <CCard>
            <CCardHeader>
              <b> รายงาน Regulation Compliance ({dataTable.length}) </b>
              <div style={{ fontSize: "12px" }}>
                <SearchPanel getData={getData} />
              </div>
            </CCardHeader>
            <CCardBody>
              {/* <Tables refreshData={getData} data={dataTable} /> */}
              {/* <Doughnut 
                style={{ maxWidth: "100%" }}
                data={{
                  labels: datasetGauge1.map((x) => x.dbtype),
                  datasets: [
                    {
                      data: datasetGauge1.map((x) => Math.floor((x.cnt / totalGauge1) * 100)),

                      backgroundColor: Object.values(CHART_COLORS),
                    },
                  ],
                }}
                options={option}  
              /> */}
              <CRow>
                  <Doughnut
                    style={{ maxWidth: "50%" }}
                    data={{
                      labels: datasetGauge1.filter((type) => type.dbtype != "all_document").map((x) => x.dbtypeTH),
                      datasets: [
                        {
                          data: datasetGauge1.filter((type) => type.dbtype != "all_document").map((x) => x.cnt/totalGauge1 * 100),

                          backgroundColor: Object.values(CHART_COLORS),
                          rotation:  -90,
                          circumference: 180
                        },
                      ],
                    }}
                    options={{
                      responsive: true,
                      maintainAspectRatio: false,
                    }}
                  />
                  <Doughnut
                    style={{ maxWidth: "50%", height: 300 }}
                    data={{
                      labels: datasetGauge2.map((x) => x.dbtypeTH),
                      datasets: [
                        {
                          data: datasetGauge2.map((x) => x.cnt/totalGauge1 * 100),

                          backgroundColor: Object.values(CHART_COLORS),
                          rotation:  -90,
                          circumference: 180,
                        },
                      ],
                    }}
                    options={{
                      responsive: true,
                      maintainAspectRatio: false,
                    }}
                  />
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>
        <CCol sm="12" lg="6">
          <CCard>
            <CCardHeader>สรุปสถานะเอกสารการประเมิน</CCardHeader>
            <CCardBody>
              <CListGroup>
                {section1Type.map((type) => {
                  return (
                    dataTable.find((x) => x.dbtype == type) && (
                      <CListGroupItem className="justify-content-between d-flex">
                        {dataTable.find((x) => x.dbtype == type).dbtypeTH}
                        <CBadge
                          style={{ width: "auto" }}
                          className="float-right"
                          shape="pill"
                          color="success"
                        >
                          {dataTable.find((x) => x.dbtype == type).cnt}
                        </CBadge>
                      </CListGroupItem>
                    )
                  );
                })}
              </CListGroup>
            </CCardBody>
          </CCard>
        </CCol>
        <CCol sm="12" lg="6">
          <CCard>
            <CCardHeader>สรุปรายงานกลุ่มกฏหมายอยู่ยอมรับได้</CCardHeader>
            <CCardBody>
              <CListGroup>
                {section2Type.map((type) => {
                  return (
                    dataTable.find((x) => x.dbtype == type) && (
                      <CListGroupItem className="justify-content-between d-flex">
                        {dataTable.find((x) => x.dbtype == type).dbtypeTH}
                        <CBadge
                          style={{ width: "auto" }}
                          className="float-right"
                          shape="pill"
                          color="success"
                        >
                          {dataTable.find((x) => x.dbtype == type).cnt}
                        </CBadge>
                      </CListGroupItem>
                    )
                  );
                })}
              </CListGroup>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default ReportGraph1;
