import React, { useEffect, useState } from 'react'
import {
    CBadge,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CModal,
    CModalHeader,
    CModalFooter,
    CButton,
    CNav,
    CForm,
    CLabel,
    CSelect,
    CInput,
} from '@coreui/react'
import CIcon from "@coreui/icons-react";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
// import Api from '../../api/ApiRole';
import ApiSystemType from '../../../api/ApiSystemType';
import ApiLawGroup from '../../../api/ApiLawGroup';
import ApiLawAgencies from '../../../api/ApiLawAgencies';
import ApiLawType from '../../../api/ApiLawType';
import ApiBU from '../../../api/ApiBU';
import ApiPlant from '../../../api/ApiPlant';

const SearchPanel = ({getData}) => {
    const [systemTypeData, setSystemTypeData] = useState([]);
    const [deptData, setDeptData] = useState([]);
    const [lawGroupData, setLawGroupData] = useState([]);
    const [lawTypeData, setLawTypeData] = useState([]);
    const [buData, setBUData] = useState([]);
    const [plantData, setPlantData] = useState([]);
    
    const [qDate, setQDate] = useState({ startDate: null, endDate: null });
    const [qFocused, setQFocused] = useState();

    const [systemtype, setSystemType] = useState(0);
    const [lawagency, setLawAgency] = useState(0);
    const [lawType, setLawType] = useState(0);
    const [mainLaw, setMainLaw] = useState("");
    const [lawGroup, setLawGroup] = useState(0);
    const [bu, setBU] = useState(0);
    const [plant, setPlant] = useState(0);
    const [docno, setDocNo] = useState("");

    useEffect(() => {
        getSystemTypeOption();
        getLawGroupOption();
        getDeptOption();
        getLawTypeOption();
        getBUOption();
        // getPlantOption();
        return () => {
  
        }
    }, []);

    const getSystemTypeOption = async () => {
        try{
            const result = await ApiSystemType.get();
            if (result.status == 200) {
              setSystemTypeData(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }

    const getLawGroupOption = async () => {
        try{
            const result = await ApiLawGroup.get();
            if (result.status == 200) {
              setLawGroupData(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }

    const getDeptOption = async () => {
        try{
            const result = await ApiLawAgencies.get();
            if (result.status == 200) {
              setDeptData(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }

    const getLawTypeOption = async () => {
        try{
            const result = await ApiLawType.get();
            if (result.status == 200) {
              setLawTypeData(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }

    const getBUOption = async () => {
        try{
            const result = await ApiBU.get();
            if (result.status == 200) {
              setBUData(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }

    const getPlantOption = async (bu) => {
        try{
            const result = await ApiPlant.getByBu(bu);
            if (result.status == 200) {
              setPlantData(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }

    const resetSearchOption = (e,type) => {
        setSystemType(0);
        setLawAgency(0);
        setLawType(0);
        setMainLaw("");
        setDocNo("");
        setLawGroup(0);
        setBU(0);
        setPlant(0);
        setQDate({ startDate: null, endDate: null });
        const model = {
          "typeReset": "reset"
        };
        getData(model);
    }

    const packData = (e, type) => {
      // e.preventDefault();
      const model = {
        "systemtype": systemtype,
        "lawagency": lawagency,
        "lawType": lawType,
        "mainLaw": mainLaw,
        "lawGroup": lawGroup,
        "bu": bu,
        "plant": plant,
        "docNo": docno,
        "startDate": qDate.startDate,
        "endDate": qDate.endDate,
      }
      getData(model);
    }

    const onChangeBu = (bu) => {
        setBU(bu);
        if(bu != 0){
            getPlantOption(bu);
        }
    }

    return (
        <>
        <CRow>
            <CCol sm="12" md="3">
                <CRow>
                    <CCol sm="12" md="12">
                        <CLabel htmlFor="systemtype">
                            <b>ประเภทของระบบ</b>
                        </CLabel>
                    </CCol>
                    <CCol sm="12" md="12">
                        <CSelect
                            custom
                            name="systemtype"
                            id="systemtype"
                            value={systemtype}
                            onChange={(e)=>setSystemType(e.target.value)}
                        >
                            <option value="">กรุณาเลือก</option>
                            {
                                systemTypeData.map((item, index) => {
                                    return <option key={item.id} value={item.id}>{item.name}</option>
                                })
                            }
                        </CSelect>
                    </CCol>
                </CRow>
            </CCol>
            <CCol sm="12" md="3">
                <CRow>
                    <CCol sm="12" md="12">
                    <CLabel htmlFor="lawagency">
                        <b>หน่วยงานผู้ออกกฎหมาย</b>
                    </CLabel>
                    </CCol>
                    <CCol sm="12" md="12">
                    <CSelect
                        custom
                        name="lawagency"
                        id="lawagency"
                        value={lawagency}
                        onChange={(e)=>setLawAgency(e.target.value)}
                    >
                        <option value="">กรุณาเลือก</option>
                        {
                            deptData.map((item, index) => {
                                return <option key={item.id} value={item.id}>{item.name}</option>
                            })
                        }
                    </CSelect>
                    </CCol>
                </CRow>
            </CCol>
            <CCol sm="12" md="3">
                <CRow>
                    <CCol sm="12" md="12">
                    <CLabel htmlFor="lawType">
                        <b>ประเภทกฎหมาย</b>
                    </CLabel>
                    </CCol>
                    <CCol sm="12" md="12">
                    <CSelect
                        custom
                        name="lawType"
                        id="lawType"
                        value={lawType}
                        onChange={(e)=>setLawType(e.target.value)}
                    >
                        <option value="">กรุณาเลือก</option>
                        {
                        lawTypeData.map((item, index) => {
                            return <option key={item.id} value={item.id}>{item.name}</option>
                        })
                        }
                    </CSelect>
                    </CCol>
                </CRow>
            </CCol>
            <CCol sm="12" md="3">
                <CRow>
                    <CCol sm="12" md="12">
                        <CLabel htmlFor="mainLaw">
                            <b>ชื่อกฎหมาย</b>
                        </CLabel>
                    </CCol>
                    <CCol sm="10" md="12">
                        <CInput
                        custom
                        name="mainLaw"
                        id="mainLaw"
                        value={mainLaw}
                        onChange={(e) => setMainLaw(e.target.value)}
                        />
                    </CCol>
                </CRow>
            </CCol>
        </CRow>
        <CRow className="mt-3">
            <CCol sm="12" md="3">
                <CRow>
                    <CCol sm="12" md="12">
                        <CLabel htmlFor="lawGroup">
                            {" "}
                            <b>กลุ่มของกฎหมาย</b>{" "}
                        </CLabel>
                    </CCol>
                    <CCol sm="10" md="12">
                        <CSelect
                            custom
                            name="lawGroup"
                            id="lawGroup"
                            value={lawGroup}
                            onChange={(e)=>setLawGroup(e.target.value)}
                        >
                            <option value="">กรุณาเลือก</option>
                            {
                            lawGroupData.map((item, index) => {
                                return <option key={item.id} value={item.id}>{item.name}</option>
                            })
                            }
                        </CSelect>
                    </CCol>
                </CRow>
            </CCol>
            <CCol sm="12" md="3">
                <CRow>
                    <CCol sm="12" md="12">
                        <CLabel htmlFor="bu">
                            {" "}
                            <b>กลุ่มธุรกิจ</b>{" "}
                        </CLabel>
                    </CCol>
                    <CCol sm="10" md="12">
                        <CSelect
                            custom
                            name="bu"
                            id="bu"
                            value={bu}
                            onChange={(e)=> onChangeBu(e.target.value)}
                        >
                            <option value="">กรุณาเลือก</option>
                            {
                            buData.map((item, index) => {
                                return <option key={item.id} value={item.id}>{item.code}</option>
                            })
                            }
                        </CSelect>
                    </CCol>
                </CRow>
            </CCol>
            <CCol sm="12" md="3">
                <CRow>
                    <CCol sm="12" md="12">
                        <CLabel htmlFor="plant">
                            {" "}
                            <b>โรงงาน</b>{" "}
                        </CLabel>
                    </CCol>
                    <CCol sm="10" md="12">
                        <CSelect
                            custom
                            name="plant"
                            id="plant"
                            value={plant}
                            onChange={(e)=>setPlant(e.target.value)}
                        >
                            <option value="">กรุณาเลือก</option>
                            {
                            plantData.map((item, index) => {
                                return <option key={item.id} value={item.plant}>{item.siteDescription}</option>
                            })
                            }
                        </CSelect>
                    </CCol>
                </CRow>
            </CCol>
            <CCol sm="12" md="3">
                <CRow>
                    <CCol sm="12" md="12">
                    <CLabel>
                        {" "}
                        <b>วันที่สร้างเอกสาร</b>{" "}
                    </CLabel>
                    </CCol>
                    <CCol col xs="12" md="12">
                    <DateRangePicker
                        displayFormat="DD/MM/YYYY"
                        small
                        block
                        align="center"
                        startDatePlaceholderText={"จากวันที่"}
                        startDate={qDate.startDate}
                        startDateId="startQDate"
                        endDate={qDate.endDate}
                        endDateId="endQDate"
                        endDatePlaceholderText={"ถึงวันที่"}
                        onDatesChange={(value) => setQDate(value)}
                        focusedInput={qFocused}
                        onFocusChange={(focusedInput) =>
                            setQFocused(focusedInput)
                        }
                        orientation="horizontal"
                        openDirection="down"
                        minimumNights={0}
                        isOutsideRange={() => false}
                    />
                    </CCol>
                </CRow>
            </CCol>
        </CRow>
        <CRow>
            <CCol sm="12" md="3">
                <CRow>
                    <CCol sm="12" md="12">
                        <CLabel htmlFor="docno">
                            <b>เลขที่เอกสาร</b>
                        </CLabel>
                    </CCol>
                    <CCol sm="10" md="12">
                        <CInput
                        custom
                        name="docno"
                        id="docno"
                        value={docno}
                        onChange={(e) => setDocNo(e.target.value)}
                        />
                    </CCol>
                </CRow>
            </CCol>
        </CRow>
        <CRow className="">
            <CCol sm="12" md="3" className="mt-4">
                <CRow className="justify-content-end">
                    <CCol sm="12" md="6" className="justify-content-center">
                    <CButton
                        color="danger"
                        variant="outline"
                        block
                        onClick={() => resetSearchOption()}
                    >
                        <b> ล้างข้อมูล </b>{" "}
                    </CButton>
                    </CCol>
                    <CCol sm="12" md="6" className="justify-content-center">
                    <CButton
                        color="success"
                        variant="outline"
                        block
                        onClick={() => packData()}
                    >
                        <CIcon name="cil-search" style={{ marginRight: "2px" }} />{" "}
                        <b> ค้นหา </b>
                    </CButton>
                    </CCol>
                </CRow>
            </CCol>
        </CRow>
        </>
    );
}
export default SearchPanel;
