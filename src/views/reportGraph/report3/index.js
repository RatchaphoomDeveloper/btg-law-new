import React, { useEffect, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CNav,
  CForm,
  CLabel,
  CSelect,
  CInput,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import Api from "../../../api/ApiReportGraph";
// import Tables from './Tables';
import SearchPanel from "../component/searchpanel";
import Swal from "sweetalert2";
import { Doughnut, Bar } from "react-chartjs-2";
import { Divider } from "antd";
import Tables from "./Tables";
import Chart from "react-apexcharts";
import { useDispatch, useSelector } from "react-redux";

const ReportGraph3 = () => {
  const [dataTable, setDataTable] = useState([]);
  const userState = useSelector((state) => state.changeState.user);
  const [datasetCharts, setdatasetCharts] = useState({
    labels_list: [],
    count_list: [],
    background: [],
  });
  const [datasetPriorityCharts, setdatasetPriorityCharts] = useState({
    labels_list: [],
    count_list: [],
    background: [],
  });
  useEffect(() => {
    getData();
    return () => {};
  }, []);

  const getData = async (data) => {
    Swal.showLoading();
    try {
      if (data == undefined) {
        data = {
          role: userState.role
        };
      }else{
        data.role = userState.role
      }
      const result = await Api.Report3(data);
      const result2 = await Api.Report3Detail(data);
      const result3 = await Api.Report3Priority(data);
      if (result.status == 200) {
        setdatasetCharts({
          labels_list: result.data.map((x) => x.sysstatus),
          count_list: result.data.map((x) => x.cnt),
          background: result.data.map((x) => {
            var letters = "BCDEF".split("");
            var color = "#";
            for (var i = 0; i < 6; i++) {
              color += letters[Math.floor(Math.random() * letters.length)];
            }
            return color;
          }),
        });
      }
      if (result2.status == 200) {
        setDataTable(result2.data);
        console.log(result2.data);
      }
      if (result3.status == 200) {
        setdatasetPriorityCharts({
          labels_list: result3.data.map((x) => x.priority_name),
          count_list: result3.data.map((x) => x.cnt),
          background: result3.data.map((x) => {
            var letters = "BCDEF".split("");
            var color = "#";
            for (var i = 0; i < 6; i++) {
              color += letters[Math.floor(Math.random() * letters.length)];
            }
            return color;
          }),
        });
      }
      Swal.close();
      // setDataTable(tableData);
    } catch (error) {
      Swal.close();
      console.log(error);
    }
  };

  return (
    <>
      <CRow>
        <CCol xs="12" lg="12">
          <CCard>
            <CCardHeader>
              <b> รายงาน Overdue Action Plan ({dataTable.length}) </b>
              <div style={{ fontSize: "12px" }}>
                <SearchPanel getData={getData} />
              </div>
            </CCardHeader>
            <CCardBody>
              {/* <Tables refreshData={getData} data={dataTable} /> */}
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
      <CRow>
        <CCol sm={12}>
          <CCard>
            <CCardBody>
              <CRow className="justify-content-center">
                <CCol sm={6}>
                  {datasetPriorityCharts.labels_list.length > 0 && (
                    // <Doughnut
                    //   title="Graph Overdue Action Plan "
                    //   //   style={{ maxWidth: "100%" }}
                    //   data={{
                    //     labels: datasetCharts.labels_list,
                    //     datasets: [
                    //       {
                    //         data: datasetCharts.count_list,
                    //         backgroundColor: datasetCharts.background,
                    //       },
                    //     ],
                    //   }}
                    //   //   options={option}
                    // />
                    <Chart options={{
                      series: datasetPriorityCharts.count_list,
                      labels: datasetPriorityCharts.labels_list,
                      legend: {
                        position: 'bottom'
                      },
                      title: {
                        text: "Graph Action Plan Priority",
                        align: "center"
                      }
                    }} series={datasetPriorityCharts.count_list} type="donut" style={{ maxWidth: "100%" }} />
                  )}
                </CCol>
                <CCol sm={6}>
                  {datasetCharts.labels_list.length > 0 && (
                    // <Doughnut
                    //   title="Graph Overdue Action Plan "
                    //   //   style={{ maxWidth: "100%" }}
                    //   data={{
                    //     labels: datasetCharts.labels_list,
                    //     datasets: [
                    //       {
                    //         data: datasetCharts.count_list,
                    //         backgroundColor: datasetCharts.background,
                    //       },
                    //     ],
                    //   }}
                    //   //   options={option}
                    // />
                    <Chart options={{
                      series: datasetCharts.count_list,
                      labels: datasetCharts.labels_list,
                      legend: {
                        position: 'bottom'
                      },
                      title: {
                        text: "Graph Overdue Action Plan",
                        align: "center"
                      }
                    }} series={datasetCharts.count_list} type="donut" style={{ maxWidth: "100%" }} />
                  )}
                </CCol>
              </CRow>
              <Divider
                style={{
                  marginTop: 10,
                  marginBottom: 10,
                  border: "0.5px solid #efefef",
                }}
              />
              <CRow className="mt-3">
                <CCol sm={12}>
                  <Tables refreshData={getData} data={dataTable} />
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default ReportGraph3;
