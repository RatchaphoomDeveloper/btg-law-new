export const lawTypeData = [
    { id: 0, code: 'L01', name: 'LAW_ประกาศกรม/ประกาศคณะกรรมการ/ประกาศสำนักงาน (DNT)',  statuscode: 'I', status: 'Inactive', editby: "Admin", editdate: "12/05/2020" },
    { id: 1, code: 'L02', name: 'LAW_ประกาศกระทรวง (MNT)',  statuscode: 'A',status: 'Active', editby: "Admin", editdate: "12/05/2020" }
]

export const statusData = [
    { id: 0, name: 'Active', code: "A" },
    { id: 1, name: 'In Active', code: "I" },
]

