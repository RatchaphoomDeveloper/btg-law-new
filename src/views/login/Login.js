import React, { useRef, useState, useContext, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { adfs_redirect_url, adfs_client_id } from "../../env.js";
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
  CLink,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { users } from "../../assets/data/users.js";
import { useHistory } from "react-router-dom";
import Api from "../../api/ApiLogin";
import { setLocalStorage } from "../../utils/localStorage.js";
import AlertError from "../../reusable/AlertError.js";
import Swal, { swal } from "sweetalert2/dist/sweetalert2.js";
import Background from '../../assets/images/BRCH_2020_Login.png';

const Login = () => {
  const history = useHistory();

  const formInput = useRef(null);
  const [error, seterror] = useState();

  const dispatch = useDispatch();
  const userState = useSelector((state) => state.changeState.user);

  const login = async (e) => {
    seterror("");
    e.preventDefault();
    Swal.showLoading();
    const { username, password } = e.target;
    const model = {
      username: username.value,
      password: password.value,
    };
    // const user = users.find(
    //   (x) => x.username === model.username && x.password === model.password
    // );

    try {
      const result = await Api.login(model);
      Swal.close();
      setTimeout(() => {
        if (result.status === 200) {
          const { user, token } = result.data;
          if (user) {
            history.push("/home");
            setLocalStorage("token", token);
            setLocalStorage("role", user.role);
            dispatch({ type: "set_user", user: user, token: token });
          } else {
            formInput.current.reset();
            seterror("ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง โปรดลองใหม่อีกครั้ง");
            // seterror(result.message);
          }
        }
      }, 1500);
    } catch (error) {
      // console.log(error.response.data);
      Swal.close();
      // const { data } = error.response;
      // AlertError("ผิดพลาด", "รหัสผู้ใช้งานไม่ถูกต้อง");
      // seterror("ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง โปรดลองใหม่อีกครั้ง");
      seterror(error.response.data);
    }
  };

  useEffect(() => {
    dispatch({ type: "set_user", user: {}, token: "" });

    return () => {};
  }, []);

  return (
    // <div className="c-app c-default-layout bg-dark flex-row align-items-center">
    <div className="c-app c-default-layout flex-row align-items-center bg-login" style={{backgroundImage: `url(${Background})`, backgroundSize: "cover"}} >
      <CContainer>
        <CRow className="justify-content-end">
          <CCol md="6">
            <CCardGroup>
              <CCard className="p-4 border">
                <CCardBody>
                  <form ref={formInput} onSubmit={login}>
                    <h1 style={{color: "#3ca220"}}>Login</h1>
                    <p className="text-muted">Sign In to your account</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        required
                        type="text"
                        name="username"
                        placeholder="Username"
                        autoComplete={false}
                      />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        required
                        type="password"
                        name="password"
                        placeholder="Password"
                        autoComplete={false}
                      />
                    </CInputGroup>
                    <CRow>
                      <CCol xs="6">
                        <CButton
                          variant="outline"
                          // onClick={login}
                          type="submit"
                          color="success"
                          className="px-4"
                        >
                          Login
                        </CButton>
                        <CButton variant="ghost" size="sm" />
                        <CButton color="secondary" variant="outline" className="px-4" onClick={() => history.push("/")}>
                          Back
                        </CButton>
                      </CCol>
                    </CRow>
                    <CRow className="mt-3">
                      <CCol sm="12">
                        <CLink href={`https://login.microsoftonline.com/eb1f94dc-8d25-4c67-985c-04be74c8f698/oauth2/v2.0/authorize?client_id=${adfs_client_id}&response_type=token&redirect_uri=${adfs_redirect_url}&scope=https%3A%2F%2Fgraph.microsoft.com%2F.default%20&state=12345`}>Sign in with Azure AD</CLink>
                      </CCol>
                    </CRow>
                  </form>
                  <label className="text-danger mt-3">{error}</label>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  
  );
};

export default Login;
