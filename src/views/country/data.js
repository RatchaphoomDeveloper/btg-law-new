export const countryData = [
    { id: 0, code: '01', name: 'ฟินแลนด์', statuscode: 'I', status: 'Inactive', editby: "Admin", editdate: "12/05/2020" },
    { id: 1, code: '02', name: 'เดนมาร์ก', statuscode: 'A', status: 'Active', editby: "Admin", editdate: "12/05/2020" }
]

export const statusData = [
    { id: 0, name: 'Active', code: "A" },
    { id: 1, name: 'In Active', code: "I" },
]

