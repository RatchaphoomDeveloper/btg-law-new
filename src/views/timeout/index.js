import React, { useRef, useState, useContext, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { users } from "../../assets/data/users.js";
import { useHistory } from "react-router-dom";
import Api from "../../api/ApiLogin";
import { setLocalStorage } from "../../utils/localStorage.js";
import AlertError from "../../reusable/AlertError.js";
import Swal, { swal } from "sweetalert2/dist/sweetalert2.js";
import Background from '../../assets/images/BRCH_2020_Login.png';

const Timeout = () => {
  const history = useHistory();

  const dispatch = useDispatch();


  useEffect(() => {
    dispatch({ type: "set_user", user: {}, token: "" });
    history.push("/dashboard")

    return () => {};
  }, []);

  return (<></>);
};

export default Timeout;
