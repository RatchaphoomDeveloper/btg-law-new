import React, { useState, useEffect } from "react";
import {
  CCardBody,
  CBadge,
  CButton,
  CCollapse,
  CDataTable,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CCol,
  CCard,
  CRow,
  CCardHeader,
  CFormGroup,
  CLabel,
  CInput,
  CSelect,
  CForm,
  CModalTitle,
  CButtonGroup,
} from "@coreui/react";
import ApiRole from "../../api/ApiRole";
import Api from "../../api/ApiUserRole";

import CIcon from "@coreui/icons-react";
import { statusData } from "./data";
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2/src/sweetalert2.js";

const getBadge = (status) => {
  switch (status) {
    case "A":
      return "success";
    case "I":
      return "secondary";
    default:
      return "primary";
  }
};

const getStatus = (status) => {
  switch (status) {
    case "A":
      return "Active";
    case "I":
      return "Inactive";
    default:
      return "";
  }
};

const fields = [
  { key: "option", label: "", _style: { width: "1%" }, filter: false },
  { key: "roleid", label: "ประเภทผู้ใช้งาน" },
  { key: "status", label: "สถานะการใช้งาน", _style: { width: "10%" } },
  { key: "updateBy", label: "ผู้แก้ไขข้อมูล", _style: { width: "15%" } },
  { key: "updateDate", label: "วันที่แก้ไขข้อมูล", _style: { width: "15%" } },
];

const Tables = ({ data = [], refreshData = () => {}, loginuserid }) => {
  const userState = useSelector((state) => state.changeState.user);
  const [roleData, setRoleData] = useState([]);
  const [role, setRole] = useState("");
  const [modal, setModal] = useState(false);
  const [id, setId] = useState("");
  const [disablerole, setDisableRole] = useState("");
  const [recordstatus, setRecordStatus] = useState("");
  const [modalConfirm, setModalConfirm] = useState(false);

  useEffect(() => {
    getRoleOption();
    return () => {};
  }, []);

  const getRoleOption = async () => {
    try {
      const result = await ApiRole.get();
      if (result.status == 200) {
        setRoleData(result.data);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const toggle = () => {
    setModal(!modal);
  };

  const toggleConfirm = () => {
    setModalConfirm(!modalConfirm);
  };

  const newDetails = (record) => {
    toggle();
    setRole("");
    setRecordStatus("A");
    setDisableRole("");
  };

  const editDetails = (record) => {
    toggle();
    setRole(record.roleid);
    setRecordStatus(record.recordstatus);
    setDisableRole("On");
  };

  const onSubmit = (e) => {
    e.preventDefault();
    const model = {
      roleid: role,
      userid: loginuserid,
      recordstatus: recordstatus,
    };
    saveData(model);
  };

  const saveData = async (data) => {
    try {
      const result = await Api.create(data);
      if (result.status === 200) {
        const { data } = result.data;
        toggle();
        refreshData();

        Swal.fire({
          icon: "success",
          title: "Save Success",
        });
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: error.response.data,
      });
    }
  };

  const handleChange = (e, type) => {
    switch (type) {
      case "role":
        setRole(e.target.value);
        break;
      default:
        setRecordStatus(e.target.value);
        break;
    }
  };
  return (
    <>
      <CRow>
        <CCol xs="12" lg="12">
          <CButton onClick={newDetails} color={"success"} variant={"outline"}>
            <CIcon name="cil-plus" />
            <span className="ml-2">สร้าง ประเภทผู้ใช้งาน</span>
          </CButton>
        </CCol>
      </CRow>
      <CRow className="mt-3">
        <CCol xs="12" lg="12">
          <CCard>
            <CCardBody>
              <CDataTable
                items={data}
                fields={fields}
                tableFilter={{
                  label: "ค้นหา",
                  placeholder: "พิมพ์คำที่ต้องการค้นหา",
                }}
                cleaner
                itemsPerPageSelect={{
                  label: "จำนวนการแสดงผล",
                  values: [10, 25, 50, 100],
                }}
                itemsPerPage={10}
                hover
                sorter
                striped
                bordered
                pagination
                scopedSlots={{
                  roleid: (item) => {
                    const newRole = roleData.find((x) => x.id == item.roleid);
                    return (
                      <td style={{ textAlign: "center" }}>{newRole ? newRole.code : ""}</td>
                    );
                  },
                  status: (item) => (
                    <td>
                      <CBadge color={getBadge(item.recordstatus)}>
                        {getStatus(item.recordstatus)}
                      </CBadge>
                    </td>
                  ),
                  option: (item) => (
                    <td className="center">
                      <CButtonGroup>
                        <CButton
                          color="success"
                          variant="outline"
                          shape="square"
                          size="sm"
                          onClick={() => {
                            editDetails(item);
                          }}
                        >
                          <CIcon name="cilPen" />
                        </CButton>
                      </CButtonGroup>
                    </td>
                  ),
                }}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>

      <CModal show={modal} onClose={toggle}>
        <CModalHeader closeButton>
          <CModalTitle>รายละเอียด ข้อมูลผู้ใช้งาน</CModalTitle>
        </CModalHeader>
        <CForm onSubmit={onSubmit} action="">
          <CModalBody>
            <CInput type="hidden" value={id} id="id" />

            <CRow>
              <CCol xs="12">
                <CFormGroup>
                  <CLabel htmlFor="role">Group</CLabel>
                  <CSelect
                    custom
                    name="role"
                    id="role"
                    value={role}
                    disabled={disablerole != ""}
                    onChange={(e) => handleChange(e, "role")}
                    required
                  >
                    <option value={""}>-- Group --</option>
                    {roleData.map((item, index) => {
                      return (
                        <option key={item.id} value={item.id}>
                          {item.code}
                        </option>
                      );
                    })}
                  </CSelect>
                </CFormGroup>
              </CCol>
            </CRow>

            <CRow>
              <CCol xs="12">
                <CFormGroup>
                  <CLabel htmlFor="recordstatus">สถานะการใช้งาน</CLabel>
                  <CSelect
                    custom
                    name="recordstatus"
                    id="recordstatus"
                    value={recordstatus}
                    onChange={(e) => handleChange(e, "status")}
                    required
                  >
                    <option value={""}>-- สถานะการใช้งาน --</option>
                    {statusData.map((item, index) => {
                      return (
                        <option key={item.code} value={item.code}>
                          {item.name}
                        </option>
                      );
                    })}
                  </CSelect>
                </CFormGroup>
              </CCol>
            </CRow>
          </CModalBody>
          <CModalFooter>
            <CButton variant="outline" type="submit" color="primary">
              ตกลง
            </CButton>
            <CButton variant="outline" color="secondary" onClick={toggle}>
              ยกเลิก
            </CButton>
          </CModalFooter>
        </CForm>
      </CModal>
    </>
  );
};

export default Tables;
