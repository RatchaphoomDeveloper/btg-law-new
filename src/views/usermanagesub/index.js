import React, { useEffect, useState } from 'react'
import {
    CBadge,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CModal,
    CModalHeader,
    CModalFooter,
    CButton
} from '@coreui/react'
import Api from '../../api/ApiUserRole';
import ApiUser from '../../api/ApiUser';
import Tables from './Tables';

const UserManageSub = (props) => {
    const id = new URLSearchParams(props.location.search).get("id");
    const [dataTable, setDataTable] = useState([]);
    const [username, setUsername] = useState("");
    const [fullname, setFullname] = useState("");
    const [bu, setBU] = useState("");
    useEffect(() => {
        getData();
        getUserData();
        return () => {

        }
    }, []);
  
    const getData = async () => {
        try{
            const result = await Api.get(id);
            if (result.status == 200) {
                setDataTable(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }
  
    const getUserData = async () => {
        try{
            const result = await ApiUser.getDetail(id);
            if (result.status == 200) {
                var data = result.data;
                setUsername(data.userlogin);
                setFullname(data.fullname);
                setBU(data.buDescription);
            }
        }catch(error){
            console.log(error);
        }
    }
    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardHeader>
                            <b> ประเภทผู้ใช้งาน ({dataTable.length}) {username} ({fullname}) - BU : {bu}</b>
                        </CCardHeader>
                        <CCardBody>
                            <Tables refreshData={getData} data={dataTable} loginuserid={id} />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    )
}

export default UserManageSub;