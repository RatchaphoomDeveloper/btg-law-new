import React, { useState, useEffect } from 'react'
import {
    CCardBody,
    CBadge,
    CButton,
    CCollapse,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CCol,
    CCard,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CSelect,
    CForm,
    CModalTitle,
    CButtonGroup
} from '@coreui/react'
import Api from '../../api/ApiBuAssign';
import ApiPlant from '../../api/ApiPlant';
import CIcon from '@coreui/icons-react'
import { statusData } from './data';
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2/src/sweetalert2.js";

const getBadge = status => {
    switch (status) {
        case 'A': return 'success'
        case 'I': return 'secondary'
        default: return 'primary'
    }
}

const getStatus = status => {
    switch (status) {
        case 'A': return 'Active'
        case 'I': return 'Inactive'
        default: return ''
    }
}

const fields = [
    // { key: 'order', label: 'ลำดับ', _style: { width: '1%' } },
    { key: 'option', label: '', _style: { width: '1%' }, filter: false, },
    { key: 'assignGroupCodeLink', label: 'Assign Group Code' },
    { key: 'assignGroupName', label: 'Assign Group Name' },
    { key: 'plant', label: 'Plant' },
    { key: 'siteDescription', label: 'Plant Description' },
    { key: 'status', label: 'สถานะการใช้งาน', _style: { width: '10%' } },
    { key: 'updateBy', label: 'ผู้แก้ไขข้อมูล', _style: { width: '15%' } },
    { key: 'updateDate', label: 'วันที่แก้ไขข้อมูล', _style: { width: '15%' } },
]



const Tables = ({ data = [], refreshData = () => { } }) => {
  const userState = useSelector((state) => state.changeState.user);
  const [siteData, setSiteData] = useState([]);

    const [modal, setModal] = useState(false);
    const [id, setId] = useState('');
    const [code, setCode] = useState('');
    const [name, setName] = useState('');
    const [recordstatus, setRecordStatus] = useState('');
    const [dataSelected, setDataSelected] = useState('');
    const [modalConfirm, setModalConfirm] = useState(false);
    const [plantid, setSite] = useState('');

    useEffect(() => {
        getSiteOption();
          return () => {
  
          }
    }, []);

    const getSiteOption = async () => {
        try{
            const result = await ApiPlant.get();
            if (result.status == 200) {
                setSiteData(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }

    const toggle = () => {
        setModal(!modal);
    }

    const toggleConfirm = () => {
      setModalConfirm(!modalConfirm);
    }

    const newDetails = (record) => {
        toggle();
        setId('');
        setCode('');
        setName('');
        setSite(0);
        setRecordStatus("A");
    }

    const deleteDetails = (record) => {
        toggleConfirm();
        setDataSelected(record);
        setId(record.id);
    }

    const onDelete = async () => {
      try {
          const result = await Api.delete(dataSelected);
          if (result.status === 200) {
              const { data } = result.data;
              toggleConfirm();
              refreshData();
              
              Swal.fire({
                  icon: "success",
                  title: "Delete Success",
              });
          }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
              });
        }
    }
    
    
    const editDetails = (record) => {
        toggle();
        setId(record.id);
        setCode(record.assignGroupCode);
        setName(record.assignGroupName);
        setSite(record.plantid);
        setRecordStatus(record.recordstatus);
    }

    const onSubmit = (e) => {
      e.preventDefault();
      const model = {
        "id": id == "" ? 0 : id,
        "assignGroupCode": code,
        "assignGroupName": name,
        "plantid": plantid,
        "user": userState.id,
        "recordstatus": recordstatus
      }
      saveData(model);
    }
  
    const saveData = async (data) => {
      try {
          const result = await Api.create(data);
          if (result.status === 200) {
              const { data } = result.data;
              toggle();
              refreshData();
              
              Swal.fire({
                  icon: "success",
                  title: "Save Success",
              });
          }
      } catch (error) {
        Swal.fire({
            icon: "error",
            title: error.response.data,
          });
      }
    }

    const handleChange = (e,type) => {
        switch (type) {
            case "site":
                setSite(e.target.value);
                break;
            default:
                setRecordStatus(e.target.value);
                break;
        }
       
    }

    return (
        <>
            <CRow>
                <CCol xs="12" lg="12" >
                    <CButton variant="outline" onClick={newDetails} color={'success'}><CIcon name="cil-plus" /><span className="ml-2">สร้าง Plant Assign</span></CButton>
                </CCol>
            </CRow>
            <CRow className="mt-3">
                <CCol xs="12" lg="12" >
                    <CCard>
                        <CCardBody>
                            <CDataTable
                                items={data}
                                fields={fields}
                                tableFilter={{ label: "ค้นหา", placeholder: 'พิมพ์คำที่ต้องการค้นหา' }}
                                cleaner
                                itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
                                itemsPerPage={10}
                                hover
                                sorter
                                striped
                                bordered
                                pagination
                                scopedSlots={{
                                    'assignGroupCodeLink':
                                        (item) => (
                                            <td>
                                                <a href={`/home/master/buAssignUser?id=${item.id}`}>{item.assignGroupCode}</a>
                                            </td>
                                    ),
                                    'status':
                                        (item) => (
                                            <td>
                                                <CBadge color={getBadge(item.recordstatus)}>
                                                    {getStatus(item.recordstatus)}
                                                </CBadge>
                                            </td>
                                        ),
                                    'option':
                                        (item) => (
                                            <td className="center">
                                                <CButtonGroup>
                                                    <CButton
                                                        color="success"
                                                        variant="outline"
                                                        shape="square"
                                                        size="sm"
                                                        onClick={() => { editDetails(item) }}
                                                    >
                                                        <CIcon name="cilPen" />
                                                    </CButton>
                                                    {/* <CButton variant="ghost" size="sm" />
                                                    <CButton
                                                        color="danger"
                                                        variant="outline"
                                                        shape="square"
                                                        size="sm"
                                                        onClick={() => { deleteDetails(item) }}
                                                    >
                                                        <CIcon name="cilTrash" />
                                                    </CButton> */}
                                                </CButtonGroup>
                                            </td>
                                        ),

                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>

            </CRow>

            <CModal show={modalConfirm} onClose={setModalConfirm} color="danger" >
                <CModalHeader closeButton>
                    <CModalTitle> Delete Data : {code}</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    Are you confirm to delete {code} ?
                </CModalBody>
                <CModalFooter>
                    <CButton variant="outline" onClick={onDelete} color="primary">Confirm</CButton>{' '}
                    <CButton variant="outline" color="secondary" onClick={() => setModalConfirm(false)}>Close</CButton>
                </CModalFooter>
            </CModal>
            <CModal
                show={modal}
                onClose={toggle}
            >
                <CModalHeader closeButton>
                    <CModalTitle>รายละเอียด Plant Assign</CModalTitle>
                </CModalHeader>
                <CForm onSubmit={onSubmit} action="" >
                    <CModalBody>
                        <CInput type="hidden" value={id} id="id"/>
                        <CRow>
                            <CCol xs="12" sm="12">
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="code">Assign Group Code</CLabel>
                                            <CInput onChange={e => setCode(e.target.value)} value={code} readOnly={id != ""} id="code" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="name">Assign Group Name</CLabel>
                                            <CInput onChange={e => setName(e.target.value)} value={name} id="name" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                            </CCol>
                        </CRow >
                        <CRow>
                            <CCol xs="12">
                                <CFormGroup>
                                    <CLabel htmlFor="plantid">Plant</CLabel>
                                    <CSelect custom name="plantid" id="plantid" value={plantid}  onChange={(e) => handleChange(e,'site')} required>
                                        <option value={""}>-- Plant --</option>
                                        {
                                            siteData.map((item, index) => {
                                                return <option key={item.id} value={item.id}>{item.plant} : {item.siteDescription}</option>
                                            })
                                        }
                                    </CSelect>
                                </CFormGroup>
                            </CCol>
                        </CRow>
                        <CRow>
                            <CCol xs="12">
                                <CFormGroup>
                                    <CLabel htmlFor="recordstatus">สถานะการใช้งาน</CLabel>
                                    <CSelect custom name="recordstatus" id="recordstatus" value={recordstatus}  onChange={(e) => handleChange(e,'status')} required>
                                        <option value={""}>-- สถานะการใช้งาน --</option>
                                        {
                                            statusData.map((item, index) => {
                                                return <option key={item.code} value={item.code}>{item.name}</option>
                                            })
                                        }
                                    </CSelect>
                                </CFormGroup>
                            </CCol>
                        </CRow>
                    </CModalBody>
                    <CModalFooter>
                        <CButton variant="outline" type="submit" color="primary">ตกลง</CButton>
                        <CButton
                            variant="outline"
                            color="secondary"
                            onClick={toggle}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CForm >
            </CModal >

        </>
    )
}

export default Tables
