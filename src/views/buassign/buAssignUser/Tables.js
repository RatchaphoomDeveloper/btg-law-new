import React, { useState, useEffect } from 'react'
import {
    CCardBody,
    CBadge,
    CButton,
    CCollapse,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CCol,
    CCard,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CSelect,
    CForm,
    CModalTitle,
    CButtonGroup
} from '@coreui/react'
import Api from '../../../api/ApiBuAssignUser';
import ApiUser from '../../../api/ApiUser';
import ApiDivision from '../../../api/ApiDeivsion';
import CIcon from '@coreui/icons-react'
import { statusData } from './data';
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2/src/sweetalert2.js";
import Select from "react-select";

const getBadge = status => {
    switch (status) {
        case 'A': return 'success'
        case 'I': return 'secondary'
        default: return 'primary'
    }
}

const getStatus = status => {
    switch (status) {
        case 'A': return 'Active'
        case 'I': return 'Inactive'
        default: return ''
    }
}

const fields = [
    // { key: 'order', label: 'ลำดับ', _style: { width: '1%' } },
    { key: 'option', label: '', _style: { width: '1%' }, filter: false, },
    { key: 'assigngroupCode', label: 'Assign Group Code' },
    { key: 'divisionDesc', label: 'Department' },
    { key: 'fullname', label: 'User Full Name' },
    { key: 'status', label: 'สถานะการใช้งาน', _style: { width: '10%' } },
    { key: 'updateBy', label: 'ผู้แก้ไขข้อมูล', _style: { width: '15%' } },
    { key: 'updateDate', label: 'วันที่แก้ไขข้อมูล', _style: { width: '15%' } },
]



const Tables = ({ data = [], refreshData = () => { }, assignId }) => {
  const userState = useSelector((state) => state.changeState.user);
  const [userData, setUserData] = useState([]);
  const [divisionData, setDivisionData] = useState([]);

    const [modal, setModal] = useState(false);
    const [id, setId] = useState('');
    const [userid, setUser] = useState('');
    const [divisionid, setDivisionId] = useState('');
    const [recordstatus, setRecordStatus] = useState('');
    const [dataSelected, setDataSelected] = useState('');
    const [modalConfirm, setModalConfirm] = useState(false);
    const [plantid, setSite] = useState('');
    const [userList, setUserList] = useState([]);
    const [userListDefault, setUserListDefault] = useState("");

    useEffect(() => {
        getUserOption();
        getDivisionOption();
          return () => {
  
          }
    }, []);

    const getUserOption = async () => {
        const dataPush = [];
        try{
            const result = await ApiUser.getFromBuAssign(assignId);
            if (result.status == 200) {
                result.data.forEach((res) => {
                  dataPush.push({ value: res.id, label: res.fullname });
                  // if (res.id === parseInt(code)) {
                  //   dataPush.push({ value: res.code, label: res.code });
                  // }
                });
                setUserData(dataPush);
            }
        }catch(error){
            console.log(error);
        }
    }

    const getDivisionOption = async () => {
        try{
            const result = await ApiDivision.get();
            if (result.status == 200) {
                setDivisionData(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }

    const toggle = () => {
        setModal(!modal);
    }

    const toggleConfirm = () => {
      setModalConfirm(!modalConfirm);
    }

    const newDetails = (record) => {
        toggle();
        setId('');
        setUserList([]);
        setDivisionId(0);
        setRecordStatus("A");
    }

    const deleteDetails = (record) => {
        toggleConfirm();
        setDataSelected(record);
        setId(record.id);
    }

    const onDelete = async () => {
      try {
          const result = await Api.delete(dataSelected);
          if (result.status === 200) {
              const { data } = result.data;
              toggleConfirm();
              refreshData();
              
              Swal.fire({
                  icon: "success",
                  title: "Delete Success",
              });
          }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
              });
        }
    }
    
    
    const editDetails = (record) => {
        toggle();
        setId(record.id);
        var user = userData.find((x) => x.value == record.userid);
    // const user = users.find(
    //   (x) => x.username === model.username && x.password === model.password
    // );
        setUserList(user);

        setDivisionId(record.divisionid);
        setRecordStatus(record.recordstatus);
    }

    const onSubmit = (e) => {
        e.preventDefault();
        console.log("userList", userList);
        var userIdList;
        if(id == ""){
            userIdList = userList.map((x) => x.value).join(",");
        }else{
            userIdList = userList.value
        }
        const model = {
            "id": id == "" ? 0 : id,
            "assignGroupId": assignId,
            "userIdList": userIdList,
            "divisionid": divisionid,
            "user": userState.id,
            "recordstatus": recordstatus
        }
        saveData(model);
    }
  
    const saveData = async (data) => {
      try {
          const result = await Api.create(data);
          if (result.status === 200) {
              const { data } = result.data;
              toggle();
              refreshData();
              
              Swal.fire({
                  icon: "success",
                  title: "Save Success",
              });
          }
      } catch (error) {
        Swal.fire({
            icon: "error",
            title: error.response.data,
          });
      }
    }

    const handleChange = (e,type) => {
        switch (type) {
            case "user":
                setUser(e.target.value);
                break;
            case "division":
                setDivisionId(e.target.value);
                break;
            default:
                setRecordStatus(e.target.value);
                break;
        }
       
    }

    return (
        <>
            <CRow>
                <CCol xs="12" lg="12" >
                    <CButton variant="outline" onClick={newDetails} color={'success'}><CIcon name="cil-plus" /><span className="ml-2">รายละเอียด User Assignment</span></CButton>
                </CCol>
            </CRow>
            <CRow className="mt-3">
                <CCol xs="12" lg="12" >
                    <CCard>
                        <CCardBody>
                            <CDataTable
                                items={data}
                                fields={fields}
                                tableFilter={{ label: "ค้นหา", placeholder: 'พิมพ์คำที่ต้องการค้นหา' }}
                                cleaner
                                itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
                                itemsPerPage={10}
                                hover
                                sorter
                                striped
                                bordered
                                pagination
                                scopedSlots={{
                                    'status':
                                        (item) => (
                                            <td>
                                                <CBadge color={getBadge(item.recordstatus)}>
                                                    {getStatus(item.recordstatus)}
                                                </CBadge>
                                            </td>
                                        ),
                                    'option':
                                        (item) => (
                                            <td className="center">
                                                <CButtonGroup>
                                                    <CButton
                                                        color="success"
                                                        variant="outline"
                                                        shape="square"
                                                        size="sm"
                                                        onClick={() => { editDetails(item) }}
                                                    >
                                                        <CIcon name="cilPen" />
                                                    </CButton>
                                                    {/* <CButton variant="ghost" size="sm" />
                                                    <CButton
                                                        color="danger"
                                                        variant="outline"
                                                        shape="square"
                                                        size="sm"
                                                        onClick={() => { deleteDetails(item) }}
                                                    >
                                                        <CIcon name="cilTrash" />
                                                    </CButton> */}
                                                </CButtonGroup>
                                            </td>
                                        ),

                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>

            </CRow>
            <CRow>
                <CCol xs="12" lg="12" >
                    <CButton variant="outline" onClick={() => window.location.href = "/home/master/buAssign"} color={'info'}><span>กลับ</span></CButton>
                </CCol>
            </CRow>

            <CModal show={modalConfirm} onClose={setModalConfirm} color="danger" >
                <CModalHeader closeButton>
                    <CModalTitle> Delete Data : </CModalTitle>
                </CModalHeader>
                <CModalBody>
                    Are you confirm to delete ?
                </CModalBody>
                <CModalFooter>
                    <CButton variant="outline" onClick={onDelete} color="primary">Confirm</CButton>{' '}
                    <CButton variant="outline" color="secondary" onClick={() => setModalConfirm(false)}>Close</CButton>
                </CModalFooter>
            </CModal>
            <CModal
                show={modal}
                onClose={toggle}
            >
                <CModalHeader closeButton>
                    <CModalTitle>รายละเอียด User Assignment</CModalTitle>
                </CModalHeader>
                <CForm onSubmit={onSubmit} action="" >
                    <CModalBody>
                        <CInput type="hidden" value={id} id="id"/>
                        <CRow>
                            <CCol xs="12">
                                <CFormGroup>
                                    <CLabel htmlFor="divisionid">แผนก</CLabel>
                                    <CSelect custom name="divisionid" id="divisionid" value={divisionid}  onChange={(e) => handleChange(e,'division')} required>
                                        <option value={""}>-- แผนก --</option>
                                        {
                                            divisionData.map((item, index) => {
                                                return <option key={item.id} value={item.id}>{item.description}</option>
                                            })
                                        }
                                    </CSelect>
                                </CFormGroup>
                            </CCol>
                        </CRow>
                        <CRow>
                            <CCol xs="12">
                                <CFormGroup>
                                    <CLabel htmlFor="userid">ผู้ใช้งาน</CLabel>
                                    {/* <CSelect custom name="userid" id="userid" value={userid}  onChange={(e) => handleChange(e,'user')} required>
                                        <option value={""}>-- ผู้ใช้งาน --</option>
                                        {
                                            userData.map((item, index) => {
                                                return <option key={item.id} value={item.id}>{item.fullname}</option>
                                            })
                                        }
                                    </CSelect> */}
                                    <Select
                                        name="userid"
                                        id="userid"
                                        value={userList}
                                        options={userData}
                                        onChange={(e) => {
                                            setUserList(e);
                                        }}
                                        isMulti={!id}
                                    />
                                </CFormGroup>
                            </CCol>
                        </CRow>
                        <CRow>
                            <CCol xs="12">
                                <CFormGroup>
                                    <CLabel htmlFor="recordstatus">สถานะการใช้งาน</CLabel>
                                    <CSelect custom name="recordstatus" id="recordstatus" value={recordstatus}  onChange={(e) => handleChange(e,'status')} required>
                                        <option value={""}>-- สถานะการใช้งาน --</option>
                                        {
                                            statusData.map((item, index) => {
                                                return <option key={item.code} value={item.code}>{item.name}</option>
                                            })
                                        }
                                    </CSelect>
                                </CFormGroup>
                            </CCol>
                        </CRow>
                    </CModalBody>
                    <CModalFooter>
                        <CButton variant="outline" type="submit" color="primary">ตกลง</CButton>
                        <CButton
                            variant="outline"
                            color="secondary"
                            onClick={toggle}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CForm >
            </CModal >

        </>
    )
}

export default Tables
