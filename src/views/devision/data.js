export const devisionData = [
    { id: 0, code: 'HR.', name: 'หน่วยงาน บุคคล',  statuscode: 'I',status: 'Inactive', editby: "Admin", editdate: "12/05/2020" },
    { id: 1, code: 'QA.', name: 'หน่วยงาน ตรวจสอบคุณภาพ', statuscode: 'A', status: 'Active', editby: "Admin", editdate: "12/05/2020" }
]

export const statusData = [
    { id: 0, name: 'Active', code: "A" },
    { id: 1, name: 'In Active', code: "I" },
]

