import React, { useEffect, useState } from 'react'
import {
    CBadge,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CModal,
    CModalHeader,
    CModalFooter,
    CButton
} from '@coreui/react'
import ApiBU from '../../api/ApiBU';
import { buData } from './data';
import Tables from './Tables';

const BU = () => {
  const [dataTable, setDataTable] = useState([]);
  useEffect(() => {
        getBU();
        return () => {

        }
    }, []);

    const getBU = async () => {
        try{
            const result = await ApiBU.get();
            if (result.status == 200) {
                setDataTable(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }
    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardHeader>
                            <b> กลุ่มธุรกิจ (Business Unit) </b>
                        </CCardHeader>
                        <CCardBody>
                            <Tables refreshData={getBU} data={dataTable} />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    )
}

export default BU;