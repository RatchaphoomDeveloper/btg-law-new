export const buData = [
    { id: 0, code: 'B01', name: 'Agro', statuscode: 'I', status: 'Inactive', editby: "Admin", editdate: "12/05/2020" },
    { id: 1, code: 'B02', name: 'BFood', statuscode: 'A', status: 'Active', editby: "Admin", editdate: "12/05/2020" }
]

export const statusData = [
    { id: 0, name: 'Active', code: "A" },
    { id: 1, name: 'In Active', code: "I" },
]

