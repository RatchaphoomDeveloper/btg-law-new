import React, { useState, useEffect } from 'react'
import {
    CCardBody,
    CBadge,
    CButton,
    CCollapse,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CCol,
    CCard,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CSelect,
    CForm,
    CModalTitle,
    CButtonGroup
} from '@coreui/react'
import Api from '../../../api/ApiRegularLaw';
import CIcon from '@coreui/icons-react'
import { statusData } from './data';
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2/src/sweetalert2.js";

const getBadge = status => {
    switch (status) {
        case 'A': return 'success'
        case 'I': return 'secondary'
        default: return 'primary'
    }
}

const getStatus = status => {
    switch (status) {
        case 'A': return 'Active'
        case 'I': return 'Inactive'
        default: return ''
    }
}

const fields = [
    { key: 'option', label: '', _style: { width: '1%' }, filter: false, },
    { key: 'lawtypeName', label: 'ประเภทกฎหมาย' },
    { key: 'status', label: 'สถานะการใช้งาน', _style: { width: '10%' } },
    { key: 'updateBy', label: 'ผู้แก้ไขข้อมูล', _style: { width: '15%' } },
    { key: 'updateDate', label: 'วันที่แก้ไขข้อมูล', _style: { width: '15%' } },
]


const Tables = ({ data = [], refreshData = () => { }, regulardeptId }) => {    
  const userState = useSelector((state) => state.changeState.user);
  const [typeData, setTypeData] = useState([]);

    const [modal, setModal] = useState(false);
    const [id, setId] = useState('');
    const [lawtypeId, setLawtypeId] = useState('');
    const [recordstatus, setRecordStatus] = useState('');
    const [dataSelected, setDataSelected] = useState('');
    const [modalConfirm, setModalConfirm] = useState(false);

    useEffect(() => {
        getTypeOption();
          return () => {
  
          }
    }, []);

    const getTypeOption = async () => {
        try{
            const result = await Api.getTypeOption();
            if (result.status == 200) {
                setTypeData(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }

    const toggle = () => {
        setModal(!modal);
    }

    const toggleConfirm = () => {
      setModalConfirm(!modalConfirm);
    }

    const newDetails = (record) => {
        toggle();
        setId('');
        setLawtypeId(0);
        setRecordStatus("A");
    }

    const deleteDetails = (record) => {
        toggleConfirm();
        setDataSelected(record);
        setId(record.id);
    }

    const onDelete = async () => {
      try {
          const result = await Api.deleteType(dataSelected);
          if (result.status === 200) {
              const { data } = result.data;
              toggleConfirm();
              refreshData();
              
              Swal.fire({
                  icon: "success",
                  title: "Delete Success",
              });
          }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
              });
        }
    }
    
    
    const editDetails = (record) => {
        toggle();
        setId(record.id);
        setLawtypeId(record.lawtypeId);
        setRecordStatus(record.recordstatus);
    }

    const onSubmit = (e) => {
      e.preventDefault();
      const model = {
        "id": id == "" ? 0 : id,
        "regulardeptId": regulardeptId,
        "lawtypeId": lawtypeId,
        "user": userState.id,
        "recordstatus": recordstatus
      }
      saveData(model);
    }
  
    const saveData = async (data) => {
      try {
          const result = await Api.createType(data);
          if (result.status === 200) {
              const { data } = result.data;
              toggle();
              refreshData();
              
              Swal.fire({
                  icon: "success",
                  title: "Save Success",
              });
          }
      } catch (error) {
        Swal.fire({
            icon: "error",
            title: error.response.data,
          });
      }
    }

    const handleChange = (e,type) => {
        switch (type) {
            case "type":
                setLawtypeId(e.target.value);
                break;
            default:
                setRecordStatus(e.target.value);
                break;
        }
       
    }

    return (
        <>
            <CRow>
                <CCol xs="12" lg="12" >
                    <CButton variant="outline" onclick={newDetails} color={'success'}><CIcon name="cil-plus" /><span className="ml-2">สร้างประเภทกฎหมาย</span></CButton>
                </CCol>
            </CRow>
            <CRow className="mt-3">
                <CCol xs="12" lg="12" >
                    <CCard>
                        <CCardBody>
                            <CDataTable
                                items={data}
                                fields={fields}
                                tableFilter={{ label: "ค้นหา", placeholder: 'พิมพ์คำที่ต้องการค้นหา' }}
                                cleaner
                                itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
                                itemsPerPage={10}
                                hover
                                sorter
                                striped
                                bordered
                                pagination
                                scopedSlots={{
                                        'lawtypeName':
                                            (item) => (
                                                <td>
                                                    <a href={`/home/master/regularlaw?id=${item.id}`}>{item.lawtypeName}</a>
                                                </td>
                                        ),
                                        'status':
                                            (item) => (
                                                <td>
                                                    <CBadge color={getBadge(item.recordstatus)}>
                                                        {getStatus(item.recordstatus)}
                                                    </CBadge>
                                                </td>
                                            ),
                                        'option':
                                            (item) => (
                                                <td className="center">
                                                    <CButtonGroup>
                                                        <CButton
                                                            color="success"
                                                            variant="outline"
                                                            shape="square"
                                                            size="sm"
                                                            onClick={() => { editDetails(item) }}
                                                        >
                                                            <CIcon name="cilPen" />
                                                        </CButton>
                                                        {/* <CButton variant="ghost" size="sm" />
                                                        <CButton
                                                            color="danger"
                                                            variant="outline"
                                                            shape="square"
                                                            size="sm"
                                                            onClick={() => { deleteDetails(item) }}
                                                        >
                                                            <CIcon name="cilTrash" />
                                                        </CButton> */}
                                                    </CButtonGroup>
                                                </td>
                                            ),

                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>

            </CRow>

            <CModal show={modalConfirm} onClose={setModalConfirm} color="danger" >
                <CModalHeader closeButton>
                    <CModalTitle> Delete Data</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    Are you confirm to delete ?
                </CModalBody>
                <CModalFooter>
                    <CButton variant="outline" onclick={onDelete} color="primary">Confirm</CButton>{' '}
                    <CButton variant="outline" color="secondary" onClick={() => setModalConfirm(false)}>Close</CButton>
                </CModalFooter>
            </CModal>
            <CModal
                show={modal}
                onClose={toggle}
            >
                <CModalHeader closeButton>
                    <CModalTitle>รายละเอียดประเภทกฎหมาย</CModalTitle>
                </CModalHeader>
                <CForm onSubmit={onSubmit} action="" >
                    <CModalBody>
                        <CInput type="hidden" value={id} id="id"/>
                        <CRow>
                            <CCol xs="12">
                                <CFormGroup>
                                    <CLabel htmlFor="lawtypeId">ประเภทกฎหมาย</CLabel>
                                    <CSelect custom name="lawtypeId" id="lawtypeId" value={lawtypeId} onChange={(e) => handleChange(e,'type')} required>
                                        <option value={""}>-- ประเภทกฎหมาย --</option>
                                        {
                                            typeData.map((item, index) => {
                                                return <option key={item.id} value={item.id}>{item.name}</option>
                                            })
                                        }
                                    </CSelect>
                                </CFormGroup>
                            </CCol>
                        </CRow>
                        <CRow>
                            <CCol xs="12">
                                <CFormGroup>
                                    <CLabel htmlFor="recordstatus">สถานะการใช้งาน</CLabel>
                                    <CSelect custom name="recordstatus" id="recordstatus" value={recordstatus} onChange={handleChange} required>
                                        <option value={""}>-- สถานะการใช้งาน --</option>
                                        {
                                            statusData.map((item, index) => {
                                                return <option key={item.code} value={item.code}>{item.name}</option>
                                            })
                                        }
                                    </CSelect>
                                </CFormGroup>
                            </CCol>
                        </CRow>
                    </CModalBody>
                    <CModalFooter>
                        <CButton variant="outline" type="submit" color="primary">ตกลง</CButton>
                        <CButton
                            variant="outline"
                            color="secondary"
                            onClick={toggle}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CForm >
            </CModal >

        </>
    )
}

export default Tables
