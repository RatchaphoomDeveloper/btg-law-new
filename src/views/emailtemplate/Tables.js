import React, { useState, useEffect } from 'react'
import {
    CCardBody,
    CBadge,
    CButton,
    CCollapse,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CCol,
    CCard,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CSelect,
    CForm,
    CModalTitle,
    CButtonGroup
} from '@coreui/react'
import ReactQuill from 'react-quill'
import 'quill/dist/quill.snow.css'
import Api from '../../api/ApiEmailTemplate';
import CIcon from '@coreui/icons-react'
import { statusData } from './data';
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2/src/sweetalert2.js";

const getBadge = status => {
    switch (status) {
        case 'Active': return 'success'
        case 'Inactive': return 'secondary'
        case 'Pending': return 'warning'
        case 'Banned': return 'danger'
        default: return 'primary'
    }
}

const fields = [
    // { key: 'order', label: 'ลำดับ', _style: { width: '1%' } },
    { key: 'option', label: '', _style: { width: '1%' }, filter: false, },
    { key: 'emailTemplateName', label: 'ชื่อ' },
    { key: 'emailTemplateSubject', label: 'เรื่อง' },
    // { key: 'emailTemplateBody', label: 'ข้อความ' },
    // { key: 'recordstatus', label: 'สถานะการใช้งาน', _style: { width: '10%' } },
    { key: 'updateuser', label: 'ผู้แก้ไขข้อมูล', _style: { width: '15%' } },
    { key: 'updatedate', label: 'วันที่แก้ไขข้อมูล', _style: { width: '15%' } },
]


const Tables = ({ data = [], refreshData = () => { } }) => {
    const userState = useSelector((state) => state.changeState.user);

    const modules = {
      toolbar: [
        ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
        // ['blockquote', 'code-block'],
        // [{ 'header': 1 }, { 'header': 2 }],               // custom button values
        [{ 'list': 'ordered'}, { 'list': 'bullet' }],
        // [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
        // [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
        // [{ 'direction': 'rtl' }],                         // text direction
        // [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
        // [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
        [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
        [{ 'font': [] }],
        [{ 'align': [] }],
        // ['clean']                                         // remove formatting button
      ]
    }

    const [modal, setModal] = useState(false);
    const [id, setId] = useState('');
    const [name, setName] = useState('');
    const [subject, setSubject] = useState('');
    const [body, setBody] = React.useState('');
    const [to, setTo] = useState('');
    const [cc, setCC] = useState('');

    const toggle = () => {
        setModal(!modal);
    }

    // const newDetails = (record) => {
    //     toggle();
    //     setId('');
    //     setName('');
    //     setSubject('');
    //     setBody('');
    //     setTo('');
    //     setCC('');
    // }    
    
    const editDetails = (record) => {
        toggle();
        setId(record.emailTemplateId);
        setName(record.emailTemplateName);
        setSubject(record.emailTemplateSubject);
        setBody(record.emailTemplateBody);
        setTo(record.emailTemplateTo);
        setCC(record.emailTemplateCc);

    }

    const onSubmit = (e) => {
      e.preventDefault();
      const model = {
        "emailTemplateId": id == "" ? 0 : id,
        "emailTemplateName": name,
        "emailTemplateSubject": subject,
        "emailTemplateBody": body,
        "emailTemplateTo": to,
        "emailTemplateCc": cc,
        "user": userState.id
      }
      saveData(model);
    }
  
    const saveData = async (data) => {
      try {
          const result = await Api.create(data);
          if (result.status === 200) {
              const { data } = result.data;
              toggle();
              refreshData();
              
              Swal.fire({
                  icon: "success",
                  title: "Save Success",
              });
          }
      } catch (error) {
  
      }
    }

    return (
        <>
            {/* <CRow>
                <CCol xs="12" lg="12" >
                    <CButton variant="outline" onclick={newDetails} color={'success'}><CIcon name="cil-plus" /><span className="ml-2">สร้าง Email Template</span></CButton>
                </CCol>
            </CRow> */}
            <CRow className="mt-3">
                <CCol xs="12" lg="12" >
                    <CCard>
                        <CCardBody>
                            <CDataTable
                                items={data}
                                fields={fields}
                                tableFilter={{ label: "ค้นหา", placeholder: 'พิมพ์คำที่ต้องการค้นหา' }}
                                cleaner
                                itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
                                itemsPerPage={10}
                                hover
                                sorter
                                striped
                                bordered
                                pagination
                                scopedSlots={{
                                        'option':
                                            (item) => (
                                                <td className="center">
                                                    <CButtonGroup>
                                                        <CButton
                                                            color="success"
                                                            variant="outline"
                                                            shape="square"
                                                            size="sm"
                                                            onClick={() => { editDetails(item) }}
                                                        >
                                                            <CIcon name="cilPen" />
                                                        </CButton>
                                                    </CButtonGroup>
                                                </td>
                                            ),

                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>

            </CRow>
            <CModal
                show={modal}
                onClose={toggle}
                size="lg"
            >
                <CModalHeader closeButton>
                    <CModalTitle>รายละเอียด Email Template</CModalTitle>
                </CModalHeader>
                <CForm onSubmit={onSubmit} action="" >
                    <CModalBody>
                        <CInput type="hidden" value={id} id="id"/>
                        <CRow>
                            <CCol xs="12" sm="12">
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="to">To</CLabel>
                                            <CInput onChange={e => setTo(e.target.value)} value={to} id="to" placeholder="To" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="cc">CC</CLabel>
                                            <CInput onChange={e => setCC(e.target.value)} value={cc} id="cc" placeholder="CC" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="name">Name</CLabel>
                                            <CInput onChange={e => setName(e.target.value)} value={name} id="name" placeholder="Name" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="subject">Subject</CLabel>
                                            <CInput onChange={e => setSubject(e.target.value)} value={subject} id="subject" placeholder="Subject" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="body">Body</CLabel>
                                            <ReactQuill onChange={setBody} value={body} modules={modules} id="body" placeholder="Body" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                            </CCol>
                        </CRow >
                    </CModalBody>
                    <CModalFooter>
                        <CButton variant="outline" type="submit" color="primary">ตกลง</CButton>
                        <CButton
                            variant="outline"
                            color="secondary"
                            onClick={toggle}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CForm >
            </CModal >

        </>
    )
}

export default Tables
