import React, { useEffect, useState } from 'react'
import {
    CBadge,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CModal,
    CModalHeader,
    CModalFooter,
    CButton
} from '@coreui/react'
import Api from '../../api/ApiLawAgencieSub';
// import { legalGroupData } from './data';
import Tables from './Tables';

const LawAgenCieSub = (props) => {
    const id = new URLSearchParams(props.location.search).get("id");
    const [dataTable, setDataTable] = useState([]);
    useEffect(() => {
        getData();
          return () => {
  
          }
      }, []);
  
      const getData = async () => {
          try{
              const result = await Api.get(id);
              if (result.status == 200) {
                  setDataTable(result.data);
              }
          }catch(error){
              console.log(error);
          }
      }
    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardHeader>
                            <b> หน่วยงานย่อยผู้ออกกฏหมาย ({dataTable.length}) </b>
                        </CCardHeader>
                        <CCardBody>
                            <Tables refreshData={getData} data={dataTable} groupId={id} />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    )
}

export default LawAgenCieSub;