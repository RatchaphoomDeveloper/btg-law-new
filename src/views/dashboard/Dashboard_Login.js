import React, { useEffect, useState } from "react";
import {
  CBadge,
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CCallout,
  CDataTable,
  CModal,
  CModalBody,
  CModalFooter,
  CInput,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import ApiDashboard from "../../api/ApiDashboard";
import ApiLawInfo from "../../api/ApiLawInfo";
import { useDispatch, useSelector } from "react-redux";
import { WEB_API, WEB_FILE_URL } from "../../env";
import { Doughnut, Bar, Pie } from "react-chartjs-2";
import Chart from "react-apexcharts";
import "./index.scss";

const getStatus = (status) => {
  switch (status) {
    case "SADR":
      return "Draft";
    case "NEDO":
      return "Submit";
    case "SEEV":
      return "Send To Evaluate";
    case "REFQ":
      return "Reject To FQO";
    case "SEA1":
      return "Send To Verification";
    case "SEA2":
      return "Send To Approver";
    case "REDI":
      return "Reject To User Division";
    case "DOAP":
      return "Document Approve";
    case "INPR":
      return "In Process";
    case "CLNE":
      return "Close New";
    case "CLOS":
      return "Complete Send To Verification";
    case "CLS2":
      return "Complete Send To Approver";
    case "RJCL":
      return "Reject Complete To User Division";
    case "REEV":
      return "Re Evaluate";
    default:
      return "";
  }
};

const fields = [
  { key: "order", label: "ลำดับ", _style: { width: "1%" } },
  { key: "mainLawName", label: "ชื่อรายการกฎหมาย", _style: { width: "40%" } },
  { key: "lawagency", label: "หน่วยงานที่ออกกฎหมาย" },
  { key: "brcsDate", label: "วันที่" },
  // { key: 'systemStatus', label: 'สถานะ'},
];

const fieldFiles = [
  // { key: "id", _style: { width: "40%" } },
  { key: "filename" },
];

const fieldsTask = [
  { key: "task", label: "Task" },
  { key: "jan", label: "Jan" },
  { key: "feb", label: "Feb" },
  { key: "mar", label: "Mar" },
  { key: "apr", label: "Apr" },
  { key: "may", label: "May" },
  { key: "jun", label: "Jun" },
  { key: "jul", label: "Jul" },
  { key: "aug", label: "Aug" },
  { key: "sep", label: "Sep" },
  { key: "oct", label: "Oct" },
  { key: "nov", label: "Nov" },
  { key: "dec", label: "Dec" },
];

const CHART_COLORS = {
  red: 'rgb(255, 99, 132)',
  orange: 'rgb(255, 159, 64)',
  yellow: 'rgb(255, 205, 86)',
  green: 'rgb(75, 192, 192)',
  blue: 'rgb(54, 162, 235)',
  purple: 'rgb(153, 102, 255)',
  grey: 'rgb(201, 203, 207)'
};

// const dataTaskTable =[
//   {task: "ประเมินความสอดคล้อง", jan: 4, feb: 4, mar: 4, apr: 4, may: 4, jun: 4, jul: 4, aug: 4, sep: 4, oct: 4, nov: 4, dec: 4},
//   {task: "Action Plan on progress", jan: 4, feb: 4, mar: 4, apr: 4, may: 4, jun: 4, jul: 4, aug: 4, sep: 4, oct: 4, nov: 4, dec: 4},
//   {task: "Verify", jan: 4, feb: 4, mar: 4, apr: 4, may: 4, jun: 4, jul: 4, aug: 4, sep: 4, oct: 4, nov: 4, dec: 4},
//   {task: "Approve", jan: 4, feb: 4, mar: 4, apr: 4, may: 4, jun: 4, jul: 4, aug: 4, sep: 4, oct: 4, nov: 4, dec: 4},
// ];

const DashboardLogin = () => {
  const [dataTableRaw, setDataTableRaw] = useState([]);
  const [dataTable, setDataTable] = useState([]);
  const [dataTaskTable, setDataTaskTable] = useState([]);
  const userState = useSelector((state) => state.changeState.user);
  const [allLaws, setAllLaws] = useState("");
  const [monthLaws, setMonthLaws] = useState("");
  const [users, setUsers] = useState("");
  const [search, setSearch] = useState("");
  const [modalFile, setModalFile] = useState(false);
  const [dataFileTable, setDataFileTable] = useState([]);
  const [datasetDonuts, setdatasetDonuts] = useState([]);
  const [datasetDonuts2, setdatasetDonuts2] = useState([]);
  const [datasetCharts, setdatasetCharts] = useState({
    labels_list: [],
    count_list: [],
    background: [],
  });
  useEffect(() => {
    getData();
    getDashboardData();
    getDataGraphMainlawByBu();
    getDataGraphMainlawByMonth();
    getDashboardGraphMainlawByLawGroup();
    return () => {};
  }, []);

  useEffect(() => {
    if (userState.id) {
      getTask(userState.id);
    }
    return () => {};
  }, [userState.id]);

  const getData = async () => {
    try {
      const result = await ApiDashboard.get();
      if (result.status == 200) {
        const dataResult = result.data;
        // console.log("result.data", result.data);
        // console.log("dataResult", dataResult);
        for (let i = 0; i < dataResult.length; i++) {
          dataResult[i].order = i + 1;
        }
        setDataTableRaw(dataResult);
        setDataTable(dataResult);
        generateDetailHistory();
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getDashboardData = async () => {
    try {
      const result = await ApiDashboard.getData();
      if (result.status == 200) {
        var data = result.data;
        console.log("data", data);
        setAllLaws(data.allLaws);
        setMonthLaws(data.monthLaws);
        setUsers(data.users);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getTask = async (id) => {
    try {
      const result = await ApiDashboard.getTask(id);
      if (result.status == 200) {
        setDataTaskTable(result.data);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getFileList = (data) => {
    console.log("dataMain", data);
    getFileData(data.id);
    toggleFile();
  };

  const getFileData = async (id) => {
    try {
      const result = await ApiLawInfo.getTranBrcsFileSel(id);
      if (result.status == 200) {
        setDataFileTable(result.data.response);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const toggleFile = () => {
    setModalFile(!modalFile);
  };

  const getDataGraphMainlawByBu = async () => {
    try {
      const result = await ApiDashboard.getDataGraphMainlawByBu();
      if (result.status == 200) {
        console.log("bybu::", result.data);
        setdatasetDonuts(result.data);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getDataGraphMainlawByMonth = async () => {
    try {
      const result = await ApiDashboard.getDataGraphMainlawByMonth();
      if (result.status == 200) {
        console.log("bymonth::", result.data);
        setdatasetCharts({
          labels_list: result.data.map((x) => x.month),
          count_list: result.data.map((x) => x.cnt),
          // background: result.data.map((x) => {
          //   var letters = "BCDEF".split("");
          //   var color = "#";
          //   for (var i = 0; i < 6; i++) {
          //     color += letters[Math.floor(Math.random() * letters.length)];
          //   }
          //   return color;
          // }),
          // background: {
          //   red: 'rgb(255, 99, 132)',
          //   orange: 'rgb(255, 159, 64)',
          //   yellow: 'rgb(255, 205, 86)',
          //   green: 'rgb(75, 192, 192)',
          //   blue: 'rgb(54, 162, 235)',
          //   purple: 'rgb(153, 102, 255)',
          //   grey: 'rgb(201, 203, 207)'
          // }
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getDashboardGraphMainlawByLawGroup = async () => {
    try {
      const result = await ApiDashboard.getDashboardGraphMainlawByLawGroup();
      if (result.status == 200) {
        setdatasetDonuts2(result.data);
        // setdatasetCharts(result.data);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const data = {
    labels: ["MFA", "NON-MFA"],
    datasets: [
      {
        data: [5667, 223829],
        backgroundColor: ["#FF6384", "#36A2EB"],
        hoverBackgroundColor: ["#FF6384", "#36A2EB"],
      },
    ],
  };

  const option = {
    tooltip: {
      callbacks: {
        label: function (tooltipItem, data) {
          alert("");
          var dataset = data.datasets[tooltipItem.datasetIndex];
          var meta = dataset._meta[Object.keys(dataset._meta)[0]];
          var total = meta.total;
          var currentValue = dataset.data[tooltipItem.index];
          var percentage = parseFloat(
            ((currentValue / total) * 100).toFixed(1)
          );
          return currentValue + " (" + percentage + "%)";
        },
        title: function (tooltipItem, data) {
          alert("");
          return data.labels[tooltipItem[0].index];
        },
      },
    },
  };

  const options = {
    
    // plugins: {
    //   dataLabels: {
    //     color: '#ff0011',
    //     display: function(context) {
    //       return context;
    //     },
    //     font: {
    //       weight: 'bold'
    //     },
    //     formatter: Math.round
    //   }
    // },
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: false,
          },
        },
      ],
    },
  };

  function generateDetailHistory() {
    return (
      <DetailHistory dataTable={dataTable} getFileListMain={getFileList} />
    );
  }

  const onSearchChange = (val) =>{
    // console.log("val",val);
    var rawData = dataTableRaw;
    // console.log("rawData", rawData);
    var result = rawData.filter((x) => 
    (x.mainLawName.length > 0 ? x.mainLawName[0].includes(val) : false)
     || x.lawagency.includes(val)
     || x.lawagencySub.includes(val)
     || getStatus(x.systemStatus).includes(val));

     setDataTable(result);
     generateDetailHistory();
    //  console.log("result", result);
  }

  return (
    <>
      <CRow>
        <CCol sm="12">
          <h1>ยินดีต้อนรับเข้าสู่ระบบ</h1>
        </CCol>
      </CRow>
      <CRow>
        <CCol sm="12">
          <h1>Betagro Regulation Compliance System (BRCS)</h1>
        </CCol>
      </CRow>
      <CRow className="dashboard-page mt-5">
        <CCol sm="12">
          <CRow>
            <CCol sm="4">
              <CCard>
                <CCardBody>
                  <CRow className="text-title">
                    <CCol sm="12">
                      <CIcon name="cil-pen-nib" />
                      <span className="ml-2">จำนวนกฎหมายทั้งหมด</span>
                    </CCol>
                  </CRow>
                  <CRow className="mt-4">
                    <CCol sm="12">
                      <label
                        className="w-75"
                        style={{ color: "#8CC63F", fontSize: "64px" }}
                      >
                        {allLaws}
                      </label>
                    </CCol>
                  </CRow>
                </CCardBody>
              </CCard>
            </CCol>
            <CCol sm="4">
              <CCard>
                <CCardBody>
                  <CRow className="text-title">
                    <CCol sm="12">
                      <CIcon name="cil-file" />
                      <span className="ml-2">จำนวนกฎหมายใหม่รายเดือน</span>
                    </CCol>
                  </CRow>
                  <CRow className="mt-4">
                    <CCol sm="12">
                      <label
                        className="w-75"
                        style={{ color: "#8CC63F", fontSize: "64px" }}
                      >
                        {monthLaws}
                      </label>
                    </CCol>
                  </CRow>
                </CCardBody>
              </CCard>
            </CCol>
            <CCol sm="4">
              <CCard>
                <CCardBody>
                  <CRow className="text-title">
                    <CCol sm="12">
                      <CIcon name="cil-user" />
                      <span className="ml-2">จำนวนผู้ใช้งานในระบบ</span>
                    </CCol>
                  </CRow>
                  <CRow className="mt-4">
                    <CCol sm="12">
                      <label
                        className="w-75"
                        style={{ color: "#8CC63F", fontSize: "64px" }}
                      >
                        {users}
                      </label>
                      {/* <label className="text-right w-25" style={{color:"#828282", fontSize: "12px"}}>ดูรายละเอียด</label> */}
                    </CCol>
                  </CRow>
                </CCardBody>
              </CCard>
            </CCol>
          </CRow>
        </CCol>
      </CRow>
      <CRow>
        <CCol sm={12}>
          <CCard>
            <CCardBody>
              <CRow>
                <CCol sm={5}>
                  {datasetDonuts.length > 0 && (
                    <Bar
                      data={{
                        labels: datasetCharts.labels_list,
                        datasets: [
                          {
                            title: "กราฟแท่งจำนวนกฎหมายรายเดือน",
                            label: "กราฟแท่งจำนวนกฎหมายรายเดือน",
                            data: datasetCharts.count_list,
                            // backgroundColor: datasetCharts.background,
                            backgroundColor: Object.values(CHART_COLORS),
                            dataLabels: {
                              align: 'end',
                              anchor: 'start'
                            }
                          },
                        ],
                      }}
                      options={options}
                    />
                  )}
                </CCol>
                <CCol sm={3}>
                  {/* <Doughnut
                    style={{ maxWidth: "100%" }}
                    data={{
                      labels: datasetDonuts2.map((x) => x.name),
                      datasets: [
                        {
                          data: datasetDonuts2.map((x) => Math.floor((x.cnt / (datasetDonuts2.reduce((a,v) =>  a = a + v.cnt , 0 ))) * 100)),

                          backgroundColor: datasetDonuts2.map((x) => {
                            var letters = "BCDEF".split("");
                            var color = "#";
                            for (var i = 0; i < 6; i++) {
                              color +=
                                letters[
                                  Math.floor(Math.random() * letters.length)
                                ];
                            }
                            return color;
                          }),
                        },
                      ],
                    }}
                    options={option}
                  /> */}
                  {/* <Chart options={{}} series={datasetDonuts2.map((x) => Math.floor((x.cnt / (datasetDonuts2.reduce((a,v) =>  a = a + v.cnt , 0 ))) * 100))} labels={datasetDonuts2.map((x) => x.name)} type="donut" width="380" /> */}
                  <Chart options={{
                    series: datasetDonuts2.map((x) => x.cnt),
                    labels: datasetDonuts2.map((x) => x.name),
                    legend: {
                      position: 'bottom'
                    },
                    title: {
                      text: "% กฎหมายแยกตามกลุ่มกฎหมาย",
                      align: "center"
                    }
                  }} series={datasetDonuts2.map((x) => x.cnt)} type="donut" style={{ maxWidth: "100%" }} />
                </CCol>
                <CCol sm={4}>
                  {/* <Doughnut
                    style={{ maxWidth: "100%" }}
                    data={{
                      labels: datasetDonuts.map((x) => x.code),
                      datasets: [
                        {
                          data: datasetDonuts.map((x) => x.cntcode),

                          backgroundColor: datasetDonuts.map((x) => {
                            var letters = "BCDEF".split("");
                            var color = "#";
                            for (var i = 0; i < 6; i++) {
                              color +=
                                letters[
                                  Math.floor(Math.random() * letters.length)
                                ];
                            }
                            return color;
                          }),
                        },
                      ],
                    }}
                    options={option}
                  /> */}
                  <Chart options={{
                    series: datasetDonuts.map((x) => x.cntcode),
                    labels: datasetDonuts.map((x) => x.code),
                    legend: {
                      position: 'bottom'
                    },
                    title: {
                      text: "จำนวนกฎหมายแต่ละ BU",
                      align: "center"
                    }
                  }} series={datasetDonuts.map((x) => x.cntcode)} type="donut" style={{ maxWidth: "100%" }} />
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
      <CRow>
        <CCol sm="12" className="row">
          <label style={{ fontSize: "24px", fontWeight: "bold" }} className="sm-8">
            <label style={{ color: "#8CC63F" }}>
              <CIcon name="cil-balance-scale" />
            </label>{" "}
            รายการกฎหมาย ล่าสุด{" "}
          </label>
          <CCol sm="2" className="text-right">
            <CInput placeHolder="ค้นหา" onChange={(e) => onSearchChange(e.target.value)}/>
          </CCol>
        </CCol>
      </CRow>
      <div>
        {/* <CCard>
          <CCardBody>
            <CDataTable
              items={dataTable}
              fields={fields}
              hover
              sorter
              striped
              bordered
              scopedSlots={{
                systemStatus: (item) => (
                  <td>
                          {getStatus(item.systemStatus)}
                  </td>
                ),
                mainLawName: (item) => (
                  <td>
                      <label style={{color:"#8CC63F"}}>{item.mainLawName.length > 0 ? item.mainLawName[0] : ""}</label>
                  </td>
                ),
              }}
            />
          </CCardBody>
        </CCard> */}
        {generateDetailHistory()}
        
      </div>
      <div hidden={!userState.id}>
        <CRow>
          <CCol sm="12">
            <label style={{ fontSize: "24px", fontWeight: "bold" }}>
              <label style={{ color: "#8CC63F" }}>
                <CIcon name="cil-description" />
              </label>{" "}
              Task on hand{" "}
            </label>
          </CCol>
        </CRow>
        <div>
          <CCard>
            <CCardBody>
              <CDataTable
                items={dataTaskTable}
                fields={fieldsTask}
                // tableFilter={{ label: "ค้นหา", placeholder: 'พิมพ์คำที่ต้องการค้นหา' }}
                // cleaner
                // itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
                // itemsPerPage={10}
                hover
                sorter
                striped
                bordered
                // pagination
                scopedSlots={{
                  task: (item) => (
                    <td>
                      <label style={{ color: "#8CC63F" }}>{item.task}</label>
                    </td>
                  ),
                }}
              />
            </CCardBody>
          </CCard>
        </div>
      </div>
      <CModal show={modalFile} onClose={toggleFile}>
        <CModalBody>
          <CRow>
            <CCol sm={"12"}>
              <CDataTable
                items={dataFileTable}
                fields={fieldFiles}
                // tableFilter
                // itemsPerPageSelect
                // itemsPerPage={10}
                hover
                sorter
                pagination
                scopedSlots={{
                  filename: (item, index) => (
                    <td>
                      <a
                        target="_blank"
                        download=""
                        href={`${WEB_FILE_URL}${item.filepath}`}
                      >
                        {item.filename}
                      </a>
                    </td>
                  ),
                }}
              />
            </CCol>
          </CRow>
        </CModalBody>
        <CModalFooter>
          <CButton variant="outline" color="dark" onClick={toggleFile}>
            ปิด
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  );
};

const DetailHistory = ({ dataTable, getFileListMain }) => {
  const getFileList = (data) => {
    console.log("data", data);
    getFileListMain(data);
  };
  return dataTable.map((item) => {
    return (
      <div className="card mb-0 mt-3">
        <div className="card-header">
          <div className="d-flex" style={{ justifyContent: "space-between" }}>
            <div className="flex-1">
              <div className="col-12 row" style={{ marginRight: "15px" }}>
                {/* <h5 class="m-0 p-0 project-name mr-2">{item.brcsDate}</h5> */}
                <h5
                  className="m-0 p-0 project-name"
                  style={{ color: "#8CC63F" }}
                >
                  {item.mainLawName.length > 0 ? item.mainLawName[0] : ""}
                </h5>
              </div>
              <div className="d-flex mt-2">
                <div className="flex-1 flex-column">
                  <div className="row ml-1">
                    <CIcon name="cil-tags" className="mr-2" />
                    <label className="mr-2">สถานะ :</label>
                    <label
                      className="badge badge-sm badge-info"
                      style={{ width: "100px" }}
                    >
                      {getStatus(item.systemStatus)}
                    </label>
                  </div>
                  <div>
                    <CIcon name="cil-user" className="mr-2" />
                    <label className="mr-2">หน่วยงานผู้ออกกฏหมาย :</label>
                    <label className="mr-2">{item.lawagency}</label>
                  </div>
                  <div>
                    <CIcon name="cil-user" className="mr-2" />
                    <label className="mr-2">หน่วยงานย่อยผู้ออกกฏหมาย :</label>
                    <label className="mr-2">{item.lawagencySub}</label>
                  </div>
                  <div>
                    <CIcon name="cil-calendar" className="mr-2" />
                    <label className="mr-2">วันที่บังคับใช้ :</label>
                    <label className="mr-2">{item.brcsDate}</label>
                  </div>
                  {/* <div>
                                  <span class="badge badge-primary" size="30" style="font-size:.765625rem;font-weight:100">User 1</span>
                                  <span class="badge badge-primary" size="30" style="font-size:.765625rem;font-weight:100">Verifier 1</span>
                                  <span class="badge badge-warning" size="30" style="font-size:.765625rem;font-weight:100">Approver 1</span>
                              </div> */}
                </div>
              </div>
            </div>
            <div
              className="d-flex flex-row"
              style={{
                minWidth: 70,
                justifyContent: "flex-end",
                position: "absolute",
                bottom: 15,
                right: 20,
              }}
            >
              <div>
                {/* <a class="card-header-action btn-minimize collapsed" data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                              <svg class="c-icon">
                                  <use xlink:href="node_modules/@@coreui/icons/sprites/free.svg#cil-arrow-circle-bottom"></use>
                              </svg>
                          </a> */}
                {item.fileCount > 0 ? (
                  <div
                    className="d-flex flex-column"
                    style={{ float: "right" }}
                  >
                    <CButton
                      onClick={() => {
                        getFileList(item);
                      }}
                      size="sm"
                      color={"success"}
                      variant={"outline"}
                    >
                      <CIcon name="cil-file"></CIcon>File
                    </CButton>
                    {/* <button class="btn btn-info btn-sm"><i class="c-icon cil-magnifying-glass mr-2"></i>File</button> */}
                  </div>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  });
};

export default DashboardLogin;
