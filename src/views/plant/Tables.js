import React, { useState, useEffect } from 'react'
import {
    CCardBody,
    CBadge,
    CButton,
    CCollapse,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CCol,
    CCard,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CSelect,
    CForm,
    CModalTitle,
    CButtonGroup
} from '@coreui/react'
import Api from '../../api/ApiPlant';
import CIcon from '@coreui/icons-react'
import { statusData } from './data';
// import { budata } from './data';
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2/src/sweetalert2.js";

const getBadge = status => {
    switch (status) {
        case 'A': return 'success'
        case 'I': return 'secondary'
        default: return 'primary'
    }
}

const getStatus = status => {
    switch (status) {
        case 'A': return 'Active'
        case 'I': return 'Inactive'
        default: return ''
    }
}

const fields = [
    // { key: 'order', label: 'ลำดับ', _style: { width: '1%' } },
    { key: 'option', label: '', _style: { width: '1%' }, filter: false, },
    { key: 'buDescription', label: 'รหัส กลุ่มธุรกิจ' },
    { key: 'siteCode', label: 'รหัสโรงงาน' },
    { key: 'plant', label: 'โรงงาน' },
    { key: 'siteDescription', label: 'ชื่อบริษัท' },
    { key: 'status', label: 'สถานะการใช้งาน', _style: { width: '10%' } },
    { key: 'updateBy', label: 'ผู้แก้ไขข้อมูล', _style: { width: '15%' } },
    { key: 'updateDate', label: 'วันที่แก้ไขข้อมูล', _style: { width: '15%' } },
]


const Tables = ({ data = [], refreshData = () => { } }) => {
  const userState = useSelector((state) => state.changeState.user);
  const [budata, setBuData] = useState([]);

    const [modal, setModal] = useState(false);
    const [id, setId] = useState('');
    const [plant, setPlant] = useState('');
    const [siteCode, setSiteCode] = useState('');
    const [siteDescription, setSiteDescription] = useState('');
    const [recordstatus, setRecordStatus] = useState('');
    const [dataSelected, setDataSelected] = useState('');
    const [modalConfirm, setModalConfirm] = useState(false);
    const [buid, setBUId] = useState('');

    useEffect(() => {
        getBuOption();
          return () => {
  
          }
    }, []);

    const getBuOption = async () => {
        try{
            const result = await Api.getBuOption();
            if (result.status == 200) {
                setBuData(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }

    const toggle = () => {
        setModal(!modal);
    }

    const toggleConfirm = () => {
      setModalConfirm(!modalConfirm);
    }

    const newDetails = (record) => {
        toggle();
        setId('');
        setSiteCode('');
        setSiteDescription('');
        setPlant('');
        setBUId(0);
        setRecordStatus("A");
    }

    const deleteDetails = (record) => {
        toggleConfirm();
        setDataSelected(record);
        setId(record.id);
        setPlant(record.plant);
        setSiteCode(record.siteCode);
        setSiteDescription(record.siteDescription);
        setBUId(record.buid);
    }

    const onDelete = async () => {
      try {
          const result = await Api.delete(dataSelected);
          if (result.status === 200) {
              const { data } = result.data;
              toggleConfirm();
              refreshData();
              
              Swal.fire({
                  icon: "success",
                  title: "Delete Success",
              });
          }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
              });
        }
    }
    
    const editDetails = (record) => {
        toggle();
        setId(record.id);
        setSiteCode(record.siteCode);
        setSiteDescription(record.siteDescription);
        setPlant(record.plant);
        setRecordStatus(record.recordstatus);
        setBUId(record.buid);
    }

    const onSubmit = (e) => {
      e.preventDefault();
      const model = {
        "id": id == "" ? 0 : id,
        "plant": plant,
        "siteCode": siteCode,
        "siteDescription": siteDescription,
        "buid": buid,
        "user": userState.id,
        "recordstatus": recordstatus
      }
      saveData(model);
    }
  
    const saveData = async (data) => {
      try {
          const result = await Api.create(data);
          if (result.status === 200) {
              const { data } = result.data;
              toggle();
              refreshData();
              
              Swal.fire({
                  icon: "success",
                  title: "Save Success",
              });
          }
      } catch (error) {
        Swal.fire({
            icon: "error",
            title: error.response.data,
          });
      }
    }


    const handleChange = (e,type) => {
        switch (type) {
            case "buid":
                setBUId(e.target.value);
                break;
            default:
                setRecordStatus(e.target.value);
                break;
        }
       
    }

    return (
        <>
            <CRow>
                <CCol xs="12" lg="12" >
                    <CButton onClick={newDetails} color={"success"} variant={"outline"}><CIcon name="cil-plus" /><span className="ml-2">สร้าง โรงงาน (Plant)</span></CButton>
                </CCol>
            </CRow>
            <CRow className="mt-3">
                <CCol xs="12" lg="12" >
                    <CCard>
                        <CCardBody>
                            <CDataTable
                                items={data}
                                fields={fields}
                                tableFilter={{ label: "ค้นหา", placeholder: 'พิมพ์คำที่ต้องการค้นหา' }}
                                cleaner
                                itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
                                itemsPerPage={10}
                                hover
                                sorter
                                striped
                                bordered
                                pagination
                                scopedSlots={{
                                    'status':
                                        (item) => (
                                            <td>
                                                <CBadge color={getBadge(item.recordstatus)}>
                                                    {getStatus(item.recordstatus)}
                                                </CBadge>
                                            </td>
                                        ),
                                        'option':
                                            (item) => (
                                                <td className="center">
                                                    <CButtonGroup>
                                                        <CButton
                                                            color="success"
                                                            variant="outline"
                                                            shape="square"
                                                            size="sm"
                                                            onClick={() => { editDetails(item) }}
                                                        >
                                                            <CIcon name="cilPen" />
                                                        </CButton>
                                                        {/* <CButton variant="ghost" size="sm" />
                                                        <CButton
                                                            color="danger"
                                                            variant="outline"
                                                            shape="square"
                                                            size="sm"
                                                            onClick={() => { deleteDetails(item) }}
                                                        >
                                                            <CIcon name="cilTrash" />
                                                        </CButton> */}
                                                    </CButtonGroup>
                                                </td>
                                            ),

                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>

            </CRow>

            <CModal show={modalConfirm} onClose={setModalConfirm} color="danger" >
                <CModalHeader closeButton>
                    <CModalTitle> Delete Data : {siteCode}</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    Are you confirm to delete {siteCode} ?
                </CModalBody>
                <CModalFooter>
                    <CButton variant="outline" onclick={onDelete} color="primary">Confirm</CButton>{' '}
                    <CButton variant="outline" color="secondary" onClick={() => setModalConfirm(false)}>Close</CButton>
                </CModalFooter>
            </CModal>
            <CModal
                show={modal}
                onClose={toggle}
            >
                <CModalHeader closeButton>
                    <CModalTitle>รายละเอียด Plant</CModalTitle>
                </CModalHeader>
                <CForm onSubmit={onSubmit} action="" >
                    <CModalBody>
                        <CInput type="hidden" value={id} id="id"/>
                        <CRow>
                            <CCol xs="12">
                                <CFormGroup>
                                    <CLabel htmlFor="buid">กลุ่มธุรกิจ</CLabel>
                                    <CSelect custom name="buid" id="buid" value={buid} onChange={(e) => handleChange(e,'buid')} required>
                                        <option value={""}>-- กลุ่มธุรกิจ--</option>
                                        {
                                            budata.map((item, index) => {
                                                return <option key={item.id} value={item.id}>{item.description}</option>
                                            })
                                        }
                                    </CSelect>
                                </CFormGroup>
                            </CCol>
                        </CRow>
                        <CRow>
                            <CCol xs="12" sm="12">
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="plant">โรงงาน</CLabel>
                                            <CInput onChange={e => setPlant(e.target.value)} value={plant} readOnly={id != ""} id="plant" placeholder="กรอก โรงงาน" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="siteCode">รหัสโรงงาน</CLabel>
                                            <CInput onChange={e => setSiteCode(e.target.value)} value={siteCode} readOnly={id != ""} id="siteCode" placeholder="กรอก รหัสโรงงาน" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                            </CCol>
                        </CRow >
                        <CRow>
                            <CCol xs="12" sm="12">
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="siteDescription">ชื่อบริษัท</CLabel>
                                            <CInput onChange={e => setSiteDescription(e.target.value)} value={siteDescription} id="siteDescription" placeholder="กรอกร ชื่อบริษัท" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                               
                            </CCol>
                        </CRow >
                        <CRow>
                            <CCol xs="12">
                                <CFormGroup>
                                    <CLabel htmlFor="recordstatus">สถานะการใช้งาน</CLabel>
                                    <CSelect custom name="recordstatus" id="recordstatus" value={recordstatus} onChange={(e) => handleChange(e,'recordstatus')} required>
                                        <option value={""}>-- สถานะการใช้งาน --</option>
                                        {
                                            statusData.map((item, index) => {
                                                return <option key={item.code} value={item.code}>{item.name}</option>
                                            })
                                        }
                                    </CSelect>
                                </CFormGroup>
                            </CCol>
                        </CRow>
                    </CModalBody>
                    <CModalFooter>
                        <CButton variant="outline" type="submit" color="primary">ตกลง</CButton>
                        <CButton
                            variant="outline"
                            color="secondary"
                            onClick={toggle}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CForm >
            </CModal >

        </>
    )
}

export default Tables
