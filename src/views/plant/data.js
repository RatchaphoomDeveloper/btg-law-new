export const plantdata = [
    { id: 0, bu: 'Agro',bucode: "B01", plant: 'A01', sitecode: 'BTG-LR1', sitedesc: 'บริษํท เบทาโกร จำกัด มหาชน (โรงงานลพบุรี 1)',   statuscode: 'I',status: 'Inactive', editby: "Admin", editdate: "12/05/2020" },
    { id: 1, bu: 'BFood',bucode: "B02", plant: 'H607', sitecode: 'ABS', sitedesc: 'บริษํท อายิโนะโมโต๊ะ เบทาโกร สเปเชียลดี้ ฟู๊ด จำกัด',   statuscode: 'A',status: 'Active', editby: "Admin", editdate: "12/05/2020" }
]

export const statusData = [
    { id: 0, name: 'Active', code: "A" },
    { id: 1, name: 'In Active', code: "I" },
]

export const budata = [
    { id: 0, name: 'Agro', code: "B01" },
    { id: 1, name: 'BFood', code: "B02" },
]

