import React, { useState, useEffect } from 'react'
import {
    CCardBody,
    CBadge,
    CButton,
    CCollapse,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CCol,
    CCard,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CSelect,
    CForm,
    CModalTitle,
    CButtonGroup
} from '@coreui/react'
import Api from '../../api/ApiUser';
//import ApiRole from '../../api/ApiRole';
import CIcon from '@coreui/icons-react'
import { statusData } from './data';
// import { roleData } from './data';
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2/src/sweetalert2.js";
import { useHistory } from "react-router-dom";

const getBadge = status => {
    switch (status) {
        case 'A': return 'success'
        case 'I': return 'secondary'
        default: return 'primary'
    }
}

const getStatus = status => {
    switch (status) {
        case 'A': return 'Active'
        case 'I': return 'Inactive'
        default: return ''
    }
}

const fields = [
    // { key: 'order', label: 'ลำดับ', _style: { width: '1%' } },
    { key: 'option', label: '', _style: { width: '1%' }, filter: false, },
    { key: 'userlogin', label: 'User Login' },
    { key: 'fullname', label: 'Full Name' },
    { key: 'email', label: 'Email' },
    { key: 'buDescription', label: 'BU' },
    // { key: 'siteCode', label: 'Site' },
    // { key: 'divisionCode', label: 'Division' },
    //{ key: 'roleName', label: 'Group' },
    { key: 'status', label: 'สถานะการใช้งาน', _style: { width: '10%' } },
    { key: 'updateBy', label: 'ผู้แก้ไขข้อมูล', _style: { width: '15%' } },
    { key: 'updateDate', label: 'วันที่แก้ไขข้อมูล', _style: { width: '15%' } },
]



const Tables = ({ data = [], refreshData = () => { } }) => {
    const history = useHistory();
    const userState = useSelector((state) => state.changeState.user);
  const [buData, setBuData] = useState([]);
    // const [siteData, setSiteData] = useState([]);
    // const [divisionData, setDivisionData] = useState([]);
    //const [roleData, setRoleData] = useState([]);

    const [modal, setModal] = useState(false);
    const [id, setId] = useState('');
    const [userlogin, setuserlogin] = useState('');
    const [fname, setfname] = useState('');
    const [email, setemail] = useState('');
    const [recordstatus, setRecordStatus] = useState('');
    const [dataSelected, setDataSelected] = useState('');
    const [modalConfirm, setModalConfirm] = useState(false);
    const [buid, setBU] = useState('');
    // const [plantid, setSite] = useState('');
    // const [divisionid, setDivision] = useState('');
    //const [role, setRole] = useState('');

    useEffect(() => {
        getBuOption();
        // getSiteOption();
        // getDivisionOption();
        //getRoleOption();
          return () => {
  
          }
    }, []);

    const getBuOption = async () => {
        try{
            const result = await Api.getBuOption();
            if (result.status == 200) {
                setBuData(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }

    // const getSiteOption = async () => {
    //     try{
    //         const result = await Api.getSiteOption();
    //         if (result.status == 200) {
    //             setSiteData(result.data);
    //         }
    //     }catch(error){
    //         console.log(error);
    //     }
    // }

    // const getDivisionOption = async () => {
    //     try{
    //         const result = await Api.getDivisionOption();
    //         if (result.status == 200) {
    //             setDivisionData(result.data);
    //         }
    //     }catch(error){
    //         console.log(error);
    //     }
    // }

    // const getRoleOption = async () => {
    //     try{
    //         const result = await ApiRole.get();
    //         if (result.status == 200) {
    //             setRoleData(result.data);
    //         }
    //     }catch(error){
    //         console.log(error);
    //     }
    // }

    const toggle = () => {
        setModal(!modal);
    }

    const toggleConfirm = () => {
      setModalConfirm(!modalConfirm);
    }

    const newDetails = (record) => {
        toggle();
        setId('');
        setuserlogin('');
        setfname('');
        setemail('');
        setBU(0);
        // setSite(0);
        // setDivision(0);
        //setRole(0);
        setRecordStatus("A");
    }

    const deleteDetails = (record) => {
        toggleConfirm();
        setDataSelected(record);
        setId(record.id);
    }

    const onDelete = async () => {
      try {
          const result = await Api.delete(dataSelected);
          if (result.status === 200) {
              const { data } = result.data;
              toggleConfirm();
              refreshData();
              
              Swal.fire({
                  icon: "success",
                  title: "Delete Success",
              });
          }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
              });
        }
    }
    
    
    const editDetails = (record) => {
        toggle();
        setId(record.id);
        setuserlogin(record.userlogin);
        setfname(record.fullname);
        setemail(record.email);
       // setRole(record.role);
        setBU(record.buid);
        // setSite(record.plantid);
        // setDivision(record.divisionid);
        setRecordStatus(record.recordstatus);
    }

    const onSubmit = (e) => {
      e.preventDefault();
      const model = {
        "id": id == "" ? 0 : id,
        "userlogin": userlogin,
        "fullname": fname,
        "email": email,
        "buid": buid,
        // "plantid": plantid,
        // "divisionid": divisionid,
        //"role": role,
        "user": userState.Id,
        "recordstatus": recordstatus
      }
      saveData(model);
    }
  
    const saveData = async (data) => {
      try {
          const result = await Api.create(data);
          if (result.status === 200) {
              const { data } = result.data;
              toggle();
              refreshData();
              
              Swal.fire({
                  icon: "success",
                  title: "Save Success",
              });
          }
      } catch (error) {
        Swal.fire({
            icon: "error",
            title: error.response.data,
          });
      }
    }

    const handleChange = (e,type) => {
        switch (type) {
            case "bu":
                setBU(e.target.value);
                break;
            // case "site":
            //     setSite(e.target.value);
            //     break;
            // case "division":
            //     setDivision(e.target.value);
            //     break;
            // case "role":
            //     setRole(e.target.value);
            //     break;
            default:
                setRecordStatus(e.target.value);
                break;
        }
       
    }
    const gotoSubMenu = (item) => {
        history.push(`/home/usersetting/usermanagesub?id=${item.id}`)
    
    }
    return (
        <>
            <CRow>
                <CCol xs="12" lg="12" >
                    <CButton onClick={newDetails} color={"success"} variant={"outline"}><CIcon name="cil-plus" /><span className="ml-2">สร้าง ข้อมูลผู้ใช้งาน</span></CButton>
                </CCol>
            </CRow>
            <CRow className="mt-3">
                <CCol xs="12" lg="12" >
                    <CCard>
                        <CCardBody>
                            <CDataTable
                                items={data}
                                fields={fields}
                                tableFilter={{ label: "ค้นหา", placeholder: 'พิมพ์คำที่ต้องการค้นหา' }}
                                cleaner
                                itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
                                itemsPerPage={10}
                                hover
                                sorter
                                striped
                                bordered
                                pagination
                                scopedSlots={{
                                    userlogin:
                                    (item) => (
                                        <td>
                                            <a className="text-info cursor-pointer" onClick={() => gotoSubMenu(item)}>{item.userlogin}</a>
                                        </td>
                                        ),
                                    status:
                                        (item) => (
                                            <td>
                                                <CBadge color={getBadge(item.recordstatus)}>
                                                    {getStatus(item.recordstatus)}
                                                </CBadge>
                                            </td>
                                        ),
                                    option:
                                        (item) => (
                                            <td className="center">
                                                <CButtonGroup>
                                                    <CButton
                                                        color="success"
                                                        variant="outline"
                                                        shape="square"
                                                        size="sm"
                                                        onClick={() => { editDetails(item) }}
                                                    >
                                                        <CIcon name="cilPen" />
                                                    </CButton>
                                                    {/* <CButton variant="ghost" size="sm" />
                                                    <CButton
                                                        color="danger"
                                                        variant="outline"
                                                        shape="square"
                                                        size="sm"
                                                        onClick={() => { deleteDetails(item) }}
                                                    >
                                                        <CIcon name="cilTrash" />
                                                    </CButton> */}
                                                </CButtonGroup>
                                            </td>
                                        ),

                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>

            </CRow>

            <CModal show={modalConfirm} onClose={setModalConfirm} color="danger" >
                <CModalHeader closeButton>
                    <CModalTitle> Delete Data : {userlogin}</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    Are you confirm to delete {userlogin} ?
                </CModalBody>
                <CModalFooter>
                    <CButton variant="outline" onclick={onDelete} color="primary">Confirm</CButton>{' '}
                    <CButton variant="outline" color="secondary" onClick={() => setModalConfirm(false)}>Close</CButton>
                </CModalFooter>
            </CModal>
            <CModal
                show={modal}
                onClose={toggle}
            >
                <CModalHeader closeButton>
                    <CModalTitle>รายละเอียด ข้อมูลผู้ใช้งาน</CModalTitle>
                </CModalHeader>
                <CForm onSubmit={onSubmit} action="" >
                    <CModalBody>
                        <CInput type="hidden" value={id} id="id"/>
                        <CRow>
                            <CCol xs="12" sm="12">
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="userlogin">User Login</CLabel>
                                            <CInput onChange={e => setuserlogin(e.target.value)} value={userlogin} readOnly={id != ""} id="userlogin" placeholder="กรอก User Login" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="fname">Full Name</CLabel>
                                            <CInput onChange={e => setfname(e.target.value)} value={fname} id="fname" placeholder="กรอก Full Name" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                            </CCol>
                        </CRow >
                        <CRow>
                            <CCol xs="12" sm="12">
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="email">Email</CLabel>
                                            <CInput onChange={e => setemail(e.target.value)} value={email} id="email" placeholder="กรอก Email" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                            </CCol>
                        </CRow >
                        <CRow>
                            <CCol xs="12">
                                <CFormGroup>
                                    <CLabel htmlFor="buid">BU</CLabel>
                                    <CSelect custom name="buid" id="buid" value={buid} onChange={(e) => handleChange(e,'bu')} required>
                                        <option value={""}>-- BU Description --</option>
                                        {
                                            buData.map((item, index) => {
                                                return <option key={item.id} value={item.id}>{item.description}</option>
                                            })
                                        }
                                    </CSelect>
                                </CFormGroup>
                            </CCol>
                        </CRow>
                        {/* <CRow>
                            <CCol xs="12">
                                <CFormGroup>
                                    <CLabel htmlFor="plantid">Site</CLabel>
                                    <CSelect custom name="plantid" id="plantid" value={plantid}  onChange={(e) => handleChange(e,'site')} required>
                                        <option value={""}>-- Site --</option>
                                        {
                                            siteData.map((item, index) => {
                                                return <option key={item.id} value={item.id}>{item.siteCode}</option>
                                            })
                                        }
                                    </CSelect>
                                </CFormGroup>
                            </CCol>
                        </CRow>
                        <CRow>
                            <CCol xs="12">
                                <CFormGroup>
                                    <CLabel htmlFor="divisionid">Division</CLabel>
                                    <CSelect custom name="divisionid" id="divisionid" value={divisionid}  onChange={(e) => handleChange(e,'division')} required>
                                        <option value={""}>-- Division --</option>
                                        {
                                            divisionData.map((item, index) => {
                                                return <option key={item.id} value={item.id}>{item.code}</option>
                                            })
                                        }
                                    </CSelect>
                                </CFormGroup>
                            </CCol>
                        </CRow> */}
                        {/* <CRow>
                            <CCol xs="12">
                                <CFormGroup>
                                    <CLabel htmlFor="role">Group</CLabel>
                                    <CSelect custom name="role" id="role" value={role}  onChange={(e) => handleChange(e,'role')} required>
                                        <option value={""}>-- Group --</option>
                                        {
                                            roleData.map((item, index) => {
                                                return <option key={item.id} value={item.id}>{item.code}</option>
                                            })
                                        }
                                    </CSelect>
                                </CFormGroup>
                            </CCol>
                        </CRow> */}
                        
                        <CRow>
                            <CCol xs="12">
                                <CFormGroup>
                                    <CLabel htmlFor="recordstatus">สถานะการใช้งาน</CLabel>
                                    <CSelect custom name="recordstatus" id="recordstatus" value={recordstatus}  onChange={(e) => handleChange(e,'status')} required>
                                        <option value={""}>-- สถานะการใช้งาน --</option>
                                        {
                                            statusData.map((item, index) => {
                                                return <option key={item.code} value={item.code}>{item.name}</option>
                                            })
                                        }
                                    </CSelect>
                                </CFormGroup>
                            </CCol>
                        </CRow>
                    </CModalBody>
                    <CModalFooter>
                        <CButton variant="outline" type="submit" color="primary">ตกลง</CButton>
                        <CButton
                            variant="outline"
                            color="secondary"
                            onClick={toggle}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CForm >
            </CModal >

        </>
    )
}

export default Tables
