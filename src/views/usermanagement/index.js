import React, { useEffect, useState } from 'react'
import {
    CBadge,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CModal,
    CModalHeader,
    CModalFooter,
    CButton
} from '@coreui/react'
import Api from '../../api/ApiUser';
// import { userData } from './data';
import Tables from './Tables';

const User = () => {
    const [dataTable, setDataTable] = useState([]);
    useEffect(() => {
        getData();
          return () => {
  
          }
      }, []);
  
      const getData = async () => {
          try{
              const result = await Api.get();
              if (result.status == 200) {
                  setDataTable(result.data);
              }
          }catch(error){
              console.log(error);
          }
      }
    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardHeader>
                            <b> จัดการข้อมูลผู้ใช้งาน ({dataTable.length}) </b>
                        </CCardHeader>
                        <CCardBody>
                            <Tables refreshData={getData} data={dataTable} />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    )
}

export default User;