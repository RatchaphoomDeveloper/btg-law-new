export const userData = [
    { id: 0, login: 'Pra hhh',fullname: "Pralinda", email: 'Pra@beta', bu: 'Agroo',bucode: "B01", site: 'ABS', devision: 'HR.', rolegroup: 'FQO',  statuscode: 'I', status: 'Inactive', editby: "Admin", editdate: "12/05/2020" },
    { id: 1, login: 'Kryy uuu',fullname: "Kieky", email: 'Kung@be', bu: 'BFood',bucode: "B02", site: 'ABS',  devision: 'QA.', rolegroup: 'BU',  statuscode: 'A', status: 'Active', editby: "Admin", editdate: "12/05/2020" }
]

export const plantData = [
    { id: 0, bu: 'Agro', plant: 'A01', sitecode: 'BTG-LR1', sitedesc: 'บริษํท เบทาโกร จำกัด มหาชน (โรงงานลพบุรี 1)'},
    { id: 1, bu: 'BFood', plant: 'H607', sitecode: 'ABS', sitedesc: 'บริษํท อายิโนะโมโต๊ะ เบทาโกร สเปเชียลดี้ ฟู๊ด จำกัด' }
]

export const statusData = [
    { id: 0, name: 'Active', code: "A" },
    { id: 1, name: 'In Active', code: "I" },
]

export const buData = [
    { id: 0, name: 'Agro', code: "B01" },
    { id: 1, name: 'BFood', code: "B02" },
]

export const roleData = [
    { id: 0, name: 'Admin', code: "Admin" },
    { id: 1, name: 'User', code: "User" },
    { id: 2, name: 'FQO', code: "FQO" },
    { id: 3, name: 'BU', code: "BU" },
]

export const devisionData = [
    { id: 0, code: 'HR.', name: 'หน่วยงาน บุคคล' },
    { id: 1, code: 'QA.', name: 'หน่วยงาน ตรวจสอบคุณภาพ'}
]
