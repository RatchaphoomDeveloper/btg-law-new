export const lawAgenciesData = [
    { id: 0, code: 'M01', name: 'LAW_กระทรวงเกษตรและสหกรณ์ (MOA)',  statuscode: 'I', status: 'Pending', editby: "Admin", editdate: "12/05/2020" },
    { id: 1, code: 'Q01', name: 'LAW_กระทรวงคมนาคม (MOT)',  statuscode: 'A', status: 'Active', editby: "Admin", editdate: "12/05/2020" }
]

export const statusData = [
    { id: 0, name: 'Active', code: "A" },
    { id: 1, name: 'In Active', code: "I" },
]

