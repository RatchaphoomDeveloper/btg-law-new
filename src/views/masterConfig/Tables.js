import React, { useState, useEffect } from 'react'
import {
    CCardBody,
    CBadge,
    CButton,
    CCollapse,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CCol,
    CCard,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CSelect,
    CForm,
    CModalTitle,
    CButtonGroup
} from '@coreui/react'
import Api from '../../api/ApiConfig';
import CIcon from '@coreui/icons-react'
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2/src/sweetalert2.js";

// const getBadge = status => {
//     switch (status) {
//         case 'A': return 'success'
//         case 'I': return 'secondary'
//         default: return 'primary'
//     }
// }

// const getStatus = status => {
//     switch (status) {
//         case 'A': return 'Active'
//         case 'I': return 'Inactive'
//         default: return ''
//     }
// }

const fields = [
    // { key: 'order', label: 'ลำดับ', _style: { width: '1%' } },
    { key: 'option', label: '', _style: { width: '1%' }, filter: false, },
    { key: 'code', label: 'ชื่อแบบฟอร์ม' },
    { key: 'value', label: 'รหัสแบบฟอร์ม' },
    { key: 'updateBy', label: 'ผู้แก้ไขข้อมูล', _style: { width: '15%' } },
    { key: 'updateDate', label: 'วันที่แก้ไขข้อมูล', _style: { width: '15%' } },
    { key: 'history', label: '', _style: { width: '1%' }, filter: false, },
]

const fieldsHistory = [
    { key: 'value', label: 'รหัสแบบฟอร์ม' },
    { key: 'createBy', label: 'ผู้แก้ไขข้อมูล', _style: { width: '15%' } },
    { key: 'createDate', label: 'วันที่แก้ไขข้อมูล', _style: { width: '15%' } },
]


const Tables = ({ data = [], refreshData = () => { } }) => {

  const userState = useSelector((state) => state.changeState.user);
  const [modal, setModal] = useState(false);
    const [id, setId] = useState('');
    const [value, setValue] = useState('');
    const [code, setCode] = useState('');
    const [dataSelected, setDataSelected] = useState('');
    const [modalHistory, setModalHistory] = useState(false);
    const [dataHistory, setDataHistory] = useState([]);

    const toggle = () => {
        setModal(!modal);
    }
    const toggleHistory = () => {
        setModalHistory(!modalHistory);
    }
    
    const editDetails = (record) => {
        console.log("record", record);
        toggle();
        setId(record.id);
        setValue(record.value);
        setCode(record.code);
    }

    const onSubmit = (e) => {
      e.preventDefault();
      const model = {
        "id": id,
        "code": code,
        "value": value,
        "user": userState.id,
      }
      saveData(model);
    }
  
    const saveData = async (data) => {
      try {
          const result = await Api.create(data);
          if (result.status === 200) {
              const { data } = result.data;
              toggle();
              refreshData();
              
              Swal.fire({
                  icon: "success",
                  title: "Save Success",
              });
          }
      } catch (error) {
        Swal.fire({
            icon: "error",
            title: error.response.data,
          });
      }
    }

    const onGetHistory = (id) => {
        getHistoryData(id);
        toggleHistory();
    }

    const getHistoryData = async (id) => {
        try {
            const result = await Api.getHistory(id);
            if (result.status == 200) {
                setDataHistory(result.data);
            }
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <>
            <CRow className="mt-3">
                <CCol xs="12" lg="12" >
                    <CCard>
                        <CCardBody>
                            <CDataTable
                                items={data}
                                fields={fields}
                                tableFilter={{ label: "ค้นหา", placeholder: 'พิมพ์คำที่ต้องการค้นหา' }}
                                cleaner
                                itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
                                itemsPerPage={10}
                                hover
                                sorter
                                striped
                                bordered
                                pagination
                                scopedSlots={{
                                        'option':
                                            (item) => (
                                                <td className="center">
                                                    <CButtonGroup>
                                                        <CButton
                                                            color="success"
                                                            variant="outline"
                                                            shape="square"
                                                            size="sm"
                                                            onClick={() => { editDetails(item) }}
                                                        >
                                                            <CIcon name="cilPen" />
                                                        </CButton>
                                                    </CButtonGroup>
                                                </td>
                                        ),
                                        'history':
                                            (item) => (
                                                <td className="center">
                                                    <CButtonGroup>
                                                        <CButton
                                                            color="info"
                                                            variant="outline"
                                                            shape="square"
                                                            size="sm"
                                                            onClick={() => { onGetHistory(item.id) }}
                                                        >
                                                            <CIcon name="cil-history" />
                                                        </CButton>
                                                    </CButtonGroup>
                                                </td>
                                            ),
                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>

            </CRow>

            <CModal
                show={modal}
                onClose={toggle}
            >
                <CModalHeader closeButton>
                    <CModalTitle>รายละเอียด การตั้งค่า</CModalTitle>
                </CModalHeader>
                <CForm onSubmit={onSubmit} action="">
                    <CModalBody>
                        <CInput type="hidden" value={id} id="id"/>
                        <CRow>
                            <CCol xs="12" sm="12">
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="code">ชื่อแบบฟอร์ม</CLabel>
                                            <CInput onChange={e => setCode(e.target.value)} value={code} readOnly id="code" placeholder="กรอก ชื่อแบบฟอร์ม" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="value">รหัสแบบฟอร์ม</CLabel>
                                            <CInput onChange={e => setValue(e.target.value)} value={value} id="value" placeholder="กรอก รหัสแบบฟอร์ม" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                            </CCol>
                        </CRow >
                    </CModalBody>
                    <CModalFooter>
                        <CButton variant="outline" type="submit" color="primary">ตกลง</CButton>
                        <CButton
                            variant="outline"
                            color="secondary"
                            onClick={toggle}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CForm >
            </CModal >

            <CModal show={modalHistory} onClose={toggleHistory} size="lg" >
                <CModalHeader closeButton>
                    <CModalTitle>ประวัติการแก้ไข</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    <CDataTable
                        items={dataHistory}
                        fields={fieldsHistory}
                        tableFilter={{ label: "ค้นหา", placeholder: 'พิมพ์คำที่ต้องการค้นหา' }}
                        cleaner
                        itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
                        itemsPerPage={10}
                        hover
                        sorter
                        striped
                        bordered
                        pagination
                        scopedSlots={{ }}
                    />
                </CModalBody>
                <CModalFooter>
                    <CButton variant="outline" color="secondary" onClick={toggleHistory}>ปิด</CButton>
                </CModalFooter>
            </CModal >

        </>
    )
}

export default Tables
