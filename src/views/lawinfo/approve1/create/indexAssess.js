import React, { useEffect, useRef, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CForm,
  CContainer,
  CLabel,
  CSelect,
  CInput,
  CTabs,
  CTextarea,
  CModalBody,
  CFormGroup
} from "@coreui/react";
import Api from '../../../../api/ApiLawInfo';
// import MainLaw from "../../component/mainlawTab";
// import HowTo from "../../component/howtoTab";
import Estimate from "../../component/estimateTab";
import TableFiles from "../../component/TableFilesAssessment";
import EmployeeModal from "../../component/employeeModal";
import FilePreview from "./../../../../assets/ตัวอย่าง ประเมินความสอดคล้อง (FM-SE-49).pdf";
import DetailModal from "../../component/detailModal";
import CauseModal from '../../component/causeModal';
import "./index.scss";
import CreatHeadPanel from "../../component/headpanelAssess";
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2/src/sweetalert2.js";
import { removeLocalStorage } from "../../../../utils/localStorage";

const CreateLawInfo = (props) => {
  const userState = useSelector((state) => state.changeState.user);
  var id = new URLSearchParams(props.location.search).get("id");
  var code = new URLSearchParams(props.location.search).get("code");
  var assessId = new URLSearchParams(props.location.search).get("assessId");
  var type = new URLSearchParams(props.location.search).get("type");
  var role = new URLSearchParams(props.location.search).get("role");
  const [tabAddress, setTabAddress] = useState("1");
  const [lawid, setLawId] = useState(0);
  const [mainlawid, setMainLawId] = useState();
  const [modal, setModal] = useState(false);
  const [modalDetail, setModalDetail] = useState(false);
  const [modalCause, setModalCause] = useState(false);
  const [modelSaveHeader, setModalSaveHeader] = useState([]);
  const [isHideCancelRemark, setHideCancelRemark] = useState(true);
  const [cancelRemark, setCancelRemark] = useState("");
  const [modalRemark, setModalRemark] = useState(false);

  const [systemStatus, setSystemStatus] = useState([]);

  const [mainLawName, setMainLawName] = useState([]);
  const [lawDescription, setLawDescription] = useState([]);
  const [lawGroupSub, setLawGroupSub] = useState("");
  const [brcsDate, setBrcsDate] = useState("");
  const [estimate, setEstimate] = useState([]);
  const [description, setDescription] = useState([]);
  const [rejectReason, setRejectReason] = useState([]);

  const [fileCount, setFileCount] = useState(0);

  const [logTable, setLogTable] = useState([]);
  const [statusData, setStatusData] = useState([]);
  const statusOption =  [
    { value: 'SEA2', label: 'Send To Approver 2'},
    { value: 'REDI', label: 'Reject To User Division'},
  ]
  const dispatch = useDispatch();

  useEffect(() => {
    if (type == "email"){
      if (role == "verification" && userState.role != "VERIFICATION"){
        removeLocalStorage("token");
        window.location.href = "/timeout";
      }
    }

    return () => {};
  }, [type, role]);

  useEffect(() => {
    setStatusData(statusOption);
    setLawId(id);
    getAssess();
    return () => {};
  }, []);

  const toggleRemark = () => {
    setCancelRemark("");
    setModalRemark(!modalRemark);
  }

  const handleMainLawId = (idx) => {
    setMainLawId(idx);
  };

  const handleTabAddress = (idx) => {
    setTabAddress(idx);
  };
  
  const saveDataMainLaw = async (data) => {
    try {
        const result = await Api.createMainLaw(data);
        if (result.status === 200) {
            const { data } = result.data;
            window.location.href = "/home/lawinfo/approve1/search";
        }
    } catch (error) {

    }
  }

  const saveDataHowTo = async (data) => {
    try {
        const result = await Api.createMainLawSub(data);
        if (result.status === 200) {
            const { data } = result.data;
            window.location.href = "/home/lawinfo/approve1/search";
        }
    } catch (error) {

    }
  }
  
  const validateDataHeader = async (data) => {
    try {
      if (data.type == "APPROVE"){
        data.SystemStatus = "SEA2";
        saveDataHeader(data);
      }
      else if (data.type == "REJECT") {
        data.SystemStatus = "REDI";
        setModalRemark(true);
        // setModal(true);
        setModalSaveHeader(data);
      }
      else if (data.type == "REJECTCLOSE") {
        data.SystemStatus = "RJCL";
        setModalRemark(true);
        // setModal(true);
        setModalSaveHeader(data);
      }else if (data.type == "CANCEL") {
        window.location.href = "/home/lawinfo/approve1/search";
      }else if (data.type == "CLOSE2"){
        data.SystemStatus = "CLS2";
        saveDataHeader(data);
      }
    } catch (error) {

    }
  }
  
  const onSubmitRemark = async () => {
    try {
      if (cancelRemark == null || cancelRemark.length == 0){
        Swal.fire({
          icon: "error",
          title: "ERROR",
          text: "กรุณากรอกหมายเหตุ"
        });
        return false;
      }
      modelSaveHeader.RejectReason = cancelRemark;
      saveDataHeader(modelSaveHeader);
    } catch (error) {

    }
  }
  
  const saveDataHeader = async (data) => {
    try {
      Swal.showLoading();
      const result = await Api.saveAssess(data);
      Swal.close();
      if (result.status === 200) {
            const { data } = result.data;
            Swal.fire({
              icon: "success",
              title: "Save Success",
            });
            window.location.href = "/home/lawinfo/approve1/search";
        }
    } catch (error) {
      if (error.response.data.statusCode && error.response.data.statusCode == 550){
        Swal.fire({
          icon: "error",
          title: "ERROR",
          text: `Send Email Error ${error.response.data.message}`
        });
        window.location.href = "/home/lawinfo/approve1/search";
      }else{
        Swal.fire({
          icon: "error",
          title: "ERROR",
          text: error.response.data
        });
        if(error.response.data == "Send Email Error"){
          window.location.href = "/home/lawinfo/approve1/search";
        }
      }
    }
  };

  const getAssess = async () => {
    try {
      const model = {
        assessId: assessId,
        user: userState.id,
      };
      const result = await Api.getAssessmentDetail(model);
      if (result.status == 200) {
        var res = result.data[0];
        setMainLawName(res.mainLawName);
        setLawDescription(res.description);
        setEstimate(res.estimateResult);
        setDescription(res.estimateDesc);
        setRejectReason(res.rejectReason);
        setLogTable(res.log);
        setSystemStatus(res.systemStatus);
        setBrcsDate(res.brcsDate);
        setLawGroupSub(res.lawGroupSub);
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <h2>ทวนสอบเอกสาร</h2>
      <CCard fluid className="mt-3">
        <CCardHeader>
          <CRow>
            <CCol sm="12">
              ข้อมูลกฏหมาย
            </CCol>
          </CRow>
          <div style={{ fontSize: "12px" }}>
            <CreatHeadPanel id={id} systemStatus={systemStatus} assessId={assessId} validateDataHeader={validateDataHeader} code={code} page={"approve1"}/>
          </div>
        </CCardHeader>
        <CCardBody>
          <CTabs
            activeTab={tabAddress}
            onActiveTabChange={(idx) => handleTabAddress(idx)}
          >
            <CNav variant="tabs">
              <CNavItem>
                <CNavLink data-tab="1">
                  <b>บันทึกผลการประเมิน</b>
                </CNavLink>
              </CNavItem>
              {/* <CNavItem>
                <CNavLink data-tab="1">
                  <b>กฎหมายหลัก</b>
                </CNavLink>
              </CNavItem> */}
              {/* <CNavItem>
                <CNavLink data-tab="2" disabled>
                  <b>วิธีการดำเนินงาน/แผนการดำเนินงาน</b>
                </CNavLink>
              </CNavItem> */}
              <CNavItem>
                <CNavLink data-tab="2">
                  <b>แนบไฟล์ {fileCount}</b>
                </CNavLink>
              </CNavItem>
            </CNav>
            <CTabContent>
              <CTabPane data-tab="1">
                <Estimate
                  assessId={assessId}
                  // onSubmitEstimate={onSubmitEstimate}
                  page={"approve1"}
                  description={description}
                  lawGroupSub={lawGroupSub}
                  brcsDate={brcsDate}
                  estimate={estimate}
                  lawDescription={lawDescription}
                  mainLawName={mainLawName}
                  rejectReason={rejectReason}
                  logTable={logTable}
                  systemStatus={systemStatus}
                />
              </CTabPane>
              {/* <CTabPane data-tab="1">
                <MainLaw
                  lawid={lawid}
                  handleMainLawId={handleMainLawId}
                  handleTabAddress={handleTabAddress}
                  saveDataMainLaw={saveDataMainLaw}
                />
              </CTabPane> */}
              {/* <CTabPane data-tab="2">
                <HowTo
                  modalCauseOpen={setModalCause}
                  modalDetailOpen={setModalDetail}
                  mainlawId={mainlawid}
                  saveDataHowTo={saveDataHowTo}
                  buCode={code}
                />
              </CTabPane> */}
              <CTabPane data-tab="2">
                <div>
                  <TableFiles assessId={assessId} page={"approve1"}  systemStatus={systemStatus} setFileCount={setFileCount} />
                </div>
              </CTabPane>
            </CTabContent>
          </CTabs>
        </CCardBody>
      </CCard>
      <EmployeeModal modal={modal} setModal={setModal} />
      <DetailModal modal={modalDetail} setModal={setModalDetail} />
      <CauseModal modal={modalCause} setModal={setModalCause} />
      <CModal
                show={modalRemark}
                onClose={toggleRemark}
            >
                <CForm onSubmit={onSubmitRemark} action="" >
                    <CModalBody>
                        <CInput type="hidden" value={id} id="id"/>
                        <CRow>
                            <CCol xs="12" sm="12">
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="cancelRemark">หมายเหตุ</CLabel>
                                            <CTextarea onChange={e => setCancelRemark(e.target.value)} rows={8} value={cancelRemark} id="cancelRemark" placeholder="กรุณากรอก หมายเหตุ" />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                            </CCol>
                        </CRow >
                    </CModalBody>
                    <CModalFooter>
                        <CButton variant="outline" type="submit" color="primary">ตกลง</CButton>
                        <CButton
                            variant="outline"
                            color="secondary"
                            onClick={toggleRemark}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CForm >
            </CModal >
    </>
  );
};

export default CreateLawInfo;
