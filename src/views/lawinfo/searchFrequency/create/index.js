import React, { useEffect, useRef, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CForm,
  CContainer,
  CLabel,
  CSelect,
  CInput,
  CTabs,
  CTextarea,
} from "@coreui/react";
import Api from "../../../../api/ApiLawInfo";
import MainLaw from "../../component/mainlawTab";
import HowTo from "../../component/howtoTab";
import EmployeeModal from "../../component/employeeModal";
import FilePreview from "./../../../../assets/ตัวอย่าง ประเมินความสอดคล้อง (FM-SE-49).pdf";
import DetailModal from "../../component/detailModal";
import CauseModal from "../../component/causeModal";
import "./index.scss";
import CreatHeadPanel from "../../component/headpanelReadOnly";
import Swal from "sweetalert2/src/sweetalert2.js";
import { useDispatch, useSelector } from "react-redux";
import { fetchgetMainLaw } from "../../../../redux/mainLaw/mainLawAction";
import { saveButoDB } from "../../../../redux/saveBu/saveBuAction";
import { Upload, message, Button } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import { WEB_API } from "../../../../env";
import { saveDataToTransBrcsFileDB } from "../../../../redux/savetranbrcsfiledb/savetranbrsfileAction";
import { fetchgetMainLawSub } from "../../../../redux/mainLawSub/mainLawSubAction";
import TableFiles from "../../create/TableFiles";
const CreateAddFrequency = (props) => {
  var id = new URLSearchParams(props.location.search).get("id");
  var code = new URLSearchParams(props.location.search).get("code");
  var buCode = new URLSearchParams(props.location.search).get("buCode");
  const buAssignData = useSelector((state) => state.buassignReducer.data);
  const buAssignSelectorData = useSelector(
    (state) => state.buassignSelectorReducer.data
  );
  const [monthlistState, setmonthlistState] = useState([]);
  const lawinfoData = useSelector((state) => state.lawinfoReducer.data);
  const [mainlawState, setmainlawState] = useState([]);
  const [mainlawSubState, setmainlawSubState] = useState([]);
  const userData = useSelector((state) => state.changeState.user);
  const userStateToken = useSelector((state) => state.changeState.token);
  const [tabAddress, setTabAddress] = useState("1");
  const [lawid, setLawId] = useState(0);
  const [mainlawid, setMainLawId] = useState();
  const [modal, setModal] = useState(false);
  const [modalDetail, setModalDetail] = useState(false);
  const [modalCause, setModalCause] = useState(false);
  const [modelSaveHeader, setModelSaveHeader] = useState([]);
  const [isHideCancelRemark, setHideCancelRemark] = useState(true);
  const [systemStatus, setSystemStatus] = useState();
  const [maimlawFlag, setmainlawFlag] = useState(false);
  const [maimlawFlagSub, setmainlawFlagSub] = useState(false);
  const [idSub, setidSub] = useState(0);
  const [mainlawSubData, setmainlawSubData] = useState([]);
  const [headerData, setHeaderData] = useState([]);
  const [fileCount, setFileCount] = useState(0);
  const mainlawsSubState = useSelector((state) => {
    return state.mainLawsSub.loaded === true && state.mainLawsSub.mainlawsSub;
  });
  const mainlawsState = useSelector((state) => {
    return state.mainLaws.loaded === true && state.mainLaws.mainlaws;
  });
  const tranbrsBuState = useSelector((state) => state.gettranbrcsbu.data);
  const [statusData, setStatusData] = useState([]);
  const buttonOption = [
    // { value: "SEEV", label: "Send To Evaluate" },
    // { value: "REFQ", label: "Reject To FQO" },
  ];
  const statusOption = [
    { value: "SADR", label: "Save" },
    { value: "NEDO", label: "Submit" },
    { value: "CACE", label: "Delete" },
    { value: "SEEV", label: "Send To Evaluate" },
    { value: "REFQ", label: "Reject To FQO" },
    { value: "SEA2", label: "Send to Approver 2" },
    { value: "DOAP", label: "Document Approve" },
    { value: "REDI", label: "Reject To User Division" },
  ];

  const dispatch = useDispatch();

  const fileprops = {
    name: "files",
    multiple: true,
    action: `${WEB_API}Master/SaveTranBrcsFile`,
    headers: {
      authorization: `Bearer ${userStateToken}`,
    },
    // action: `https://www.mocky.io/v2/5cc8019d300000980a055e76`,
    onChange(info) {
      const { status } = info.file;
      if (info.file.status !== "uploading") {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === "done") {
        saveToTranBrcsFileDB(info.file);
        message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === "error") {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };

  //  const returnMainLawSubAsync = async (value, bu_id) => {
  //    let resultss = await getMainLawSubPromise(value, bu_id);
  //    setmainlawSubData(resultss);
  //  };

  const getMainLawSubPromise = (value, bu_id) => {
    return new Promise((resolve, reject) => {
      setTimeout(async () => {
        let results = await Api.getMainLawSub(value, bu_id);
        if (results.data.status === 200) {
          let prepare = results.data.response;
          let result = [];
          prepare.length !== 0 &&
            prepare.forEach((data) => {
              if (data.monthList !== " ") {
                let monthParse = data && JSON.parse(data["monthList"]);
                result.push({
                  id: data.id,
                  seq: data.seq,
                  description: data.description,
                  descriptionlink: data.descriptionLink,
                  mainlawsub_buid: data.mainlawsubbuid,
                  estimate: data.estimateNo,
                  priority: data.priority,
                  month01: monthParse.month01,
                  month02: monthParse.month02,
                  month03: monthParse.month03,
                  month04: monthParse.month04,
                  month05: monthParse.month05,
                  month06: monthParse.month06,
                  month07: monthParse.month07,
                  month08: monthParse.month08,
                  month09: monthParse.month09,
                  month10: monthParse.month10,
                  month11: monthParse.month11,
                  month12: monthParse.month12,
                  lawGroupSubId: data.lawGroupSubId,
                  brcsDate: data.brcsDate,
                  brcsDateForPicker: data.brcsDateForPicker,
                });
              } else {
                result.push({
                  id: data.id,
                  seq: data.seq,
                  description: data.description,
                  descriptionlink: data.descriptionLink,
                  mainlawsub_buid: data.mainlawsubbuid,
                  estimate: data.estimateNo,
                  priority: data.priority,
                  month01: false,
                  month02: false,
                  month03: false,
                  month04: false,
                  month05: false,
                  month06: false,
                  month07: false,
                  month08: false,
                  month09: false,
                  month10: false,
                  month11: false,
                  month12: false,
                  lawGroupSubId: data.lawGroupSubId,
                  brcsDate: data.brcsDate,
                  brcsDateForPicker: data.brcsDateForPicker,
                });
              }
            });
          resolve(result);
        }
      }, 600);
    });
  };

  const saveToTranBrcsFileDB = (data) => {
    try {
      //lawinfoData.docno
      const beforeSave = {
        FileName: data.name,
        FilePath: data.response.url,
        CreateBy: userData.id,
        UpdateBy: userData.id,
        Docno: lawinfoData.docno,
      };
      dispatch(saveDataToTransBrcsFileDB(beforeSave));
      console.log(data);
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: error.message,
      });
    }
  };

  useEffect(() => {
    if (lawid === null) {
      dispatch(fetchgetMainLaw({}));
      dispatch(saveDataToTransBrcsFileDB({}));
    } else {
      returnMainLawAsync(id);
      setmainlawFlag(true);
      getMonthListAsync();
      // dispatch(fetchgetMainLaw(lawid));
    }

    setStatusData(statusOption);
    setLawId(id);

    return () => {};
  }, [0]);

  useEffect(() => {
    if (maimlawFlag !== false) {
      returnMainLawAsync(id);
    }
    if (maimlawFlagSub !== false) {
      returnMainLawSubAsync(idSub);
    }
    return () => {};
  }, [mainlawState, mainlawsSubState]);

  const getMonthListPromise = () => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        let response = Api.getAssesmentMonthList(id);
        console.log("getMonthListAsync", response);
        if (response.status === 200) {
          resolve(response.data);
        }
      }, 600);
    });
  };

  const getMonthListAsync = async () => {
    let results = await getMonthListPromise();

    setmonthlistState(results);
  };

  const getMainLaw = (value) => {
    return new Promise((resolve, reject) => {
      return setTimeout(() => {
        dispatch(fetchgetMainLaw(value));
        resolve(mainlawsState);
      }, 1500);
    }).then(() => {
      Swal.close();
    });
  };
  const getMainLawSub = (value, bu_id) => {
    return new Promise((resolve, reject) => {
      return setTimeout(() => {
        dispatch(fetchgetMainLawSub(value, bu_id));
        resolve(mainlawsSubState);
      }, 600);
    }).then(() => {
      Swal.close();
    });
  };

  const returnMainLawAsync = async (value) => {
    let results = await getMainLaw(value);
    setmainlawState(results);
    setmainlawFlag(false);
  };

  const returnMainLawSubAsync = async (value, bu_id) => {
    let results = await getMainLawSubPromise(value, bu_id);
    console.log("getMainLawSubPromise", results);
    setmainlawSubData(results);
    setmainlawFlagSub(false);
  };

  const handleMainLawId = (idx) => {
    setMainLawId(idx);
    setidSub(idx);
    if (code === null && buCode == null) {
      returnMainLawSubAsync(idx, 0);
    } else {
      returnMainLawSubAsync(idx, buCode);
    }
  };

  const handleTabAddress = (idx) => {
    setTabAddress(idx);
  };

  const saveDataMainLaw = async (data) => {
    try {
      const result = await Api.createMainLaw(data);
      if (result.status === 200) {
        const { data } = result.data;
        window.location.href = "/home/lawinfo/addfrequency/search";
      }
    } catch (error) {}
  };
  const promiseMainLawSub = (values) => {
    return new Promise((resolve, reject) => {
      return setTimeout(() => {
        Api.createMainLawSub(values);
        resolve("success");
      }, 600);
    });
  };
  const saveMainlawSubwithCode = async (values) => {
    let results = await promiseMainLawSub(values);
    if (results === "success") {
      if (code === null && buCode === null) {
        returnMainLawSubAsync(values.bucode, 0);
      }
      if (code !== null && buCode !== null) {
        returnMainLawSubAsync(values.bucode, buCode);
      }
      setmainlawFlagSub(true);
    }
  };

  const saveDataHowTo = async (data) => {
    try {
      if (id !== null && code !== null) {
        const result = await Api.createMainLawSub(data);
        
        if (result.status === 200) {
          const { data } = result.data;
          Swal.fire({
            icon: "success",
            title: "Success",
          });
          // history.push("/home/lawinfo/create?id=3");
          //  history.goBack();
          // setTabAddress("1");
          returnMainLawSubAsync(mainlawid, buCode);
          // dispatch(fetchgetMainLawSub(mainlawid, buCode));

          // window.location.href = "/home/lawinfo/create?id=3";
        }
      }
      if (id !== null && code === null) {
        let tranBrcsData = [];
        tranBrcsData = tranbrsBuState;
        let count = 0;
        tranBrcsData.forEach(async (res, i) => {
          let datas = {
            id: data.editflag === true ? data.id : data.id,
            MainlawId: data.MainlawId,
            Seq: data.Seq,
            description: data.description,
            descriptionlink: data.descriptionlink,
            estimateNo: data.estimateNo,
            priority: data.priority,
            user: data.user,
            month_list: data.month_list,
            bucode: res.bU_ID,
            count: i + 1,
            editflag: data.editflag,
          };
          await saveMainlawSubwithCode(datas);
        });
      }
    } catch (error) {}
  };

  const validateDataHeader = async (data) => {
    try {
      setSystemStatus(data.SystemStatus);
      if (data.SystemStatus == "SEEV") {
        setModal(true);
      } else {
        saveDataHeader(data);
      }
    } catch (error) {}
  };

  const validateDataHeaderAfterBu = async () => {
    try {
      if (systemStatus == "SEEV") {
        saveBuAssignData(buAssignData);
      } else {
        saveDataHeader(modelSaveHeader);
      }
    } catch (error) {}
  };

  const saveBuAssignData = (asda) => {
    try {
      console.log("asda", asda);
      let beforSav = [];
      let buassignFilter = [];
      var freeze = Object.freeze(asda);
      let filterMianlaw = [];
      filterMianlaw = mainlawsState;
      filterMianlaw = filterMianlaw.filter(
        (fil) => fil.brcsId === parseInt(id)
      );
      buAssignSelectorData.forEach((res) => {
        let result = freeze.filter((item) => {
          console.log(item);
          return item.key === res;
        });
        buassignFilter = result;
      });
      if (buassignFilter.length > 0) {
        buassignFilter.forEach((res) => {
          beforSav.push({
            ...res,
            docno: lawinfoData.docno,
            user_id: userData.id,
          });
        });
        if (beforSav.length > 0) {
          beforSav.forEach((data) => {
            console.log("filterMianlaw", filterMianlaw);
            console.log("data", data);
            const saveData = {
              Docno: data.docno,
              User_create: userData.id,
              freq_id: id,
              // Mainlawsubid: filterMianlaw[0].id,
              mainlaw_id: filterMianlaw[0].id,
              plant_id: data.plantid,
              bu_assign_id: data.id,
            };
            dispatch(saveButoDB(saveData));
          });
        }
        window.location.href = "/home/lawinfo/addfrequency/search";
      }
    } catch (error) {}
  };

  const saveDataHeader = async (data) => {
    try {
      let dataParam = data;
      if (data.buList.length > 0) {
        dataParam.BuIdList = data.buList.map((x) => x.value).join(",");
      }
      const result = await Api.create(data);
      if (result.status === 200) {
        const { data } = result.data;
        Swal.fire({
          icon: "success",
          title: "Success",
        });
        dispatch(fetchgetMainLaw(lawid));
        window.location.href = "/home/lawinfo/addfrequency/search";
      } else {
        Swal.fire({
          icon: "error",
          title: "Process Error!",
          text: "Please choose system status!",
        });
      }
    } catch (error) {}
  };

  return (
    <>
      <h2>{userData.role === "FQO" ? "สร้างข้อมูลกฏหมาย" : "ใส่ความถี่"}</h2>
      <CCard fluid className="mt-3">
        <CCardHeader>
          <CRow>
            <CCol sm="12">ข้อมูลกฏหมาย</CCol>
          </CRow>
          <div style={{ fontSize: "12px" }}>
            <CreatHeadPanel
              statusOption={statusData}
              buttonOption={buttonOption}
              id={id}
              validateDataHeader={validateDataHeader}
              isHideCancelRemark={isHideCancelRemark}
              setHeaderData={setHeaderData}
              code={code}
            />
          </div>
        </CCardHeader>
        <CCardBody>
          <CTabs
            activeTab={tabAddress}
            onActiveTabChange={(idx) => handleTabAddress(idx)}
          >
            <CNav variant="tabs">
              <CNavItem>
                <CNavLink data-tab="1">
                  <b>กฎหมายหลัก</b>
                </CNavLink>
              </CNavItem>
              {userData.role === "FQO" && (
                <CNavItem>
                  <CNavLink data-tab="2" disabled>
                    <b>วิธีการดำเนินงาน/แผนการดำเนินงาน</b>
                  </CNavLink>
                </CNavItem>
              )}

              <CNavItem>
                <CNavLink data-tab="3">
                  <b>แนบไฟล์ ({fileCount})</b>
                </CNavLink>
              </CNavItem>
            </CNav>
            <CTabContent>
              <CTabPane data-tab="1">
                <MainLaw
                  lawid={lawid}
                  handleMainLawId={handleMainLawId}
                  handleTabAddress={handleTabAddress}
                  saveDataMainLaw={saveDataMainLaw}
                  mainlawsStates={code !== null ? mainlawState : mainlawsState}
                  buCode={buCode}
                  isView={true}
                  headerData={headerData}
                />
              </CTabPane>
              {/* {userData.role === "FQO" && (
                <CTabPane data-tab="2">
                  <HowTo
                    modalCauseOpen={setModalCause}
                    modalDetailOpen={setModalDetail}
                    mainlawId={mainlawid}
                    saveDataHowTo={saveDataHowTo}
                    mainLawsSubDatas={mainlawSubData}
                    buCode={code}
                  />
                </CTabPane>
              )} */}

              <CTabPane data-tab="3">
                {/* <div style={{ paddingTop: "1pc" }}>
                  <Upload {...fileprops} maxCount={5}>
                    <Button icon={<UploadOutlined />}>แนบไฟล์</Button>
                    <div style={{ paddingTop: "0.5pc" }}>
                      <label style={{ color: "red", fontSize: "12px" }}>
                        * สามารถอัพโหลดไฟล์ ประเภท
                        .xls,.xlsx,.pdf,.xml,.doc,.docx,.bmp,.gif,.jpg,.jpeg,.png
                        ได้ไม่เกิน 5 ไฟล์ ขนาดไม่เกิน 5 เมกะไบท์
                      </label>
                    </div>
                  </Upload>
                </div> */}
                <TableFiles id={id} code={code} buCode={buCode} page={"view"} setFileCount={setFileCount} />
              </CTabPane>
            </CTabContent>
          </CTabs>
        </CCardBody>
      </CCard>
      <EmployeeModal
        modal={modal}
        setModal={setModal}
        validateDataHeaderAfterBu={validateDataHeaderAfterBu}
      />
      <DetailModal modal={modalDetail} setModal={setModalDetail} />
      <CauseModal modal={modalCause} setModal={setModalCause} />
    </>
  );
};

export default CreateAddFrequency;
