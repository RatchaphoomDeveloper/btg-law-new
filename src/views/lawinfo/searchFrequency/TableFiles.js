import React, { useEffect, useCallback, useState } from "react";
import { CDataTable, CCard, CCardBody, CButton } from "@coreui/react";
import Api from "../../../api/ApiLawInfo";
import { saveDataToTransBrcsFileDB } from "../../../redux/savetranbrcsfiledb/savetranbrsfileAction";
import { WEB_API } from "../../../env";
import { useDispatch, useSelector } from "react-redux";
import { Upload, message, Button } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import Swal from "sweetalert2/dist/sweetalert2.js";

const fields = [
  { key: "id", _style: { width: "1%" } },
  { key: "filename", _style: { width: "40%" } },
  // "registered",
  // { key: "role", _style: { width: "20%" } },
  // { key: "status", _style: { width: "20%" } },
  // {
  //   key: "show_details",
  //   label: "",
  //   _style: { width: "1%" },
  //   sorter: false,
  //   filter: false,
  // },
];
const TableFiles = ({ id, code, buCode }) => {
  const [fileList, setFileList] = useState([]);
  const userState = useSelector((state) => state.changeState.user);
  const userStateToken = useSelector((state) => state.changeState.token);
  const lawinfoData = useSelector((state) => state.lawinfoReducer.data);
  const [successFileFlag,setsuccessFileFlag] = useState(false)

  const dispatch = useDispatch();
  useEffect(() => {
    getFileAsync(id);
    setsuccessFileFlag(false);
    return () => {};
  }, []);

  useEffect(() => {
    if (successFileFlag === true) {
      getFileAsync(id);
    }
  }, [successFileFlag]);

  const getFilePromise = (val) => {
    return new Promise((resolve, reject) => {
      setTimeout(async () => {
        let results = await Api.getTranBrcsFileSel(val);
        if (results.status === 200) {
          resolve(results.data.response);
          setsuccessFileFlag(false)
        }
      }, 600);
    });
  };
  const getFileAsync = async (val) => {
    let results = await getFilePromise(val);
    setFileList(results);
    
  };

  const fileprops = {
    name: "files",
    multiple: true,
    action: `${WEB_API}Master/SaveTranBrcsFile`,
    headers: {
      authorization: `Bearer ${userStateToken}`,
    },
    // action: `https://www.mocky.io/v2/5cc8019d300000980a055e76`,
    onChange(info) {
      const { status } = info.file;
      if (info.file.status !== "uploading") {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === "done") {
        saveToTranBrcsFileDB(info.file);
        message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === "error") {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };

  const saveToTranBrcsFileDB = (data) => {
    try {
      //lawinfoData.docno
      const beforeSave = {
        FileName: data.name,
        FilePath: data.response.url,
        CreateBy: userState.id,
        UpdateBy: userState.id,
        Docno: lawinfoData.docno,
      };
      dispatch(saveDataToTransBrcsFileDB(beforeSave));
      console.log(data);
      setsuccessFileFlag(true);
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: error.message,
      });
    }
  };
  return (
    <div>
      <div style={{ paddingTop: "1pc" }}>
        <Upload {...fileprops} maxCount={1}  showUploadList={false}>
          <Button icon={<UploadOutlined />}>แนบไฟล์</Button>
          <div style={{ paddingTop: "0.5pc" }}>
            <label style={{ color: "red", fontSize: "12px" }}>
              * สามารถอัพโหลดไฟล์ ประเภท
              .xls,.xlsx,.pdf,.xml,.doc,.docx,.bmp,.gif,.jpg,.jpeg,.png
              ได้ไม่เกิน 5 ไฟล์ ขนาดไม่เกิน 5 เมกะไบท์
            </label>
          </div>
        </Upload>
      </div>
      {fileList && (
        <CDataTable
          items={fileList}
          fields={fields}
          tableFilter
          itemsPerPageSelect
          itemsPerPage={10}
          hover
          sorter
          pagination
          scopedSlots={{}}
        />
      )}
    </div>
  );
};

export default TableFiles;
