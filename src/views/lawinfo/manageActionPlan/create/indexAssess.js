import React, { useEffect, useRef, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CForm,
  CContainer,
  CLabel,
  CSelect,
  CInput,
  CTabs,
  CTextarea,
  CModalBody,
  CFormGroup,
} from "@coreui/react";
import Api from "../../../../api/ApiLawInfo";
// import MainLaw from "../../component/mainlawTab";
// import HowTo from "../../component/howtoTab";
import Estimate from "../../component/estimateTab";
import TableFiles from "../../component/TableFilesAssessment";
import "./index.scss";
import CreatHeadPanel from "../../component/headpanelAssess";
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2/src/sweetalert2.js";
import { removeLocalStorage } from "../../../../utils/localStorage";

const ManageActionPlanDetail = (props) => {
  const userState = useSelector((state) => state.changeState.user);
  var id = new URLSearchParams(props.location.search).get("id");
  var code = new URLSearchParams(props.location.search).get("code");
  var assessId = new URLSearchParams(props.location.search).get("assessId");
  var type = new URLSearchParams(props.location.search).get("type");
  var role = new URLSearchParams(props.location.search).get("role");
  const [tabAddress, setTabAddress] = useState("1");
  const [lawid, setLawId] = useState(0);
  const [mainlawid, setMainLawId] = useState();
  const [modelSaveHeader, setModalSaveHeader] = useState([]);
  const [isHideCancelRemark, setHideCancelRemark] = useState(true);
  const [cancelRemark, setCancelRemark] = useState("");
  const estimateRef = useRef(null);

  const [systemStatus, setSystemStatus] = useState([]);

  const [mainLawName, setMainLawName] = useState([]);
  const [lawDescription, setLawDescription] = useState([]);
  const [lawGroupSub, setLawGroupSub] = useState("");
  const [brcsDate, setBrcsDate] = useState("");
  const [estimate, setEstimate] = useState([]);
  const [description, setDescription] = useState([]);
  const [rejectReason, setRejectReason] = useState([]);
  const [logTable, setLogTable] = useState([]);

  const [modalCause, setModalCause] = useState(false);
  const [causeSeq, setCauseSeq] = useState([]);
  const [causeDesc, setCauseDesc] = useState([]);
  const [causeNote, setCauseNote] = useState([]);

  const [statusData, setStatusData] = useState([]);
  const statusOption = [
    { value: "SEEV", label: "Save Draft" },
    { value: 'SEA1', label: 'Send To Verification'},
    // { value: "REBU", label: "Reject To BU" },
  ];
  const dispatch = useDispatch();

  useEffect(() => {
    if (type == "email"){
      if (role == "user" && userState.role != "USER"){
        removeLocalStorage("token");
        window.location.href = "/timeout";
      }
    }

    return () => {};
  }, [type, role]);

  useEffect(() => {
    setStatusData(statusOption);
    setLawId(id);
    getAssess();
    return () => {};
  }, []);

  const toggleCause = () => {
    setModalCause(!modalCause);
  };

  const handleTabAddress = (idx) => {
    setTabAddress(idx);
  };

  const validateDataHeader = async (data) => {
    try {
      if (data.type == "APPROVE") {
        data.SystemStatus = "SEA1";
        saveDataHeader(data);
      } else if (data.type == "SAVE") {
        data.SystemStatus = "SEEV";
        // setModalRemark(true);
        // setModal(true);
        saveDataHeader(data);
      } else if (data.type == "CANCEL") {
        window.location.href = "/home/lawinfo/manageActionPlan/search";
      } else if (data.type == "CLOSE"){
        data.SystemStatus = "CLOS";
        saveDataHeader(data);
      } else if (data.type == "CLOSENEW"){
        data.SystemStatus = "CLNE";
        saveDataHeader(data);
      } else if (data.type == "CLOSE2"){
        data.SystemStatus = "CLS2";
        saveDataHeader(data);
      }
    } catch (error) {}
  };

  const saveDataHeader = async (data) => {
    try {
      const result = await Api.saveAssess(data);
      if (result.status === 200) {
        const { data } = result.data;
        Swal.fire({
          icon: "success",
          title: "Save Success",
        });
        window.location.href = "/home/lawinfo/manageActionPlan/search";
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "ERROR",
        text: error.response.data
      });
    }
  };

  const onSubmitEstimate = async (model) => {
    try {
      Swal.showLoading();
      const oldData = model
      console.log("dataBEFSave", oldData);
      const result = await Api.saveEstimate(model);
      Swal.close();
      if (result.status === 200) {
        const { data } = result.data;
        // toggleEstimate();
        Swal.fire({
          icon: "success",
          title: "Save Success",
        });
        console.log("dataAFTSave", oldData);
        if(oldData.systemStatus == "SEEV"){
          window.location.reload();
        }else{
          window.location.href = "/home/lawinfo/manageActionPlan/search";
        }
      }
    } catch (error) {
      Swal.close();
      console.log(error);
      if (error.response.data.statusCode && error.response.data.statusCode == 550){
        Swal.fire({
          icon: "error",
          title: "ERROR",
          text: `Send Email Error ${error.response.data.message}`
        });
        window.location.href = "/home/lawinfo/manageActionPlan/search";
      }else{
        Swal.fire({
          icon: "error",
          title: "ERROR",
          text: error.response.data
        });
        if(error.response.data == "Send Email Error"){
          window.location.href = "/home/lawinfo/manageActionPlan/search";
        }
      }
    }
  };

  const onSubmitVerify = async (e, status) => {
    e.preventDefault();
    if (
      estimate == "" ||
      description == ""
    ) {
      Swal.fire({
        icon: "error",
        title: "Process Error!",
        text: "กรุณากรอกข้อมูลให้ครบ !",
      });
      return false;
    }

    var sysStatus = "SEA1";
    if (status == "SAVE") {sysStatus = "SEEV"}
    const model = {
      id: assessId,
      systemStatus: sysStatus,
      estimateResult: estimate,
      estimateDesc: description,
      user: userState.id,
    };
    onSubmitEstimate(model);
  };

  const getAssess = async () => {
    try {
      const model = {
        assessId: assessId,
        user: userState.id,
      };
      const result = await Api.getAssessmentDetail(model);
      if (result.status == 200) {
        var res = result.data[0];
        setMainLawName(res.mainLawName);
        setLawDescription(res.description);
        setEstimate(res.estimateResult);
        setDescription(res.estimateDesc);
        setRejectReason(res.rejectReason);
        setLogTable(res.log);
        setSystemStatus(res.systemStatus);
        setBrcsDate(res.brcsDate);
        setLawGroupSub(res.lawGroupSub);
        console.log("log", logTable);
        // console.log("res.systemStatus", res.systemStatus);
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <h2>บันทึกผลการประเมิน</h2>
      <CCard fluid className="mt-3">
        <CCardHeader>
          <CRow>
            <CCol sm="12">ข้อมูลกฏหมาย</CCol>
          </CRow>
          <div style={{ fontSize: "12px" }}>
            <CreatHeadPanel
              onSubmitVerify={onSubmitVerify}
              id={id}
              assessId={assessId}
              validateDataHeader={validateDataHeader}
              code={code}
              page={"estimate"}
              systemStatus={systemStatus}
            />
          </div>
        </CCardHeader>
        <CCardBody>
          <CTabs
            activeTab={tabAddress}
            onActiveTabChange={(idx) => handleTabAddress(idx)}
          >
            <CNav variant="tabs">
              <CNavItem>
                <CNavLink data-tab="1">
                  <b>บันทึกผลการประเมิน</b>
                </CNavLink>
              </CNavItem>
              <CNavItem>
                <CNavLink data-tab="2">
                  <b>แนบไฟล์</b>
                </CNavLink>
              </CNavItem>
            </CNav>
            <CTabContent>
              <CTabPane data-tab="1">
                <Estimate
                  assessId={assessId}
                  page={"estimate"}
                  description={description}
                  lawGroupSub={lawGroupSub}
                  brcsDate={brcsDate}
                  estimate={estimate}
                  lawDescription={lawDescription}
                  mainLawName={mainLawName}
                  rejectReason={rejectReason}
                  setDescription={setDescription}
                  setEstimate={setEstimate}
                  setLawDescription={setLawDescription}
                  setRejectReason={setRejectReason}
                  logTable={logTable}
                  systemStatus={systemStatus}
              />
              </CTabPane>
              <CTabPane data-tab="2">
                <div>
                  <TableFiles assessId={assessId} page={"estimate"} systemStatus={systemStatus}/>
                </div>
              </CTabPane>
            </CTabContent>
          </CTabs>
        </CCardBody>
      </CCard>
      {/* <CModal show={modalCause} onClose={toggleCause}>
        <CForm onSubmit={onSubmitCause} action="">
          <CModalBody>
            <CInput type="hidden" value={causeId} id="causeId" />
            <CRow>
              <CCol xs="12" sm="12">
                <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel htmlFor="causeSeq">ลำดับที่</CLabel>
                      <CInput
                        onChange={(e) => setCauseSeq(e.target.value)}
                        value={causeSeq}
                        id="causeSeq"
                        type="number"
                        pattern="[0-9]*"
                        inputMode="numeric"
                        min="1"
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel htmlFor="causeDesc">รายละเอียดการแก้ไข</CLabel>
                      <CTextarea
                        onChange={(e) => setCauseDesc(e.target.value)}
                        rows={8}
                        value={causeDesc}
                        id="causeDesc"
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel htmlFor="causeNote">หมายเหตุ</CLabel>
                      <CTextarea
                        onChange={(e) => setCauseNote(e.target.value)}
                        rows={8}
                        value={causeNote}
                        id="causeNote"
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>
              </CCol>
            </CRow>
          </CModalBody>
          <CModalFooter>
            <CButton variant="outline" type="submit" color="primary">
              ตกลง
            </CButton>
            <CButton variant="outline" color="secondary" onClick={toggleCause}>
              ยกเลิก
            </CButton>
          </CModalFooter>
        </CForm>
      </CModal> */}
    </>
  );
};

export default ManageActionPlanDetail;
