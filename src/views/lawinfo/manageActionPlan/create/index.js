import React, { useEffect, useRef, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CForm,
  CContainer,
  CLabel,
  CSelect,
  CInput,
  CTabs,
  CTextarea,
} from "@coreui/react";
import Api from "../../../../api/ApiLawInfo";
import MainLaw from "../../component/mainlawTab";
import HowTo from "../../component/howtoTab";
import EmployeeModal from "../../component/employeeModal";
import FilePreview from "./../../../../assets/ตัวอย่าง ประเมินความสอดคล้อง (FM-SE-49).pdf";
import DetailModal from "../../component/detailModal";
import CauseModal from "../../component/causeModal";
import "./index.scss";
import CreatHeadPanel from "../../component/headpanel";

const CreateAssessConsistency = (props) => {
  var id = new URLSearchParams(props.location.search).get("id");
  const [tabAddress, setTabAddress] = useState("1");
  const [lawid, setLawId] = useState(0);
  const [mainlawid, setMainLawId] = useState();
  const [modal, setModal] = useState(false);
  const [modalDetail, setModalDetail] = useState(false);
  const [modalCause, setModalCause] = useState(false);
  const [modelSaveHeader, setModalSaveHeader] = useState([]);
  const [isHideCancelRemark, setHideCancelRemark] = useState(true);

  const [statusData, setStatusData] = useState([]);
  const statusOption = [
    { value: "SADR", label: "Save" },
    { value: "NEDO", label: "Submit" },
    { value: "CACE", label: "Delete" },
    { value: "SEEV", label: "Send To Evaluate" },
    { value: "REFQ", label: "Reject To FQO" },
    { value: 'SEA1', label: 'Send To Verification'},
    { value: "SEA2", label: "Send to Approver" },
    { value: "DOAP", label: "Document Approve" },
    { value: "REDI", label: "Reject To User Division" },
  ];
  const buttonOption = [
    { value: "EVAU", label: "Send To Approver 1" },
    // { value: "REBU", label: "Reject To BU" },
  ];

  useEffect(() => {
    setStatusData(statusOption);
    setLawId(id);
    return () => {};
  }, []);

  const handleMainLawId = (idx) => {
    setMainLawId(idx);
  };

  const handleTabAddress = (idx) => {
    setTabAddress(idx);
  };

  const saveDataMainLaw = async (data) => {
    try {
      const result = await Api.createMainLaw(data);
      if (result.status === 200) {
        const { data } = result.data;
        window.location.href = "/home/lawinfo/assessConsistency/search";
      }
    } catch (error) {}
  };

  const saveDataHowTo = async (data) => {
    try {
      const result = await Api.createMainLawSub(data);
      if (result.status === 200) {
        const { data } = result.data;
        window.location.href = "/home/lawinfo/assessConsistency/search";
      }
    } catch (error) {}
  };

  const validateDataHeader = async (data) => {
    try {
      if (data.SystemStatus == "CACE") {
        setHideCancelRemark(true);
        // setModal(true);
        setModalSaveHeader(data);
      } else {
        saveDataHeader(data);
      }
    } catch (error) {}
  };

  const saveDataHeader = async (data) => {
    try {
      const result = await Api.create(data);
      if (result.status === 200) {
        const { data } = result.data;
        window.location.href = "/home/lawinfo/assessConsistency/search";
      }
    } catch (error) {}
  };

  return (
    <>
      <h2>สร้างข้อมูลกฏหมาย</h2>
      <CCard fluid className="mt-3">
        <CCardHeader>
          <CRow>
            <CCol sm="12">ข้อมูลกฏหมาย</CCol>
          </CRow>
          <div style={{ fontSize: "12px" }}>
            <CreatHeadPanel
              statusOption={statusData}
              buttonOption={buttonOption}
              id={id}
              validateDataHeader={validateDataHeader}
              isHideCancelRemark={isHideCancelRemark}
            />
          </div>
        </CCardHeader>
        <CCardBody>
          <CTabs
            activeTab={tabAddress}
            onActiveTabChange={(idx) => handleTabAddress(idx)}
          >
            <CNav variant="tabs">
              <CNavItem>
                <CNavLink data-tab="1">
                  <b>กฎหมายหลัก</b>
                </CNavLink>
              </CNavItem>
              <CNavItem>
                <CNavLink data-tab="2" disabled>
                  <b>วิธีการดำเนินงาน/แผนการดำเนินงาน</b>
                </CNavLink>
              </CNavItem>
              <CNavItem>
                <CNavLink data-tab="3">
                  <b>แนบไฟล์</b>
                </CNavLink>
              </CNavItem>
            </CNav>
            <CTabContent>
              <CTabPane data-tab="1">
                <MainLaw
                  lawid={lawid}
                  handleMainLawId={handleMainLawId}
                  handleTabAddress={handleTabAddress}
                  saveDataMainLaw={saveDataMainLaw}
                />
              </CTabPane>
              <CTabPane data-tab="2">
                <HowTo
                  modalCauseOpen={setModalCause}
                  modalDetailOpen={setModalDetail}
                  mainlawId={mainlawid}
                  saveDataHowTo={saveDataHowTo}
                />
              </CTabPane>
              <CTabPane data-tab="3">
                <div>แนบไฟล์</div>
              </CTabPane>
            </CTabContent>
          </CTabs>
        </CCardBody>
      </CCard>
      <EmployeeModal modal={modal} setModal={setModal} />
      <DetailModal modal={modalDetail} setModal={setModalDetail} />
      <CauseModal modal={modalCause} setModal={setModalCause} />
    </>
  );
};

export default CreateAssessConsistency;
