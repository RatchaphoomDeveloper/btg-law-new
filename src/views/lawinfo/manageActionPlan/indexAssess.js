import React, { useEffect, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CNav,
  CForm,
  CContainer,
  CLabel,
  CSelect,
  CInput,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import Api from "../../../api/ApiLawInfo";
import Tables from "./TablesAssess";
import SearchPanel from "../component/searchpanelAssess";
import DetailModal from "../component/detailModal";
import { useDispatch, useSelector } from "react-redux";

const ManageActionPlan = () => {
  const userState = useSelector((state) => state.changeState.user);
  const [dataTable, setDataTable] = useState([]);
  const [modalDetail, setModalDetail] = useState(false);
  const [assessmentId, setAssessmentId] = useState('');
  const [mainLawName, setMainLawName] = useState('');
  const [lawDescription, setLawDescription] = useState('');
  const [estimate, setEstimate] = useState('');
  const [description, setDescription] = useState('');
  const [searchData, setSearchData] = useState([]);

  const [statusData, setStatusData] = useState([]);
  const statusOption = [
    { value: "SEEV", label: "Send To Evaluate" },
    { value: "INPR", label: "InProcess" },
    { value: "REDI", label: "Reject To User Division" },
  ];

  useEffect(() => {
    setStatusData(statusOption);
    getData();
    return () => {};
  }, []);
  
  const toggleEstimate = () => {
      setModalDetail(!modalDetail);
  }

  const getData = async (data) => {
    try {
      if (data == undefined) {
        data = {
          "page": "manageap",
          "userId": userState.id,
          "Role": userState.role
        }
      }else{
        data.page = "manageap";
        data.userId = userState.id;
        data.Role = userState.role;
      }
      setSearchData(data);
      const result = await Api.getAssessment(data);
      if (result.status == 200) {
        setDataTable(result.data);
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <h2>ค้นหาข้อมูลกฎหมาย</h2>
      <CCard fluid className="mt-3">
        <CCardHeader>
          <div style={{ fontSize: "12px" }}>
            <SearchPanel getData={getData} statusOption={statusData} page={"manageap"} />
          </div>
        </CCardHeader>
        <CCardBody>
          <Tables data={dataTable} refreshData={getData} searchData={searchData} />
        </CCardBody>
      </CCard>
    </>
  );
};

export default ManageActionPlan;
