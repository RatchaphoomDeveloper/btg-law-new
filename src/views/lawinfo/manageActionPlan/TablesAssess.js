import React, { useState, useEffect } from 'react'
import {
    CCardBody,
    CBadge,
    CButton,
    CCollapse,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CCol,
    CCard,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CSelect,
    CForm,
    CModalTitle,
    CButtonGroup
} from '@coreui/react'
import { useDispatch, useSelector } from "react-redux";
import { getBrcsAssessmentDetail } from "../../../redux/getbrcaassessment/getbrcaassessmentAction";
import CIcon from '@coreui/icons-react'
import Api from "../../../api/ApiLawInfo";
import { WEB_API, WEB_FILE_URL } from "../../../env";

const getEstimate = est => {
    switch (est) {
        case '1': return 'ไม่เกี่ยวข้อง'
        case '2': return 'สอดคล้อง'
        case '3': return 'ไม่สอดคล้อง'
        case '4': return 'สอดคล้อง A'
        default: return ''
    }
}

const getStatus = status => {
    switch (status) {
        case 'SEEV': return 'Send To Evaluate'
        case 'REFQ': return 'Reject To FQO'
        case 'SEA1': return 'Send To Verification'
        case 'SEA2': return 'Send To Approver'
        case 'REDI': return 'Reject To User Division'
        case 'DOAP': return 'Document Approve'
        case 'INPR': return 'In Process'
        case 'CLNE': return 'Close New'
        case 'CLOS': return 'Complete Send To Verification'
        case 'CLS2': return 'Complete Send To Approver'
        case 'RJCL': return 'Reject Complete To User Division'
        default: return ''
    }
}


const getBadge = (status) => {
  switch (status) {
    case 'SEEV': return 'info'
    case 'REFQ': return 'dark'
    case 'SEA1': return 'warning'
    case 'SEA2': return 'danger'
    case 'REDI': return 'dark'
    case 'DOAP': return 'success'
    case 'INPR': return 'primary'
    case 'CLNE': return 'info'
    case 'CLOS': return 'warning'
    case 'CLS2': return 'danger'
    case 'RJCL': return 'dark'
  }
};

const fields = [
    // { key: 'assessmentNo', label: 'ASSESSMENT No' },
    { key: 'docno', label: 'เลขที่เอกสาร', _style: { width: '15%' } },
    // { key: 'docNoBu', label: 'เลขที่เอกสาร BU' },
    { key: 'mainLawName', label: 'ชื่อกฎหมาย' , _style: { width: '35%' }},
    { key: 'description', label: 'สาระสำคัญกฎหมาย' , _style: { width: '50%' } },
    // { key: 'valYear', label: 'ปีที่ประเมิน' },
    // { key: 'valMonth', label: 'เดือนที่ประเมิน' },
    // { key: 'userLogin', label: 'ผู้ประเมิน' },
    // { key: 'email', label: 'Email' },
    // { key: 'estimateResult', label: 'ผลการประเมิน' },
    // { key: 'estimateDesc', label: 'รายละเอียดผลการประเมิน' },
    // { key: 'option', label: '', _style: { width: '7%' }, filter: false, },
]

const subfields = [
    { key: 'assessmentNo', label: 'เลขที่ประเมิน', _style: { width: '10%' } },
    // { key: 'docno', label: 'เลขที่เอกสาร' },
    { key: 'docNoBu', label: 'เลขที่เอกสาร BU', _style: { width: '10%' } },
    // { key: 'mainLawName', label: 'ชื่อกฎหมาย' },
    // { key: 'description', label: 'สาระสำคัญกฎหมาย' },
    { key: 'valYear', label: 'ปี' , _style: { width: '3%' }},
    { key: 'valMonth', label: 'เดือน' , _style: { width: '2%' }},
    { key: 'userLogin', label: 'ผู้ประเมิน', _style: { width: '15%' }  },
    { key: 'email', label: 'Email', _style: { width: '15%' }},
    { key: 'estimateResult', label: 'ผลการประเมิน' , _style: { width: '10%' }},
    // { key: 'estimateDesc', label: 'รายละเอียดผลการประเมิน' },
    { key: 'systemStatus', label: 'สถานะ', _style: { width: '10%' }},
    { key: 'option', label: '', _style: { width: '8%' }, filter: false, },
    { key: "fileCount", label: "File", _style: { width: "7%" } },
  ]

  const fieldFiles = [
    // { key: "id", _style: { width: "40%" } },
    { key: "filetype", label: "ประเภทไฟล์" },
    { key: "filename" },
  ];


const Tables = ({ data = [], refreshData = () => {}, searchData}) => {
    const userState = useSelector((state) => state.changeState.user);
    const [details, setDetails] = useState([]);
  const [lastIndex, setLastIndex] = useState(null);
  const [modalFile, setModalFile] = useState(false);
  const [dataFileTable, setDataFileTable] = useState([]);
  const dispatch = useDispatch();
    const tranbuState = useSelector((state) => {
        console.log("state",state.getbrcaassessment);
        return state.getbrcaassessment.data;
    });

    useEffect(() => {
      setDetails([]);
      return () => {};
    }, [searchData]);

    const toggleDetails = (index) => {
      console.log("data", data);
      const position = data.indexOf(index);
      let newDetails = data.slice();
      if (lastIndex == index) {
        newDetails.splice(position, 1);
        setLastIndex(null);
      } else {
        newDetails = [...data, index];
        setLastIndex(index);
      }
      setDetails(newDetails);
      console.log("setDetail", details);
    };

    const getSubTable = (Docno, MainLawName, Desc) => {
      searchData.DetailDocno = Docno;
      searchData.DetailMainLaw = MainLawName;
      searchData.DetailDesc = Desc;
      try {
        dispatch(getBrcsAssessmentDetail(searchData));
      } catch (error) {
        console.log(error.message);
      }
    };

    const getFileList = (data) =>{
      getFileData(data.assessmentId);
      toggleFile();
    }
  
    const getFileData = async (id) => {
      try {
        const result = await Api.getAssessmentAllFile(id);
        if (result.status == 200) {
          setDataFileTable(result.data);
        }
      } catch (error) {
        console.log(error);
      }
    };
  
    const toggleFile = () => {
      setModalFile(!modalFile);
    }

    return (
        <>
            <CRow className="">
                <CCol xs="12" lg="12" >
                    <CDataTable
                        items={data}
                        fields={fields}
                        itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
                        itemsPerPage={10}
                        hover
                        sorter
                        striped
                        bordered
                        pagination
                        scopedSlots={{
                            docno: (item, index) => (
                              <td>
                                <label
                                  style={{ cursor: "pointer", color: "blue" }}
                                  onClick={() => {
                                    toggleDetails(index);
                                    getSubTable(item.docno, item.mainLawName, item.description);
                                  }}
                                >
                                  {details.includes(index) ? item.docno : item.docno}
                                </label>
                              </td>
                            ),
                            details: (item, index) => {
                              return (
                                <CCollapse show={details.includes(index)}>
                                  <CCardBody>
                                    <CDataTable
                                      items={tranbuState}
                                      fields={subfields}
                                      hover
                                      sorter
                                      striped
                                      bordered
                                      pagination
                                      scopedSlots={{
                                        systemStatus: (item) => (
                                            <td>
                                              
                                              <CBadge color={getBadge(item.systemStatus)}>
                                                    {getStatus(item.systemStatus)}
                                                </CBadge>
                                            </td>
                                        ),
                                        estimateResult: (item) => (
                                            <td>
                                                <CBadge>
                                                    {getEstimate(item.estimateResult)}
                                                </CBadge>
                                            </td>
                                        ),
                                        option: (items) => (
                                            <td className="center">
                                            <CButtonGroup>
                                                <CButton
                                                    color="success"
                                                    variant="outline"
                                                    // shape="square"
                                                    size="sm"
                                                    onClick={() => (window.location.href = `/home/lawinfo/manageActionPlan/create?id=${items.id}&code=${items.brcsBuId}&assessId=${items.assessmentId}`)}
                                                >
                                                    รายละเอียด
                                                </CButton>
                                                {items.fileCount > 0 ? 
                                                    <CButton variant="ghost" size="sm" />
                                                : ""}
                                                {items.fileCount > 0 ? 
                                                  <CButton
                                                    color="info"
                                                    variant="outline"
                                                    // shape="square"
                                                    size="sm"
                                                    onClick={() => { getFileList(items) }}
                                                  >
                                                    <CIcon name="cil-file" />Files
                                                  </CButton>
                                                : ""}
                                            </CButtonGroup>
                                        </td>
                                        ),
                                        fileCount: (items) => (
                                          <td>
                                            File : {items.fileCount}
                                          </td>
                                        ),
                                      }}
                                    ></CDataTable>
                                  </CCardBody>
                                </CCollapse>
                              );
                            },
                        }}
                    />
                </CCol>
            </CRow>
        <CModal show={modalFile} onClose={toggleFile}>
          <CModalBody>
            <CRow>
              <CCol sm={"12"}>
                <CDataTable
                  items={dataFileTable}
                  fields={fieldFiles}
                  // tableFilter
                  // itemsPerPageSelect
                  // itemsPerPage={10}
                  hover
                  sorter
                  pagination
                  scopedSlots={{
                    filename: (item, index) => (
                      <td>
                        
                        <a
                          target="_blank"
                          download=""
                          href={`${WEB_FILE_URL}${item.filepath}`}
                        >
                          {item.filename}
                        </a>
                      </td>
                    ),
                  }}
                />
              </CCol>
            </CRow>
          </CModalBody>
          <CModalFooter>
            <CButton variant="outline" color="dark" onClick={toggleFile}>
              ปิด
            </CButton>
          </CModalFooter>
        </CModal>
        </>
    )
}

export default Tables
