import React, { useState, useEffect } from "react";
import {
  CCardBody,
  CBadge,
  CButton,
  CCollapse,
  CDataTable,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CCol,
  CCard,
  CRow,
  CCardHeader,
  CFormGroup,
  CLabel,
  CInput,
  CSelect,
  CForm,
  CModalTitle,
  CButtonGroup,
  CInputCheckbox,
  CPopover,
  CTextarea,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { useSelector, useDispatch } from "react-redux";
import Swal from "sweetalert2/src/sweetalert2.js";
import Select from "react-select";
import ApiBu from "../../../api/ApiBU";
import Api from "../../../api/ApiLawInfo";

const fields = [
    { key: "select", label: "", filter: false },
    { key: "errorLog", label: "ERROR" },
    { key: "dummyCode", label: "Dummy Code" },
    { key: "mainlawSeq", label: "ลำดับกฎหมายหลัก" },
    { key: "mainlawName", label: "กฎหมายหลัก" },
    { key: "mainlawSubSeq", label: "ลำดับสาระสำคัญ" },
    { key: "mainlawSubName", label: "สาระสำคัญ" },
    { key: "mainlawSubDesc", label: "งานที่เกี่ยวข้อง" },
    { key: "lawgroupSubMl", label: "กลุ่มของกฎหมายย่อย" },
    { key: "brcsdateMl", label: "วันบังคับใช้กฎหมาย" },
    { key: "priority", label: "ลำดับความสำคัญ" },
    { key: "estimateno", label: "จำนวนครั้งที่ประเมิน" },
    { key: "monthList", label: "เดือนที่ประเมิน" },
    { key: "systemtype", label: "ระบบ" },
    { key: "countrytype", label: "แหล่งที่มากฎหมาย" },
    { key: "countryassign", label: "ประเทศที่ออกกฎหมาย" },
    { key: "lawtype", label: "ประเภทกฎหมาย" },
    { key: "lawagencies", label: "หน่วยงานผู้ออกกฎหมาย" },
    { key: "lawagenciesSub", label: "หน่วยงานย่อยผู้ออกกฎหมาย" },
    { key: "lawgroup", label: "กลุ่มของกฎหมาย" },
    { key: "lawgroupSub", label: "กลุ่มของกฎหมายย่อย" },
    { key: "brcsdate", label: "วันบังคับใช้กฎหมาย" },
];

const Tables = ({ data = [], refreshData = () => {} }) => {
    const userState = useSelector((state) => state.changeState.user);
    const [selected, setSelected] = useState([]);
    const [buData, setBUData] = useState([]);
    const [buList, setBuList] = useState([]);
    const [modal, setModal] = useState(false);


    const getBuOption = async () => {
        const dataPush = [];
        setBuList([]);
        // const selectedValue = [];
        // const singleDataPush = [];
        try {
          const result = await ApiBu.get();
          if (result.status == 200) {
            result.data.forEach((res) => {
              dataPush.push({ value: res.code, label: res.code });
            });
    
            setBUData(dataPush);
          }
        } catch (error) {
          console.log(error);
        }
    };

    const importData = () => {
        console.log("selected", selected);
        if (selected.length == 0) {
          Swal.fire({
            icon: "error",
            title: "กรุณาเลือกรายการ",
          });
          return false;
        }
        getBuOption();
        toggle();
    };

    const toggle = () => {
        setModal(!modal);
    };

    const check = (e, id) => {
        if (e.target.checked) {
            setSelected([...selected, id]);
        } else {
            setSelected(selected.filter((itemId) => itemId !== id));
        }
    };

    const onSubmit = (e) => {
        e.preventDefault();
        const model = {
            ids: selected.join(","),
            buList: buList.map((x) => x.value).join(","),
            user: userState.id,
        };
        console.log("saveData", model);
        saveData(model);
    };

    const saveData = async (model) => {
        try {
            const result = await Api.saveImport(model);
            if (result.status === 200) {
                const { data } = result.data;
                toggle();
                refreshData();
        
                Swal.fire({
                    icon: "success",
                    title: "Save Success",
                });
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    };

    const selectAll = () => {
        var dataNotError = data.filter((x) => x.isError != true);
        // console.log("select not error", dataNotError);
        const selectDataAll = dataNotError.map((x) => x.id);
        setSelected(selectDataAll);
    };

    const selectAllBu = () => {
      // const selectDataAll = tranbuState.map((x) => x.assessmentId);
      console.log("buData", buData);
      setBuList(buData);
    };

    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CButton className="mr-2" onClick={selectAll} color={"info"} variant={"outline"}>
                        <span className="ml-2">เลือกทั้งหมด</span>
                    </CButton>
                    <CButton onClick={importData} color={"success"} variant={"outline"}>
                        <span className="ml-2">นำเข้าข้อมูล</span>
                    </CButton>
                </CCol>
            </CRow>
            <CRow className="">
                <CCol xs="12" lg="12">
                    <CDataTable
                    items={data}
                    fields={fields}
                    // tableFilter={{ label: "ค้นหา", placeholder: 'พิมพ์คำที่ต้องการค้นหา' }}
                    // cleaner
                    itemsPerPageSelect={{
                        label: "จำนวนการแสดงผล",
                        values: [10, 25, 50, 100],
                    }}
                    itemsPerPage={10}
                    hover
                    sorter
                    striped
                    bordered
                    pagination
                    scopedSlots={{
                        select: (item) => {
                            if (item.isError == true){
                                return(<td></td>);
                            }else{
                                return(
                                    <td>
                                        <CFormGroup variant="custom-checkbox">
                                        <CInputCheckbox
                                            custom
                                            id={`checkbox-${item.id}`}
                                            checked={selected.includes(item.id)}
                                            onChange={(e) => check(e, item.id)}
                                        />
                                        <CLabel
                                            variant="custom-checkbox"
                                            htmlFor={`checkbox-${item.id}`}
                                        />
                                        </CFormGroup>
                                    </td>
                                );
                            }
                        },
                        errorLog: (item) => {
                            if (item.isError == true){
                                return(
                                    <td>
                                        <CPopover
                                            header="ERROR"
                                            content={item.errorMessage}
                                            placement={"right-start"}
                                            interactive={true}
                                            trigger="click"
                                        >
                                            <CButton className="ml-2" color="danger" size="sm">
                                                <CIcon name="cil-warning" />
                                            </CButton>
                                        </CPopover>
                                    </td>
                                );
                            }else{
                                return(<td></td>);
                            }
                        }
                    }}
                    />
                </CCol>
            </CRow>
            <CModal show={modal} onClose={toggle}>
                <CModalHeader closeButton>
                    <CModalTitle>เพิ่ม BU</CModalTitle>
                </CModalHeader>
                <CForm onSubmit={onSubmit} action="">
                    <CModalBody>
                        <CRow>
                            <CCol xs="12">
                                <CFormGroup>
                                    <CLabel htmlFor="buList">ธุรกิจที่เกี่ยวข้อง</CLabel>
                                    <CButton className="ml-2 pt-0 pb-0" size="sm" onClick={selectAllBu} color={"info"} variant={"outline"}>เลือกทั้งหมด</CButton>
                                    <Select
                                        name="buList"
                                        id="buList"
                                        value={buList}
                                        options={buData}
                                        onChange={(e) => {
                                        setBuList(e);
                                        }}
                                        isMulti
                                    />
                                </CFormGroup>
                            </CCol>
                        </CRow>
                    </CModalBody>
                    <CModalFooter>
                        <CButton variant="outline" type="submit" color="primary">
                            ตกลง
                        </CButton>
                        <CButton variant="outline" color="secondary" onClick={toggle}>
                            ยกเลิก
                        </CButton>
                    </CModalFooter>
                </CForm>
            </CModal>
        </>
    );
}

export default Tables;
