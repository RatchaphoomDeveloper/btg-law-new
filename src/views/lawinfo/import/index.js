import React, { useEffect, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CNav,
  CForm,
  CContainer,
  CLabel,
  CSelect,
  CInput,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import Api from "../../../api/ApiLawInfo";
import { Upload, Button } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { UploadOutlined } from "@ant-design/icons";
import { WEB_API, WEB_FILE_URL } from "../../../env";
import Tables from "./Tables";
import Swal from "sweetalert2/src/sweetalert2.js";

const ImportBrcs = () => {
    const [dataTable, setDataTable] = useState([]);
    const userState = useSelector((state) => state.changeState.user);
    const userStateToken = useSelector((state) => state.changeState.token);

    useEffect(() => {
        // getData();
        return () => {};
    }, []);

    const fileprops = {
        name: "files",
        multiple: true,
        action: `${WEB_API}Master/SaveTranBrcsFile`,
        headers: {
          authorization: `Bearer ${userStateToken}`,
        },
        // action: `https://www.mocky.io/v2/5cc8019d300000980a055e76`,
        onChange(info) {
            const { status } = info.file;
            if (info.file.status !== "uploading") {
                console.log(info.file, info.fileList);
            }
            if (info.file.status === "done") {
                saveFileToTemp(info.file);
                console.log(`${info.file.name} file uploaded successfully`);
                // message.success(`${info.file.name} file uploaded successfully`);
            } else if (info.file.status === "error") {
                console.log(`${info.file.name} file upload failed.`);
                // message.error(`${info.file.name} file upload failed.`);
            }
        },
    };

    const saveFileToTemp = async (data) => {
        try {
            const model = {
                FileName: data.name,
                FilePath: data.response.url,
                user: userState.id,
            };
            Swal.showLoading();
            const result = await Api.saveImportFileToTemp(model);
            Swal.close();
            if (result.status === 200) {
                const { data } = result.data;
                getData();
                // window.location.reload();
            }
        } catch (error) {
            console.log(error);
        }
    };

    const getData = async () => {
        try {
            const result = await Api.getTemp(userState.id);
            if (result.status == 200) {
                console.log("data", result.data);
                setDataTable(result.data);
            }
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <>
            <CRow>
                <CCol sm="9">
                    <h2>นำเข้าเอกสารกฎหมาย</h2>
                </CCol>
                <CCol sm="3" className="text-right">
                    <Upload showUploadList={true}  {...fileprops} maxCount={1}>
                        <CButton color="info" className="mr-2" size="sm">
                            <CIcon name="cilPaperclip" /> แบบไฟล์แนบ
                        </CButton>
                    </Upload>
                </CCol>
            </CRow>
            <CCard fluid className="mt-3">
                <CCardHeader>
                    <CRow>
                        <CCol sm="12">
                            รายละเอียดข้อมูลนำเข้า
                        </CCol>
                    </CRow>
                </CCardHeader>
                <CCardBody>
                    <Tables data={dataTable} refreshData={getData}/>
                </CCardBody>
            </CCard>
        </>
    );
};

export default ImportBrcs;
