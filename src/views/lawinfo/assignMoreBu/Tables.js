import React, { useState, useEffect } from "react";
import {
  CCardBody,
  CBadge,
  CButton,
  CCollapse,
  CDataTable,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CCol,
  CCard,
  CRow,
  CCardHeader,
  CFormGroup,
  CLabel,
  CInput,
  CSelect,
  CForm,
  CModalTitle,
  CButtonGroup,
} from "@coreui/react";
import { useSelector, useDispatch } from "react-redux";
import { getTranBrcsBu } from "../../../redux/gettranbrcsbu/gettranbrcsbuAction";
import { fetchgetMainLaw } from "../../../redux/mainLaw/mainLawAction";
import { fetchgetMainLawSub } from "../../../redux/mainLawSub/mainLawSubAction";
import Api from "../../../api/ApiAssignMoreBu";
import Select from "react-select";
import Swal from "sweetalert2/src/sweetalert2.js";

const getStatus = (status) => {
  switch (status) {
    case "SADR":
      return "Save";
    case "NEDO":
      return "Submit";
    case "CACE":
      return "Delete";
    case "SEEV":
      return "Send To Evaluate";
    case "REFQ":
      return "Reject To FQO";
    case "EVAU":
      return "Send To Approver 1";
    case "REBU":
      return "Reject To BU";
    case "SEA2":
      return "Send To Approver 2";
    case "REDI":
      return "Reject To User Division";
    case "DOAP":
      return "Document Approve";
    case "REA1":
      return "Reject To Approve 1";
    default:
      return "";
  }
};
const getBadge = (status) => {
  switch (status) {
    case "SADR":
      return "light";
    case "NEDO":
      return "secondary";
    case "CACE":
      return "dark";
    case "SEEV":
      return "info";
    case "REFQ":
      return "dark";
    case "EVAU":
      return "warning";
    case "REBU":
      return "dark";
    case "SEA2":
      return "danger";
    case "REDI":
      return "dark";
    case "DOAP":
      return "success";
    case "REA1":
      return "dark";
  }
};
const fields = [
  { key: "docno", label: "เลขที่เอกสาร", _style: { width: "15%" } },
  { key: "mainLawName", label: "ชื่อกฎหมาย", _style: { width: "40%" } },
  { key: "revisionNo", label: "ครั้งที่แก้ไข" },
  { key: "systemStatus", label: "สถานะ" },
  { key: "createDate", label: "วันที่สร้างเอกสาร" },
  { key: "createBy", label: "ผู้สร้างเอกสาร" },
  { key: "updateDate", label: "วันที่แก้ไข" },
  { key: "updateBy", label: "ผู้แก้ไข" },
  { key: "option", label: "", _style: { width: "15%" }, filter: false },
];

const subfields = [
  { key: "docno", label: "เลขที่เอกสาร BU" },
  { key: "code", label: "โครงสร้าง" },
  { key: "revisioN_NO", label: "ครั้งที่แก้ไข" },
  { key: "systeM_STATUS", label: "สถานะ" },
  { key: "creatE_DATE", label: "วันที่สร้างเอกสาร" },
  { key: "creatE_BY", label: "ผู้สร้างเอกสาร" },
  { key: "updatE_DATE", label: "วันที่แก้ไข" },
  { key: "updatE_BY", label: "ผู้แก้ไข" },
  { key: "option", label: "", _style: { width: "7%" }, filter: false },
];

const Tables = ({ data = [], refreshData = () => {}, page, searchData }) => {
  const [details, setDetails] = useState([]);
  const [lastIndex, setLastIndex] = useState(null);
  const [buData, setBUData] = useState([]);
  const userState = useSelector((state) => state.changeState.user);
  const dispatch = useDispatch();
  const [buList, setBuList] = useState([]);
  const [id, setId] = useState();
  const [modal, setModal] = useState(false);
  const tranbuState = useSelector((state) => {
    console.log(state.gettranbrcsbu);
    return state.gettranbrcsbu.data;
  });
  const [selected, setSelected] = useState([]);

  const toggleDetails = (index) => {
    console.log("data", data);
    const position = data.indexOf(index);
    let newDetails = data.slice();
    if (lastIndex == index) {
      newDetails.splice(position, 1);
      setLastIndex(null);
    } else {
      newDetails = [...data, index];
      setLastIndex(index);
    }
    setDetails(newDetails);
    console.log("setDetail", details);
    // console.log(details);
  };

  const getBuOption = async (id) => {
    const dataPush = [];
    setBuList([]);
    // const selectedValue = [];
    // const singleDataPush = [];
    try {
      const result = await Api.getBuOption(id);
      if (result.status == 200) {
        result.data.forEach((res) => {
          dataPush.push({ value: res.code, label: res.code });
        });

        setBUData(dataPush);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const toggle = () => {
    setModal(!modal);
  };

  const getSubTable = (tranData) => {
    try {
      if (searchData == undefined) {
        searchData = {
          refDocNo: tranData,
          page: page ?? "",
        };
      } else {
        searchData.page = page ?? "";
        searchData.refDocNo = tranData;
      }
      dispatch(getTranBrcsBu(searchData));
    } catch (error) {
      console.log(error.message);
    }
  };

  useEffect(() => {
    // dispatch(fetchgetMainLaw({}));
    // dispatch(fetchgetMainLawSub({}));
    return () => {};
  }, []);

  const assignBu = (record) => {
    getBuOption(record.id);
    setId(record.id);
    toggle();
  };

  const onSubmit = (e) => {
    e.preventDefault();
    // console.log("buList", buList);
    // console.log("id", id);
    const model = {
      id: id,
      buList: buList.map((x) => x.value).join(","),
      user: userState.id,
    };
    saveData(model);
  };

  const saveData = async (data) => {
    try {
      Swal.showLoading();
      const result = await Api.create(data);
      Swal.close();
      if (result.status === 200) {
        const { data } = result.data;
        toggle();
        refreshData();

        Swal.fire({
          icon: "success",
          title: "Save Success",
        });
      }
    } catch (error) {
      if (error.response.data.statusCode && error.response.data.statusCode == 550){
        Swal.fire({
          icon: "error",
          title: "ERROR",
          text: `Send Email Error ${error.response.data.message}`
        });
        window.location.href = "/home/lawinfo/search";
      }else{
        Swal.fire({
          icon: "error",
          title: "ERROR",
          text: error.response.data
        });
        if(error.response.data == "Send Email Error"){
          window.location.href = "/home/lawinfo/search";
        }
      }
    }
  };

  const selectAll = () => {
    // const selectDataAll = tranbuState.map((x) => x.assessmentId);
    console.log("buData", buData);
    setBuList(buData);
  };

  return (
    <>
      {/* <CRow>
                  <CCol xs="12" lg="12" >
                      <CButton variant="outline" onclick={newDetails} color={'success'}><CIcon name="cil-plus" /><span className="ml-2">สร้างประเภทกฏหมาย</span></CButton>
                  </CCol>
              </CRow> */}
      <CRow className="">
        <CCol xs="12" lg="12">
          <CDataTable
            items={data}
            fields={fields}
            // tableFilter={{ label: "ค้นหา", placeholder: 'พิมพ์คำที่ต้องการค้นหา' }}
            // cleaner
            itemsPerPageSelect={{
              label: "จำนวนการแสดงผล",
              values: [10, 25, 50, 100],
            }}
            itemsPerPage={10}
            hover
            sorter
            striped
            bordered
            pagination
            scopedSlots={{
              docno: (item, index) => (
                <td>
                  <label
                    style={{ cursor: "pointer", color: "blue" }}
                    onClick={() => {
                      toggleDetails(index);
                      getSubTable(item.docno);
                    }}
                  >
                    {item.docno}
                  </label>
                </td>
              ),
              mainLawName: (item) => (
                <td>
                  {item.mainLawName[0]}
                </td>
              ),
              option: (item, index) => {
                if (userState.role === "BU") {
                  return <td>-</td>;
                }
                return (
                  <td>
                    <CButton
                      color="success"
                      variant="outline"
                      // shape="square"
                      size="sm"
                      onClick={() => {
                        window.location.href = `/home/lawinfo/assignMoreBu/create?id=${item.id}`;
                      }}
                    >
                      {/* <CIcon name="cilPen" /> */}
                      รายละเอียด
                    </CButton>
                    <CButton variant="ghost" size="sm" />
                    <CButton
                      color="primary"
                      variant="outline"
                      shape="square"
                      size="sm"
                      onClick={() => {
                        assignBu(item);
                      }}
                    >
                      Assign More Bu
                    </CButton>
                  </td>
                );
              },
              details: (item, index) => {
                return (
                  <CCollapse show={details.includes(index)}>
                    <CCardBody>
                      <CDataTable
                        items={tranbuState}
                        fields={subfields}
                        hover
                        sorter
                        striped
                        bordered
                        pagination
                        scopedSlots={{
                          option: (items) => (
                            <td className="center">
                              <CButtonGroup>
                                <CButton
                                  color="success"
                                  variant="outline"
                                  // shape="square"
                                  size="sm"
                                  onClick={() => {
                                    window.location.href = `/home/lawinfo/assignMoreBu/create?id=${item.id}&code=${items.id}&buCode=${items.bU_ID}`;
                                  }}
                                >
                                  {/* <CIcon name="cilPen" /> */}
                                  รายละเอียด
                                </CButton>
                              </CButtonGroup>
                            </td>
                          ),
                          systeM_STATUS: (items) => (
                            <td>
                              <CBadge color={getBadge(items.systeM_STATUS)}>
                                {getStatus(items.systeM_STATUS)}
                              </CBadge>
                            </td>
                          ),
                        }}
                      ></CDataTable>
                    </CCardBody>
                  </CCollapse>
                );
              },
              systemStatus: (item) => (
                <td>
                  <CBadge color={getBadge(item.systemStatus)}>
                    {getStatus(item.systemStatus)}
                  </CBadge>
                </td>
              ),
            }}
          />
        </CCol>
      </CRow>
      {/* <TableToggle modal={modal} toggle={toggle} /> */}

      <CModal show={modal} onClose={toggle}>
        <CModalHeader closeButton>
          <CModalTitle>เพิ่ม BU</CModalTitle>
        </CModalHeader>
        <CForm onSubmit={onSubmit} action="">
          <CModalBody>
            <CInput type="hidden" value={id} id="id" />
            <CRow>
              <CCol xs="12">
                <CFormGroup>
                  <CLabel htmlFor="buList">ธุรกิจที่เกี่ยวข้อง</CLabel>
                  <CButton className="ml-2 pt-0 pb-0" size="sm" onClick={selectAll} color={"info"} variant={"outline"}>เลือกทั้งหมด</CButton>
                  <Select
                    name="buList"
                    id="buList"
                    value={buList}
                    options={buData}
                    onChange={(e) => {
                      setBuList(e);
                    }}
                    isMulti
                  />
                </CFormGroup>
              </CCol>
            </CRow>
          </CModalBody>
          <CModalFooter>
            <CButton variant="outline" type="submit" color="primary">
              ตกลง
            </CButton>
            <CButton variant="outline" color="secondary" onClick={toggle}>
              ยกเลิก
            </CButton>
          </CModalFooter>
        </CForm>
      </CModal>
    </>
  );
};

export default Tables;
