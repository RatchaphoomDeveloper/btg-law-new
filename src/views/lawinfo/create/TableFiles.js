import React, { useEffect, useCallback, useState } from "react";
import { CDataTable, CCard, CCardBody, CButton, CButtonGroup,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CModalTitle, } from "@coreui/react";
import CIcon from '@coreui/icons-react'
import { Link } from "react-router-dom";
import Api from "../../../api/ApiLawInfo";
import { saveDataToTransBrcsFileDB } from "../../../redux/savetranbrcsfiledb/savetranbrsfileAction";
import { WEB_API, WEB_FILE_URL } from "../../../env";
import { useDispatch, useSelector } from "react-redux";
import { Upload, message, Button } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import Swal from "sweetalert2/dist/sweetalert2.js";

const fields = [
  // { key: "id", _style: { width: "40%" } },
  { key: "filename" },
  // "registered",
  // { key: "role", _style: { width: "20%" } },
  // { key: "status", _style: { width: "20%" } },
  // {
  //   key: "show_details",
  //   label: "",
  //   _style: { width: "1%" },
  //   sorter: false,
  //   filter: false,
  // },
  { key: 'option', label: '', _style: { width: '1%' }, filter: false, },
];
const TableFiles = ({ id, code, buCode, page, setFileCount }) => {
  const [fileList, setFileList] = useState([]);
  const userState = useSelector((state) => state.changeState.user);
  const userStateToken = useSelector((state) => state.changeState.token);
  const lawinfoData = useSelector((state) => state.lawinfoReducer.data);
  const [successFileFlag, setsuccessFileFlag] = useState(false);
  const [modalConfirm, setModalConfirm] = useState(false);
  const [dataSelected, setDataSelected] = useState('');
  const [fileId, setFileId] = useState('');

  const dispatch = useDispatch();
  useEffect(() => {
    if (id != null) {
      getFileAsync(id);
    }
    setsuccessFileFlag(false);
    return () => {};
  }, []);

  useEffect(() => {
    if (successFileFlag === true) {
      getFileAsync(id);
    }
  }, [successFileFlag]);

  const getFilePromise = (val) => {
    return new Promise((resolve, reject) => {
      setTimeout(async () => {
        let results = await Api.getTranBrcsFileSel(val);
        console.log("file::", results);
        if (results.status === 200) {
          resolve(results.data.response);
          setFileCount(results.data.response.length)
          setsuccessFileFlag(false);
        }
      }, 600);
    });
  };
  const getFileAsync = async (val) => {
    let results = await getFilePromise(val);
    setFileList(results);
  };

  const fileprops = {
    name: "files",
    multiple: true,
    action: `${WEB_API}Master/SaveTranBrcsFile`,
    headers: {
      authorization: `Bearer ${userStateToken}`,
    },
    // action: `https://www.mocky.io/v2/5cc8019d300000980a055e76`,
    onChange(info) {
      const { status } = info.file;
      if (info.file.status !== "uploading") {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === "done") {
        saveToTranBrcsFileDB(info.file);
        message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === "error") {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };

  const saveToTranBrcsFileDB = (data) => {
    try {
      //lawinfoData.docno
      const beforeSave = {
        FileName: data.name,
        FilePath: data.response.url,
        CreateBy: userState.id,
        UpdateBy: userState.id,
        Docno: lawinfoData.docno,
      };
      dispatch(saveDataToTransBrcsFileDB(beforeSave));
      console.log(data);
      setsuccessFileFlag(true);
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: error.message,
      });
    }
  };

  const toggleConfirm = () => {
    setModalConfirm(!modalConfirm);
  }

  const deleteDetails = (record) => {
      toggleConfirm();
      setDataSelected(record);
      setFileId(record.id);
  }

  const onDelete = async () => {
    try {
        const result = await Api.deleteFile(dataSelected);
        if (result.status === 200) {
            const { data } = result.data;
            toggleConfirm();
            Swal.fire({
                icon: "success",
                title: "Delete Success",
            });
            setsuccessFileFlag(true);
        }
      } catch (error) {
          Swal.fire({
              icon: "error",
              title: error.response.data,
            });
      }
  }

  function generateUploadBtn(){
    if (page != "view"){
      return(
        <>
          <div style={{ paddingTop: "1pc" }}>
            <Upload {...fileprops} maxCount={1}  showUploadList={false}>
              <Button icon={<UploadOutlined />}>แนบไฟล์</Button>
              <div style={{ paddingTop: "0.5pc" }}>
                <label style={{ color: "red", fontSize: "12px" }}>
                  * สามารถอัพโหลดไฟล์ ประเภท
                  .xls,.xlsx,.pdf,.xml,.doc,.docx,.bmp,.gif,.jpg,.jpeg,.png
                  ได้ไม่เกิน 5 ไฟล์ ขนาดไม่เกิน 5 เมกะไบท์
                </label>
              </div>
            </Upload>
          </div>
        </>
      );
    }
  }

  return (
    <div>
      {generateUploadBtn()}
      {fileList && (
        <CDataTable
          items={fileList}
          fields={fields}
          tableFilter
          itemsPerPageSelect
          itemsPerPage={10}
          hover
          sorter
          pagination
          scopedSlots={{
            filename: (item, index) => (
              <td>
                
                <a
                  target="_blank"
                  download=""
                  href={`${WEB_FILE_URL}${item.filepath}`}
                >
                  {item.filename}
                </a>
              </td>
            ),
            option:
                (item) => (
                    <td className="center">
                        <CButtonGroup>
                            <CButton
                                color="danger"
                                variant="outline"
                                shape="square"
                                size="sm"
                                onClick={() => { deleteDetails(item) }}
                            >
                                <CIcon name="cilTrash" />
                            </CButton>
                        </CButtonGroup>
                    </td>
                ),
          }}
        />
      )}

      <CModal show={modalConfirm} onClose={setModalConfirm} color="danger" >
          <CModalHeader closeButton>
              <CModalTitle> Delete Data :</CModalTitle>
          </CModalHeader>
          <CModalBody>
              Are you confirm to delete ?
          </CModalBody>
          <CModalFooter>
              <CButton variant="outline" onClick={onDelete} color="primary">Confirm</CButton>{' '}
              <CButton variant="outline" color="secondary" onClick={() => setModalConfirm(false)}>Close</CButton>
          </CModalFooter>
      </CModal>
    </div>
  );
};

export default TableFiles;
