import React, { useEffect, useRef, useState } from "react";
import { useHistory } from "react-router-dom";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CForm,
  CContainer,
  CLabel,
  CSelect,
  CInput,
  CTabs,
  CTextarea,
} from "@coreui/react";
import Api from "../../../api/ApiLawInfo";
import MainLaw from "../component/mainlawTab";
import HowTo from "../component/howtoTab";
import EmployeeModal from "../component/employeeModal";
import FilePreview from "./../../../assets/ตัวอย่าง ประเมินความสอดคล้อง (FM-SE-49).pdf";
import DetailModal from "../component/detailModal";
import CauseModal from "../component/causeModal";
import "./index.scss";
import CreatHeadPanel from "../component/headpanel";
import CreatHeadPanelReadOnly from "../component/headpanelReadOnly";
import Swal, { swal } from "sweetalert2/dist/sweetalert2.js";
import { useDispatch, useSelector } from "react-redux";
import { fetchgetMainLaw } from "../../../redux/mainLaw/mainLawAction";
import { fetchgetMainLawSub } from "../../../redux/mainLawSub/mainLawSubAction";
import { fetchPriority } from "../../../redux/priorityOption/priorityOptionAction";
import { saveTranBrcsBu } from "../../../redux/savetransbrsbu/savetransbrsbuAction";
import { saveButoDB } from "../../../redux/saveBu/saveBuAction";
import { saveDataToTransBrcsFileDB } from "../../../redux/savetranbrcsfiledb/savetranbrsfileAction";
import { Upload, message, Button } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import { WEB_API } from "../../../env";
import TableFiles from "./TableFiles";
import { removeLocalStorage } from "../../../utils/localStorage";

const CreateLawInfo = (props) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const mainlawsState = useSelector((state) => state.mainLaws.mainlaws);
  const mainlawsSubState = useSelector(
    (state) => state.mainLawsSub.mainlawsSub
  );

  var id = new URLSearchParams(props.location.search).get("id");
  var code = new URLSearchParams(props.location.search).get("code");
  var buCode = new URLSearchParams(props.location.search).get("buCode");
  var type = new URLSearchParams(props.location.search).get("type");
  var role = new URLSearchParams(props.location.search).get("role");
  const lawinfoData = useSelector((state) => state.lawinfoReducer.data);
  const [mainlawState, setmainlawState] = useState([]);
  const [mainlawSubState, setmainlawSubState] = useState([]);
  const [tabAddress, setTabAddress] = useState("1");
  const [lawid, setLawId] = useState(0);
  const [mainlawid, setMainLawId] = useState();
  const [modal, setModal] = useState(false);
  const [modalDetail, setModalDetail] = useState(false);
  const [modalCause, setModalCause] = useState(false);
  const [modelSaveHeader, setModalSaveHeader] = useState([]);
  const [isHideCancelRemark, setHideCancelRemark] = useState(true);
  const userState = useSelector((state) => state.changeState.user);
  const userStateToken = useSelector((state) => state.changeState.token);
  const [statusData, setStatusData] = useState([]);
  const [maimlawFlag, setmainlawFlag] = useState(false);
  const [maimlawFlagSub, setmainlawFlagSub] = useState(false);
  const [idSub, setidSub] = useState(0);
  const tranbrsBuState = useSelector((state) => state.gettranbrcsbu.data);
  const [successFileFlag, setsuccessFileFlag] = useState(false);
  const [mainlawSubData, setmainlawSubData] = useState([]);
  const [headerData, setHeaderData] = useState([]);
  const [fileCount, setFileCount] = useState(0);
  const statusOption = [
    { value: "SADR", label: "Save" },
    { value: "NEDO", label: "Submit" },
    { value: "CACE", label: "Delete" },
    { value: "SEEV", label: "Send To Evaluate" },
    { value: "REFQ", label: "Reject To FQO" },
    { value: "SEA2", label: "Send to Approver 2" },
    { value: "DOAP", label: "Document Approve" },
    { value: "REDI", label: "Reject To User Division" },
  ];

  const buttonOption = [
    { value: "SADR", label: "SAVE" },
    { value: "NEDO", label: "SUBMIT" },
    { value: "CACE", label: "DELETE" },
  ];

  useEffect(() => {
    if (type == "email"){
      if (role == "fqo" && userState.role != "FQO"){
        removeLocalStorage("token");
        window.location.href = "/timeout";
      }
    }

    return () => {};
  }, [type, role]);

  useEffect(() => {
    // getData();
    if (id === null) {
      dispatch(fetchgetMainLaw({}));
    } else {
      returnMainLawAsync(id);
      setmainlawFlag(true);
      // dispatch(fetchgetMainLawSub({}));
    }
    setStatusData(statusOption);
    setLawId(id);

    return () => {};
  }, []);

  useEffect(() => {
    if (maimlawFlag !== false) {
      returnMainLawAsync(id);
    }
    if (maimlawFlagSub !== false) {
      returnMainLawSubAsync(idSub);
    }

    return () => {};
  }, [mainlawState, mainlawSubState]);
  const getMainLawSubPromise = (value, bu_id) => {
    return new Promise((resolve, reject) => {
      setTimeout(async () => {
        let results = await Api.getMainLawSub(value, bu_id);
        if (results.data.status === 200) {
          let prepare = results.data.response;
          let result = [];
          prepare.length !== 0 &&
            prepare.forEach((data) => {
              if (data.monthList !== " ") {
                let monthParse = data && JSON.parse(data["monthList"]);
                result.push({
                  id: data.id,
                  seq: data.seq,
                  description: data.description,
                  descriptionlink: data.descriptionLink,
                  mainlawsub_buid: data.mainlawsubbuid,
                  estimate: data.estimateNo,
                  priority: data.priority,
                  month01: monthParse.month01,
                  month02: monthParse.month02,
                  month03: monthParse.month03,
                  month04: monthParse.month04,
                  month05: monthParse.month05,
                  month06: monthParse.month06,
                  month07: monthParse.month07,
                  month08: monthParse.month08,
                  month09: monthParse.month09,
                  month10: monthParse.month10,
                  month11: monthParse.month11,
                  month12: monthParse.month12,
                  lawGroupSubId: data.lawGroupSubId,
                  brcsDate: data.brcsDate,
                  brcsDateForPicker: data.brcsDateForPicker,
                });
              } else {
                result.push({
                  id: data.id,
                  seq: data.seq,
                  description: data.description,
                  descriptionlink: data.descriptionLink,
                  mainlawsub_buid: data.mainlawsubbuid,
                  estimate: data.estimateNo,
                  priority: data.priority,
                  month01: false,
                  month02: false,
                  month03: false,
                  month04: false,
                  month05: false,
                  month06: false,
                  month07: false,
                  month08: false,
                  month09: false,
                  month10: false,
                  month11: false,
                  month12: false,
                  lawGroupSubId: data.lawGroupSubId,
                  brcsDate: data.brcsDate,
                  brcsDateForPicker: data.brcsDateForPicker,
                });
              }
            });
          resolve(result);
        }
      }, 600);
    });
  };

  // const returnMainLawSubAsync = async (value, bu_id) => {
  //   let results = await getMainLawSubPromise(value, bu_id);
  //   setmainlawSubData(results);
  //   setmainlawFlagSub(false);
  // };

  const fileprops = {
    name: "files",
    multiple: true,
    action: `${WEB_API}Master/SaveTranBrcsFile`,
    headers: {
      authorization: `Bearer ${userStateToken}`,
    },
    // action: `https://www.mocky.io/v2/5cc8019d300000980a055e76`,
    onChange(info) {
      const { status } = info.file;
      if (info.file.status !== "uploading") {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === "done") {
        saveToTranBrcsFileDB(info.file);
        message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === "error") {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };

  const saveToTranBrcsFileDB = (data) => {
    try {
      //lawinfoData.docno
      const beforeSave = {
        FileName: data.name,
        FilePath: data.response.url,
        CreateBy: userState.id,
        UpdateBy: userState.id,
        Docno: lawinfoData.docno,
      };
      dispatch(saveDataToTransBrcsFileDB(beforeSave));
      console.log(data);
      setsuccessFileFlag(true);
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: error.message,
      });
    }
  };

  const getMainLaw = (value) => {
    return new Promise((resolve, reject) => {
      return setTimeout(() => {
        dispatch(fetchgetMainLaw(value));
        Swal.showLoading();
        resolve(mainlawsState);
      }, 1500);
    }).then(() => {
      Swal.close();
    });
  };
  
  const getMainLawSub = (value, bu_id) => {
    return new Promise((resolve, reject) => {
      return setTimeout(() => {
        dispatch(fetchgetMainLawSub(value, bu_id));
        Swal.showLoading();
        resolve(mainlawsSubState);
      }, 600);
    }).then(() => {
      Swal.close();
    });
  };

  const returnMainLawAsync = async (value) => {
    let results = await getMainLaw(value);
    setmainlawState(results);
    setmainlawFlag(false);
  };

  const returnMainLawSubAsync = async (value, bu_id) => {
    let results = await getMainLawSubPromise(value, bu_id);
    console.log("getMainLawSubPromise", results);
    setmainlawSubData(results);
    setmainlawFlagSub(false);
    if (results) {
      Swal.close();
    }
  };

  const handleMainLawId = (idx) => {
    setMainLawId(idx);
    setidSub(idx);
    if (code === null && buCode == null) {
      returnMainLawSubAsync(idx, 0);
    } else {
      returnMainLawSubAsync(idx, buCode);
    }
  };

  const handleTabAddress = (idx) => {
    setTabAddress(idx);
  };

  const saveDataMainLaw = async (data) => {
    try {
      const result = await Api.createMainLaw(data);
      if (result.status === 200) {
        const { data } = result.data;
        // Swal.fire({
        //   icon: "success",
        //   title: "Success",
        // });
        dispatch(fetchgetMainLaw(lawid));
        setmainlawFlag(true);
        // window.location.href = "/home/lawinfo/search";
        // history.push("/home/lawinfo/search");
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Process Error!",
        text: error.response.data,
      });
    }
  };

  const deleteDataMainLaw = async (data) => {
    try {
      const result = await Api.deleteMainLaw(data);
      if (result.status === 200) {
        const { data } = result.data;
        // Swal.fire({
        //   icon: "success",
        //   title: "Success",
        // });
        dispatch(fetchgetMainLaw(lawid));
        setmainlawFlag(true);
        // window.location.href = "/home/lawinfo/search";
        // history.push("/home/lawinfo/search");
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Process Error!",
        text: error.response.data,
      });
    }
  };

  const deleteDataHowTo = async (data) => {
    try {
      const result = await Api.deleteMainLawSub(data);
      if (result.status === 200) {
        const { data } = result.data;
        // Swal.fire({
        //   icon: "success",
        //   title: "Success",
        // });
        returnMainLawSubAsync(mainlawid, buCode);
        // window.location.href = "/home/lawinfo/search";
        // history.push("/home/lawinfo/search");
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Process Error!",
        text: error.response.data,
      });
    }
  };

  // const promiseMainLawSub = (values) => {
  //   return new Promise((resolve, reject) => {
  //     return setTimeout(() => {
  //       Api.createMainLawSub(values);
  //       resolve("success");
  //     }, 600);
  //   });
  // };

  const saveMainlawSubwithCode = async (values) => {
    let results = await Api.createMainLawSub(values);
    if (results.status == 200) {
      if (code === null && buCode === null) {
        alert(mainlawid);
        returnMainLawSubAsync(mainlawid, 0);
      }
      if (code !== null && buCode !== null) {
        returnMainLawSubAsync(values.bucode, buCode);
      }
      setmainlawFlagSub(true);
    }
  };

  const saveMainlawSubwithCodeList = async (values) => {
    let results = await Api.createMainLawSubList(values);
    if (results.status == 200) {
      // Swal.fire({
      //   icon: "success",
      //   title: "Success",
      // });
      if (code === null && buCode === null) {
        returnMainLawSubAsync(mainlawid, 0);
      }
      if (code !== null && buCode !== null) {
        returnMainLawSubAsync(values.bucode, buCode);
      }
      setmainlawFlagSub(true);
    }
  };

  const saveDataHowTo = async (data) => {
    try {
      if (id !== null && code !== null) {
        const result = await Api.createMainLawSub(data);
        if (result.status === 200) {
          const { data } = result.data;
          // Swal.fire({
          //   icon: "success",
          //   title: "Success",
          // });
          // history.push("/home/lawinfo/create?id=3");
          //  history.goBack();
          // setTabAddress("1");
          returnMainLawSubAsync(mainlawid, buCode);
          // dispatch(fetchgetMainLawSub(mainlawid, buCode));

          // window.location.href = "/home/lawinfo/create?id=3";
        }
      }
      if (id !== null && code === null) {
        let tranBrcsData = [];
        tranBrcsData = tranbrsBuState;
        let count = 0;

        if (data.editflag === false) {
          // await Api.delBrcsMainLawSub(data.MainlawId);
        }
        let datasModel = [];
        tranBrcsData.map((res, i) => {
          let datas = {
            Id: data.editflag === true ? data.id : data.id,
            MainlawId: data.MainlawId,
            Seq: data.Seq,
            description: data.description,
            descriptionlink: data.descriptionlink,
            estimateNo: data.estimateNo,
            priority: data.priority,
            user: data.user,
            month_list: data.month_list,
            bucode: res.bU_ID,
            count: i + 1,
            editflag: data.editflag,
            brcsDate: data.brcsDate,
            lawGroupSubId: data.lawGroupSubId
          };
          datasModel.push(datas);
          // await saveMainlawSubwithCode(datas);
        });
        await saveMainlawSubwithCodeList(datasModel);
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Process Error!",
        text: error.response.data,
      });
    }
  };

  const validateDataHeader = async (data) => {
    try {
      // console.log("data", data);
      if (data.SystemStatus == "CACE") {
        Swal.close();
        //setHideCancelRemark(false);
        // setModal(true);
        // setModalSaveHeader(data);
        saveDataHeader(data);
      } else {
        saveDataHeader(data);
      }
    } catch (error) {}
  };

  const savetoTranBrsBu = async (data) => {
    try {
        if (code == null && id == null) {
          let buList = [];
          buList = data.buList;
          await buList.forEach((res) => {
          dispatch(
            saveTranBrcsBu({
              Bucode: res.value,
              CreateBy: userState.id,
              UpdateBy: userState.id,
            })
          );
          });
        } else {
          updateTranBrcsBu({
            system_status: data.SystemStatus,
            code_id: code,
            tran_id: id,
            user: userState.id
          });
        }
    } catch (error) {
      console.log(error.Message);
    }
  };

  const saveDataHeader = async (data) => {
    try {
      console.log("dataHeaderBF", data);
      let dataParam = data;
      if (data.buList.length > 0) {
        dataParam.BuIdList = data.buList.map((x) => x.value).join(",");
      }
      // Swal.close();
      console.log("dataHeader", data);
      const result = await Api.create(data);
      if (result.status === 200) {
        //  window.location.href = "/home/lawinfo/addfrequency/search";
        const { data } = result.data;
        Swal.fire({
          icon: "success",
          title: "Success",
        });
        // alert(id)
        savetoTranBrsBu(dataParam);
        history.push("/home/lawinfo/search");
        // window.location.href = "/home/lawinfo/search";
      } else {
        Swal.fire({
          icon: "error",
          title: "Process Error!",
          text: "Please choose system status!",
        });
      }
    } catch (error) {
      // console.log(error.Message);
      if (error.response.data.statusCode && error.response.data.statusCode == 550){
        Swal.fire({
          icon: "error",
          title: "ERROR",
          text: `Send Email Error ${error.response.data.message}`
        });
        history.push("/home/lawinfo/search");
        // window.location.href = "/home/lawinfo/search";
      }else{
        Swal.fire({
          icon: "error",
          title: "ERROR",
          text: error.response.data
        });
        if(error.response.data == "Send Email Error"){
          history.push("/home/lawinfo/search");
        // window.location.href = "/home/lawinfo/search";
        }
      }
    }
  };

  const updateTranBrcsBu = async (data) => {
    try {
      const result = await Api.updateTranBu(data);
      if (result.status === 200) {
        //  window.location.href = "/home/lawinfo/addfrequency/search";
        // const { data } = result.data;
        // Swal.fire({
        //   icon: "success",
        //   title: "Success",
        // });
        // // alert(id)
        // savetoTranBrsBu(dataParam);
        // // history.push("/home/lawinfo/search");
        // window.location.href = "/home/lawinfo/search";
      } else {
        // Swal.fire({
        //   icon: "error",
        //   title: "Process Error!",
        //   text: "Please choose system status!",
        // });
      }
    } catch (error) {
      console.log(error.Message);
    }
  };

  function generateHeadPanel (){
    console.log("id", id);
    if(!id){
      return (
        <CreatHeadPanel
          statusOption={statusData}
          buttonOption={buttonOption}
          id={id}
          validateDataHeader={validateDataHeader}
          // isHideCancelRemark={isHideCancelRemark}
          code={code}
        />
      );
    }else{
      return (
        <CreatHeadPanelReadOnly
              statusOption={statusData}
              buttonOption={buttonOption}
              id={id}
              validateDataHeader={validateDataHeader}
              // isHideCancelRemark={isHideCancelRemark}
              code={code}
              setHeaderData={setHeaderData}
            />
      );
    }
  }

  return (
    <>
      <h2>สร้างข้อมูลกฏหมาย</h2>
      <CCard fluid className="mt-3">
        <CCardHeader>
          <CRow>
            <CCol sm="12">
              ข้อมูลกฏหมาย
              {/* <CNav className="card-header-actions">
                <CForm inline>
                  <CButton
                    size="sm"
                    variant="outline"
                    color="success"
                    // disabled={selected.length <= 0}
                    style={{ marginRight: "4px" }}
                    // onClick={() => handleConfirm()}
                  >
                    <b> บันทึกร่าง </b>
                  </CButton>
                  <CButton
                    size="sm"
                    variant="outline"
                    color="primary"
                    // disabled={selected.length <= 0}
                    style={{ marginRight: "4px" }}
                    onClick={() => setModal(true)}
                  >
                    <b> บันทึก </b>
                  </CButton>
                  <CButton
                    size="sm"
                    variant="outline"
                    color="danger"
                    // disabled={selected.length <= 0}
                    style={{ marginRight: "4px" }}
                    // onClick={() => handleConfirm()}
                  >
                    <b> ปฏิเสธ </b>
                  </CButton>
                  <CButton
                    href={FilePreview}
                    size="sm"
                    variant="outline"
                    color="info"
                    // disabled={selected.length <= 0}
                    style={{ marginRight: "4px" }}
                    download
                    // onClick={() => (window.location.href = FilePreview)}
                  >
                    <b> เอกสาร </b>
                  </CButton>
                </CForm>
              </CNav> */}
            </CCol>
          </CRow>
          <div style={{ fontSize: "12px" }}>
          {generateHeadPanel()}
            {/* <CreatHeadPanel
              statusOption={statusData}
              buttonOption={buttonOption}
              id={id}
              validateDataHeader={validateDataHeader}
              // isHideCancelRemark={isHideCancelRemark}
              code={code}
            /> */}
          </div>
        </CCardHeader>
        {lawinfoData.docno && (
          <CCardBody>
            <CTabs
              activeTab={tabAddress}
              onActiveTabChange={(idx) => handleTabAddress(idx)}
            >
              <CNav variant="tabs">
                <CNavItem>
                  <CNavLink data-tab="1">
                    <b>กฎหมายหลัก</b>
                  </CNavLink>
                </CNavItem>
                <CNavItem>
                  {userState.role === "FQO" && (
                    <CNavLink data-tab="2" activeTab={true} disabled>
                      <b>วิธีการดำเนินงาน/แผนการดำเนินงาน</b>
                    </CNavLink>
                  )}
                </CNavItem>
                <CNavItem>
                  <CNavLink data-tab="3">
                    <b>แนบไฟล์ ({fileCount})</b>
                  </CNavLink>
                </CNavItem>
              </CNav>
              <CTabContent>
                <CTabPane data-tab="1">
                  <MainLaw
                    // refreshData={getMainData}
                    // data={dataMainTable}
                    code={code}
                    lawid={lawid}
                    handleMainLawId={handleMainLawId}
                    handleTabAddress={handleTabAddress}
                    saveDataMainLaw={saveDataMainLaw}
                    deleteDataMainLaw={deleteDataMainLaw}
                    mainlawsStates={mainlawsState}
                    isView={userState.role === "ADMIN"}
                    headerData={headerData}
                  // localTab={tabAddress}
                    // runningBatch={runningBatch}
                    // workOrders={workOrders}
                    // setRunningBatch={setRunningBatch}
                    // setworkOrders={setworkOrders}
                  />
                </CTabPane>
                {userState.role === "FQO" && (
                  <CTabPane data-tab="2">
                    <HowTo
                      code={code}
                      modalCauseOpen={setModalCause}
                      modalDetailOpen={setModalDetail}
                      buCode={code}
                      // refreshData={getData}
                      // data={dataTable}
                      mainlawId={mainlawid}
                      saveDataHowTo={saveDataHowTo}
                      deleteDataHowTo={deleteDataHowTo}
                      mainLawsSubDatas={mainlawSubData}
                      headerData={headerData}
                    // priorityDatas={}
                      
                    />
                  </CTabPane>
                )}

                <CTabPane data-tab="3">
                  <div>
                    <TableFiles id={id} code={code} buCode={buCode} setFileCount={setFileCount} />
                  </div>
                </CTabPane>
              </CTabContent>
            </CTabs>
          </CCardBody>
        )}
      </CCard>
      <EmployeeModal modal={modal} setModal={setModal} />
      <DetailModal modal={modalDetail} setModal={setModalDetail} />
      <CauseModal modal={modalCause} setModal={setModalCause} />
    </>
  );
};

export default CreateLawInfo;
