import React, { useState, useEffect } from "react";
import {
  CCardBody,
  CBadge,
  CButton,
  CCollapse,
  CDataTable,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CCol,
  CCard,
  CRow,
  CCardHeader,
  CFormGroup,
  CLabel,
  CInput,
  CSelect,
  CForm,
  CModalTitle,
  CButtonGroup,
  CInputCheckbox,
  CTextarea,
} from "@coreui/react";
import Api from "../../api/ApiLawInfo";
import ApiLawGroupSub from "../../api/ApiLawGroupSub";
import CIcon from "@coreui/icons-react";
import { statusData } from "./data";
import TableToggle from "./searchOnly/TableToggle";
import { useSelector, useDispatch } from "react-redux";
import { getTranBrcsBu, getTransBrcsBuFailure } from "../../redux/gettranbrcsbu/gettranbrcsbuAction";
import { fetchgetMainLaw } from "../../redux/mainLaw/mainLawAction";
import { fetchgetMainLawSub } from "../../redux/mainLawSub/mainLawSubAction";
import Swal from "sweetalert2/src/sweetalert2.js";
import Select from "react-select";
import { useHistory } from "react-router-dom";
import {getTransBrcsBuSuccess} from '../../redux/gettranbrcsbu/gettranbrcsbuAction'

const getStatus = (status) => {
  switch (status) {
    case "SADR":
      return "Save";
    case "NEDO":
      return "Submit";
    case "CACE":
      return "Delete";
    case "SEEV":
      return "Send To Evaluate";
    case "REFQ":
      return "Reject To FQO";
    case "SEA1":
      return "Send To Verification";
    case "REBU":
      return "Reject To BU";
    case "SEA2":
      return "Send To Approver";
    case "REDI":
      return "Reject To User Division";
    case "DOAP":
      return "Document Approve";
    case "REA1":
      return "Reject To Approve 1";
    default:
      return "";
  }
};

const getBadge = (status) => {
  switch (status) {
    case "SADR":
      return "light";
    case "NEDO":
      return "secondary";
    case "CACE":
      return "dark";
    case "SEEV":
      return "info";
    case "REFQ":
      return "dark";
    case "EVAU":
      return "warning";
    case "REBU":
      return "dark";
    case "SEA2":
      return "danger";
    case "REDI":
      return "dark";
    case "DOAP":
      return "success";
    case "REA1":
      return "dark";
  }
};

const options = [
  { value: "", label: "กรุณาเลือก" },
  { value: "12", label: "ทุกเดือน" },
  { value: "1", label: "1 รอบ/ปี" },
  { value: "2", label: "2 รอบ/ปี" },
  { value: "3", label: "3 รอบ/ปี" },
  { value: "4", label: "4 รอบ/ปี" },
  { value: "5", label: "5 รอบ/ปี" },
  { value: "6", label: "6 รอบ/ปี" },
  { value: "7", label: "7 รอบ/ปี" },
  { value: "8", label: "8 รอบ/ปี" },
  { value: "9", label: "9 รอบ/ปี" },
  { value: "10", label: "10 รอบ/ปี" },
  { value: "11", label: "11 รอบ/ปี" },
];

const fields = [
  { key: "docno", label: "เลขที่เอกสาร", _style: { width: "15%" } },
  { key: "mainLawName", label: "ชื่อกฎหมาย", _style: { width: "40%" } },
  { key: "revisionNo", label: "ครั้งที่แก้ไข" },
  { key: "systemStatus", label: "สถานะ" },
  { key: "createDate", label: "วันที่สร้างเอกสาร" },
  { key: "createBy", label: "ผู้สร้างเอกสาร" },
  { key: "updateDate", label: "วันที่แก้ไข" },
  { key: "updateBy", label: "ผู้แก้ไข" },
  {
    key: "option",
    label: "",
    _style: { width: "200px" },
    filter: false,
  },
];

const subfields = [
  { key: "select", label: "", filter: false },
  { key: "docno", label: "เลขที่เอกสาร BU" },
  { key: "code", label: "โครงสร้าง" },
  { key: "revisioN_NO", label: "ครั้งที่แก้ไข" },
  { key: "systeM_STATUS", label: "สถานะ" },
  { key: "creatE_DATE", label: "วันที่สร้างเอกสาร" },
  { key: "creatE_BY", label: "ผู้สร้างเอกสาร" },
  { key: "updatE_DATE", label: "วันที่แก้ไข" },
  { key: "updatE_BY", label: "ผู้แก้ไข" },
  { key: "reviewdate", label: "วันที่ทบทวนเอกสาร" },
  { key: "option", label: "", _style: { width: "100px" }, filter: false },
];

const Tables = ({ data = [], refreshData = () => {}, page, searchData }) => {
  const [details, setDetails] = useState([]);
  const [lastIndex, setLastIndex] = useState(null);
  const userState = useSelector((state) => state.changeState.user);
  const dispatch = useDispatch();
  const tranbuState = useSelector((state) => {
    console.log(state.gettranbrcsbu);
    return state.gettranbrcsbu.data;
  });
  const [selected, setSelected] = useState([]);
  const [mainLaw, setMainLaw] = useState();
  const [mainLawSub, setMainLawSub] = useState([]);
  const [mlData, setMlData] = useState([]);
  const [mlsData, setMlsData] = useState([]);
  const [modalMl, setModalMl] = useState(false);
  const [modal, setModal] = useState(false);
  const [allMonth, setAllMonth] = useState(false);
  const [month01, setMonth01] = useState(false);
  const [month02, setMonth02] = useState(false);
  const [month03, setMonth03] = useState(false);
  const [month04, setMonth04] = useState(false);
  const [month05, setMonth05] = useState(false);
  const [month06, setMonth06] = useState(false);
  const [month07, setMonth07] = useState(false);
  const [month08, setMonth08] = useState(false);
  const [month09, setMonth09] = useState(false);
  const [month10, setMonth10] = useState(false);
  const [month11, setMonth11] = useState(false);
  const [month12, setMonth12] = useState(false);
  const [seq, setSeq] = useState("");
  const [description1, setDescription1] = useState("");
  const [description2, setDescription2] = useState("");
  const [priority, setPriority] = useState("");
  const [estimate, setEstimate] = useState("");
  const [lawGroupSubData, setLawGroupSubData] = useState([]);
  const [lawGroupSubHeader, setLawGroupSubHeader] = useState("");
  const [lawGroupSub, setLawGroupSub] = useState("");
  const [brcsDate, setBrcsDate] = useState("");
  const [priorityData, setPriorityData] = useState([]);
  const [flagClear, setflagClear] = useState(false);
  const history = useHistory();

  useEffect(() => {
    getPriorityOption();
    return () => {};
  }, []);

  const getPriorityOption = async () => {
    try {
      const result = await Api.getPriority();
      if (result.status == 200) {
        setPriorityData(result.data);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const toggleMl = () => {
    setMainLaw(0);
    setMainLawSub(0);
    setModalMl(!modalMl);
  };

  const toggleDetails = (index) => {
    // console.log("index", index);
    // console.log("data", data);
    const position = data.indexOf(index);
    // console.log("data.indexOf(item)", data.indexOf(item));
    // console.log("position", position);
    let newDetails = data.slice();
    // console.log("newDetails", newDetails);
    if (lastIndex == index) {
      newDetails.splice(position, 1);
      setLastIndex(null);
    } else {
      newDetails = [...data, index];
      setLastIndex(index);
    }
    // newDetails.splice(position, 1);
    setSelected([]);
    setDetails(newDetails);
    console.log("setDetail", details);
    // console.log(details);
  };

  const check = (e, id) => {
    if (e.target.checked) {
      setSelected([...selected, id]);
    } else {
      setSelected(selected.filter((itemId) => itemId !== id));
    }
  };

  const getSubTable = async(tranData,isRedirect = "",item = "") => {
    try {
      if (searchData == undefined) {
        searchData = {
          refDocNo: tranData,
          page: page ?? "",
        };
      } else {
        searchData.page = page ?? "";
        searchData.refDocNo = tranData;
      }
      const response = await Api.getTranBrcsBu(searchData);
      dispatch(getTransBrcsBuSuccess(response.data.response))
      if(isRedirect){
        if (userState.role == "BU") {
          window.location.href = `/home/lawinfo/addfrequency/create?id=${item.id}`;
        } else {
          window.location.href = `/home/lawinfo/create?id=${item.id}`;
        }
      }
      
      //dispatch(getTranBrcsBu(searchData));
    } catch (error) {
      dispatch(getTransBrcsBuFailure(error.message))
      console.log(error.message);
    }
  };

  const toggleAddFrequency = (item) => {
    if (selected.length == 0) {
      Swal.fire({
        icon: "error",
        title: "กรุณาเลือกรายการ",
      });
      return false;
    }
    getMainlawOption(item.id);
    getLawGroupSubOption(item.lawgroupId);
    setLawGroupSub(item.lawgroupSubId);
    setBrcsDate(item.brcsDateForPicker);
    toggleMl();
  };

  const getLawGroupSubOption = async (val) => {
    try {
      var result;
      result = await ApiLawGroupSub.get(val);
      if (result.status == 200) {
        setLawGroupSubData(result.data);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getMainlawOption = async (id) => {
    try {
      const result = await Api.getMainLaw(id);
      if (result.status == 200) {
        // setMlData(result.data);
        console.log("result", result.data);
        const dataPush = [];
        result.data.forEach((res) => {
          dataPush.push({ value: res.id, label: res.mainlawName });
        });
        setMlData(dataPush);
      }
    } catch (error) {
      console.log(error);
      
    }
  };

  const onMainLawChange = (data) => {
    setMainLaw(data);
    console.log("data", data);
    var ml = mlData.find((x) => x.id == data.value);
    getMainlawSubOption(data.value);
  };

  const getMainlawSubOption = async (id) => {
    try {
      const result = await Api.getMainLawSub(id);
      if (result.status == 200) {
        console.log("result", result.data.response);
        const dataPush = [];
        result.data.response.forEach((res) => {
          var text = "(" + res.seq + ") " + res.description;
          dataPush.push({ value: res.id, label: text });
        });
        setMlsData(dataPush);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const onMainLawSubChange = (data) => {
    console.log("data", data);
    setMainLawSub(data);
    // getMainlawSubOption(val);
    // var mls = mlsData.find((x) => x.id == data.value);
    // console.log("mls", mls);
    clearMonth();
    // setSeq(mls.seq);
    setPriority("");
    setEstimate("");
    // setDescription1(mls.description);
    // setDescription2(mls.descriptionLink);
  };
  
  // const onSubmitMl = (e) => {
  //   e.preventDefault();
  //   var mls = mlsData.find((x) => x.id == mainLawSub);
  //   clearMonth();
  //   setSeq(mls.seq);
  //   setPriority("");
  //   setEstimate("");
  //   setDescription1(mls.description);
  //   setDescription2(mls.descriptionLink);
  //   toggle();
  // };

  const onSubmit = (e) => {
    e.preventDefault();
    const monthArr = [
      month01,
      month02,
      month03,
      month04,
      month05,
      month06,
      month07,
      month08,
      month09,
      month10,
      month11,
      month12,
    ];

    // var estimateNo = "";
    // if (id == 0){
    //   estimateNo = estimate;
    // }else{
    //   estimateNo = estimate[0].value;
    // }

    const monthCounth = monthArr.filter((res) => {
      return res === true;
    });
    var estimateValue = 0;
    if (estimate.length > 0) {
      if (estimate[0].value) {
        estimateValue = parseInt(estimate[0].value);
      }
    }
    const model = {
      ids: selected.join(","),
      MainlawId: mainLaw.value,
      MainlawSubIds: mainLawSub.map((x) => x.value).join(","),
      // Seq: seq,
      // description: description1,
      // descriptionlink: description2,
      estimateNo: estimateValue,
      priority: priority,
      // month01: month01,
      // month02: month02,
      // month03: month03,
      // month04: month04,
      // month05: month05,
      // month06: month06,
      // month07: month07,
      // month08: month08,
      // month09: month09,
      // month10: month10,
      // month11: month11,
      // month12: month12,
      user: userState.id,
      month_list: JSON.stringify({
        month01: month01,
        month02: month02,
        month03: month03,
        month04: month04,
        month05: month05,
        month06: month06,
        month07: month07,
        month08: month08,
        month09: month09,
        month10: month10,
        month11: month11,
        month12: month12,
      }),
      // bucode: mainlawId,
      // count: 2,
      // editflag: editDetailFlag,
      lawGroupSubId: lawGroupSub,
      brcsDate: brcsDate,
    };

    if (model.estimateNo == "" || model.priority == "") {
      Swal.fire({
        icon: "error",
        title: "กรุณากรอกข้อมูลให้ครบ !",
      });
      return false;
    }

    if (monthCounth.length === estimateValue) {
      // if (model.id === 0) {
      saveDataHowToBuList(model);
      //   setModal(false);
      //   seteditDetailFlag(false);
      //   newDetails();
      // } else {
      //   saveDataHowTo(model);
      //   setModal(false);
      // }
    } else {
      Swal.fire({
        icon: "error",
        title: "กรุณาเลือกจำนวนเดือนให้ตรงกับความถี่ !",
      });
      return false;
    }
  };

  const saveDataHowToBuList = async (data) => {
    try {
      const result = await Api.saveMainLawSubBuList(data);
      if (result.status === 200) {
        const { data } = result.data;
        Swal.fire({
          icon: "success",
          title: "Success",
        });
        console.log("reload");
        history.go(0)
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Process Error!",
        text: error.response.data,
      });
    }
  };

  const clearMonth = () => {
    setAllMonth(false);
    setMonth01(false);
    setMonth02(false);
    setMonth03(false);
    setMonth04(false);
    setMonth05(false);
    setMonth06(false);
    setMonth07(false);
    setMonth08(false);
    setMonth09(false);
    setMonth10(false);
    setMonth11(false);
    setMonth12(false);
  };

  const allMonthCheck = () => {
    if (flagClear === true) {
      clearMonth();
    } else {
      setAllMonth(true);
      setMonth01(true);
      setMonth02(true);
      setMonth03(true);
      setMonth04(true);
      setMonth05(true);
      setMonth06(true);
      setMonth07(true);
      setMonth08(true);
      setMonth09(true);
      setMonth10(true);
      setMonth11(true);
      setMonth12(true);
    }
  };

  useEffect(() => {
    // dispatch(fetchgetMainLaw({}));
    // dispatch(fetchgetMainLawSub({}));
    return () => {};
  }, []);

  const selectAll = () => {
    const selectDataAll = tranbuState.map((x) => x.id);
    setSelected(selectDataAll);
  };
  const unSelectAll = () => {
    setSelected([]);
  };
  return (
    <>
      {page == "info" ?
      <CRow>
          {/* <CCol xs="12" lg="12" >
              <CButton variant="outline" onclick={reviewDoc} color={'success'}><span className="ml-2">ทบทวนเอกสาร</span></CButton>
          </CCol> */}
          <CCol xs="12" lg="12">
            {/* <CButton onClick={approve} color={"success"} variant={"outline"}>
              <span className="ml-2">Approve</span>
            </CButton> */}
            <CButton className="mr-2" onClick={selectAll} color={"info"} variant={"outline"}>
              <span className="ml-2">เลือกทั้งหมด</span>
            </CButton>
            <CButton className="mr-2" onClick={unSelectAll} color={"danger"} variant={"outline"}>
              <span className="ml-2">ยกเลิก</span>
            </CButton>
          </CCol>
      </CRow>
      : ""}
      <CRow className="">
        <CCol xs="12" lg="12">
          <CDataTable
            items={data}
            fields={fields}
            // tableFilter={{ label: "ค้นหา", placeholder: 'พิมพ์คำที่ต้องการค้นหา' }}
            // cleaner
            itemsPerPageSelect={{
              label: "จำนวนการแสดงผล",
              values: [10, 25, 50, 100],
            }}
            itemsPerPage={10}
            hover
            sorter
            striped
            bordered
            pagination
            scopedSlots={{
              docno: (item, index) => (
                <td>
                  <label
                    style={{ cursor: "pointer", color: "blue" }}
                    onClick={() => {
                      toggleDetails(index);
                      getSubTable(item.docno);
                    }}
                  >
                    {details.includes(index) ? item.docno : item.docno}
                  </label>
                </td>
              ),
              mainLawName: (item) => (
                <td>
                  {item.mainLawName[0]}
                </td>
              ),
              option: (item, index) => {
                if (userState.role === "BU") {
                  return <td>-</td>;
                }
                return (
                  <td>
                    <CButton
                      color="success"
                      variant="outline"
                      shape="square"
                      size="sm"
                      onClick={() => {
                        toggleAddFrequency(item);
                      }}
                    >
                      ใส่ความถี่
                    </CButton>
                    <CButton variant="ghost" size="sm" />
                    <CButton
                      color="success"
                      variant="outline"
                      // shape="square"
                      size="sm"
                      onClick={() => {
                       
                        getSubTable(item.docno,'redirect',item);
                      }}
                    >
                      {/* <CIcon name="cilPen" /> */}
                      รายละเอียด
                    </CButton>
                  </td>
                );
              },
              details: (item, index) => {
                return (
                  <CCollapse show={details.includes(index)}>
                    <CCardBody>
                      <CDataTable
                        items={tranbuState}
                        fields={subfields}
                        hover
                        sorter
                        striped
                        bordered
                        pagination
                        scopedSlots={{
                          select: (items) => {
                            if(userState.role == "FQO"){
                              return (
                                <td>
                                  <CFormGroup variant="custom-checkbox">
                                    <CInputCheckbox
                                      custom
                                      id={`checkbox-${items.id}`}
                                      checked={selected.includes(items.id)}
                                      onChange={(e) => check(e, items.id)}
                                    />
                                    <CLabel
                                      variant="custom-checkbox"
                                      htmlFor={`checkbox-${items.id}`}
                                    />
                                  </CFormGroup>
                                </td>
                              );
                            }else{
                              return(<td></td>);
                            }
                          },
                          option: (items) => (
                            <td className="center">
                              <CButtonGroup>
                                {/* <CButton
                                    color="danger"
                                    variant="outline"
                                    shape="square"
                                    size="sm"
                                    // onClick={() => { deleteDetails(item) }}
                                >
                                    <CIcon name="cilTrash" />
                                </CButton> */}
                                {/* <CButton variant="ghost" size="sm" /> */}
                                <CButton
                                  color="success"
                                  variant="outline"
                                  // shape="square"
                                  size="sm"
                                  onClick={() => {
                                    if (
                                      userState.role === "BU" &&
                                      page == "frequency"
                                    ) {
                                      window.location.href = `/home/lawinfo/addfrequency/create?id=${item.id}&code=${items.id}&buCode=${items.bU_ID}`;
                                    } else if (
                                      userState.role === "BU" &&
                                      page == "searchFrequency"
                                    ) {
                                      window.location.href = `/home/lawinfo/searchFrequency/create?id=${item.id}&code=${items.id}&buCode=${items.bU_ID}`;
                                    } else {
                                      window.location.href = `/home/lawinfo/create?id=${item.id}&code=${items.id}&buCode=${items.bU_ID}`;
                                    }
                                  }}
                                >
                                  {/* <CIcon name="cilPen" /> */}
                                  รายละเอียด
                                </CButton>
                              </CButtonGroup>
                            </td>
                          ),
                          systeM_STATUS: (items) => (
                            <td>
                              <CBadge color={getBadge(items.systeM_STATUS)}>
                                {getStatus(items.systeM_STATUS)}
                              </CBadge>
                            </td>
                          ),
                        }}
                      ></CDataTable>
                      {/* {tranbuState.map((subi, i) => {
                        return (
                          <div key={i}>
                            <label>{subi.code}</label>
                          </div>
                        );
                      })} */}
                      {/* <p className="text-muted">
                          User since: {item.registered}
                        </p>
                        <CButton size="sm" color="info">
                          User Settings
                        </CButton>
                        <CButton size="sm" color="danger" className="ml-1">
                          Delete
                        </CButton> */}
                    </CCardBody>
                  </CCollapse>
                );
              },
              systemStatus: (item) => (
                <td>
                  <CBadge color={getBadge(item.systemStatus)}>
                    {getStatus(item.systemStatus)}
                  </CBadge>
                </td>
              ),
              // option: (item) => (
              //   <td className="center">
              //     <CButtonGroup>
              //       <CButton
              //         color="success"
              //         variant="outline"
              //         // shape="square"
              //         size="sm"
              //         onClick={() =>
              //           (window.location.href = `/home/lawinfo/create?id=${item.id}`)
              //         }
              //       >
              //         {/* <CIcon name="cilPen" /> */}
              //         รายละเอียด
              //       </CButton>
              //       {/* <CButton variant="ghost" size="sm" /> */}
              //       {/* <CButton
              //                                 color="danger"
              //                                 variant="outline"
              //                                 shape="square"
              //                                 size="sm"
              //                                 // onClick={() => { deleteDetails(item) }}
              //                             >
              //                                 <CIcon name="cilTrash" />
              //                             </CButton> */}
              //     </CButtonGroup>
              //   </td>
              // ),
            }}
          />
        </CCol>
      </CRow>
      {/* <TableToggle modal={modal} toggle={toggle} /> */}

      <CModal show={modalMl} onClose={toggleMl} size="lg">
        <CModalHeader closeButton>
          <CModalTitle>เลือก กฎหมาย, สาระสำคัญ</CModalTitle>
        </CModalHeader>
        <CForm onSubmit={(e) => onSubmit(e)} action="">
          <CModalBody>
            {/* <CInput type="hidden" value={id} id="id"/> */}
            <CRow>
              <CCol xs="12" sm="12">
                <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel>กฎหมาย</CLabel>
                      {/* <CSelect
                        custom
                        name="mainLaw"
                        id="mainLaw"
                        value={mainLaw}
                        onChange={(e) => onMainLawChange(e.target.value)}
                      >
                        <option value="">กรุณาเลือก</option>
                        {mlData.map((item, index) => {
                          return (
                            <option key={item.id} value={item.id}>
                              {item.mainlawName}
                            </option>
                          );
                        })}
                      </CSelect> */}
                      
                      <Select
                        name="mainLaw"
                        id="mainLaw"
                        value={mainLaw}
                        options={mlData}
                        onChange={(e) => {
                          onMainLawChange(e);
                        }}
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>
              </CCol>
            </CRow>
            <CRow>
              <CCol xs="12" sm="12">
                <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel>สาระสำคัญ</CLabel>
                      {/* <CSelect
                        disabled={userState.role != "FQO"}
                        custom
                        name="mainLawSub"
                        id="mainLawSub"
                        value={mainLawSub}
                        onChange={(e) => onMainLawSubChange(e.target.value)}
                      >
                        <option value="">กรุณาเลือก</option>
                        {mlsData.map((item, index) => {
                          return (
                            <option key={item.id} value={item.id}>
                              {item.description}
                            </option>
                          );
                        })}
                      </CSelect> */}
                      <Select
                        name="mainLawSub"
                        id="mainLawSub"
                        value={mainLawSub}
                        options={mlsData}
                        onChange={(e) => {
                          onMainLawSubChange(e);
                        }}
                        isMulti
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>
              </CCol>
            </CRow>
            <CRow hidden={!mainLawSub}>
              <CCol xs="12" sm="12">
                <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel>กลุ่มของกฎหมายย่อย</CLabel>
                      <CSelect
                        custom
                        name="lawGroupSub"
                        id="lawGroupSub"
                        value={lawGroupSub}
                        // disabled={id !== null}
                        onChange={(e) => setLawGroupSub(e.target.value)}
                        required
                      >
                        <option value="">กรุณาเลือก</option>
                        {lawGroupSubData.map((item, index) => {
                          return (
                            <option key={item.id} value={item.id}>
                              {item.name}
                            </option>
                          );
                        })}
                      </CSelect>
                    </CFormGroup>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel>วันบังคับใช้กฎหมาย</CLabel>
                      <CInput
                        type="date"
                        name="brcsDate"
                        value={brcsDate}
                        onChange={(e) => {
                          setBrcsDate(e.target.value);
                        }}
                        required
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel>ลำดับความสำคัญ</CLabel>
                      <CSelect
                        custom
                        name="priority"
                        id="priority"
                        value={priority}
                        onChange={(e) => setPriority(e.target.value)}
                        required={userState.role == "BU"}
                      >
                        <option value="">กรุณาเลือก</option>
                        {priorityData.map((item, index) => {
                          return (
                            <option key={item.id} value={item.id}>
                              {item.priorityName}
                            </option>
                          );
                        })}
                      </CSelect>
                    </CFormGroup>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel>จำนวนครั้งที่ประเมิน</CLabel>
                      <Select
                        className="basic-single"
                        classNamePrefix="select"
                        options={options}
                        value={estimate}
                        required={userState.role == "BU"}
                        onChange={(e) =>
                          setEstimate(
                            options.filter((d) => d.value === e.value)
                          )
                        }
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol xs="12">
                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="chkAll"
                        name="chkAll"
                        value=""
                        checked={allMonth}
                        onClick={() => {
                          allMonthCheck();
                          setflagClear(!flagClear);
                        }}
                      />
                      <CLabel variant="custom-checkbox" htmlFor="chkAll">
                        เลือกทั้งหมด
                      </CLabel>
                    </CFormGroup>
                    <CButton
                      color="danger"
                      variant="outline"
                      size="sm"
                      onClick={() => clearMonth()}
                    >
                      ยกเลิกการเลือก
                    </CButton>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol xs="12">
                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="month01"
                        name="month01"
                        checked={month01}
                        onChange={(e) => setMonth01(e.target.checked)}
                      />
                      <CLabel variant="custom-checkbox" htmlFor="month01">
                        ม.ค.
                      </CLabel>
                    </CFormGroup>
                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="month02"
                        name="month02"
                        checked={month02}
                        onChange={(e) => setMonth02(e.target.checked)}
                      />
                      <CLabel variant="custom-checkbox" htmlFor="month02">
                        ก.พ.
                      </CLabel>
                    </CFormGroup>

                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="month03"
                        name="month03"
                        checked={month03}
                        onChange={(e) => setMonth03(e.target.checked)}
                      />
                      <CLabel variant="custom-checkbox" htmlFor="month03">
                        มี.ค.
                      </CLabel>
                    </CFormGroup>
                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="month04"
                        name="month04"
                        checked={month04}
                        onChange={(e) => setMonth04(e.target.checked)}
                      />
                      <CLabel variant="custom-checkbox" htmlFor="month04">
                        เม.ย.
                      </CLabel>
                    </CFormGroup>
                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="month05"
                        name="month05"
                        checked={month05}
                        onChange={(e) => setMonth05(e.target.checked)}
                      />
                      <CLabel variant="custom-checkbox" htmlFor="month05">
                        พ.ค.
                      </CLabel>
                    </CFormGroup>
                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="month06"
                        name="month06"
                        checked={month06}
                        onChange={(e) => setMonth06(e.target.checked)}
                      />
                      <CLabel variant="custom-checkbox" htmlFor="month06">
                        มิ.ย.
                      </CLabel>
                    </CFormGroup>
                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="month07"
                        name="month07"
                        checked={month07}
                        onChange={(e) => setMonth07(e.target.checked)}
                      />
                      <CLabel variant="custom-checkbox" htmlFor="month07">
                        ก.ค.
                      </CLabel>
                    </CFormGroup>
                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="month08"
                        name="month08"
                        checked={month08}
                        onChange={(e) => setMonth08(e.target.checked)}
                      />
                      <CLabel variant="custom-checkbox" htmlFor="month08">
                        ส.ค.
                      </CLabel>
                    </CFormGroup>
                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="month09"
                        name="month09"
                        checked={month09}
                        onChange={(e) => setMonth09(e.target.checked)}
                      />
                      <CLabel variant="custom-checkbox" htmlFor="month09">
                        ก.ย.
                      </CLabel>
                    </CFormGroup>
                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="month10"
                        name="month10"
                        checked={month10}
                        onChange={(e) => setMonth10(e.target.checked)}
                      />
                      <CLabel variant="custom-checkbox" htmlFor="month10">
                        ต.ค.
                      </CLabel>
                    </CFormGroup>
                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="month11"
                        name="month11"
                        checked={month11}
                        onChange={(e) => setMonth11(e.target.checked)}
                      />
                      <CLabel variant="custom-checkbox" htmlFor="month11">
                        พ.ย.
                      </CLabel>
                    </CFormGroup>
                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="month12"
                        name="month12"
                        checked={month12}
                        onChange={(e) => setMonth12(e.target.checked)}
                      />
                      <CLabel variant="custom-checkbox" htmlFor="month12">
                        ธ.ค.
                      </CLabel>
                    </CFormGroup>
                  </CCol>
                </CRow>
              </CCol>
            </CRow>
          </CModalBody>
          <CModalFooter>
            <CButton variant="outline" type="submit" color="primary">
              ตกลง
            </CButton>
            <CButton variant="outline" color="secondary" onClick={toggleMl}>
              ยกเลิก
            </CButton>
          </CModalFooter>
        </CForm>
      </CModal>

      {/* <CModal show={modal} onClose={toggle} size="lg">
        <CModalHeader closeButton>
          <CModalTitle>รายละเอียดประเภทกฏหมาย</CModalTitle>
        </CModalHeader>
        <CForm onSubmit={(e) => onSubmit(e)} action="">
          <CModalBody>
          </CModalBody>
          <CModalFooter>
            <CButton variant="outline" type="submit" color="primary">
              ตกลง
            </CButton>
            <CButton variant="outline" color="secondary" onClick={toggle}>
              ยกเลิก
            </CButton>
          </CModalFooter>
        </CForm>
      </CModal> */}
    </>
  );
};

export default Tables;
