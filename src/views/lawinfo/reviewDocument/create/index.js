import React, { useEffect, useRef, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CForm,
  CContainer,
  CLabel,
  CSelect,
  CInput,
  CTabs,
  CTextarea,
} from "@coreui/react";
import Api from '../../../../api/ApiLawInfo';
import MainLaw from "../../component/mainlawTab";
import HowTo from "../../component/howtoTab";
import EmployeeModal from "../../component/employeeModal";
import FilePreview from "./../../../../assets/ตัวอย่าง ประเมินความสอดคล้อง (FM-SE-49).pdf";
import DetailModal from "../../component/detailModal";
import CauseModal from '../../component/causeModal';
import "./index.scss";
import CreatHeadPanel from "../../component/headpanelReadOnly";

const Review = (props) => {
  var id = new URLSearchParams(props.location.search).get("id");
  var code = new URLSearchParams(props.location.search).get("code");
  var buCode = new URLSearchParams(props.location.search).get("buCode");
  const [tabAddress, setTabAddress] = useState("1");
  const [lawid, setLawId] = useState(0);
  const [mainlawid, setMainLawId] = useState();
  const [modal, setModal] = useState(false);
  const [modalDetail, setModalDetail] = useState(false);
  const [modalCause, setModalCause] = useState(false);
  const [modelSaveHeader, setModalSaveHeader] = useState([]);
  const [isHideCancelRemark, setHideCancelRemark] = useState(true);
  const [headerData, setHeaderData] = useState([]);


  useEffect(() => {
    // getData();
    // setStatusData(statusOption);
    setLawId(id);
    return () => {};
  }, []);

  const handleMainLawId = (idx) => {
    setMainLawId(idx);
  };

  const handleTabAddress = (idx) => {
    setTabAddress(idx);
  };
  
  const saveDataMainLaw = async (data) => {
    try {
        const result = await Api.createMainLaw(data);
        if (result.status === 200) {
            const { data } = result.data;
            window.location.href = "/home/lawinfo/searchOnly/search";
        }
    } catch (error) {

    }
  }

  const saveDataHowTo = async (data) => {
    try {
        const result = await Api.createMainLawSub(data);
        if (result.status === 200) {
            const { data } = result.data;
            window.location.href = "/home/lawinfo/searchOnly/search";
        }
    } catch (error) {

    }
  }
  
  const validateDataHeader = async (data) => {
    try {
        if (data.SystemStatus == "CACE") {
          setHideCancelRemark(true);
          // setModal(true);
          setModalSaveHeader(data);
        }else{
          saveDataHeader(data);
        }
    } catch (error) {

    }
  }
  
  const saveDataHeader = async (data) => {
    try {
        const result = await Api.create(data);
        if (result.status === 200) {
            const { data } = result.data;
            window.location.href = "/home/lawinfo/searchOnly/search";
        }
    } catch (error) {

    }
  }

  return (
    <>
      <h2>สร้างข้อมูลกฏหมาย</h2>
      <CCard fluid className="mt-3">
        <CCardHeader>
          <CRow>
            <CCol sm="12">
              ข้อมูลกฏหมาย
            </CCol>
          </CRow>
          <div style={{ fontSize: "12px" }}>
            <CreatHeadPanel id={id} validateDataHeader={validateDataHeader} isHideCancelRemark={isHideCancelRemark} setHeaderData={setHeaderData} page={"searchOnly"}/>
          </div>
        </CCardHeader>
        <CCardBody>
          <CTabs
            activeTab={tabAddress}
            onActiveTabChange={(idx) => handleTabAddress(idx)}
          >
            <CNav variant="tabs">
              <CNavItem>
                <CNavLink data-tab="1">
                  <b>กฎหมายหลัก</b>
                </CNavLink>
              </CNavItem>
              <CNavItem>
                <CNavLink data-tab="2" disabled>
                  <b>วิธีการดำเนินงาน/แผนการดำเนินงาน</b>
                </CNavLink>
              </CNavItem>
              <CNavItem>
                <CNavLink data-tab="3">
                  <b>แนบไฟล์</b>
                </CNavLink>
              </CNavItem>
            </CNav>
            <CTabContent>
              <CTabPane data-tab="1">
                <MainLaw
                  lawid={lawid}
                  handleMainLawId={handleMainLawId}
                  handleTabAddress={handleTabAddress}
                  saveDataMainLaw={saveDataMainLaw}
                  headerData={headerData}
                  isView={true}
                  code={code}
                  buCode={buCode}
                />
              </CTabPane>
              <CTabPane data-tab="2">
                <HowTo
                  modalCauseOpen={setModalCause}
                  modalDetailOpen={setModalDetail}
                  mainlawId={mainlawid}
                  saveDataHowTo={saveDataHowTo}
                  headerData={headerData}
                />
              </CTabPane>
              <CTabPane data-tab="3">
                <div>แนบไฟล์</div>
              </CTabPane>
            </CTabContent>
          </CTabs>
        </CCardBody>
      </CCard>
      <EmployeeModal modal={modal} setModal={setModal} />
      <DetailModal modal={modalDetail} setModal={setModalDetail} />
      <CauseModal modal={modalCause} setModal={setModalCause} />
    </>
  );
};

export default Review;
