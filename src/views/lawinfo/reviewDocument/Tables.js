import React, { useState, useEffect } from "react";
import {
  CCardBody,
  CBadge,
  CButton,
  CCollapse,
  CDataTable,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CCol,
  CCard,
  CRow,
  CCardHeader,
  CFormGroup,
  CLabel,
  CInput,
  CSelect,
  CForm,
  CModalTitle,
  CButtonGroup,
  CInputCheckbox,
} from "@coreui/react";
import Api from "../../../api/ApiLawInfo";
import Swal from "sweetalert2/src/sweetalert2.js";
import TableToggle from "./TableToggle";
import { useSelector, useDispatch } from "react-redux";
import { getTranBrcsBu } from "../../../redux/gettranbrcsbu/gettranbrcsbuAction";
const getStatus = (status) => {
  switch (status) {
    case "SADR":
      return "Save";
    case "NEDO":
      return "Submit";
    case "CACE":
      return "Delete";
    case "SEEV":
      return "Send To Evaluate";
    case "REFQ":
      return "Reject To FQO";
    case "EVAU":
      return "Send To Approver 1";
    case "REBU":
      return "Reject To BU";
    case "SEA2":
      return "Send To Approver 2";
    case "REDI":
      return "Reject To User Division";
    case "DOAP":
      return "Document Approve";
    case "REA1":
      return "Reject To Approve 1";
    default:
      return "";
  }
};

const getBadge = (status) => {
  switch (status) {
    case "SADR":
      return "light";
    case "NEDO":
      return "secondary";
    case "CACE":
      return "dark";
    case "SEEV":
      return "info";
    case "REFQ":
      return "dark";
    case "EVAU":
      return "warning";
    case "REBU":
      return "dark";
    case "SEA2":
      return "danger";
    case "REDI":
      return "dark";
    case "DOAP":
      return "success";
    case "REA1":
      return "dark";
  }
};

const fields = [
  { key: "select", label: "", filter: false },
  { key: "docno", label: "เลขที่เอกสาร" , _style: { width: "15%" }},
  { key: "mainLawName", label: "ชื่อกฎหมาย"  },
  { key: "code", label: "โครงสร้าง" },
  { key: "revisioN_NO", label: "ครั้งที่แก้ไข" },
  { key: "systeM_STATUS", label: "สถานะ" },
  { key: "creatE_DATE", label: "วันที่สร้างเอกสาร" },
  { key: "creatE_BY", label: "ผู้สร้างเอกสาร" },
  { key: "updatE_DATE", label: "วันที่แก้ไข" },
  { key: "updatE_BY", label: "ผู้แก้ไข" },
  { key: "reviewdate", label: "วันที่ทบทวนเอกสาร" },
  { key: "option", label: "", _style: { width: "12%" }, filter: false },
];

const subfields = [
  // { key: "select", label: "", filter: false },
  { key: "docno", label: "เลขที่เอกสาร" },
  { key: "code", label: "โครงสร้าง" },
  { key: "revisioN_NO", label: "ครั้งที่แก้ไข" },
  { key: "systeM_STATUS", label: "สถานะ" },
  { key: "creatE_DATE", label: "วันที่สร้างเอกสาร" },
  { key: "creatE_BY", label: "ผู้สร้างเอกสาร" },
  { key: "updatE_DATE", label: "วันที่แก้ไข" },
  { key: "updatE_BY", label: "ผู้แก้ไข" },
  { key: "reviewdate", label: "วันที่ทบทวนเอกสาร" },
  { key: "option", label: "", _style: { width: "12%" }, filter: false },
];

const Tables = ({ data = [], refreshData = () => {}, searchData }) => {
  const [selected, setSelected] = useState([]);
  const userState = useSelector((state) => state.changeState.user);
  const [details, setDetails] = useState([]);
  const [lastIndex, setLastIndex] = useState(null);
  const [modal, setModal] = useState(false);
  const [month, setMonth] = useState('');
  const [date, setDate] = useState([]);
  const dispatch = useDispatch();
  const tranbuState = useSelector((state) => {
    console.log(state.gettranbrcsbu);
    return state.gettranbrcsbu.data;
  });

  const toggle = () => {
      setModal(!modal);
  }

  useEffect(() => {
    setDetails([]);
    return () => {};
  }, [searchData]);


  const check = (e, id) => {
    if (e.target.checked) {
      setSelected([...selected, id]);
    } else {
      setSelected(selected.filter((itemId) => itemId !== id));
    }
  };

  const toggleDetails = (index) => {
      console.log("data", data);
      const position = data.indexOf(index);
    let newDetails = data.slice();
    if (lastIndex == index) {
      newDetails.splice(position, 1);
      setLastIndex(null);
    } else {
      newDetails = [...data, index];
      setLastIndex(index);
    }
    setDetails(newDetails);
      console.log("setDetail", details);
      // console.log(details);
  };

  const getSubTable = async (Docno) => {
      
    searchData.refDocNoForBu = Docno;
    searchData.page = "reviewDocumentSub";

    try {
      // const result = await Api.getTranBrcsBu(searchData);
      // if (result.status == 200) {
      //   setDetails(result.data.response);
      // }
      dispatch(getTranBrcsBu(searchData));
    } catch (error) {
      console.log(error.message);
    }
  };

  const reviewDocBtn = (id) => {
      console.log(id);
      setSelected([id]);
      console.log(selected);
      setDate([]);
      toggle();
    // console.log(id);
    // const model = {
    //   brcsIds: id,
    //   user: userState.id,
    // };
    // submitReview(model);
  };

  const reviewDoc = () => {
    console.log(selected);
    if(selected.length == 0){
      Swal.fire({
        icon: "error",
        title: "กรุณาเลือกรายการ",
      });
      return false;
    }
    setDate([]);
    toggle();
    // const model = {
    //   brcsIds: selected.join(","),
    //   user: userState.id,
    // };
    // submitReview(model);
  };

  const onSubmit =(e) => {
    e.preventDefault();
    const model = {
      brcsIds: selected.join(","),
      user: userState.id,
      date: date
    };
    console.log("model", model);
    submitReview(model);
    setSelected([]);
  }

  const submitReview = async (data) => {
    try {
      const result = await Api.review(data);
      if (result.status === 200) {
        const { data } = result.data;
        refreshData();
        toggle();

        Swal.fire({
          icon: "success",
          title: "Save Success",
        });
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: error.response.data,
      });
    }
  };

  const selectAll = () => {
    const selectDataAll = data.map((x) => x.id);
    setSelected(selectDataAll);
  };
  const unSelectAll = () => {
    setSelected([]);
  };


  return (
    <>
      <CRow>
        <CCol xs="12" lg="12">
          <CButton className="mr-2" onClick={selectAll} color={"info"} variant={"outline"}>
            <span className="ml-2">เลือกทั้งหมด</span>
          </CButton>
          <CButton className="mr-2" onClick={unSelectAll} color={"danger"} variant={"outline"}>
            <span className="ml-2">ยกเลิก</span>
          </CButton>
          <CButton onClick={reviewDoc} color={"success"} variant={"outline"}>
            <span className="ml-2">ทบทวนเอกสาร</span>
          </CButton>
        </CCol>
      </CRow>
      <CRow className="">
        <CCol xs="12" lg="12">
          <CDataTable
            items={data}
            fields={fields}
            // tableFilter={{ label: "ค้นหา", placeholder: 'พิมพ์คำที่ต้องการค้นหา' }}
            // cleaner
            itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
            itemsPerPage={10}
            hover
            sorter
            striped
            bordered
            pagination
            scopedSlots={{
              select: (item) => {
                return (
                  <td>
                    <CFormGroup variant="custom-checkbox">
                      <CInputCheckbox
                        custom
                        id={`checkbox-${item.id}`}
                        checked={selected.includes(item.id)}
                        onChange={(e) => check(e, item.id)}
                      />
                      <CLabel
                        variant="custom-checkbox"
                        htmlFor={`checkbox-${item.id}`}
                      />
                    </CFormGroup>
                  </td>
                );
              },
              docno: (item, index) => (
                <td>
                  <label
                    style={{ cursor: "pointer", color: "blue" }}
                    onClick={() => {
                      toggleDetails(index);
                      getSubTable(item.docno);
                    }}
                  >
                    {details.includes(index) ? item.docno : item.docno}
                  </label>
                </td>
              ),
              mainLawName: (item) => (
                <td>
                  {item.mainLawName[0]}
                </td>
              ),
              systeM_STATUS: (item) => (
                <td>
                  <CBadge color={getBadge(item.systeM_STATUS)}>
                                {getStatus(item.systeM_STATUS)}
                              </CBadge>
                </td>
              ),
              option: (item) => (
                <td className="center">
                  <CButtonGroup>
                    <CButton
                      color="success"
                      variant="outline"
                      // shape="square"
                      size="sm"
                      onClick={() =>
                        (window.location.href = `/home/lawinfo/reviewDocument/create?id=${item.traN_BRS_ID}&code=${item.id}&buCode=${item.bU_ID}`)
                      }
                    >
                      {/* <CIcon name="cilPen" /> */}
                      รายละเอียด
                    </CButton>
                    <CButton variant="ghost" size="sm" />
                    <CButton
                      color="danger"
                      variant="outline"
                      shape="square"
                      size="sm"
                      onClick={() => { reviewDocBtn(item.id) }}
                    >
                      ทบทวนเอกสาร
                    </CButton>
                  </CButtonGroup>
                </td>
              ),
              details: (item, index) => {
                return (
                  <CCollapse show={details.includes(index)}>
                    <CCardBody>
                      <CDataTable
                        items={tranbuState}
                        fields={subfields}
                        hover
                        sorter
                        striped
                        bordered
                        pagination
                        scopedSlots={{
                          option: (items) => (
                            <td className="center">
                              <CButtonGroup>
                                <CButton
                                  color="success"
                                  variant="outline"
                                  // shape="square"
                                  size="sm"
                                  onClick={() =>
                                    (window.location.href = `/home/lawinfo/reviewDocument/create?id=${item.traN_BRS_ID}&code=${item.id}&buCode=${item.bU_ID}`)
                                  }
                                >
                                  {/* <CIcon name="cilPen" /> */}
                                  รายละเอียด
                                </CButton>
                              </CButtonGroup>
                            </td>
                          ),
                          systeM_STATUS: (items) => (
                            <td>
                              <CBadge color={getBadge(items.systeM_STATUS)}>
                                {getStatus(items.systeM_STATUS)}
                              </CBadge>
                            </td>
                          ),
                        }}
                      ></CDataTable>
                    </CCardBody>
                  </CCollapse>
                );
              },
            }}
          />
        </CCol>
      </CRow>
      {/* <TableToggle modal={modal} toggle={toggle} /> */}
      <CModal
                show={modal}
                onClose={toggle}
            >
                <CModalHeader closeButton>
                    <CModalTitle>กรุณาเลือกวันที่ทบทวนเอกสาร</CModalTitle>
                </CModalHeader>
                <CForm onSubmit={onSubmit} action="">
                    <CModalBody>
                        <CRow>
                            <CCol xs="12">
                                <CFormGroup>
                                    <CLabel htmlFor="month">วันที่ทบทวนเอกสาร</CLabel>
                                    <CInput 
                                    displayFormat="DD/MM/YYYY"
                                    onChange={(e) => setDate(e.target.value)}
                                    value={date}
                                    id="date"
                                    required
                                    type="date" />
                                    {/* <CSelect custom name="month" id="month" value={month} onChange={e => setMonth(e.target.value)} required>
                                        <option value={""}>-- กรุณาเลือกเดือน --</option>
                                        <option value={"01"}>มกราคม</option>
                                        <option value={"02"}>กุมภาพันธ์</option>
                                        <option value={"03"}>มีนาคม</option>
                                        <option value={"04"}>เมษายน</option>
                                        <option value={"05"}>พฤษภาคม</option>
                                        <option value={"06"}>มิถุนายน</option>
                                        <option value={"07"}>กรกฎาคม</option>
                                        <option value={"08"}>สิงหาคม</option>
                                        <option value={"09"}>กันยายน</option>
                                        <option value={"10"}>ตุลาคม</option>
                                        <option value={"11"}>พฤศจิกายน</option>
                                        <option value={"12"}>ธันวาคม</option>
                                    </CSelect> */}
                                </CFormGroup>
                            </CCol>
                        </CRow>
                    </CModalBody>
                    <CModalFooter>
                        <CButton variant="outline" type="submit" color="primary">ตกลง</CButton>
                        <CButton
                            variant="outline"
                            color="secondary"
                            onClick={toggle}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CForm >
            </CModal >
    </>
  );
};

export default Tables;
