import React, { useEffect, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CNav,
  CForm,
  CContainer,
  CLabel,
  CSelect,
  CInput,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import Api from "../../api/ApiLawInfo";
import Tables from "./Tables";
import SearchPanel from "./component/searchpanel";

const LawInfo = () => {
  const [dataTable, setDataTable] = useState([]);
  const [searchData, setSearchData] = useState([]);

  const [statusData, setStatusData] = useState([]);
  const statusOption = [
    { value: "SADR", label: "Save" },
    { value: "NEDO", label: "Submit" },
    { value: "REFQ", label: "Reject To FQO" },
  ];

  useEffect(() => {
    setStatusData(statusOption);
    getData();
    return () => {};
  }, []);

  const getData = async (data) => {
    try {
      setSearchData(data);
      if (data == undefined) {
        data = {
          page: "info",
        };
      } else {
        data.page = "info";
      }
      const result = await Api.get(data);
      if (result.status == 200) {
        setDataTable(result.data);
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <CRow>
        <CCol sm="9">
          <h2>สร้างเอกสารกฎหมาย</h2>
        </CCol>
        <CCol sm="3">
          <CNav className="card-header-actions">
            <CForm inline>
              <CButton
                // size="sm"
                variant="outline"
                color="success"
                // disabled={selected.length <= 0}
                style={{ marginRight: "4px" }}
                onClick={() =>
                  (window.location.href = "/home/lawinfo/create")
                }
              >
                <CIcon
                  size="sm"
                  name="cil-check-circle"
                  style={{ marginRight: "2px" }}
                />
                <b> สร้างข้อมูลกฎหมาย </b>
              </CButton>
            </CForm>
          </CNav>
        </CCol>
      </CRow>
      <CCard fluid className="mt-3">
        <CCardHeader>
          <CRow>
            <CCol sm="12">
              ค้นหากฏหมาย
            </CCol>
          </CRow>
          <div style={{ fontSize: "12px" }}>
            <SearchPanel getData={getData} statusOption={statusData} />
          </div>
        </CCardHeader>
        <CCardBody>
          <Tables refreshData={getData} data={dataTable} page={"info"} searchData={searchData} />
        </CCardBody>
      </CCard>
    </>
  );
};

export default LawInfo;
