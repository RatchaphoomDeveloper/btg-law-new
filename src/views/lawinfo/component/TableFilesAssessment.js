import React, { useEffect, useCallback, useState } from "react";
import { CDataTable, CCard, CCardBody, CButton, CButtonGroup,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CModalTitle, } from "@coreui/react";
import CIcon from '@coreui/icons-react'
import Api from "../../../api/ApiLawInfo";
import { saveDataToTransBrcsFileDB } from "../../../redux/savetranbrcsfiledb/savetranbrsfileAction";
import { WEB_API, WEB_FILE_URL } from "../../../env";
import { useDispatch, useSelector } from "react-redux";
import { Upload, message, Button } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import Swal from "sweetalert2/dist/sweetalert2.js";

const fields = [
  // { key: "id", _style: { width: "40%" } },
  // { key: "filename", _style: { width: "40%" } },
  { key: "filename" },
  // "registered",
  // { key: "role", _style: { width: "20%" } },
  // { key: "status", _style: { width: "20%" } },
  // {
  //   key: "show_details",
  //   label: "",
  //   _style: { width: "1%" },
  //   sorter: false,
  //   filter: false,
  // },
  { key: 'option', label: '', _style: { width: '1%' }, filter: false, },
];
const TableFiles = ({ assessId, page, systemStatus, setFileCount }) => {
  const [fileList, setFileList] = useState([]);
  const userState = useSelector((state) => state.changeState.user);
  const userStateToken = useSelector((state) => state.changeState.token);
  const [successFileFlag,setsuccessFileFlag] = useState(false)
  const [dataTable, setDataTable] = useState([]);
  const [modalConfirm, setModalConfirm] = useState(false);
  const [dataSelected, setDataSelected] = useState('');
  const [id, setId] = useState('');

  useEffect(() => {
    getData();
    return () => {};
  }, []);

  const getData = async () => {
    try {
      const result = await Api.getAssessmentFile(assessId);
      if (result.status == 200) {
        setDataTable(result.data);
        setFileCount(result.data.length);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getFilePromise = (val) => {
    return new Promise((resolve, reject) => {
      setTimeout(async () => {
        let results = await Api.getTranBrcsFileSel(val);
        if (results.status === 200) {
          resolve(results.data.response);
          setsuccessFileFlag(false)
        }
      }, 600);
    });
  };
  
  const getFileAsync = async (val) => {
    let results = await getFilePromise(val);
    setFileList(results);

  };

  const fileprops = {
    name: "files",
    multiple: true,
    action: `${WEB_API}Master/SaveTranBrcsFile`,
    headers: {
      authorization: `Bearer ${userStateToken}`,
    },
    // action: `https://www.mocky.io/v2/5cc8019d300000980a055e76`,
    onChange(info) {
      const { status } = info.file;
      if (info.file.status !== "uploading") {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === "done") {
        saveFileToDB(info.file);
        console.log(`${info.file.name} file uploaded successfully`);
        // message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === "error") {
        console.log(`${info.file.name} file upload failed.`);
        // message.error(`${info.file.name} file upload failed.`);
      }
    },
  };

  const saveFileToDB = async (data) => {
    try {
      const model = {
        AssessId: assessId,
        FileName: data.name,
        FilePath: data.response.url,
        user: userState.id,
      };
      const result = await Api.saveAssessFileToDB(model);
        if (result.status === 200) {
            const { data } = result.data;
            getData();
            // window.location.reload();
        }
    } catch (error) {
      console.log(error);
    }
  };

  const toggleConfirm = () => {
    setModalConfirm(!modalConfirm);
  }

  const deleteDetails = (record) => {
      toggleConfirm();
      setDataSelected(record);
      setId(record.id);
  }

  const onDelete = async () => {
    try {
        const result = await Api.deleteFileAssessment(dataSelected);
        if (result.status === 200) {
            const { data } = result.data;
            getData();
            toggleConfirm();
            Swal.fire({
                icon: "success",
                title: "Delete Success",
            });
        }
      } catch (error) {
          Swal.fire({
              icon: "error",
              title: error.response.data,
            });
      }
  }

  function generateUploadBtn(){
    if (page == "estimate"){
      return(
        <>
          <div style={{ paddingTop: "1pc" }} hidden={(systemStatus != "SEEV" && systemStatus != "REDI" && systemStatus != "CLNE" && systemStatus != "INPR" && systemStatus != "RJCL") || page == "searchAssess"}>
            <Upload {...fileprops} maxCount={1}  showUploadList={false}>
              <Button icon={<UploadOutlined />}>แนบไฟล์</Button>
              <div style={{ paddingTop: "0.5pc" }}>
                <label style={{ color: "red", fontSize: "12px" }}>
                  * สามารถอัพโหลดไฟล์ ประเภท
                  .xls,.xlsx,.pdf,.xml,.doc,.docx,.bmp,.gif,.jpg,.jpeg,.png
                  ได้ไม่เกิน 5 ไฟล์ ขนาดไม่เกิน 5 เมกะไบท์
                </label>
              </div>
            </Upload>
          </div>
        </>
      );
    }
  }

  return (
    <div>
      {generateUploadBtn()}
      {fileList && (
        <CDataTable
          items={dataTable}
          fields={fields}
          tableFilter
          itemsPerPageSelect
          itemsPerPage={10}
          hover
          sorter
          pagination
          scopedSlots={{
            filename: (item, index) => (
              <td>
                
                <a
                  target="_blank"
                  download=""
                  href={`${WEB_FILE_URL}${item.filepath}`}
                >
                  {item.filename}
                </a>
              </td>
            ),
            option:
                (item) => (
                    <td className="center">
                        <CButtonGroup>
                            <CButton
                                hidden={(systemStatus != "SEEV" && systemStatus != "REDI" && systemStatus != "CLNE" && systemStatus != "INPR" && systemStatus != "RJCL") || page == "searchAssess"}
                                color="danger"
                                variant="outline"
                                shape="square"
                                size="sm"
                                onClick={() => { deleteDetails(item) }}
                            >
                                <CIcon name="cilTrash" />
                            </CButton>
                        </CButtonGroup>
                    </td>
                ),
          }}
        />
      )}

      <CModal show={modalConfirm} onClose={setModalConfirm} color="danger" >
          <CModalHeader closeButton>
              <CModalTitle> Delete Data :</CModalTitle>
          </CModalHeader>
          <CModalBody>
              Are you confirm to delete ?
          </CModalBody>
          <CModalFooter>
              <CButton onClick={onDelete} color={"primary"} variant={"outline"}>Confirm</CButton>{' '}
              <CButton variant="outline" color="secondary" onClick={() => setModalConfirm(false)}>Close</CButton>
          </CModalFooter>
      </CModal>
    </div>
  );
};

export default TableFiles;
