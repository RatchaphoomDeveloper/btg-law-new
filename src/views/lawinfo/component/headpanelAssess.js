import React, { useEffect, useState, useCallback } from "react";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CNav,
  CForm,
  CContainer,
  CLabel,
  CSelect,
  CInput,
  CTextarea,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import ApiSystemType from "../../../api/ApiSystemType";
import ApiCountryAssign from "../../../api/ApiCountryAssign";
import ApiCountry from "../../../api/ApiCountry";
import ApiCountryType from "../../../api/ApiCountryType";
import ApiLawGroup from "../../../api/ApiLawGroup";
import ApiLawGroupSub from "../../../api/ApiLawGroupSub";
import ApiLawAgencies from "../../../api/ApiLawAgencies";
import ApiLawAgencieSub from "../../../api/ApiLawAgencieSub";
import ApiLawType from "../../../api/ApiLawType";
import ApiBu from "../../../api/ApiBU";
import Api from "../../../api/ApiLawInfo";
import { useParams } from "react-router-dom";
// import statusData from './data';
import Select from "react-select";
import Swal from "sweetalert2/dist/sweetalert2.js";
import store from "../../../store";
import {
  setaddLawinfo,
  setrefreshLawinfo,
} from "../../../redux/lawinfoDetail/lawinfoAction";
import _ from "underscore";

const CreatHeadPanel = ({
  id,
  assessId,
  validateDataHeader,
  page,
  code,
  systemStatus,
  onSubmitVerify = () => {},
}) => {
  const dispatch = useDispatch();
  const userState = useSelector((state) => state.changeState.user);
  const lawinfo = useSelector((state) => state.lawinfoReducer.data);
  const [systemTypeData, setSystemTypeData] = useState([]);
  const [countryAssignData, setCountryAssignData] = useState([]);
  const [countryData, setCountryData] = useState([]);
  const [countryTypeData, setCountryTypeData] = useState([]);
  const [lawGroupData, setLawGroupData] = useState([]);
  const [lawGroupSubData, setLawGroupSubData] = useState([]);
  const [deptData, setDeptData] = useState([]);
  const [deptSubData, setDeptSubData] = useState([]);
  const [lawTypeData, setLawTypeData] = useState([]);
  const [buData, setBUData] = useState([]);
  const [singleBuData, setSingleBuDat] = useState([]);
  const [systemtype, setSystemType] = useState("");
  const [countryAssign, setCountryAssign] = useState("");
  const [countryType, setCountryType] = useState("");
  const [country, setCountry] = useState("");
  const [lawGroup, setLawGroup] = useState("");
  const [lawGroupSub, setLawGroupSub] = useState("");
  const [lawAgencie, setLawAgencie] = useState("");
  const [lawAgencieSub, setLawAgencieSub] = useState("");
  const [lawType, setLawType] = useState("");
  const [status, setStatus] = useState("");
  const [docNo, setDocNo] = useState("");
  const [revisionNo, setRevisionNo] = useState("");
  const [createBy, setCreateBy] = useState("");
  const [createDate, setCreateDate] = useState("");
  const [updateDate, setUpdateDate] = useState("");
  const [sysStatus, setsysStatus] = useState("");
  const [buList, setBuList] = useState("");
  const [buListDefault, setBuListDefault] = useState("");

  useCallback(() => {}, [lawinfo]);

  useEffect(() => {
    getLawInfoDetail();
    //   getSystemTypeOption();
    // getCountryAssignOption();
    // getCountryTypeOption();
    // getCountryOption();
    // getLawGroupOption();
    // getLawGroupSubOption();
    // getDeptOption();
    // getDeptSubOption();
    // getLawTypeOption();
    // getBuOption();
    // if (id != null) {
    //   getLawInfoDetail();
    //   getBuOption();
    // } else {
    //   dispatch(setaddLawinfo({}));
    // }
    return () => {};
  }, []);

  // useEffect(() => {
  //   getDefaultBuOption();
  //   return () => {};
  // }, [lawinfo]);

  useEffect(() => {
    generateSaveBtn();
    return () => {};
  }, [systemStatus]);

  const getLawInfoDetail = async () => {
    try {
      if(code == null || code == undefined){
        code='';
      }
      const result = await Api.getDetail(id, code);
      if (result.status == 200) {
        setData(result.data);
        dispatch(setaddLawinfo(result.data));
      }
    } catch (error) {
      console.log(error);
    }
  };

  const setData = (data) => {
    setSystemType(data.systemtype);
    setCountryAssign(data.countryassign);
    setCountry(data.country);
    setLawGroup(data.lawgroup);
    setLawGroupSub(data.lawgroupSub);
    setLawAgencie(data.lawagency);
    setLawAgencieSub(data.lawagencySub);
    setLawType(data.lawtype);
    setDocNo(data.docno);
    setRevisionNo(data.revisionNo);
    setStatus(data.systemStatus);
    setCreateBy(data.createBy);
    setCreateDate(data.createDate);
    setUpdateDate(data.updateDate);
    setsysStatus(data.systemStatus);
    setBuListDefault(data.buIdList);
    setBuList(data.buIdList);
  };


  const onSubmit = (e,type) => {
    e.preventDefault();
    const model = {
      id: assessId,
      user: userState.id,
      type: type,
    };
    if (model.SystemStatus !== "") {
      validateDataHeader(model);
    } else {
      Swal.fire({
        icon: "error",
        title: "Process Error!",
        text: "Please choose system status!",
      });
      dispatch({ type: "SET_HIDE_TABLE", status: 0 });
    }
  };

  function generateSaveBtn() {
    if (page == "estimate") {
      if (userState.role == "USER"){
        if (systemStatus == "INPR" || systemStatus == "RJCL"){
            return (
              <>
                <CCol md="6" className="mt-4">
                  <CRow>
                    <CCol sm="12" md="4" className="justify-content-center">
                      <CButton
                        color="success"
                        variant="outline"
                        block
                        onClick={(e) => onSubmit(e,"CLOSE")}
                      >
                        <b> Sent to Verify </b>
                      </CButton>
                    </CCol>
                    <CCol sm="12" md="4" className="justify-content-center">
                      <CButton
                        color="success"
                        variant="outline"
                        block
                        onClick={(e) => onSubmit(e,"CLOSENEW")}
                      >
                        <b> Close New </b>
                      </CButton>
                    </CCol>
                    <CCol sm="12" md="2" className="justify-content-center">
                      <CButton
                        color="primary"
                        variant="outline"
                        block
                        onClick={(e) => onSubmit(e,"CANCEL")}
                      >
                        <b> Cancel </b>
                      </CButton>
                    </CCol>
                  </CRow>
                </CCol>
              </>
            );
          }
          else if (systemStatus == "SEEV" || systemStatus == "CLNE" || systemStatus == "REDI"){
            return (
              <>
                <CCol md="6" className="mt-4">
                  <CRow>
                    <CCol sm="12" md="4" className="justify-content-center">
                      <CButton
                        color="success"
                        variant="outline"
                        block
                        onClick={(e) => onSubmitVerify(e,"SAVE")}
                      >
                        <b> Save </b>
                      </CButton>
                    </CCol>
                    <CCol sm="12" md="4" className="justify-content-center">
                      <CButton
                        color="success"
                        variant="outline"
                        block
                        onClick={(e) => onSubmitVerify(e,"APPROVE")}
                      >
                        <b> Send To Verify </b>
                      </CButton>
                    </CCol>
                    <CCol sm="12" md="2" className="justify-content-center">
                      <CButton
                        color="primary"
                        variant="outline"
                        block
                        onClick={(e) => onSubmit(e,"CANCEL")}
                      >
                        <b> Cancel </b>
                      </CButton>
                    </CCol>
                  </CRow>
                </CCol>
              </>
            );
          }
        }
      } else if (page == "approve1") {
        if (systemStatus == "CLOS"){
          return (
            <>
              <CCol md="6" className="mt-4">
                <CRow>
                  <CCol sm="12" md="4" className="justify-content-center">
                    <CButton
                      color="success"
                      variant="outline"
                      block
                      onClick={(e) => onSubmit(e,"CLOSE2")}
                    >
                      <b> Send To Approver </b>
                    </CButton>
                  </CCol>
                  <CCol sm="12" md="4" className="justify-content-center">
                    <CButton
                      color="danger"
                      variant="outline"
                      block
                      onClick={(e) => onSubmit(e,"REJECTCLOSE")}
                    >
                      <b> Reject To User Division </b>
                    </CButton>
                  </CCol>
                  <CCol sm="12" md="2" className="justify-content-center">
                    <CButton
                      color="primary"
                      variant="outline"
                      block
                      onClick={(e) => onSubmit(e,"CANCEL")}
                    >
                      <b> Cancel </b>
                    </CButton>
                  </CCol>
                </CRow>
              </CCol>
            </>
          );
        }
        return (
          <>
            <CCol md="6" className="mt-4">
              <CRow>
                <CCol sm="12" md="4" className="justify-content-center">
                  <CButton
                    color="success"
                    variant="outline"
                    block
                    onClick={(e) => onSubmit(e,"APPROVE")}
                  >
                    <b> Send To Approver </b>
                  </CButton>
                </CCol>
                <CCol sm="12" md="4" className="justify-content-center">
                  <CButton
                    color="danger"
                    variant="outline"
                    block
                    onClick={(e) => onSubmit(e,"REJECT")}
                  >
                    <b> Reject To User Division </b>
                  </CButton>
                </CCol>
                <CCol sm="12" md="2" className="justify-content-center">
                  <CButton
                    color="primary"
                    variant="outline"
                    block
                    onClick={(e) => onSubmit(e,"CANCEL")}
                  >
                    <b> Cancel </b>
                  </CButton>
                </CCol>
              </CRow>
            </CCol>
          </>
        );
      } else if (page == "approve2") {
        if (systemStatus == "CLS2"){
          return (
            <>
              <CCol md="6" className="mt-4">
                <CRow>
                  <CCol sm="12" md="4" className="justify-content-center">
                    <CButton
                      color="success"
                      variant="outline"
                      block
                      onClick={(e) => onSubmit(e,"APPROVE")}
                    >
                      <b> Approve </b>
                    </CButton>
                  </CCol>
                  <CCol sm="12" md="4" className="justify-content-center">
                    <CButton
                      color="danger"
                      variant="outline"
                      block
                      onClick={(e) => onSubmit(e,"REJECTCLOSE")}
                    >
                      <b> Reject To User Division </b>
                    </CButton>
                  </CCol>
                  <CCol sm="12" md="2" className="justify-content-center">
                    <CButton
                      color="primary"
                      variant="outline"
                      block
                      onClick={(e) => onSubmit(e,"CANCEL")}
                    >
                      <b> Cancel </b>
                    </CButton>
                  </CCol>
                </CRow>
              </CCol>
            </>
          );
        }
        return (
          <>
            <CCol md="6" className="mt-4">
              <CRow>
                <CCol sm="12" md="4" className="justify-content-center">
                  <CButton
                    color="success"
                    variant="outline"
                    block
                    onClick={(e) => onSubmit(e,"APPROVE")}
                  >
                    <b> Approve </b>
                  </CButton>
                </CCol>
                <CCol sm="12" md="4" className="justify-content-center">
                  <CButton
                    color="danger"
                    variant="outline"
                    block
                    onClick={(e) => onSubmit(e,"REJECT")}
                  >
                    <b> Reject To User Division </b>
                  </CButton>
                </CCol>
                <CCol sm="12" md="2" className="justify-content-center">
                  <CButton
                    color="primary"
                    variant="outline"
                    block
                    onClick={(e) => onSubmit(e,"CANCEL")}
                  >
                    <b> Cancel </b>
                  </CButton>
                </CCol>
              </CRow>
            </CCol>
          </>
        );
      }
  }
  return (
    <>
      <CRow>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>ระบบ</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
            <CInput
                name="systemtype"
                id="systemtype"
                value={systemtype}
                // onChange={(e) => setSystemType(e.target.value)}
                disabled/>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>แหล่งที่มากฎหมาย</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
            <CInput
                name="countryAssign"
                id="countryAssign"
                value={countryAssign}
                // onChange={(e) => setCountryAssign(e.target.value)}
                disabled/>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>ประเทศที่ออกกฎหมาย</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
            <CInput
                name="country"
                id="country"
                value={country}
                // onChange={(e) => setCountry(e.target.value)}
                disabled/>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>ประเภทกฎหมาย</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
            <CInput
                name="lawType"
                id="lawType"
                value={lawType}
                // onChange={(e) => setLawType(e.target.value)}
                disabled/>
            </CCol>
          </CRow>
        </CCol>
      </CRow>
      <CRow className="mt-3">
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>หน่วยงานผู้ออกกฎหมาย</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
            <CInput
                name="dept"
                id="dept"
                value={lawAgencie}
                // onChange={(e) => setLawAgencie(e.target.value)}
                disabled/>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>หน่วยงานผู้ออกกฎหมายย่อย</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
            <CInput
                name="dept"
                id="dept"
                value={lawAgencieSub}
                // onChange={(e) => setLawAgencieSub(e.target.value)}
                disabled/>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>กลุ่มของกฎหมาย</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
            <CInput
                name="lawGroup"
                id="lawGroup"
                value={lawGroup}
                // onChange={(e) => onLawGroupChange(e.target.value)}
                disabled/>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>กลุ่มของกฎหมายย่อย</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
            <CInput
                name="lawGroupSub"
                id="lawGroupSub"
                value={lawGroupSub}
                // onChange={(e) => setLawGroupSub(e.target.value)}
                disabled/>
            </CCol>
          </CRow>
        </CCol>
      </CRow>
      <CRow className="mt-3">
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>หมายเลขเอกสาร</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
              <CInput value={docNo} readOnly />
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>ชื่อผู้สร้างเอกสาร</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
              <CInput value={createBy} readOnly />
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>วันที่สร้างเอกสาร</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
              <CInput value={createDate} readOnly />
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>วันที่แก้ไข</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
              <CInput value={updateDate} readOnly />
            </CCol>
          </CRow>
        </CCol>
      </CRow>
      <CRow className="mt-3">
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>ครั้งที่แก้ไข</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
              <CInput value={revisionNo} readOnly />
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel>
                {" "}
                <b>ธุรกิจที่เกี่ยวข้อง</b>{" "}
              </CLabel>
            </CCol>
            <CCol col xs="12" md="12">
            <CInput
                name="form-field-name2"
                value={buList}
                // onChange={(e) => setLawAgencie(e.target.value)}
                disabled/>
            </CCol>
          </CRow>
        </CCol>
        {generateSaveBtn()}
      </CRow>
    </>
  );
};

export default CreatHeadPanel;
