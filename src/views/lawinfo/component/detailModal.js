import React, { useEffect, useRef, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CForm,
  CContainer,
  CLabel,
  CSelect,
  CInput,
  CTabs,
  CModalBody,
  CModalTitle,
  CTextarea,
  CFormGroup,
  CInputFile,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
const DetailModal = ({ modal = false, setModal, assessmentId, mainLawName, lawDescription, estimate, setEstimate, description, setDescription, onSubmitEstimate }) => {
  return (
    <CModal show={modal} onClose={() => setModal(false)}>
      <CModalHeader closeButton>
        <CModalTitle>บันทึกผลการประเมิน</CModalTitle>
      </CModalHeader>
      <CForm onSubmit={onSubmitEstimate} action="">
        <CModalBody id="summary-body">
          <CInput type="hidden" value={assessmentId} id="assessmentId" />
          
          <div className="text-header">
            <label>ชื่อกฎหมาย</label>
          </div>
          <CRow>
            <CCol xs="12" sm="12">
              <CRow>
                <CCol xs="12">
                  <CFormGroup>
                    <CTextarea rows={3}  value={mainLawName} id="mainLawName" readOnly/>
                  </CFormGroup>
                </CCol>
              </CRow>
            </CCol>
          </CRow>
          <div className="text-header">
            <label>สาระสำคัญกฎหมาย</label>
          </div>
          <CRow>
            <CCol xs="12" sm="12">
              <CRow>
                <CCol xs="12">
                  <CFormGroup>
                    <CTextarea rows={6}  value={lawDescription} id="lawDescription" readOnly/>
                  </CFormGroup>
                </CCol>
              </CRow>
            </CCol>
          </CRow>

          <div className="text-header">
            <label>ผลการประเมิน</label>
          </div>
          <CRow>
            <CCol xs="12" sm="12">
              <CRow>
                <CCol xs="12">
                  <CFormGroup>
                    {/* <CLabel htmlFor="code">Devision Code</CLabel> */}
                    <CSelect onChange={e => setEstimate(e.target.value)} value={estimate} id="estimate" required>
                      <option value="">กรุณาเลือก</option>
                      <option value="1">ไม่เกี่ยวข้อง</option>
                      <option value="2">สอดคล้อง</option>
                      <option value="3">ไม่สอดคล้อง</option>
                      <option value="4">สอดคล้อง A</option>
                    </CSelect>
                  </CFormGroup>
                </CCol>
              </CRow>
              {/* <CRow>
                <CCol xs="12">
                  <CFormGroup>
                    <CLabel htmlFor="description">จัดทำ Action</CLabel>
                    <CSelect>
                      <option value="">กรุณาเลือก</option>
                    </CSelect>
                  </CFormGroup>
                </CCol>
              </CRow> */}
            </CCol>
          </CRow>

          <div className="text-header">
            <label>รายละเอียด</label>
          </div>
          <CRow>
            <CCol xs="12" sm="12">
              <CRow>
                <CCol xs="12">
                  <CFormGroup>
                    <CTextarea rows={3}  onChange={e => setDescription(e.target.value)} value={description} id="description" required/>
                  </CFormGroup>
                </CCol>
              </CRow>
            </CCol>
            {/* <CCol xs="12" sm="12">
              <CRow>
                <CCol xs="12">
                  <CFormGroup>
                    <CLabel htmlFor="description">แนบเอกสาร</CLabel>
                    <CInputFile />
                  </CFormGroup>
                </CCol>
              </CRow>
            </CCol> */}
          </CRow>
        </CModalBody>
        <CModalFooter>
          <CButton variant="outline" type="submit" color="primary">
            Send To Verify
          </CButton>
          <CButton variant="outline" color="secondary" onClick={() => setModal(false)}>
            Cancel
          </CButton>
        </CModalFooter>
      </CForm>
    </CModal>
  );
};

export default DetailModal;
