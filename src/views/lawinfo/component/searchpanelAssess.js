import React, { useEffect, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CNav,
  CForm,
  CContainer,
  CLabel,
  CSelect,
  CInput,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import moment from "moment";

const SearchPanel = ({getData, statusOption = [], page}) => {

    const [assessmentNo, setAssessmentNo] = useState('');
    const [docno, setDocno] = useState('');
    // const [docNoBu, setDocNoBu] = useState('');
    const [mainLawName, setMainLawName] = useState('');
    // const [description, setDescription] = useState('');
    const [valYear, setValYear] = useState('');
    const [valMonth, setValMonth] = useState('');
    const [estimate, setEstimate] = useState('');
    const [yearList, setYearList] = useState([]);
    const [status, setStatus] = useState('');

    useEffect(() => {
      var years = [];
      // console.log("moment().year::", moment().year());
      for (let i = 2020; i <= moment().year(); i++) {
        console.log("year::", i);
        years.push(i)
      }
      setYearList(years);

      return () => {
      }
    }, []);


    const resetSearchOption = (e,type) => {
        setAssessmentNo('');
        setDocno('');
        // setDocNoBu('');
        setMainLawName('');
        // setDescription('');
        setValYear('');
        setValMonth('');
        setEstimate('');
        setStatus('');
        const model = {
          "typeReset": "reset"
        };
        getData(model);
       
    }

    const packData = (e) => {
      const model = {
        "AssessmentNo": assessmentNo,
        "Docno": docno,
        // "BuDocNo": docNoBu,
        "MainLaw": mainLawName,
        "EstimateResult": estimate,
        "ValYear": valYear,
        "ValMonth": valMonth,
        "SystemStatus": status,
      }
      getData(model);
    }


    return (
    <>
      <CRow>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel>
                {" "}
                <b>หมายเลขเอกสาร</b>{" "}
              </CLabel>
            </CCol>
            <CCol col xs="12" md="12">
              <CInput name="name" id="docno" value={docno} onChange={(e)=>setDocno(e.target.value)}/>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel>
                {" "}
                <b>ชื่อกฎหมาย</b>{" "}
              </CLabel>
            </CCol>
            <CCol col xs="12" md="12">
              <CInput name="name" id="mainLawName" value={mainLawName} onChange={(e)=>setMainLawName(e.target.value)}/>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel>
                {" "}
                <b>เลขที่ประเมิน</b>{" "}
              </CLabel>
            </CCol>
            <CCol col xs="12" md="12">
              <CInput name="name" id="assessmentNo" value={assessmentNo} onChange={(e)=>setAssessmentNo(e.target.value)}/>
            </CCol>
          </CRow>
        </CCol>
        {/* <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel>
                {" "}
                <b>เลขที่เอกสาร BU</b>{" "}
              </CLabel>
            </CCol>
            <CCol col xs="12" md="12">
              <CInput name="name" id="docNoBu" value={docNoBu} onChange={(e)=>setDocNoBu(e.target.value)}/>
            </CCol>
          </CRow>
        </CCol> */}
        
      </CRow>
      <CRow className="mt-3">
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel>
                {" "}
                <b>ปีที่ประเมิน</b>{" "}
              </CLabel>
            </CCol>
            <CCol col xs="12" md="12">
              {/* <CInput name="name" id="valYear" value={valYear} onChange={(e)=>setValYear(e.target.value)}/> */}
              <CSelect
                onChange={(e) => setValYear(e.target.value)}
                value={valYear}
                id="valYear"
              >
                <option value="">กรุณาเลือก</option>
                {yearList.map((item, index) => {
                  return (
                    <option value={item}>{item}</option>
                  );
                })}
              </CSelect>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel>
                {" "}
                <b>เดือนที่ประเมิน</b>{" "}
              </CLabel>
            </CCol>
            <CCol col xs="12" md="12">
              {/* <CInput name="name" id="valMonth" value={valMonth} onChange={(e)=>setValMonth(e.target.value)}/> */}
              <CSelect
                onChange={(e) => setValMonth(e.target.value)}
                value={valMonth}
                id="valMonth"
              >
                <option value="">กรุณาเลือก</option>
                <option value="1">มกราคม</option>
                <option value="2">กุมภาพันธ์</option>
                <option value="3">มีนาคม</option>
                <option value="4">เมษายน</option>
                <option value="5">พฤษภาคม</option>
                <option value="6">มิถุนายน</option>
                <option value="7">กรกฎาคม</option>
                <option value="8">สิงหาคม</option>
                <option value="9">กันยายน</option>
                <option value="10">ตุลาคม</option>
                <option value="11">พฤศจิกายน</option>
                <option value="12">ธันวาคม</option>
              </CSelect>
            </CCol>
          </CRow>
        </CCol>
        { page == "manageap" ? 
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="status">
                {" "}
                <b>สถานะแผนการแก้ไขป้องกัน</b>{" "}
              </CLabel>
            </CCol>
            <CCol col xs="12" md="12">
              <CSelect
                onChange={(e) => setStatus(e.target.value)}
                value={status}
                id="status"
              >
                <option value="">กรุณาเลือก</option>
                <option value="SEEV">Send to Evaluate</option>
                <option value="SEA1">Send To Verification</option>
                <option value="SEA2">Send To Approver</option>
                <option value="INPR">In Process</option>
                <option value="CLOS">Complete Send to Verify</option>
                <option value="CLS2">Complete Send to Approve</option>
                <option value="CLNE">Close New</option>
                <option value="DOAP">Document Approve</option>
              </CSelect>
            </CCol>
          </CRow>
        </CCol>
         : 
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="estimate">
                {" "}
                <b>ผลการประเมิน</b>{" "}
              </CLabel>
            </CCol>
            <CCol col xs="12" md="12">
              <CSelect
                onChange={(e) => setEstimate(e.target.value)}
                value={estimate}
                id="estimate"
              >
                <option value="">กรุณาเลือก</option>
                <option value="1">ไม่เกี่ยวข้อง</option>
                <option value="2">สอดคล้อง</option>
                <option value="3">ไม่สอดคล้อง</option>
                <option value="4">สอดคล้อง A</option>
              </CSelect>
            </CCol>
          </CRow>
        </CCol>
        }
      </CRow>
      <CRow className="mt-3">
        <CCol sm="12" md="9"></CCol>
        <CCol sm="12" md="3" className="mt-4">
          <CRow className="justify-content-end">
            <CCol sm="12" md="6" className="justify-content-center">
              <CButton
                color="danger"
                variant="outline"
                block
                onClick={() => resetSearchOption()}
              >
                <b> ล้างข้อมูล </b>{" "}
              </CButton>
            </CCol>
            <CCol sm="12" md="6" className="justify-content-center">
              <CButton
                color="success"
                variant="outline"
                block
                onClick={() => packData()}
              >
                <CIcon name="cil-search" style={{ marginRight: "2px" }} />{" "}
                <b> ค้นหา </b>
              </CButton>
            </CCol>
          </CRow>
        </CCol>
      </CRow>      
    </>
  );
};

export default SearchPanel;