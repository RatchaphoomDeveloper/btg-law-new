import React, { useState, useEffect, useRef } from "react";
import { render } from "react-dom";
import {
  CCardBody,
  CBadge,
  CButton,
  CCollapse,
  CDataTable,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CCol,
  CCard,
  CRow,
  CCardHeader,
  CFormGroup,
  CLabel,
  CInput,
  CSelect,
  CForm,
  CModalTitle,
  CButtonGroup,
  CTextarea,
  CInputCheckbox,
  CCardFooter,
  CFade,
  CLink
} from "@coreui/react";
import Api from "../../../api/ApiActionPlan";
import { DateRangePicker, SingleDatePickerWrapper } from "react-dates";
import CIcon from '@coreui/icons-react'
import "./index.scss";
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2/src/sweetalert2.js";
import Moment from "moment";
import { Upload, message, Button } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import { WEB_API, WEB_FILE_URL } from "../../../env";
const getEstimate = est => {
    switch (est) {
        case 1: return 'ไม่เกี่ยวข้อง'
        case 2: return 'สอดคล้อง'
        case 3: return 'ไม่สอดคล้อง'
        case 4: return 'สอดคล้อง A'
        default: return ''
    }
}

const getStatus = status => {
    switch (status) {
        case 'SEEV': return 'Send To Evaluate'
        case 'REFQ': return 'Reject To FQO'
        case 'SEA1': return 'Send To Verification'
        case 'SEA2': return 'Send To Approver'
        case 'REDI': return 'Reject To User Division'
        case 'DOAP': return 'Document Approve'
        case 'INPR': return 'In Process'
        case 'CLNE': return 'Close New'
        case 'CLOS': return 'Complete Send To Verification'
        case 'CLS2': return 'Complete Send To Approver'
        case 'RJCL': return 'Reject Complete To User Division'
        case 'REEV': return 'Re Evaluate'
        default: return ''
    }
}

const fields = [
    { key: 'eventid', label: 'Event Id'},
    { key: 'estimateResult', label: 'ผลการประเมิน'},
    { key: 'actionplanNo', label: 'Action Plan'},
    { key: 'description', label: 'รายละเอียด ผลการประเมิน' },
    { key: 'systemStatus', label: 'สถานะ' },
    { key: 'rejectReason', label: 'หมายเหตุ' },
    { key: 'createBy', label: 'ผู้ดำเนินการ' },
    { key: 'createDate', label: 'วันที่ดำเนินการ' },
]

const causeFields = [
    { key: 'seq', label: 'ลำดับที่'},
    { key: 'description', label: 'รายละเอียด' },
    { key: 'note', label: 'หมายเหตุ' },
    // { key: 'systemStatus', label: 'สถานะ' },
    { key: 'option', label: '', _style: { width: '7%' }, filter: false, },
]

const resolveFields = [
    { key: 'seq', label: 'ลำดับที่'},
    { key: 'description', label: 'รายละเอียดการแก้ไข' },
    { key: 'responseUserName', label: 'ผู้รับผิดชอบ' },
    { key: 'planStartdateString', label: 'กำหนดเริ่มต้น' },
    { key: 'planEnddateString', label: 'กำหนดแล้วเสร็จ' },
    { key: 'followup', label: 'การติดตามการแก้ไข' },
    { key: 'actualFinishdateString', label: 'วันที่ดำเนินการแล้วเสร็จ' },
    { key: 'option', label: '', _style: { width: '7%' }, filter: false, },
]

const preventFields = [
    { key: 'seq', label: 'ลำดับที่'},
    { key: 'description', label: 'รายละเอียดการป้องกันการเกิดซ้ำ' },
    { key: 'responseUserName', label: 'ผู้รับผิดชอบ' },
    { key: 'planStartdateString', label: 'กำหนดเริ่มต้น' },
    { key: 'planEnddateString', label: 'กำหนดแล้วเสร็จ' },
    { key: 'followup', label: 'การติดตามการป้องกันการเกิดซ้ำ' },
    { key: 'actualFinishdateString', label: 'วันที่ดำเนินการแล้วเสร็จ' },
    { key: 'option', label: '', _style: { width: '7%' }, filter: false, },
]

const fieldFiles = [
  // { key: "id", _style: { width: "40%" } },
  { key: "filename" },
  { key: 'option', label: '', _style: { width: '1%' }, filter: false, },
];

// const useMountEffect = fun => useEffect(fun, []);

const Estimate = ({
  assessId,
  // onSubmitEstimate,
  page,
  mainLawName,
  lawDescription,
  brcsDate,
  lawGroupSub,
  estimate,
  description,
  rejectReason,
  setLawDescription,
  setMainLawName,
  setEstimate,
  setDescription,
  setRejectReason,
  systemStatus,
  logTable=[]
}) => {
  const [fileList, setFileList] = useState([]);
  const [successFileFlag,setsuccessFileFlag] = useState(false)
  const userState = useSelector((state) => state.changeState.user);
  const userStateToken = useSelector((state) => state.changeState.token);
  const [hiddenActionPlan, setHiddenActionPlan] = useState(true);
  const [collapsed, setCollapsed] = React.useState(false);
  const [showCard, setShowCard] = React.useState(true);
  const [userData, setUserData] = useState([]);
  const [dataSelected, setDataSelected] = useState('');
  const [modalConfirm, setModalConfirm] = useState(false);
  const [deleteType, setDeleteType] = useState([]);

  const [dataCauseTable, setDataCauseTable] = useState([]);
  const [modalCause, setModalCause] = useState(false);
  const [causeId, setCauseId] = useState(false);
  // const [causeSeq, setCauseSeq] = useState([]);
  const [causeDesc, setCauseDesc] = useState([]);
  const [causeNote, setCauseNote] = useState([]);

  const [dataResolveTable, setDataResolveTable] = useState([]);
  const [modalResolve, setModalResolve] = useState(false);
  const [resolveId, setResolveId] = useState(false);
  // const [resolveSeq, setResolveSeq] = useState([]);
  const [resolveDesc, setResolveDesc] = useState([]);
  const [resolveRes, setResolveRes] = useState(0);
  const [resolveFollowUp, setResolveFollowUp] = useState([]);
  const [resolveActualDate, setResolveActualDate] = useState([]);
  const [resolveDate, setResolveDate] = useState({ startDate: null, endDate: null });
  const [resolveDateFocused, setResolveDateFocused] = useState();
  const [resolveActualDateFocus, setResolveActualDateFocus] = useState();

  const [dataPrvTable, setDataPrvTable] = useState([]);
  const [modalPrv, setModalPrv] = useState(false);
  const [prvId, setPrvId] = useState(false);
  // const [prvSeq, setPrvSeq] = useState([]);
  const [prvDesc, setPrvDesc] = useState([]);
  const [prvRes, setPrvRes] = useState(0);
  const [prvFollowUp, setPrvFollowUp] = useState([]);
  const [prvActualDate, setPrvActualDate] = useState([]);
  const [prvDate, setPrvDate] = useState({ startDate: null, endDate: null });
  const [prvDateFocused, setPrvDateFocused] = useState();

  const [modalFile, setModalFile] = useState(false);
  const [modalConfirmFile, setModalConfirmFile] = useState(false);
  const [dataFileTable, setDataFileTable] = useState([]);
  const [referFileId, setReferFileId] = useState('');
  const [referFileType, setReferFileType] = useState('');
  const [fileId, setFileId] = useState('');
  const [actionPlanId, setActionPlanId] = useState(false);
  const [isLastAcp, setIsLastAcp] = useState(true);
  const [isHideEdit, setIsHideEdit] = useState(true);
  const [isHideEditCmp, setIsHideEditCmp] = useState(true);
  
  useEffect(() => {
    getUserOption();
      return () => {

      }
  }, []);

  useEffect(() => {
    console.log("estimate", estimate);
    if (estimate == 3 || estimate == 4){
      getActionPlanData(null);
      setHiddenActionPlan(false);
    }
    return () => {};
  }, [estimate]);

  useEffect(() => {
    console.log("isLastAcp", isLastAcp);
    console.log("systemStatus", systemStatus);
    if (isLastAcp){
      if (userState.role == "USER"){
        if(systemStatus == "SEEV" || systemStatus == "REDI" || systemStatus == "CLNE"){
          setIsHideEdit(false);
          setIsHideEditCmp(false);
          console.log("case1");
        }
        else if(systemStatus == "INPR" || systemStatus == "RJCL"){
          setIsHideEdit(true);
          setIsHideEditCmp(false);
          console.log("case2");
        }else{
          setIsHideEdit(true);
          setIsHideEditCmp(true);
          console.log("case3");
        }
      }else{
        setIsHideEdit(true);
        setIsHideEditCmp(true);
      }
    }else{
      setIsHideEdit(true);
      setIsHideEditCmp(true);
    }
    generateActionPlanSection();
    return () => {};
  }, [systemStatus]);

  const actionPlanSectionRef = useRef(null)

  const getUserOption = async () => {
      try{
          const result = await Api.getUserFromAssessment(assessId);
          if (result.status == 200) {
              setUserData(result.data);
          }
      }catch(error){
          console.log(error);
      }
  }

  const getActionPlanData = async (acpId) => {
    try {
      const model = {
        id: assessId,
        acpId: acpId ? acpId : ''
      };
      const result = await Api.getAll(model);
      if (result.status == 200) {
        var res = result.data;
        setDataCauseTable(res.cause);
        setDataResolveTable(res.resolve);
        setDataPrvTable(res.prevent);
        setIsLastAcp(res.isLastAcp);
        console.log("res", res);
        // generateActionPlanSection();
      }
    } catch (error) {
      console.log(error);
    }
  };
  
  const getActionPlanDetail = (item) => {
    // console.log("actionPlanIdBEF", actionPlanId);
    setActionPlanId(item.actionplanId);
    // console.log("actionPlanIdAFT", actionPlanId);
    getActionPlanData(item.actionplanId);

    var ref = document.getElementById("actionPlanSectionRef");
    ref.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
    // useMountEffect(executeScroll);
    // executeScroll();
  }

  // const executeScroll = () => actionPlanSectionRef.current.scrollIntoView();

  const toggleConfirm = () => {
    setModalConfirm(!modalConfirm);
  }

  const fileprop = {
    name: "files",
    multiple: true,
    action: `${WEB_API}Master/SaveTranBrcsFile`,
    headers: {
      authorization: `Bearer ${userStateToken}`,
    },
    // action: `https://www.mocky.io/v2/5cc8019d300000980a055e76`,
    onChange(info) {
      const { status } = info.file;
      if (info.file.status !== "uploading") {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === "done") {
        saveFileToDB(info.file, referFileId, referFileType);
        console.log(`${info.file.name} file uploaded successfully`);
        // message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === "error") {
        console.log(`${info.file.name} file upload failed.`);
        // message.error(`${info.file.name} file upload failed.`);
      }
    },
  };


  const saveFileToDB = async (data, id, type) => {
    try {
      const model = {
        refId: id,
        type: type,
        FileName: data.name,
        FilePath: data.response.url,
        user: userState.id,
      };
      const result = await Api.saveActionPlanFileToDB(model);
        if (result.status === 200) {
            const { data } = result.data;
            getFileData(id, type);
            Swal.fire({
              icon: "success",
              title: "Upload Success",
            });
        }
    } catch (error) {
      console.log(error);
    }
  };

  const toggleCause = () => {
    setModalCause(!modalCause);
  };

  const onSubmitCause = (e) => {
    e.preventDefault();
    const model = {
      "id": causeId == "" ? 0 : causeId,
      "assessmentId": assessId,
      // "seq": causeSeq,
      "description": causeDesc,
      "note": causeNote,
      "user": userState.id,
    }
    saveCauseData(model);
  }

  const saveCauseData = async (data) => {
    try {
      Swal.showLoading();
      const result = await Api.createCause(data);
      Swal.close();
      if (result.status === 200) {
            const { data } = result.data;
            toggleCause();
            getActionPlanData();
            
            Swal.fire({
                icon: "success",
                title: "Save Success",
            });
        }
    } catch (error) {
      Swal.fire({
          icon: "error",
          title: error.response.data,
        });
    }
  }

  const toggleResolve = () => {
    setModalResolve(!modalResolve);
  };

  const onSubmitResolve = (e) => {
    e.preventDefault();
    const model = {
      "id": resolveId == "" ? 0 : resolveId,
      "assessmentId": assessId,
      // "seq": resolveSeq,
      "description": resolveDesc,
      "responseUser": resolveRes,
      "planStartdate": resolveDate.startDate,
      "planEnddate": resolveDate.endDate,
      "followup": resolveFollowUp,
      "actualFinishdate": resolveActualDate,
      "user": userState.id,
    }
    // console.log("model",model);
    saveResolveData(model);
  }

  const saveResolveData = async (data) => {
    try {
      Swal.showLoading();
      const result = await Api.createResolve(data);
      Swal.close();
      if (result.status === 200) {
            const { data } = result.data;
            toggleResolve();
            getActionPlanData();
            
            Swal.fire({
                icon: "success",
                title: "Save Success",
            });
        }
    } catch (error) {
      Swal.fire({
          icon: "error",
          title: error.response.data,
        });
    }
  }

  const togglePrv = () => {
    setModalPrv(!modalPrv);
  };

  const onSubmitPrv = (e) => {
    e.preventDefault();
    const model = {
      "id": prvId == "" ? 0 : prvId,
      "assessmentId": assessId,
      // "seq": prvSeq,
      "description": prvDesc,
      "responseUser": prvRes,
      "planStartdate": prvDate.startDate,
      "planEnddate": prvDate.endDate,
      "followup": prvFollowUp,
      "actualFinishdate": prvActualDate,
      "user": userState.id,
    }
    // console.log(prvRes);
    savePrvData(model);
  }

  const savePrvData = async (data) => {
    try {
      Swal.showLoading();
      const result = await Api.createPrv(data);
      Swal.close();
      if (result.status === 200) {
            const { data } = result.data;
            togglePrv();
            getActionPlanData();
            
            Swal.fire({
                icon: "success",
                title: "Save Success",
            });
        }
    } catch (error) {
      Swal.fire({
          icon: "error",
          title: error.response.data,
        });
    }
  }

  // const packData = async () => {
  //   const model = {
  //     id: assessId,
  //     estimateResult: estimate,
  //     estimateDesc: description,
  //     user: userState.id,
  //   };
  //   onSubmitEstimate(model);
  // };

  const onEstimateChange = (value) => {
    setEstimate(value);
    if (value == "3" || value == "4"){
      setHiddenActionPlan(false);
    }else{
      setHiddenActionPlan(true);
    }
  }

  const editCauseDetails = (item) =>{
    setCauseId(item.id);
    // setCauseSeq(item.seq);
    setCauseDesc(item.description);
    setCauseNote(item.note);
    generateModalCause();
    toggleCause();
  }

  const deleteCauseDetails = (item) =>{
    toggleConfirm();
    setDataSelected(item);
    setCauseId(item.id);
    setDeleteType("cause");
  }

  const onDelete = () => {
    if (deleteType == "cause"){
      onDeleteCause();
    }
    if (deleteType == "resolve"){
      onDeleteResolve();
    }
    if (deleteType == "prevent"){
      onDeletePrevent();
    }
  }

  const onDeleteCause = async () => {
    try {
        const result = await Api.deleteCause(dataSelected);
        if (result.status === 200) {
            const { data } = result.data;
            toggleConfirm();
            getActionPlanData();
            
            Swal.fire({
                icon: "success",
                title: "Delete Success",
            });
        }
      } catch (error) {
          Swal.fire({
              icon: "error",
              title: error.response.data,
            });
      }
  }

  const editResolveDetails = (item) =>{
    setResolveId(item.id);
    // setResolveSeq(item.seq);
    setResolveDesc(item.description);
    setResolveRes(item.responseUser);
    setResolveDate({ startDate: Moment(item.planStartdate), endDate: Moment(item.planEnddate)});
    setResolveFollowUp(item.followup);
    setResolveActualDate(Moment(item.actualFinishdate).format("yyyy-MM-DD"));
    generateModalResolve();
    toggleResolve();
  }

  const deleteResolveDetails = (item) =>{
    toggleConfirm();
    setDataSelected(item);
    setResolveId(item.id);
    setDeleteType("resolve");
  }

  const onDeleteResolve = async () => {
    try {
        const result = await Api.deleteResolve(dataSelected);
        if (result.status === 200) {
            const { data } = result.data;
            toggleConfirm();
            getActionPlanData();
            
            Swal.fire({
                icon: "success",
                title: "Delete Success",
            });
        }
      } catch (error) {
          Swal.fire({
              icon: "error",
              title: error.response.data,
            });
      }
  }

  const editPreventDetails = (item) =>{
    setPrvId(item.id);
    // setPrvSeq(item.seq);
    setPrvDesc(item.description);
    setPrvRes(item.responseUser);
    // setResolveDate({ startDate: item.planStartdateString, endDate: item.planEnddateString });
    setPrvDate({ startDate: Moment(item.planStartdate), endDate: Moment(item.planEnddate)});
    setPrvFollowUp(item.followup);
    setPrvActualDate(Moment(item.actualFinishdate).format("yyyy-MM-DD"));
    generateModalPrevent();
    togglePrv();
  }

  const deletePreventDetails = (item) =>{
    toggleConfirm();
    setDataSelected(item);
    setPrvId(item.id);
    setDeleteType("prevent");
  }

  const onDeletePrevent = async () => {
    try {
        const result = await Api.deletePrv(dataSelected);
        if (result.status === 200) {
            const { data } = result.data;
            toggleConfirm();
            getActionPlanData();
            
            Swal.fire({
                icon: "success",
                title: "Delete Success",
            });
        }
      } catch (error) {
          Swal.fire({
              icon: "error",
              title: error.response.data,
            });
      }
  }

  const newCauseDetails = (record) => {
    toggleCause();
    setCauseId('');
    // setCauseSeq('');
    setCauseDesc('');
    setCauseNote('');
  }

  const newResolveDetails = (record) => {
    toggleResolve();
    setResolveId('');
    // setResolveSeq('');
    setResolveDesc('');
    setResolveRes(0);
    setResolveDate({ startDate: null, endDate: null });
    setResolveFollowUp('');
    setResolveActualDate('');
  }

  const newPrvDetails = (record) => {
    togglePrv();
    setPrvId('');
    // setPrvSeq('');
    setPrvDesc('');
    setPrvRes(0);
    setPrvDate({ startDate: null, endDate: null });
    setPrvFollowUp('');
    setPrvActualDate('');
  }

  const handleChange = (e,type) => {
      switch (type) {
        case "resolveRes":
          setResolveRes(e.target.value);
          break;
      }
  }

  const getFileList = (data, type) =>{
    setReferFileId(data.id);
    setReferFileType(type);
    // setCauseDesc(item.description);
    // setCauseNote(item.note);
    // generateModalCause();
    // toggleCause();
    getFileData(data.id, type);
    generateModalFile();
    toggleFile();
  }

  const getFileData = async (id, type) => {
    try {
      const result = await Api.getActionPlanFile(id, type);
      if (result.status == 200) {
        setDataFileTable(result.data);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const toggleFile = () => {
    setModalFile(!modalFile);
  }

  const deleteFile = (record) => {
    // toggleConfirm();
    setDataSelected(record);
    setFileId(record.id);
    onDeleteFile(record);
}

const onDeleteFile = async (data) => {
  try {
      const result = await Api.deleteActionPlanFile(data);
      if (result.status === 200) {
          const { data } = result.data;
          // toggleConfirm();
          Swal.fire({
              icon: "success",
              title: "Delete Success",
          });
          getFileData(referFileId, referFileType);
      }
    } catch (error) {
        Swal.fire({
            icon: "error",
            title: error.response.data,
          });
    }
}
  
  // function generateSubmitEstimateBtn() {
  //   if (page == "estimate") {
  //     return (
  //       <>
  //         <CCardFooter>
  //           <CButton variant="outline" type="submit" color="primary">
  //             Send To Verify
  //           </CButton>
  //         </CCardFooter>
  //       </>
  //     );
  //   }
  // }

  function generateActionPlanSection(){
    if(hiddenActionPlan == false){
      if (systemStatus == "SEEV" || systemStatus == "REDI" || systemStatus == "CLNE"){
        return(
          <>
            <CCard>
              <CCardBody ref={actionPlanSectionRef} id="actionPlanSectionRef">
                <CRow >
                  <CCol sm="12" md="12">
                    <CLabel>
                      <b>สาเหตุ</b>
                       <CButton hidden={isLastAcp == false || userState.role != "USER"}
                       className="ml-3" size="sm" onClick={newCauseDetails} color={'success'}><CIcon name="cil-plus" /><span className="ml-2">เพิ่มสาเหตุ</span></CButton>
                    </CLabel>
                  </CCol>
                </CRow>
                <CDataTable
                  items={dataCauseTable}
                  fields={causeFields}
                  // itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
                  // itemsPerPage={10}
                  hover
                  sorter
                  striped
                  bordered
                  // pagination
                  scopedSlots={{
                    option: (item) => (
                      <td className="center">
                          <CButtonGroup>
                              <CButton hidden={isLastAcp == false || userState.role != "USER"}
                                  color="success"
                                  variant="outline"
                                  shape="square"
                                  size="sm"
                                  onClick={() => { editCauseDetails(item) }}
                              >
                                  <CIcon name="cilPen" />
                              </CButton>
                              <CButton variant="ghost" size="sm" />
                              <CButton hidden={item.fileCount == 0 && isHideEdit}
                                  color="primary"
                                  variant="outline"
                                  shape="square"
                                  size="sm"
                                  onClick={() => { getFileList(item, "CAU") }}
                              >
                              {/* <CIcon name="cil-file" /> */}
                              Files
                              </CButton>
                              <CButton variant="ghost" size="sm" />
                              <CButton hidden={isLastAcp == false || userState.role != "USER"}
                                  color="danger"
                                  variant="outline"
                                  shape="square"
                                  size="sm"
                                  onClick={() => { deleteCauseDetails(item) }}
                              >
                                  <CIcon name="cilTrash" />
                              </CButton>
                          </CButtonGroup>
                      </td>
                    ),
                  }}
                />
                <CRow className="mt-3" >
                  <CCol sm="12" md="12">
                    <CLabel>
                      <b>การแก้ไข</b> 
                      <CButton hidden={isLastAcp == false || userState.role != "USER"}
                      className="ml-3" size="sm" onClick={newResolveDetails} color={'success'}><CIcon name="cil-plus" /><span className="ml-2">เพิ่มการแก้ไข</span></CButton>
                    </CLabel>
                  </CCol>
                </CRow>
                <CDataTable
                  items={dataResolveTable}
                  fields={resolveFields}
                  // itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
                  // itemsPerPage={10}
                  hover
                  sorter
                  striped
                  bordered
                  // pagination
                  scopedSlots={{
                    option:
                        (item) => (
                            <td className="center">
                                <CButtonGroup>
                                    <CButton hidden={isLastAcp == false || userState.role != "USER"}
                                        color="success"
                                        variant="outline"
                                        shape="square"
                                        size="sm"
                                        onClick={() => { editResolveDetails(item) }}
                                    >
                                        <CIcon name="cilPen" />
                                    </CButton>
                                    <CButton variant="ghost" size="sm" />
                                    <CButton  hidden={item.fileCount == 0 && isHideEditCmp}
                                        color="primary"
                                        variant="outline"
                                        shape="square"
                                        size="sm"
                                        onClick={() => { getFileList(item, "RES") }}
                                    >
                                    {/* <CIcon name="cil-file" /> */}
                                    Files
                                    </CButton>
                                    <CButton variant="ghost" size="sm" />
                                    <CButton hidden={isLastAcp == false || userState.role != "USER"}
                                        color="danger"
                                        variant="outline"
                                        shape="square"
                                        size="sm"
                                        onClick={() => { deleteResolveDetails(item) }}
                                    >
                                        <CIcon name="cilTrash" />
                                    </CButton>
                                </CButtonGroup>
                            </td>
                        ),
                  }}
                />
                <CRow className="mt-3" >
                  <CCol sm="12" md="12">
                    <CLabel>
                      <b>การป้องกันการเกิดซ้ำ</b> 
                      <CButton hidden={isLastAcp == false || userState.role != "USER"}
                      className="ml-3" size="sm" onClick={newPrvDetails} color={'success'}><CIcon name="cil-plus" /><span className="ml-2">เพิ่มการป้องกันการเกิดซ้ำ</span></CButton>
                    </CLabel>
                  </CCol>
                </CRow>
                <CDataTable
                  items={dataPrvTable}
                  fields={preventFields}
                  // itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
                  // itemsPerPage={10}
                  hover
                  sorter
                  striped
                  bordered
                  // pagination
                  scopedSlots={{
                    option:
                        (item) => (
                            <td className="center">
                                <CButtonGroup>
                                    <CButton hidden={isLastAcp == false || userState.role != "USER"}
                                        color="success"
                                        variant="outline"
                                        shape="square"
                                        size="sm"
                                        onClick={() => { editPreventDetails(item) }}
                                    >
                                        <CIcon name="cilPen" />
                                    </CButton>
                                    <CButton variant="ghost" size="sm" />
                                    <CButton  hidden={item.fileCount == 0 && isHideEditCmp}
                                        color="primary"
                                        variant="outline"
                                        shape="square"
                                        size="sm"
                                        onClick={() => { getFileList(item, "PRE") }}
                                    >
                                    {/* <CIcon name="cil-file" /> */}
                                    Files
                                    </CButton>
                                    <CButton variant="ghost" size="sm" />
                                    <CButton hidden={isLastAcp == false || userState.role != "USER"}
                                        color="danger"
                                        variant="outline"
                                        shape="square"
                                        size="sm"
                                        onClick={() => { deletePreventDetails(item) }}
                                    >
                                        <CIcon name="cilTrash" />
                                    </CButton>
                                </CButtonGroup>
                            </td>
                        ),
                  }}
                />
              </CCardBody>
            </CCard>
          </>
        );
      }
      else if (systemStatus == "INPR" || systemStatus == "RJCL"){
        return(
          <>
            <CCard>
              <CCardBody ref={actionPlanSectionRef} id="actionPlanSectionRef">
                <CRow>
                  <CCol sm="12" md="12">
                    <CLabel>
                      <b>สาเหตุ</b>
                    </CLabel>
                  </CCol>
                </CRow>
                <CDataTable
                  items={dataCauseTable}
                  fields={causeFields}
                  // itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
                  // itemsPerPage={10}
                  hover
                  sorter
                  striped
                  bordered
                  // pagination
                  scopedSlots={{
                    option:
                        (item) => (
                            <td className="center">
                                <CButtonGroup>
                                    <CButton  hidden={item.fileCount == 0 && isHideEdit}
                                        color="primary"
                                        variant="outline"
                                        shape="square"
                                        size="sm"
                                        onClick={() => { getFileList(item, "CAU") }}
                                    >
                                    {/* <CIcon name="cil-file" /> */}
                                    Files
                                    </CButton>
                                </CButtonGroup>
                            </td>
                        ),
                  }}
                />
                <CRow className="mt-3">
                  <CCol sm="12" md="12">
                    <CLabel>
                      <b>การแก้ไข</b>
                    </CLabel>
                  </CCol>
                </CRow>
                <CDataTable
                  items={dataResolveTable}
                  fields={resolveFields}
                  // itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
                  // itemsPerPage={10}
                  hover
                  sorter
                  striped
                  bordered
                  // pagination
                  scopedSlots={{
                    option:
                        (item) => (
                            <td className="center">
                                <CButtonGroup>
                                    <CButton hidden={isLastAcp == false || userState.role != "USER"}
                                        color="success"
                                        variant="outline"
                                        shape="square"
                                        size="sm"
                                        onClick={() => { editResolveDetails(item) }}
                                    >
                                        <CIcon name="cilPen" />
                                    </CButton>
                                    <CButton variant="ghost" size="sm" />
                                    <CButton hidden={item.fileCount == 0 && isHideEditCmp}
                                        color="primary"
                                        variant="outline"
                                        shape="square"
                                        size="sm"
                                        onClick={() => { getFileList(item, "RES") }}
                                    >
                                    {/* <CIcon name="cil-file" /> */}
                                    Files
                                    </CButton>
                                </CButtonGroup>
                            </td>
                        ),
                  }}
                />
                <CRow className="mt-3">
                  <CCol sm="12" md="12">
                    <CLabel>
                      <b>การป้องกันการเกิดซ้ำ</b>
                    </CLabel>
                  </CCol>
                </CRow>
                <CDataTable
                  items={dataPrvTable}
                  fields={preventFields}
                  // itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
                  // itemsPerPage={10}
                  hover
                  sorter
                  striped
                  bordered
                  // pagination
                  scopedSlots={{
                    option:
                        (item) => (
                            <td className="center">
                                <CButtonGroup>
                                    <CButton hidden={isLastAcp == false || userState.role != "USER"}
                                        color="success"
                                        variant="outline"
                                        shape="square"
                                        size="sm"
                                        onClick={() => { editPreventDetails(item) }}
                                    >
                                        <CIcon name="cilPen" />
                                    </CButton>
                                    <CButton variant="ghost" size="sm" />
                                    <CButton hidden={item.fileCount == 0 && isHideEditCmp}
                                        color="primary"
                                        variant="outline"
                                        shape="square"
                                        size="sm"
                                        onClick={() => { getFileList(item, "PRE") }}
                                    >
                                    {/* <CIcon name="cil-file" /> */}
                                    Files
                                    </CButton>
                                </CButtonGroup>
                            </td>
                        ),
                  }}
                />
              </CCardBody>
            </CCard>
          </>
        );
      }
      return(
        <>
          <CCard>
            <CCardBody ref={actionPlanSectionRef} id="actionPlanSectionRef">
              <CRow>
                <CCol sm="12" md="12">
                  <CLabel>
                    <b>สาเหตุ</b>
                  </CLabel>
                </CCol>
              </CRow>
              <CDataTable
                items={dataCauseTable}
                fields={causeFields}
                // itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
                // itemsPerPage={10}
                hover
                sorter
                striped
                bordered
                // pagination
                scopedSlots={{
                  option:
                      (item) => (
                          <td className="center">
                            <CButtonGroup>
                                <CButton hidden={item.fileCount == 0 && isHideEdit}
                                    color="primary"
                                    variant="outline"
                                    shape="square"
                                    size="sm"
                                    onClick={() => { getFileList(item, "CAU") }}
                                >
                                {/* <CIcon name="cil-file" /> */}
                                Files
                                </CButton>
                            </CButtonGroup>
                          </td>
                      ),
                }}
              />
              <CRow className="mt-3">
                <CCol sm="12" md="12">
                  <CLabel>
                    <b>การแก้ไข</b>
                  </CLabel>
                </CCol>
              </CRow>
              <CDataTable
                items={dataResolveTable}
                fields={resolveFields}
                // itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
                // itemsPerPage={10}
                hover
                sorter
                striped
                bordered
                // pagination
                scopedSlots={{
                  option:
                      (item) => (
                          <td className="center">
                            <CButtonGroup>
                                <CButton hidden={item.fileCount == 0 && isHideEdit}
                                    color="primary"
                                    variant="outline"
                                    shape="square"
                                    size="sm"
                                    onClick={() => { getFileList(item, "RES") }}
                                >
                                {/* <CIcon name="cil-file" /> */}
                                Files
                                </CButton>
                            </CButtonGroup>
                          </td>
                      ),
                }}
              />
              <CRow className="mt-3">
                <CCol sm="12" md="12">
                  <CLabel>
                    <b>การป้องกันการเกิดซ้ำ</b>
                  </CLabel>
                </CCol>
              </CRow>
              <CDataTable
                items={dataPrvTable}
                fields={preventFields}
                // itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
                // itemsPerPage={10}
                hover
                sorter
                striped
                bordered
                // pagination
                scopedSlots={{
                  option:
                      (item) => (
                          <td className="center">
                            <CButtonGroup>
                                <CButton hidden={item.fileCount == 0 && isHideEdit}
                                    color="primary"
                                    variant="outline"
                                    shape="square"
                                    size="sm"
                                    onClick={() => { getFileList(item, "PRE") }}
                                >
                                    {/* <CIcon name="cil-file" /> */}
                                    Files
                                </CButton>
                            </CButtonGroup>
                          </td>
                      ),
                }}
              />
            </CCardBody>
          </CCard>
        </>
      );
    }
  }

  function generateModalCause() {
    console.log("causeId", causeId);
    return(
      <CModal show={modalCause} onClose={toggleCause}>
        <CForm onSubmit={onSubmitCause} action="">
          <CModalBody>
            <CInput type="hidden" value={causeId} id="causeId" />
            <CRow>
              <CCol xs="12" sm="12">
                {/* <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel htmlFor="causeSeq">ลำดับที่</CLabel>
                      <CInput
                        onChange={(e) => setCauseSeq(e.target.value)}
                        value={causeSeq}
                        id="causeSeq"
                        type="number"
                        pattern="[0-9]*"
                        inputMode="numeric"
                        min="1"
                        required
                      />
                    </CFormGroup>
                  </CCol>
                </CRow> */}
                <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel htmlFor="causeDesc">รายละเอียด</CLabel>
                      <CTextarea
                        onChange={(e) => setCauseDesc(e.target.value)}
                        rows={8}
                        value={causeDesc}
                        id="causeDesc"
                        required
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel htmlFor="causeNote">หมายเหตุ</CLabel>
                      <CTextarea
                        onChange={(e) => setCauseNote(e.target.value)}
                        rows={8}
                        value={causeNote}
                        id="causeNote"
                        // required
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>
              </CCol>
            </CRow>
          </CModalBody>
          <CModalFooter>
            <CButton variant="outline" type="submit" color="primary">
              ตกลง
            </CButton>
            <CButton variant="outline" color="secondary" onClick={toggleCause}>
              ยกเลิก
            </CButton>
          </CModalFooter>
        </CForm>
      </CModal>
    );
  }

  function generateModalResolve(){
    return (
      <CModal show={modalResolve} onClose={toggleResolve}>
        <CForm onSubmit={onSubmitResolve} action="">
          <CModalBody>
            <CInput type="hidden" value={resolveId} id="resolveId" />
            <CRow>
              <CCol xs="12" sm="12">
                {/* <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel htmlFor="resolveSeq">ลำดับที่</CLabel>
                      <CInput
                        onChange={(e) => setResolveSeq(e.target.value)}
                        value={resolveSeq}
                        id="resolveSeq"
                        type="number"
                        pattern="[0-9]*"
                        inputMode="numeric"
                        min="1"
                        readOnly={systemStatus == "INPR" || systemStatus == "RJCL"}
                        required
                      />
                    </CFormGroup>
                  </CCol>
                </CRow> */}
                <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel htmlFor="resolveDesc">รายละเอียดการแก้ไข</CLabel>
                      <CTextarea
                        onChange={(e) => setResolveDesc(e.target.value)}
                        rows={8}
                        value={resolveDesc}
                        id="resolveDesc"
                        readOnly={systemStatus == "INPR" || systemStatus == "RJCL"}
                        required
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol xs="12">
                      <CFormGroup>
                          <CLabel htmlFor="resolveRes">ผู้รับผิดชอบ</CLabel>
                          <CSelect custom name="resolveRes" id="resolveRes" value={resolveRes}  
                          onChange={(e) => handleChange(e,'resolveRes')} 
                          disabled={systemStatus == "INPR" || systemStatus == "RJCL"}
                          required>
                              <option value={""}>-- ผู้รับผิดชอบ --</option>
                              {
                                  userData.map((item, index) => {
                                      return <option key={item.id} value={item.id}>{item.fullname}</option>
                                  })
                              }
                          </CSelect>
                      </CFormGroup>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel htmlFor="resolveDate">กำหนดการ</CLabel>
                      <DateRangePicker
                        displayFormat="DD/MM/YYYY"
                        small
                        block
                        align="center"
                        startDatePlaceholderText={"เริ่มต้น"}
                        startDate={resolveDate.startDate}
                        startDateId="resolveStartDate"
                        endDate={resolveDate.endDate}
                        endDateId="resolveEndDate"
                        endDatePlaceholderText={"แล้วเสร็จ"}
                        onDatesChange={(value) => setResolveDate(value)}
                        focusedInput={resolveDateFocused}
                        onFocusChange={(focusedInput) =>
                          setResolveDateFocused(focusedInput)
                        }
                        orientation="horizontal"
                        openDirection="down"
                        minimumNights={0}
                        isOutsideRange={() => false}
                        disabled={systemStatus == "INPR" || systemStatus == "RJCL"}
                        required
                        />
                    </CFormGroup>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel htmlFor="resolveFollowUp">การติดตามการแก้ไข</CLabel>
                      <CTextarea
                        onChange={(e) => setResolveFollowUp(e.target.value)}
                        rows={8}
                        value={resolveFollowUp}
                        id="resolveFollowUp"
                        readOnly={systemStatus != "INPR" && systemStatus != "RJCL"}
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel htmlFor="resolveActualDate">วันที่ดำเนินการแล้วเสร็จ</CLabel>
                      <CInput
                        displayFormat="DD/MM/YYYY"
                        // placeholder="dd-mm-yyyy"
                        // displayFormat="YYYY/MM/DD"
                        onChange={(e) => setResolveActualDate(e.target.value)}
                        value={resolveActualDate}
                        id="resolveActualDate"
                        type="date"
                        disabled={systemStatus != "INPR" && systemStatus != "RJCL"}
                      />
                      {/* <SingleDatePickerWrapper
                        displayFormat="DD/MM/YYYY"
                        date={resolveActualDate} // momentPropTypes.momentObj or null
                        onDateChange={(date) => setResolveActualDate(date)} // PropTypes.func.isRequired
                        focused={resolveActualDateFocus} // PropTypes.bool
                        onFocusChange={(focused) => setResolveActualDateFocus(focused)} // PropTypes.func.isRequired
                        disabled={systemStatus != "INPR" && systemStatus != "RJCL"}
                      /> */}
                    </CFormGroup>
                  </CCol>
                </CRow>
              </CCol>
            </CRow>
          </CModalBody>
          <CModalFooter>
            <CButton variant="outline" type="submit" color="primary">
              ตกลง
            </CButton>
            <CButton variant="outline" color="secondary" onClick={toggleResolve}>
              ยกเลิก
            </CButton>
          </CModalFooter>
        </CForm>
      </CModal>
    );
  }

  function generateModalPrevent(){
    return (
      <CModal show={modalPrv} onClose={togglePrv}>
        <CForm onSubmit={onSubmitPrv} action="">
          <CModalBody>
            <CInput type="hidden" value={prvId} id="prvId" />
            <CRow>
              <CCol xs="12" sm="12">
                {/* <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel htmlFor="prvSeq">ลำดับที่</CLabel>
                      <CInput
                        onChange={(e) => setPrvSeq(e.target.value)}
                        value={prvSeq}
                        id="prvSeq"
                        type="number"
                        pattern="[0-9]*"
                        inputMode="numeric"
                        min="1"
                        readOnly={systemStatus == "INPR" || systemStatus == "RJCL"}
                        required
                      />
                    </CFormGroup>
                  </CCol>
                </CRow> */}
                <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel htmlFor="prvDesc">รายละเอียดการป้องกันการเกิดซ้ำ</CLabel>
                      <CTextarea
                        onChange={(e) => setPrvDesc(e.target.value)}
                        rows={8}
                        value={prvDesc}
                        id="prvDesc"
                        readOnly={systemStatus == "INPR" || systemStatus == "RJCL"}
                        required
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol xs="12">
                      <CFormGroup>
                          <CLabel htmlFor="prvRes">ผู้รับผิดชอบ</CLabel>
                          <CSelect custom name="prvRes" id="prvRes" value={prvRes} 
                          onChange={(e) => setPrvRes(e.target.value)} 
                          disabled={systemStatus == "INPR" || systemStatus == "RJCL"}
                          required>
                              <option value={""}>-- ผู้รับผิดชอบ --</option>
                              {
                                  userData.map((item, index) => {
                                      return <option key={item.id} value={item.id}>{item.fullname}</option>
                                  })
                              }
                          </CSelect>
                      </CFormGroup>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel htmlFor="prvDate">กำหนดการ</CLabel>
                      <DateRangePicker
                        displayFormat="DD/MM/YYYY"
                        small
                        block
                        align="center"
                        startDatePlaceholderText={"เริ่มต้น"}
                        startDate={prvDate.startDate}
                        startDateId="prvStartDate"
                        endDate={prvDate.endDate}
                        endDateId="prvEndDate"
                        endDatePlaceholderText={"แล้วเสร็จ"}
                        onDatesChange={(value) => setPrvDate(value)}
                        focusedInput={prvDateFocused}
                        onFocusChange={(focusedInput) =>
                          setPrvDateFocused(focusedInput)
                        }
                        orientation="horizontal"
                        openDirection="down"
                        minimumNights={0}
                        isOutsideRange={() => false}
                        disabled={systemStatus == "INPR" || systemStatus == "RJCL"}
                        required
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel htmlFor="prvFollowUp">การติดตามการป้องกันการเกิดซ้ำ</CLabel>
                      <CTextarea
                        onChange={(e) => setPrvFollowUp(e.target.value)}
                        rows={8}
                        value={prvFollowUp}
                        id="prvFollowUp"
                        readOnly={systemStatus != "INPR" && systemStatus != "RJCL"}
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel htmlFor="prvActualDate">วันที่ดำเนินการแล้วเสร็จ</CLabel>
                      <CInput
                        onChange={(e) => setPrvActualDate(e.target.value)}
                        value={prvActualDate}
                        id="prvActualDate"
                        type="date"
                        disabled={systemStatus != "INPR" && systemStatus != "RJCL"}
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>
              </CCol>
            </CRow>
          </CModalBody>
          <CModalFooter>
            <CButton variant="outline" type="submit" color="primary">
              ตกลง
            </CButton>
            <CButton variant="outline" color="secondary" onClick={togglePrv}>
              ยกเลิก
            </CButton>
          </CModalFooter>
        </CForm>
      </CModal>
    );
  }

  function generateModalFile(){
    if (referFileType=="CAU"){
      return (
        <CModal show={modalFile} onClose={toggleFile}>
          <CModalBody>
            <CRow hidden={isHideEdit}>
              <CCol xs="12">
                <div style={{ paddingTop: "1pc" }}>
                  <Upload {...fileprop} maxCount={1}  showUploadList={false}>
                    <Button icon={<UploadOutlined />}>แนบไฟล์</Button>
                    <div style={{ paddingTop: "0.5pc" }}>
                      <label style={{ color: "red", fontSize: "12px" }}>
                        * สามารถอัพโหลดไฟล์ ประเภท
                        .xls,.xlsx,.pdf,.xml,.doc,.docx,.bmp,.gif,.jpg,.jpeg,.png
                        ได้ไม่เกิน 5 ไฟล์ ขนาดไม่เกิน 5 เมกะไบท์
                      </label>
                    </div>
                  </Upload>
                </div>
              </CCol>
            </CRow>
            <CRow>
              <CCol sm={"12"}>
                <CDataTable
                  items={dataFileTable}
                  fields={fieldFiles}
                  // tableFilter
                  // itemsPerPageSelect
                  // itemsPerPage={10}
                  hover
                  sorter
                  pagination
                  scopedSlots={{
                    filename: (item, index) => (
                      <td>
                        
                        <a
                          target="_blank"
                          download=""
                          href={`${WEB_FILE_URL}${item.filepath}`}
                        >
                          {item.filename}
                        </a>
                      </td>
                    ),
                    option:
                        (item) => (
                            <td className="center">
                                <CButtonGroup hidden={isHideEdit}>
                                    <CButton
                                        color="danger"
                                        variant="outline"
                                        shape="square"
                                        size="sm"
                                        onClick={() => { deleteFile(item) }}
                                    >
                                        <CIcon name="cilTrash" />
                                    </CButton>
                                </CButtonGroup>
                            </td>
                        ),
                  }}
                />
              </CCol>
            </CRow>
          </CModalBody>
          <CModalFooter>
            <CButton variant="outline" color="dark" onClick={toggleFile}>
              ปิด
            </CButton>
          </CModalFooter>
        </CModal>
      );
    }
    else{
      return (
        <CModal show={modalFile} onClose={toggleFile}>
          <CModalBody>
            <CRow hidden={isHideEditCmp}>
              <CCol xs="12">
                <div style={{ paddingTop: "1pc" }}>
                  <Upload {...fileprop} maxCount={1}  showUploadList={false}>
                    <Button icon={<UploadOutlined />}>แนบไฟล์</Button>
                    <div style={{ paddingTop: "0.5pc" }}>
                      <label style={{ color: "red", fontSize: "12px" }}>
                        * สามารถอัพโหลดไฟล์ ประเภท
                        .xls,.xlsx,.pdf,.xml,.doc,.docx,.bmp,.gif,.jpg,.jpeg,.png
                        ได้ไม่เกิน 5 ไฟล์ ขนาดไม่เกิน 5 เมกะไบท์
                      </label>
                    </div>
                  </Upload>
                </div>
              </CCol>
            </CRow>
            <CRow>
              <CCol sm={"12"}>
                <CDataTable
                  items={dataFileTable}
                  fields={fieldFiles}
                  // tableFilter
                  // itemsPerPageSelect
                  // itemsPerPage={10}
                  hover
                  sorter
                  pagination
                  scopedSlots={{
                    filename: (item, index) => (
                      <td>
                        
                        <a
                          target="_blank"
                          download=""
                          href={`${WEB_FILE_URL}${item.filepath}`}
                        >
                          {item.filename}
                        </a>
                      </td>
                    ),
                    option:
                        (item) => (
                            <td className="center">
                                <CButtonGroup hidden={isHideEditCmp}>
                                    <CButton
                                        color="danger"
                                        variant="outline"
                                        shape="square"
                                        size="sm"
                                        onClick={() => { deleteFile(item) }}
                                    >
                                        <CIcon name="cilTrash" />
                                    </CButton>
                                </CButtonGroup>
                            </td>
                        ),
                  }}
                />
              </CCol>
            </CRow>
          </CModalBody>
          <CModalFooter>
            <CButton variant="outline" color="dark" onClick={toggleFile}>
              ปิด
            </CButton>
          </CModalFooter>
        </CModal>
      );
    }
  }

  return (
    <>
      <CRow className="mt-3">
        <CCol xs="12" lg="12">
          <CCard>
            <CForm action="">
              <CCardBody>
                <CRow>
                  <CCol sm="12" md="12">
                    <CLabel htmlFor="mainLawName">
                      <b>ชื่อกฎหมาย</b>
                    </CLabel>
                  </CCol>
                  <CCol sm="12" md="12">
                    <CInput
                      name="mainLawName"
                      id="mainLawName"
                      value={mainLawName}
                      readOnly
                    />
                  </CCol>
                </CRow>
                <CRow>
                  <CCol sm="12" md="12">
                    <CLabel htmlFor="lawDescription">
                      <b>สาระสำคัญกฎหมาย</b>
                    </CLabel>
                  </CCol>
                  <CCol sm="12" md="12">
                    <CTextarea
                      rows={8}
                      name="lawDescription"
                      id="lawDescription"
                      value={lawDescription}
                      readOnly
                    />
                  </CCol>
                </CRow>
                <CRow>
                  <CCol sm="12" md="12">
                    <CLabel htmlFor="lawGroupSub">
                      <b>กลุ่มของกฎหมายย่อย</b>
                    </CLabel>
                  </CCol>
                  <CCol sm="12" md="12">
                    <CInput
                      name="lawGroupSub"
                      id="lawGroupSub"
                      value={lawGroupSub}
                      readOnly
                    />
                  </CCol>
                </CRow>
                <CRow>
                  <CCol sm="12" md="12">
                    <CLabel htmlFor="brcsDate">
                      <b>วันบังคับใช้กฎหมาย</b>
                    </CLabel>
                  </CCol>
                  <CCol sm="12" md="12">
                    <CInput
                      name="brcsDate"
                      id="brcsDate"
                      value={brcsDate}
                      readOnly
                    />
                  </CCol>
                </CRow>
                <CRow>
                  <CCol sm="12" md="12">
                    <CLabel htmlFor="estimate">
                      <b>ผลการประเมิน</b>
                    </CLabel>
                  </CCol>
                  <CCol sm="12" md="12">
                    <CSelect
                      onChange={(e) => onEstimateChange(e.target.value)}
                      value={estimate}
                      id="estimate"
                      required
                      disabled={page !== "estimate" || (systemStatus != "SEEV" && systemStatus != "REDI")}
                    >
                      <option value="">กรุณาเลือก</option>
                      <option value="1">ไม่เกี่ยวข้อง</option>
                      <option value="2">สอดคล้อง</option>
                      <option value="3">ไม่สอดคล้อง</option>
                      <option value="4" disabled>สอดคล้อง A</option>
                    </CSelect>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol sm="12" md="12">
                    <CLabel htmlFor="description">
                      <b>รายละเอียด</b>
                    </CLabel>
                  </CCol>
                  <CCol sm="12" md="12">
                    <CTextarea
                      rows={8}
                      onChange={(e) => setDescription(e.target.value)}
                      value={description}
                      id="description"
                      required
                      readOnly={page !== "estimate" || (systemStatus != "SEEV" && systemStatus != "RJCL" && systemStatus != "REDI")}
                    />
                  </CCol>
                </CRow>
                <CRow>
                  <CCol sm="12" md="12">
                    <CLabel htmlFor="rejectReason">
                      <b>เหตุผลการปฏิเสธ</b>
                    </CLabel>
                  </CCol>
                  <CCol sm="12" md="12">
                    <CTextarea
                      rows={3}
                      value={rejectReason}
                      id="rejectReason"
                      readOnly
                    />
                  </CCol>
                </CRow>
              </CCardBody>
              {/* {generateSubmitEstimateBtn()} */}
            </CForm>
          </CCard>
          {generateActionPlanSection()}
        </CCol>
      </CRow>
          
      <CRow className="mt-3">
        <CCol xs="12" lg="12">
          <CFade in={showCard}>
            <CCard>
                <CCardHeader>
                ประวัติ
                  <div className="card-header-actions">
                    <CLink className="card-header-action" onClick={() => setCollapsed(!collapsed)}>
                      {/* <CIcon name={collapsed ? 'cil-chevron-bottom':'cil-chevron-top'} /> */}
                      {collapsed ? 'hide': 'show'}
                    </CLink>
                  </div>
                </CCardHeader>
              <CCollapse show={collapsed}>
              <CCardBody>
                {/* <CRow>
                  <CCol sm="12" md="12">
                    <CLabel>
                      <b>ประวัติ</b>
                    </CLabel>
                  </CCol>
                </CRow> */}
                <CDataTable
                  items={logTable}
                  fields={fields}
                  // itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
                  // itemsPerPage={10}
                  hover
                  sorter
                  striped
                  bordered
                  // pagination
                  scopedSlots={{
                    estimateResult: (item) => (
                      <td>
                          <CBadge>
                              {getEstimate(item.estimateResult)}
                          </CBadge>
                      </td>
                    ),
                    actionplanNo: (item) => (
                      <td>
                          <label
                            style={{ cursor: "pointer", color: "blue" }}
                            onClick={() => {
                              getActionPlanDetail(item)
                              // toggleDetails(index);
                              // getSubTable(item.docno, item.mainLawName, item.description);
                            }}
                          >
                            {item.actionplanNo}
                          </label>
                      </td>
                    ),
                    systemStatus: (item) => (
                      <td>
                          <CBadge>
                              {getStatus(item.systemStatus)}
                          </CBadge>
                      </td>
                    ),
                  }}
              />
              </CCardBody>
              </CCollapse>
            </CCard>
          </CFade>
        </CCol>
      </CRow>

      <CModal show={modalConfirm} onClose={setModalConfirm} color="danger" >
          <CModalHeader closeButton>
              <CModalTitle> Delete Data : </CModalTitle>
          </CModalHeader>
          <CModalBody>
              Are you confirm to delete ?
          </CModalBody>
          <CModalFooter>
              <CButton onClick={onDelete} color={"primary"} variant={"outline"}>Confirm</CButton>{' '}
              <CButton variant="outline" color="secondary" onClick={() => setModalConfirm(false)}>Close</CButton>
          </CModalFooter>
      </CModal>

      {generateModalCause()}
      {generateModalResolve()}
      {generateModalPrevent()}

      {generateModalFile()}

      <CModal show={modalConfirmFile} onClose={setModalConfirmFile} color="danger" >
          <CModalHeader closeButton>
              <CModalTitle> Delete Data : </CModalTitle>
          </CModalHeader>
          <CModalBody>
              Are you confirm to delete ?
          </CModalBody>
          <CModalFooter>
              <CButton onClick={onDeleteFile} color={"primary"} variant={"outline"}>Confirm</CButton>{' '}
              <CButton variant="outline" color="secondary" onClick={() => setModalConfirmFile(false)}>Close</CButton>
          </CModalFooter>
      </CModal>
    </>
  );
};

export default Estimate;
