import React, { useEffect, useRef, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CForm,
  CContainer,
  CLabel,
  CSelect,
  CInput,
  CTabs,
  CModalBody,
  CModalTitle,
  CTextarea,
  CFormGroup,
  CInputFile,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
const CauseModal = ({ modal = false, setModal }) => {
  return (
    <CModal show={modal} onClose={() => setModal(false)}>
      <CModalHeader closeButton>
        <CModalTitle>สาเหตุ</CModalTitle>
      </CModalHeader>
      <CForm action="">
        <CModalBody id="summary-body"></CModalBody>
        <CModalFooter>
          <CButton variant="outline" type="submit" color="primary">
            ตกลง
          </CButton>
          <CButton variant="outline" color="secondary" onClick={() => setModal(false)}>
            ยกเลิก
          </CButton>
        </CModalFooter>
      </CForm>
    </CModal>
  );
};

export default CauseModal;
