import React, { useEffect, useRef, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CForm,
  CContainer,
  CLabel,
  CSelect,
  CInput,
  CTabs,
  CModalBody,
  CModalTitle,
  CTextarea,
  CFormGroup,
  CInputCheckbox
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import ApiBuAssign from "../../../api/ApiBuAssign";
import { Transfer } from "antd";
import { useDispatch, useSelector } from "react-redux";
import "antd/dist/antd.css";
import "./index.scss";
import {
  buassignAction,
  setBuassign,
  setBuSelector,
} from "../../../redux/buassigndata/buassignAction";

const fields = [
  { key: "select", label: "", filter: false },
  { key: "assignGroupCode", label: "Group Code" },
  { key: "plant", label: "Plant" },
  { key: "siteDescription", label: "Plant Description" },
  { key: "departmentCode", label: "Department" },
  // { key: "userLogin", label: "User Login" },
  { key: "fullName", label: "Full Name" },
  // { key: "email", label: "Email" },
];

const EmployeeModal = ({
  modal = false,
  setModal,
  validateDataHeaderAfterBu,
}) => {
  const userData = useSelector((state) => state.changeState.user);
  const [dataTable, setDataTable] = useState([]);
  const [selected, setSelected] = useState([]);
  const [buAssignData, setBuAssignData] = useState([]);
  const mockData = [];
  const initialTargetKeys = buAssignData
    .filter((item) => +item.key > 10)
    .map((item) => item.id);
  const [targetKeys, setTargetKeys] = useState(initialTargetKeys);
  const [selectedKeys, setSelectedKeys] = useState([]);
  const onChange = (nextTargetKeys, direction, moveKeys) => {
    console.log("targetKeys:", nextTargetKeys);
    console.log("direction:", direction);
    console.log("moveKeys:", moveKeys);
    setTargetKeys(nextTargetKeys);
    dispatch(setBuSelector(nextTargetKeys));
  };
  const dispatch = useDispatch();

  const onSelectChange = (sourceSelectedKeys, targetSelectedKeys) => {
    console.log("sourceSelectedKeys:", sourceSelectedKeys);
    console.log("targetSelectedKeys:", targetSelectedKeys);
    setSelectedKeys([...sourceSelectedKeys, ...targetSelectedKeys]);
  };

  const check = (e, id) => {
    if (e.target.checked) {
      setSelected([...selected, id]);
      // setSelectedKeys([...selected, id]);
      // dispatch(setBuSelector([...selected, id]));
    } else {
      setSelected(selected.filter((itemId) => itemId !== id));
      // setSelectedKeys(selected.filter((itemId) => itemId !== id));
      // dispatch(setBuSelector(selected.filter((itemId) => itemId !== id)));
    }
  };

  // const onScroll = (direction, e) => {
  //   console.log("direction:", direction);
  //   console.log("target:", e.target);
  // };

  useEffect(() => {
    getBuAssignOption();
    // dispatch(setBuassign({}));
    // dispatch(setBuSelector({}));
    return () => {};
  }, []);

  useEffect(() => {
    if(modal == true){
      setSelected([]);
    }
    // dispatch(setBuassign({}));
    // dispatch(setBuSelector({}));
    return () => {};
  }, [modal]);

  const getBuAssignOption = async () => {
    const ress = [];
    try {
      const result = await ApiBuAssign.getFromUserBu(userData.id);
      if (result.status == 200) {
        setDataTable(result.data);
        // result.data.forEach((res, i) => {
        //   ress.push({ key: i.toString(), ...res });
        //   console.log("object", ress);
        // });
        // setBuAssignData(ress);
        // dispatch(setBuassign(ress));
      }
    } catch (error) {
      console.log(error);
    }
  };

  const PackData = () =>{
    // console.log("selected", selected);
    validateDataHeaderAfterBu(selected);
  }

  // const selectAll = () => {
  //   const selectDataAll = dataTable.map((x) => x.id);
  //   setSelected(selectDataAll);
  // };
  // const unSelectAll = () => {
  //   setSelected([]);
  // };

  const selectAll = () => {
    const selectDataAll = dataTable.map((x) => x.id);
    setSelected(selectDataAll);
  };
  const unSelectAll = () => {
    setSelected([]);
  };

  return (
    <CModal size={"lg"} show={modal} onClose={setModal} color="success">
      <CModalHeader closeButton>
        <CModalTitle>เลือกผู้ประเมิน</CModalTitle>
      </CModalHeader>
      <CModalBody>
        {/* <Transfer
          dataSource={buAssignData}
          titles={["Source", "Target"]}
          targetKeys={targetKeys}
          selectedKeys={selectedKeys}
          onChange={onChange}
          showSearch
          listStyle={{
            width: "100%",
            height: 300,
          }}
          oneWay
          onSelectChange={onSelectChange}
          onScroll={onScroll}
          render={(item) => {
            console.log("item", item);
            return item.assignGroupCode;
          }}
        /> */}
      <CRow>
        <CCol xs="12" lg="12">
          <CButton className="mr-2" onClick={selectAll} color={"info"} variant={"outline"}>
            <span className="ml-2">เลือกทั้งหมด</span>
          </CButton>
          <CButton className="mr-2" onClick={unSelectAll} color={"danger"} variant={"outline"}>
            <span className="ml-2">ยกเลิก</span>
          </CButton>
        </CCol>
      </CRow>
      <CRow className="">
        <CCol xs="12" lg="12">
          <CDataTable
            items={dataTable}
            fields={fields}
            tableFilter={{ label: "ค้นหา", placeholder: 'พิมพ์คำที่ต้องการค้นหา' }}
            // cleaner
            itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
            itemsPerPage={10}
            hover
            sorter
            striped
            bordered
            pagination
            scopedSlots={{
              select: (item) => {
                return (
                  <td>
                    <CFormGroup variant="custom-checkbox">
                      <CInputCheckbox
                        custom
                        id={`checkbox-${item.id}`}
                        checked={selected.includes(item.id)}
                        onChange={(e) => check(e, item.id)}
                      />
                      <CLabel
                        variant="custom-checkbox"
                        htmlFor={`checkbox-${item.id}`}
                      />
                    </CFormGroup>
                  </td>
                );
              },
            }}
          />
        </CCol>
      </CRow>

        <CRow className="mt-2">
          <CCol sm="12">
            <label className="mb-2">คอมเม้นท์</label>
            <CTextarea rows={3} />
          </CCol>
        </CRow>
      </CModalBody>
      <CModalFooter>
        {selected.length !== 0 && (
          <CButton
            variant="outline"
            color="primary"
            onClick={() => {
              setModal(false)
              PackData();
            }}
          >
            บันทึกและปิด
          </CButton>
        )}

        <CButton variant="outline" color="secondary" onClick={() => setModal(false)}>
          ปิด
        </CButton>
      </CModalFooter>
    </CModal>
  );
};

export default EmployeeModal;
