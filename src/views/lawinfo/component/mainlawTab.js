import React, { useState, useEffect } from "react";
import {
  CCardBody,
  CBadge,
  CButton,
  CCollapse,
  CDataTable,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CCol,
  CCard,
  CRow,
  CCardHeader,
  CFormGroup,
  CLabel,
  CInput,
  CSelect,
  CForm,
  CModalTitle,
  CButtonGroup,
  CTextarea,
  CInputCheckbox,
} from "@coreui/react";
import Api from "../../../api/ApiLawInfo";
import "./index.scss";
import CIcon from "@coreui/icons-react";
import { useDispatch, useSelector } from "react-redux";
import { fetchgetMainLaw } from "../../../redux/mainLaw/mainLawAction";
import HowTo from "./howtoTab";
import { fetchgetMainLawSub } from "../../../redux/mainLawSub/mainLawSubAction";
// import MainLawModal from '../../component/mainlawModal'
// import { statusData } from './data';
import Swal from "sweetalert2/src/sweetalert2.js";

const getBadge = (status) => {
  switch (status) {
    case "Active":
      return "success";
    case "Inactive":
      return "secondary";
    case "Pending":
      return "warning";
    case "Banned":
      return "danger";
    default:
      return "primary";
  }
};

var fieldsFqo = [
  { key: "mainlawSeq", label: "ลำดับ", _style: { width: "1%" } },
  { key: "option", label: "", _style: { width: "1%" }, filter: false },
  { key: "mainlawName", label: "ชื่อกฎหมาย", filter: true },
];

var fieldsView = [
  { key: "mainlawSeq", label: "ลำดับ", _style: { width: "1%" } },
  // { key: "option", label: "", _style: { width: "1%" }, filter: false },
  { key: "mainlawName", label: "ชื่อกฎหมาย", _style: { width: "40%" }, filter: true },
];

var fields = [
  { key: "mainlawSeq", label: "ลำดับ", _style: { width: "1%" } },
  // { key: 'option', label: '', _style: { width: '1%' }, filter: false, },
  { key: "mainlawName", label: "ชื่อกฎหมาย", _style: { width: "40%" }, filter: true },
];

const howtoFields = [
  { key: "seq", label: "ลำดับ", _style: { width: "1%" } },
  { key: "option", label: "", _style: { width: "1%" }, filter: false },
  { key: "description", label: "สาระสำคัญกฎหมาย" },
  { key: "descriptionlink", label: "งานที่เกี่ยวข้อง" },
  { key: "jan", label: "ม.ค." },
  { key: "feb", label: "ก.พ." },
  { key: "mar", label: "มี.ค." },
  { key: "apr", label: "เม.ย." },
  { key: "may", label: "พ.ค." },
  { key: "jun", label: "มิ.ย." },
  { key: "jul", label: "ก.ค." },
  { key: "aug", label: "ส.ค." },
  { key: "sep", label: "ก.ย." },
  { key: "oct", label: "ต.ค." },
  { key: "nov", label: "พ.ย." },
  { key: "dec", label: "ธ.ค." },
];

const MainLaw = ({
  lawid,
  handleMainLawId,
  handleTabAddress,
  saveDataMainLaw,
  deleteDataMainLaw,
  mainlawsStates,
  code,
  modalDetailOpen,
  modalCauseOpen,
  mainlawId,
  saveDataHowTo,
  priorityDatas,
  // mainLawsSubDatas,
  buCode,
  isView,
  headerData
}) => {
  const userState = useSelector((state) => state.changeState.user);
  const [modal, setModal] = useState(false);
  const [id, setId] = useState("");
  const [seq, setSeq] = useState("");
  const [name, setName] = useState("");
  const [dataMainTable, setDataMainTable] = useState([]);
  const dispatch = useDispatch();
  const mailLawsState = useSelector((state) => state.mainLaws);
  const [modalCause, setModalCause] = useState(false);
  const [modalDetail, setModalDetail] = useState(false);
  const [details, setDetails] = useState([]);
  const [lastIndex, setLastIndex] = useState(null);
  const [mainlawSubData, setmainlawSubData] = useState([]);
  const [maimlawFlagSub, setmainlawFlagSub] = useState(false);
  const tranbrsBuState = useSelector((state) => state.gettranbrcsbu.data);
  const [mainlawIId, setmainlawIId] = useState(0);
  const [dataSelected, setDataSelected] = useState("");
  const [modalConfirm, setModalConfirm] = useState(false);
  const mainlawsSubState = useSelector((state) => {
    return state.mainLawsSub.loaded === true && state.mainLawsSub.mainlawsSub;
  });
  const toggleDetails = (index) => {
    const position = details.indexOf(index);
    let newDetails = details.slice();
    if (position !== -1) {
      newDetails.splice(position, 1);
    } else {
      newDetails = [details, index];
    }
    console.log("newDetails", newDetails);
    setDetails(newDetails);
  };

  const toggleConfirm = () => {
    setModalConfirm(!modalConfirm);
  };

  useEffect(() => {
    // console.log("role", userState.role);
    // console.log("!code", !code);
    if (userState.role == "FQO" && !code) {
      fields = fieldsFqo;
    } else {
      fields = fieldsView;
    }
  });

  useEffect(() => {
    if (lawid != 0) {
      // getMainData();
      dispatch(fetchgetMainLaw(lawid));
      if (mailLawsState.loaded === true) {
        // let successmainlaw = mainlaw.filter((res)=>res.brcsId === 59)
        // console.log('successmainlawsuccessmainlaw',successmainlaw)
        setDataMainTable(mailLawsState.mainlaws);
      }
      // console.log(mainlawsStates);
      // generateNewBtn();
    }
    return () => {};
  }, [lawid]);

  const returnMainLawSubAsync = async (value, bu_id) => {
    let resultss = await getMainLawSubPromise(value, bu_id);
    console.log("result::results", resultss);
    setmainlawSubData(resultss);
  };

  const getMainLawSubPromise = (value, bu_id) => {
    return new Promise((resolve, reject) => {
      setTimeout(async () => {
        let results = await Api.getMainLawSub(value, bu_id);
   
        if (results.data.status === 200) {
          let prepare = results.data.response;
          let result = [];
          prepare.length !== 0 &&
            prepare.forEach((data) => {
              if (data.monthList !== " ") {
                let monthParse = data && JSON.parse(data["monthList"]);
                result.push({
                  id: data.id,
                  seq: data.seq,
                  description: data.description,
                  descriptionlink: data.descriptionLink,
                  mainlawsub_buid: data.mainlawsubbuid,
                  estimate: data.estimateNo,
                  priority: data.priority,

                  month01: monthParse.month01,
                  month02: monthParse.month02,
                  month03: monthParse.month03,
                  month04: monthParse.month04,
                  month05: monthParse.month05,
                  month06: monthParse.month06,
                  month07: monthParse.month07,
                  month08: monthParse.month08,
                  month09: monthParse.month09,
                  month10: monthParse.month10,
                  month11: monthParse.month11,
                  month12: monthParse.month12,
                  assessments: data.assessments,
                  lawGroupSubId: data.lawGroupSubId,
                  brcsDate: data.brcsDate,
                  brcsDateForPicker: data.brcsDateForPicker,
                });
              } else {
                result.push({
                  id: data.id,
                  seq: data.seq,
                  description: data.description,
                  descriptionlink: data.descriptionLink,
                  mainlawsub_buid: data.mainlawsubbuid,
                  estimate: data.estimateNo,
                  priority: data.priority,
                  month01: false,
                  month02: false,
                  month03: false,
                  month04: false,
                  month05: false,
                  month06: false,
                  month07: false,
                  month08: false,
                  month09: false,
                  month10: false,
                  month11: false,
                  month12: false,
                  assessments: data.assessments,
                  lawGroupSubId: data.lawGroupSubId,
                  brcsDate: data.brcsDate,
                  brcsDateForPicker: data.brcsDateForPicker,
                });
              }
            });

          resolve(result);
        }
      }, 600);
    });
  };

  const getMainData = async () => {
    try {
      // alert(lawid);
      const result = await Api.getMainLaw(lawid);
      // userState.role = "BU";
      // console.log("userState", userState.role);
      if (userState.role == "FQO") {
        fields = fieldsFqo;
      }
      if (result.status == 200) {
        setDataMainTable(result.data);
      }
      // setDataMainTable(mainData);
    } catch (error) {
      console.log(error);
    }
  };

  const toggle = () => {
    setModal(!modal);
  };

  // const toggleConfirm = () => {
  // //   setModalConfirm(!modalConfirm);
  // }

  const newDetails = (record) => {
    toggle();
    setId("");
    setSeq("");
    setName("");
  };

  const editDetails = (record) => {
    toggle();
    setId(record.id);
    setSeq(record.mainlawSeq);
    setName(record.mainlawName);
    // setNameEng(record.nameEng);
    // setRecordStatus(record.recordstatus);
  };

  const onSubmit = (e) => {
    e.preventDefault();
    const model = {
      id: id == "" ? 0 : id,
      // mainlawSeq: seq,
      mainlawName: name,
      brcsId: lawid,
      user: userState.id,
    };
    saveDataMainLaw(model);
    setModal(false);
  };

  const deleteDetails = (record) => {
    toggleConfirm();
    setDataSelected(record);
    setId(record.id);
  };

  const onDelete = (e) => {
    e.preventDefault();
    const model = {
      id: id,
      user: userState.id,
    };
    deleteDataMainLaw(model);
    toggleConfirm();
  };

  const handlerChangeTabs = (id) => {
    returnMainLawSubAsync(id, buCode);
    // handleTabAddress("2");
  };

  const handlerChangeTab = (id) => {
    handleMainLawId(id);
    // returnMainLawSubAsync(lawid, id);
    handleTabAddress("2");
  };

  function generateNewBtn() {
    if (userState.role == "FQO" && !code) {
      return (
        <CRow className="mt-3" hidden={isView == true}>
          <CCol xs="12" lg="12">
            <label className="" onClick={newDetails} color={"info"}>
              <CIcon name="cil-plus" />
              <span className="ml-2">สร้างกฏหมายหลัก</span>
            </label>
          </CCol>
        </CRow>
      );
    }
  }

  const saveMainlawSubwithCode = async (values) => {
    let results = await promiseMainLawSub(values);
    if (results === "success") {
      if (code === null && buCode === null) {
        returnMainLawSubAsync(values.bucode, 0);
      }
      if (code !== null && buCode !== null) {
        returnMainLawSubAsync(values.bucode, buCode);
      }
      setmainlawFlagSub(true);
    }
  };

  const promiseMainLawSub = (values) => {
    return new Promise((resolve, reject) => {
      return setTimeout(() => {
        Api.createMainLawSub(values);
        resolve("success");
      }, 600);
    });
  };

  const saveDataHowToBU = async (data) => {
    try {
      if (id !== null && code !== null) {
        const result = await Api.createMainLawSub(data);
        if (result.status === 200) {
          const { data } = result.data;
          Swal.fire({
            icon: "success",
            title: "Success",
          });

          returnMainLawSubAsync(mainlawIId, buCode);
        }
      }
      if (id !== null && code === null) {
        let tranBrcsData = [];
        tranBrcsData = tranbrsBuState;
        let count = 0;
        if (data.editflag === false) {
          // await Api.delBrcsMainLawSub(data.MainlawId);
        }
        let datasModel = [];
        tranBrcsData.map((res, i) => {
          let datas = {
            Id: data.editflag === true ? data.id : data.id,
            MainlawId: data.MainlawId,
            // Seq: data.Seq,
            description: data.description,
            descriptionlink: data.descriptionlink,
            estimateNo: data.estimateNo,
            priority: data.priority,
            user: data.user,
            month_list: data.month_list,
            bucode: res.bU_ID,
            count: i + 1,
            editflag: data.editflag,
          };
          datasModel.push(datas);
          // await saveMainlawSubwithCode(datas);
        });
        await saveMainlawSubwithCode(datasModel);
      }
    } catch (error) {}
  };

  return (
    <>
      {generateNewBtn()}
      <CRow className="mt-3">
        <CCol xs="12" lg="12">
          <CCard>
            <CCardBody>
              <CDataTable
                items={mailLawsState.mainlaws}
                fields={fields}
                tableFilter={{
                  label: "ค้นหา",
                  placeholder: "พิมพ์คำที่ต้องการค้นหา",
                }}
                cleaner
                itemsPerPageSelect={{
                  label: "จำนวนการแสดงผล",
                  values: [10, 25, 50, 100],
                }}
                itemsPerPage={10}
                hover
                sorter
                striped
                bordered
                pagination
                scopedSlots={{
                  mainlawName: (item, index) => (
                    <td>
                      {/* <a href={`/home/lawinfo/create?tab=2&id=${lawid}&mainlawId=${item.id}`}>{item.mainlawName}</a> */}
                      <label
                        style={{ cursor: "pointer", color: "blue" }}
                        onClick={() => {
                          if (userState.role === "FQO" && isView != true) {
                            handlerChangeTab(item.id);
                          } else {
                            toggleDetails(index);
                            setmainlawIId(item.id);
                            handlerChangeTabs(item.id);
                          }
                        }}
                        // onClick={() => handlerChangeTab(item.id)}
                      >
                        {details.includes(index)
                          ? item.mainlawName
                          : item.mainlawName}
                      </label>
                    </td>
                  ),
                  details: (item, index) => {
                    return (
                      <div>
                        {(userState.role !== "FQO" || isView == true) && mainlawIId === item.id ? (
                          <CCollapse show={details.includes(index)}>
                            <HowTo
                              modalCauseOpen={setModalCause}
                              modalDetailOpen={setModalDetail}
                              mainlawId={item.id}
                              saveDataHowTo={saveDataHowToBU}
                              mainLawsSubDatas={mainlawSubData}
                              code={code}
                              buCode={buCode}
                              isView={isView}
                              headerData={headerData}
                            />
                          </CCollapse>
                        ) : (
                          <div></div>
                        )}
                      </div>
                    );
                  },
                  option: (item) => (
                    <td className="center">
                      <CButtonGroup hidden={isView == true}>
                        <CButton
                          color="info"
                          variant="outline"
                          shape="square"
                          size="sm"
                          onClick={() => {
                            editDetails(item);
                          }}
                        >
                          <CIcon name="cilPen" />
                        </CButton>
                        <CButton variant="ghost" size="sm" />
                        <CButton
                          color="danger"
                          variant="outline"
                          shape="square"
                          size="sm"
                          onClick={() => {
                            deleteDetails(item);
                          }}
                        >
                          <CIcon name="cilTrash" />
                        </CButton>
                      </CButtonGroup>
                    </td>
                  ),
                }}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>

      <CModal show={modalConfirm} onClose={setModalConfirm} color="danger">
        <CModalHeader closeButton>
          <CModalTitle> Delete Data</CModalTitle>
        </CModalHeader>
        <CModalBody>Are you confirm to delete?</CModalBody>
        <CModalFooter>
          <CButton onClick={onDelete} color={"primary"} variant={"outline"}>
            Confirm
          </CButton>{" "}
          <CButton variant="outline" color="secondary" onClick={() => setModalConfirm(false)}>
            Close
          </CButton>
        </CModalFooter>
      </CModal>

      {/* <MainLawModal modal={modal} onSubmit={onSubmit} setSeq={setSeq} setName={setName} toggle={toggle} id={id} seq={seq} name={name} editDetails={editDetails}/> */}
      <CModal show={modal} onClose={toggle} size="lg">
        <CModalHeader closeButton>
          <CModalTitle>รายละเอียดประเภทกฏหมาย</CModalTitle>
        </CModalHeader>
        <CForm onSubmit={onSubmit} action="">
          <CModalBody>
            <CInput type="hidden" value={id} id="id" />
            <CRow>
              <CCol xs="12" sm="12">
                <CRow>
                  <CCol xs="4">
                    <CFormGroup>
                      <CLabel>ลำดับที่</CLabel>
                      <CInput
                        // onChange={(e) => setSeq(e.target.value)}
                        value={seq}
                        id="seq"
                        // required
                        // type="number"
                        // pattern="[0-9]*"
                        // inputMode="numeric"
                        // min="1"
                        readOnly
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel>ชื่อกฎหมาย</CLabel>
                      <CInput
                        onChange={(e) => setName(e.target.value)}
                        value={name}
                        id="name"
                        required
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>
              </CCol>
            </CRow>
          </CModalBody>
          <CModalFooter>
            <CButton variant="outline" type="submit" color="primary">
              ตกลง
            </CButton>
            <CButton variant="outline" color="secondary" onClick={toggle}>
              ยกเลิก
            </CButton>
          </CModalFooter>
        </CForm>
      </CModal>
    </>
  );
};

export default MainLaw;
