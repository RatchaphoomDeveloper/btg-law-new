import React, { useEffect, useState, useCallback } from "react";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CNav,
  CForm,
  CContainer,
  CLabel,
  CSelect,
  CInput,
  CTextarea,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import ApiSystemType from "../../../api/ApiSystemType";
import ApiCountryAssign from "../../../api/ApiCountryAssign";
import ApiCountry from "../../../api/ApiCountry";
import ApiCountryType from "../../../api/ApiCountryType";
import ApiLawGroup from "../../../api/ApiLawGroup";
import ApiLawGroupSub from "../../../api/ApiLawGroupSub";
import ApiLawAgencies from "../../../api/ApiLawAgencies";
import ApiLawAgencieSub from "../../../api/ApiLawAgencieSub";
import ApiLawType from "../../../api/ApiLawType";
import ApiBu from "../../../api/ApiBU";
import Api from "../../../api/ApiLawInfo";
import { useParams } from "react-router-dom";
// import statusData from './data';
import Select from "react-select";
import Swal, { swal } from "sweetalert2/dist/sweetalert2.js";
import store from "../../../store";
import {
  setaddLawinfo,
  setrefreshLawinfo,
} from "../../../redux/lawinfoDetail/lawinfoAction";
import _ from "underscore";
import Moment from "moment";

const CreatHeadPanel = ({
  statusOption = [],
  buttonOption = [],
  id,
  validateDataHeader,
  // isHideCancelRemark,
  page,
  code,
  setHeaderData
}) => {
  const dispatch = useDispatch();
  const userState = useSelector((state) => state.changeState.user);
  const lawinfo = useSelector((state) => state.lawinfoReducer.data);
  const [systemTypeData, setSystemTypeData] = useState([]);
  const [countryAssignData, setCountryAssignData] = useState([]);
  const [countryTypeData, setCountryTypeData] = useState([]);
  const [countryData, setCountryData] = useState([]);
  const [lawGroupData, setLawGroupData] = useState([]);
  const [lawGroupSubData, setLawGroupSubData] = useState([]);
  const [deptData, setDeptData] = useState([]);
  const [deptSubData, setDeptSubData] = useState([]);
  const [lawTypeData, setLawTypeData] = useState([]);
  const [buData, setBUData] = useState([]);
  const [singleBuData, setSingleBuDat] = useState([]);
  const [systemtype, setSystemType] = useState("");
  const [countryAssign, setCountryAssign] = useState("");
  const [countryType, setCountryType] = useState("");
  const [country, setCountry] = useState("");
  const [lawGroup, setLawGroup] = useState("");
  const [lawGroupSub, setLawGroupSub] = useState("");
  const [lawAgencie, setLawAgencie] = useState("");
  const [lawAgencieSub, setLawAgencieSub] = useState("");
  const [lawType, setLawType] = useState("");
  const [status, setStatus] = useState("");
  const [docNo, setDocNo] = useState("");
  const [revisionNo, setRevisionNo] = useState("");
  const [rejectReason, setRejectReason] = useState("");
  const [isHideCancelRemark, setHideCancelRemark] = useState(true);
  const [createBy, setCreateBy] = useState("");
  const [createDate, setCreateDate] = useState("");
  const [updateDate, setUpdateDate] = useState("");
  const [sysStatus, setsysStatus] = useState("");
  const [sysStatusText, setsysStatusText] = useState("");
  const [systemStatusforButton, setsystemStatusforButton] = useState("");
  const [buList, setBuList] = useState([]);
  const [buListText, setBuListText] = useState("");
  const [buListDefault, setBuListDefault] = useState("");
  const [lawinfoFlag, setlawinfoFlag] = useState(false);
  // const [brcsDate, setBrcsDate] = useState();
  const [brcsDate, setBrcsDate] = useState(
    Moment(Date.now()).format("yyyy-MM-DD")
  );
  const [reviewDate, setReviewDate] = useState('');
  const [isHideReviewDate, setHideReviewDate] = useState(true);
  const statusList = [
    { value: "SADR", label: "Save" },
    { value: "NEDO", label: "Submit" },
    { value: "CACE", label: "Delete" },
    { value: "SEEV", label: "Send To Evaluate" },
    { value: "REFQ", label: "Reject To FQO" },
    { value: "SEA1", label: "Send to VERIFY" },
    { value: "SEA2", label: "Send to Approver" },
    { value: "DOAP", label: "Document Approve" },
    { value: "REDI", label: "Reject To User Division" },
  ];
  useCallback(() => {}, [lawinfo]);

  // const [statusData, setStatusData] = useState([]);

  const [qDate, setQDate] = useState({ startDate: null, endDate: null });
  const [qFocused, setQFocused] = useState();

  useEffect(() => {
      getLawInfoDetail();
    //   getSystemTypeOption();
    // getCountryAssignOption();
    // getCountryTypeOption();
    // getCountryOption();
    // getLawGroupOption();
    // getLawGroupSubOption();
    // getDeptOption();
    // getDeptSubOption();
    // getLawTypeOption();
    getBuOption();
    // if (id != null) {
    //   getLawInfoDetail();
    // } else {
    //   // getBuOption();
    //   dispatch(setaddLawinfo({}));
    // }

    // setTimeout(() => {
    //   getBuOption();
    // }, 1000);

    // getStatusOption();
    return () => {};
  }, []);

  useEffect(() => {
    getDefaultBuOption();
    return () => {};
  }, [lawinfo]);

  useEffect(() => {
    // console.log("rejectReason: ", rejectReason);
    // console.log("rejectReason != null: ", rejectReason != null);
    // console.log("rejectReason.length > 0: ", rejectReason.length > 0);
    // console.log("isHideCancelRemark: ", isHideCancelRemark);
    if (rejectReason != null && rejectReason.length > 0) {
      setHideCancelRemark(false);
      // generateSaveBtn();
      // console.log("isHideCancelRemark: ", isHideCancelRemark);
    }
    return () => {};
  }, [rejectReason]);

  useEffect(() => {
    if (reviewDate != null && reviewDate.length > 0) {
      setHideReviewDate(false);
    }
    return () => {};
  }, [reviewDate]);

  useEffect(() => {
    console.log("isHideCancelRemark: ", isHideCancelRemark);
    generateSaveBtn();
    return () => {};
  }, [isHideCancelRemark]);

  useEffect(() => {
    // console.log("isHideCancelRemark: ", isHideCancelRemark);
    generateSaveBtn();
    return () => {};
  }, [isHideReviewDate]);

  // const setCurrentDate = () => {
  //   console.log(Date());
  //   return null;
  // };

  const getLawInfoDetail = async () => {
    try {
      if(code == null || code == undefined){
        code='';
      }
      const result = await Api.getDetail(id, code);
      console.log("lawinfo::", result.data);
      if (result.status == 200) {
        setData(result.data);
        dispatch(setaddLawinfo(result.data));
        setlawinfoFlag(true);
      }
    } catch (error) {
      console.log(error);
    }
  };

  function getStatusText(status){
    console.log("statusList", statusList);
    var result = statusList.filter((d) => d.value == status)[0].label;
    // console.log("getStatusText", result);
    return result;
  }

  const setData = (data) => {
    // console.log("data", data);
    // console.log("systemtypeId", data.systemtypeId);
    setSystemType(data.systemtype);
    setCountryAssign(data.countryassign);
    setCountry(data.country);
    setLawGroup(data.lawgroup);
    setLawGroupSub(data.lawgroupSub);
    setLawAgencie(data.lawagency);
    setLawAgencieSub(data.lawagencySub);
    setLawType(data.lawtype);
    setDocNo(data.docno);
    setRevisionNo(data.revisionNo);
    setStatus(data.systemStatus);
    setCreateBy(data.createBy);
    setCreateDate(data.createDate);
    setUpdateDate(data.updateDate);
    setsysStatus(data.systemStatus);
    setBuListDefault(data.buIdList);
    setBuListText(data.buIdList);
    setBrcsDate(data.brcsDate);
    setReviewDate(data.reviewDate);
    setRejectReason(data.rejectReason);
    setsysStatusText(getStatusText(data.systemStatus));
    // console.log("data.rejectReason", data.rejectReason);
    // console.log("rejectReason", rejectReason);
    if(rejectReason != null && rejectReason.length > 0){
      isHideCancelRemark = false;
    }
    var headerData = {
      systemtype: data.systemtype,
      countryassign: data.countryassign,
      country: data.country,
      lawgroup: data.lawgroup,
      lawgroupId: data.lawgroupId,
      lawgroupSub: data.lawgroupSub,
      lawgroupSubId: data.lawgroupSubId,
      lawagency: data.lawagency,
      lawagencySub: data.lawagencySub,
      lawtype: data.lawtype,
      docno: data.docno,
      revisionNo: data.revisionNo,
      systemStatus: data.systemStatus,
      createBy: data.createBy,
      createDate: data.createDate,
      updateDate: data.updateDate,
      buIdList: data.buIdList,
      brcsDate: data.brcsDate,
      brcsDateForPicker: data.brcsDateForPicker,
      reviewDate: data.reviewDate,
      rejectReason: data.rejectReason,
    }
    setHeaderData(headerData);
  };

  const getDefaultBuOption = async () => {
    const selectedValue = [];
    try {
      buData.forEach((res) => {
        if (
          id > 0 &&
          lawinfo.buIdList &&
          lawinfo.buIdList.includes(res.value)
        ) {
          selectedValue.push({ value: res.value, label: res.value });
        }
      });
      setBuList(selectedValue);
    } catch (error) {
      console.log(error);
    }
  };

  const getBuOption = async () => {
    const dataPush = [];
    const selectedValue = [];
    const singleDataPush = [];
    try {
      const result = await ApiBu.get();
      if (result.status == 200) {
        result.data.forEach((res) => {
          dataPush.push({ value: res.code, label: res.code });
          // if (res.id === parseInt(code)) {
          //   dataPush.push({ value: res.code, label: res.code });
          // }
        });

        setBUData(dataPush);
        // setBuList(selectedValue);
        // if (code) {
        //   result.data.forEach((res) => {
        //     if (res.id === parseInt(code)) {
        //       singleDataPush.push({ value: res.code, label: res.code });
        //       setSingleBuDat(singleDataPush);
        //     }
        //   });
        // }
        // buoption = result.data.map((item, index) =>{
        //   return value={item.id}
        // })
        // {
        //   buData.map((item, index) => {
        //     return <option key={item.id} value={item.id}>{item.code}</option>
        //   })
        // }
      }
    } catch (error) {
      console.log(error);
    }
  };


  const onsubmitPromise = (val) => {
    try {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          setsysStatus(val);
          if (val !== "SEEV") {
            Swal.showLoading();
          }
          resolve(200);
        }, 600);
      });
    } catch (error) {}
  };

  const onsubmitAsync = async (val) => {
    try {
      let result = await onsubmitPromise(val);

      if (result === 200) {
        onSubmit(val);
      }
    } catch (error) {
      console.log(error.message);
    }
  };

  const prepareonSubmit = (val) => {
    try {
      onsubmitAsync(val);
    } catch (error) {
      console.log(error.message);
    }
  };

  const onSubmit = (e) => {    
    if (e === "REFQ") {
      Swal.fire({
        title: "เหตุผล",
        input: "text",
        inputAttributes: {
          autocapitalize: "off",
        },
        showCancelButton: true,
        confirmButtonText: "OK",
        showLoaderOnConfirm: true,
        preConfirm: (input) => {
          if (input == "") {
            Swal.fire({
              icon: "error",
              title: "Process Error!",
              text: "กรุณากรอกเหตุผล !",
            });
            return false;
          }
          const model = {
            id: id == null ? 0 : id,
            SystemStatus: e ? e : sysStatus,
            user: userState.id,
            RejectReason: input,
          };
          if (model.SystemStatus !== "") {
            validateDataHeader(model);
          } else {
            Swal.fire({
              icon: "error",
              title: "Process Error!",
              text: "Please choose system status!",
            });
            dispatch({ type: "SET_HIDE_TABLE", status: 0 });
          }
        },
        allowOutsideClick: () => !Swal.isLoading(),
      }).then((result) => {});
    } else {
      const model = {
        id: id == null ? 0 : id,
        SystemStatus: e ? e : sysStatus,
        user: userState.id,
        buList: buList,
        RejectReason: "",
      };
      console.log("model", model);
      if (model.SystemStatus !== "") {
        validateDataHeader(model);
      } else {
        Swal.fire({
          icon: "error",
          title: "Process Error!",
          text: "Please choose system status!",
        });
        dispatch({ type: "SET_HIDE_TABLE", status: 0 });
      }
    }
  };

  function generateSaveBtn() {
    if (page != "searchOnly") {
      return (
        <>
          <CCol hidden>
            <CRow>
              <CCol sm="12" md="12">
                <CLabel htmlFor="Priority">
                  {" "}
                  <b>สถานะระบบ</b>{" "}
                </CLabel>
              </CCol>
              <CCol sm="12" md="12">
                <CInput 
                  name="status"
                  id="status"
                  value={sysStatus}
                  // onChange={(e) => setsysStatus(e.target.value)}
                  disabled/>
              </CCol>
            </CRow>
          </CCol>
          <CCol sm="12" md="3" hidden={isHideReviewDate}>
            <CRow>
              <CCol sm="12" md="12">
                <CLabel>
                  {" "}
                  <b>วันที่ทบทวนเอกสาร</b>{" "}
                </CLabel>
              </CCol>
              <CCol col xs="12" md="12">
                <CInput
                  name="reviewDate"
                  id="reviewDate"
                  value={reviewDate}
                  // onChange={(e) => setBrcsDate(e.target.value)}
                  disabled/>
              </CCol>
            </CRow>
          </CCol>
          <CCol sm="12" md="3" hidden={isHideCancelRemark} id="remarkContainer">
            <CRow>
              <CCol sm="12" md="12">
                <CLabel htmlFor="Priority">
                  {" "}
                  <b>เหตุผลการปฏิเสธ</b>{" "}
                </CLabel>
              </CCol>
              <CCol sm="12" md="12">
                <CTextarea rows="3" value={rejectReason} readOnly></CTextarea>
              </CCol>
            </CRow>
          </CCol>
          {(!code || window.location.href.includes("addfrequency")) && (
            <CCol md="6" className="mt-4">
              <CRow>
                {buttonOption.map((item, index) => {
                  return (
                    <CCol
                      sm="12"
                      md="4"
                      className="justify-content-center"
                      key={item.value}
                      value={item.value}
                      onClick={() => prepareonSubmit(item.value)}
                    >
                      <CButton color="primary" variant="outline" block>
                        {item.label}
                      </CButton>
                    </CCol>
                  );
                })}
              </CRow>
            </CCol>
          )}

        </>
      );
    }
  }
  return (
    <>
      <CRow>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>ระบบ</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
              <CInput
                name="systemtype"
                id="systemtype"
                value={systemtype}
                // onChange={(e) => setSystemType(e.target.value)}
                disabled/>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>แหล่งที่มากฎหมาย</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
              <CInput
                name="countryAssign"
                id="countryAssign"
                value={countryAssign}
                // onChange={(e) => setCountryAssign(e.target.value)}
                disabled/>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>ประเทศที่ออกกฎหมาย</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
              <CInput
                name="country"
                id="country"
                value={country}
                // onChange={(e) => setCountry(e.target.value)}
                disabled/>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>ประเภทกฎหมาย</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
              <CInput
                name="lawType"
                id="lawType"
                value={lawType}
                // onChange={(e) => setLawType(e.target.value)}
                disabled/>
            </CCol>
          </CRow>
        </CCol>
      </CRow>
      <CRow className="mt-3">
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>หน่วยงานผู้ออกกฎหมาย</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
              <CInput
                name="dept"
                id="dept"
                value={lawAgencie}
                // onChange={(e) => setLawAgencie(e.target.value)}
                disabled/>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>หน่วยงานย่อยผู้ออกกฎหมาย</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
              <CInput
                name="dept"
                id="dept"
                value={lawAgencieSub}
                // onChange={(e) => setLawAgencieSub(e.target.value)}
                disabled/>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>กลุ่มของกฎหมาย</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
              <CInput
                name="lawGroup"
                id="lawGroup"
                value={lawGroup}
                // onChange={(e) => onLawGroupChange(e.target.value)}
                disabled/>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>กลุ่มของกฎหมายย่อย</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
              <CInput
                name="lawGroupSub"
                id="lawGroupSub"
                value={lawGroupSub}
                // onChange={(e) => setLawGroupSub(e.target.value)}
                disabled/>
            </CCol>
          </CRow>
        </CCol>
      </CRow>
      <CRow className="mt-3">
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>หมายเลขเอกสาร</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
              <CInput value={docNo} readOnly />
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel>
                {" "}
                <b>วันบังคับใช้กฎหมาย</b>{" "}
              </CLabel>
            </CCol>
            <CCol col xs="12" md="12">
              <CInput
                name="brcsDate"
                id="brcsDate"
                value={brcsDate}
                // onChange={(e) => setBrcsDate(e.target.value)}
                disabled/>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>ชื่อผู้สร้างเอกสาร</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
              <CInput value={createBy} readOnly />
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>วันที่สร้างเอกสาร</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
              <CInput value={createDate} readOnly />
            </CCol>
          </CRow>
        </CCol>
      </CRow>
      <CRow className="mt-3">
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority"><b>สถานะระบบ</b></CLabel>
            </CCol>
            <CCol sm="12" md="12">
              <CInput value={sysStatusText} disabled/>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3" hidden>
          <CRow>
            <CCol sm="12" md="12">
              <CLabel>
                {" "}
                <b>ธุรกิจที่เกี่ยวข้อง</b>{" "}
              </CLabel>
            </CCol>
            <CCol col xs="12" md="12">
              <Select
                name="form-field-name2"
                value={buList}
                // value={lawinfo.buIdList?.split(",").map((x) => {
                //   return { label: x, code: x };
                // })}
                options={buData}
                isDisabled={
                  window.location.href.includes("id") === true ? true : false
                }
                onChange={(e) => {
                  // const buStep = [];
                  // buStep.push(e);
                  setBuList(e);
                }}
                isMulti
              />
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel>
                {" "}
                <b>ธุรกิจที่เกี่ยวข้อง</b>{" "}
              </CLabel>
            </CCol>
            <CCol col xs="12" md="12">
              <CInput
                name="form-field-name2"
                value={buListText}
                // onChange={(e) => setLawAgencie(e.target.value)}
                disabled/>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>ครั้งที่แก้ไข</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
              <CInput value={revisionNo} readOnly />
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>วันที่แก้ไข</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
              <CInput value={updateDate} readOnly />
            </CCol>
          </CRow>
        </CCol>
      </CRow>
      <CRow className="mt-3">{generateSaveBtn()}</CRow>
    </>
  );
};

export default CreatHeadPanel;
