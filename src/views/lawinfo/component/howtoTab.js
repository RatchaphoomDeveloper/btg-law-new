import React, { useState, useEffect } from "react";
import {
  CCardBody,
  CBadge,
  CButton,
  CCollapse,
  CDataTable,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CCol,
  CCard,
  CRow,
  CCardHeader,
  CFormGroup,
  CLabel,
  CInput,
  CSelect,
  CForm,
  CModalTitle,
  CButtonGroup,
  CTextarea,
  CInputCheckbox,
  CCardFooter,
} from "@coreui/react";
import Api from "../../../api/ApiLawInfo";
import ApiLawGroupSub from "../../../api/ApiLawGroupSub";
import CIcon from "@coreui/icons-react";
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2/src/sweetalert2.js";
import { fetchgetMainLawSub } from "../../../redux/mainLawSub/mainLawSubAction";
import Select from "react-select";
import Moment from "moment";

// import { statusData } from './data';

const getBadge = (status) => {
  switch (status) {
    case "SEEV":
      return "warning";
    case "SEA1":
      return "primary";
    case "SEA2":
      return "info";
    case "DOAP":
      return "success";
    case "REDI":
      return "danger";
    default:
      return "";
  }
};
const options = [
  { value: "", label: "กรุณาเลือก" },
  { value: "12", label: "ทุกเดือน" },
  { value: "1", label: "1 รอบ/ปี" },
  { value: "2", label: "2 รอบ/ปี" },
  { value: "3", label: "3 รอบ/ปี" },
  { value: "4", label: "4 รอบ/ปี" },
  { value: "5", label: "5 รอบ/ปี" },
  { value: "6", label: "6 รอบ/ปี" },
  { value: "7", label: "7 รอบ/ปี" },
  { value: "8", label: "8 รอบ/ปี" },
  { value: "9", label: "9 รอบ/ปี" },
  { value: "10", label: "10 รอบ/ปี" },
  { value: "11", label: "11 รอบ/ปี" },
];

const HowTo = ({
  modalDetailOpen,
  modalCauseOpen,
  mainlawId,
  saveDataHowTo,
  deleteDataHowTo,
  priorityDatas,
  mainLawsSubDatas,
  buCode,
  code,
  isView,
  headerData
}) => {
  const userState = useSelector((state) => state.changeState.user);
  const [editDetailFlag, seteditDetailFlag] = useState(false);
  const [priorityData, setPriorityData] = useState([]);
  const [modal, setModal] = useState(false);
  const [dataTable, setDataTable] = useState([]);
  const [id, setId] = useState("");
  const [seq, setSeq] = useState("");
  const [description1, setDescription1] = useState("");
  const [description2, setDescription2] = useState("");
  const [priority, setPriority] = useState("");
  const [estimate, setEstimate] = useState("");
  const [lawGroupSubData, setLawGroupSubData] = useState([]);
  const [lawGroupSubHeader, setLawGroupSubHeader] = useState("");
  const [lawGroupSub, setLawGroupSub] = useState("");
  const [brcsDateHeader, setBrcsDateHeader] = useState("");
  const [brcsDate, setBrcsDate] = useState("");
  const [destimate, setdEstimate] = useState([]);
  const [allMonth, setAllMonth] = useState(false);
  const [month01, setMonth01] = useState(false);
  const [month02, setMonth02] = useState(false);
  const [month03, setMonth03] = useState(false);
  const [month04, setMonth04] = useState(false);
  const [month05, setMonth05] = useState(false);
  const [month06, setMonth06] = useState(false);
  const [month07, setMonth07] = useState(false);
  const [month08, setMonth08] = useState(false);
  const [month09, setMonth09] = useState(false);
  const [month10, setMonth10] = useState(false);
  const [month11, setMonth11] = useState(false);
  const [month12, setMonth12] = useState(false);
  const dispatch = useDispatch();
  const [toggles, setToggles] = useState([]);
  const [lastIndex, setLastIndex] = useState(null);
  const mainlawsSubState = useSelector((state) => {
    return state.mainLawsSub.loaded === true && state.mainLawsSub.mainlawsSub;
  });
  const [flagClear, setflagClear] = useState(false);
  const [dataSelected, setDataSelected] = useState("");
  const [modalConfirm, setModalConfirm] = useState(false);
  var fields = [
    { key: "seq", label: "ลำดับ", _style: { width: "1%" } },
    { key: "option", label: "", _style: { width: "1%" }, filter: false },
    { key: "description", label: "สาระสำคัญกฎหมาย", width: "30%" },
    // {
    //   key: "descriptionlink",
    //   label: userState.role === "FQO" ? "งานที่เกี่ยวข้อง" : "ผู้ประเมิน",
    // },
    { key: "jan", label: "ม.ค." },
    { key: "feb", label: "ก.พ." },
    { key: "mar", label: "มี.ค." },
    { key: "apr", label: "เม.ย." },
    { key: "may", label: "พ.ค." },
    { key: "jun", label: "มิ.ย." },
    { key: "jul", label: "ก.ค." },
    { key: "aug", label: "ส.ค." },
    { key: "sep", label: "ก.ย." },
    { key: "oct", label: "ต.ค." },
    { key: "nov", label: "พ.ย." },
    { key: "dec", label: "ธ.ค." },
  ];
  var fieldsFqo = [
    { key: "seq", label: "ลำดับ", _style: { width: "1%" } },
    { key: "option", label: "", _style: { width: "1%" }, filter: false },
    { key: "description", label: "สาระสำคัญกฎหมาย", width: "30%" },
  ];
  var fieldsOther = [
    { key: "seq", label: "ลำดับ", _style: { width: "1%" } },
    { key: "option", label: "", _style: { width: "1%" }, filter: false },
    { key: "description", label: "สาระสำคัญกฎหมาย", width: "30%" },
    { key: "jan", label: "ม.ค." },
    { key: "feb", label: "ก.พ." },
    { key: "mar", label: "มี.ค." },
    { key: "apr", label: "เม.ย." },
    { key: "may", label: "พ.ค." },
    { key: "jun", label: "มิ.ย." },
    { key: "jul", label: "ก.ค." },
    { key: "aug", label: "ส.ค." },
    { key: "sep", label: "ก.ย." },
    { key: "oct", label: "ต.ค." },
    { key: "nov", label: "พ.ย." },
    { key: "dec", label: "ธ.ค." },
  ];

  var fieldsAssignment = [
    { key: "seq", label: "ลำดับ", _style: { width: "1%" } },

    // { key: "option", label: "", _style: { width: "1%" }, filter: false },
    { key: "assessmentNo", label: "เลข Assessment", width: "30%" },
    { key: "fullname", label: "ผู้ประเมิน", width: "10%" },
    { key: "jan", label: "ม.ค." },
    { key: "feb", label: "ก.พ." },
    { key: "mar", label: "มี.ค." },
    { key: "apr", label: "เม.ย." },
    { key: "may", label: "พ.ค." },
    { key: "jun", label: "มิ.ย." },
    { key: "jul", label: "ก.ค." },
    { key: "aug", label: "ส.ค." },
    { key: "sep", label: "ก.ย." },
    { key: "oct", label: "ต.ค." },
    { key: "nov", label: "พ.ย." },
    { key: "dec", label: "ธ.ค." },
  ];

  const toggleDetails = (index) => {
    let newDetails = toggles.slice();
    const position = toggles.indexOf(index);
    if (position !== -1) {
      newDetails.splice(position, 1);
    } else {
      newDetails = [...toggles, index];
    }
    setToggles(newDetails);
  };

  // useEffect(() => {
  //   // console.log("role", userState.role);
  //   // console.log("!code", !code);
  //   // if (userState.role == "FQO" && !code) {
  //   //   fields = fieldsFqo;
  //   //   console.log("fields", fields);
  //   // } else {
  //   //   fields = fieldsOther;
  //   // }
  //   // generateTable();
  // });

  useEffect(() => {
    // console.log("role", userState.role);
    // console.log("code", code);
    // console.log("buCode", buCode);
    // console.log("isView", isView);
    getData();
    getPriorityOption();
    // returnMainLawSubAsync(mainlawId, buCode);
    return () => {};
  }, [mainlawId]);

  useEffect(() => {
    console.log("headerData", headerData);
    getLawGroupSubOption(headerData.lawgroupId);
    setLawGroupSubHeader(headerData.lawgroupSubId);
    setBrcsDateHeader(headerData.brcsDateForPicker);
    // console.log("headerData", headerData);

    // returnMainLawSubAsync(mainlawId, buCode);
    return () => {};
  }, [headerData]);

  const getData = async () => {
    try {
      if (mainlawId != null) {
        const result = await Api.getMainLawSub(mainlawId, buCode);
        // userState.role = "BU";
        // console.log("userState", userState.role);

        if (result.status == 200) {
          setDataTable(result.data);
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getLawGroupSubOption = async (val) => {
    try {
      var result;
      // console.log("val", val);
      // console.log("lawGroup", lawGroup);
      result = await ApiLawGroupSub.get(val);
      // const result = await ApiLawGroupSub.get(lawGroup);
      if (result.status == 200) {
        setLawGroupSubData(result.data);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getPriorityOption = async () => {
    try {
      const result = await Api.getPriority();
      if (result.status == 200) {
        setPriorityData(result.data);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const toggle = () => {
    setModal(!modal);
  };

  const toggleConfirm = () => {
    setModalConfirm(!modalConfirm);
  };

  const clearMonth = () => {
    setAllMonth(false);
    setMonth01(false);
    setMonth02(false);
    setMonth03(false);
    setMonth04(false);
    setMonth05(false);
    setMonth06(false);
    setMonth07(false);
    setMonth08(false);
    setMonth09(false);
    setMonth10(false);
    setMonth11(false);
    setMonth12(false);
  };

  const allMonthCheck = () => {
    if (flagClear === true) {
      clearMonth();
    } else {
      setAllMonth(true);
      setMonth01(true);
      setMonth02(true);
      setMonth03(true);
      setMonth04(true);
      setMonth05(true);
      setMonth06(true);
      setMonth07(true);
      setMonth08(true);
      setMonth09(true);
      setMonth10(true);
      setMonth11(true);
      setMonth12(true);
    }
  };

  const newDetails = () => {
    // console.log("new HowTo");
    toggle();
    setId("");
    setSeq("");
    setDescription1("");
    setDescription2("");
    setPriority(0);
    setEstimate(options.filter((d) => d.value === ""));
    setAllMonth(false);
    setMonth01(false);
    setMonth02(false);
    setMonth03(false);
    setMonth04(false);
    setMonth05(false);
    setMonth06(false);
    setMonth07(false);
    setMonth08(false);
    setMonth09(false);
    setMonth10(false);
    setMonth11(false);
    setMonth12(false);
    seteditDetailFlag(false);
    setBrcsDate(brcsDateHeader);
    setLawGroupSub(lawGroupSubHeader);
  };

  const editDetails = (record) => {
    // console.log("record", record);
    // console.log("estimate", record.estimate);
    // setdEstimate(options.filter((d) => d.value === record.estimateNo));
    setEstimate(options.filter((d) => d.value === record.estimate));

    // setPriority(priorityData.filter((d) => d.value === record.priority));
    toggle();
    setId(record.id);
    setSeq(record.seq);
    setDescription1(record.description);
    setDescription2(record.descriptionlink);
    setPriority(record.priority);
    setLawGroupSub(record.lawGroupSubId);
    if (record.lawGroupSubId == 0){
      setLawGroupSub(lawGroupSubHeader);
    }
    setBrcsDate(record.brcsDateForPicker);
    if (record.brcsDateForPicker == ""){
      // setBrcsDate(Moment(record.brcsDate).format("yyyy-MM-dd"));
      setBrcsDate(brcsDateHeader);
    }
    // setEstimate(record.estimate);

    if (
      record.month01 &&
      record.month02 &&
      record.month03 &&
      record.month04 &&
      record.month05 &&
      record.month06 &&
      record.month07 &&
      record.month08 &&
      record.month09 &&
      record.month10 &&
      record.month11 &&
      record.month12
    ) {
      setAllMonth(true);
    } else {
      setAllMonth(false);
    }

    setMonth01(record.month01);
    setMonth02(record.month02);
    setMonth03(record.month03);
    setMonth04(record.month04);
    setMonth05(record.month05);
    setMonth06(record.month06);
    setMonth07(record.month07);
    setMonth08(record.month08);
    setMonth09(record.month09);
    setMonth10(record.month10);
    setMonth11(record.month11);
    setMonth12(record.month12);
    seteditDetailFlag(true);
  };

  const onSubmit = (e) => {
    e.preventDefault();
    const monthArr = [
      month01,
      month02,
      month03,
      month04,
      month05,
      month06,
      month07,
      month08,
      month09,
      month10,
      month11,
      month12,
    ];

    // var estimateNo = "";
    // if (id == 0){
    //   estimateNo = estimate;
    // }else{
    //   estimateNo = estimate[0].value;
    // }

    const monthCounth = monthArr.filter((res) => {
      return res === true;
    });
    let mainlawsub_bu_id = [];

    mainlawsub_bu_id =
      editDetailFlag === true &&
      mainLawsSubDatas.filter((res) => res.id === id);

    var estimateValue = 0;
    if (estimate.length > 0){
      if (estimate[0].value) {
        estimateValue = parseInt(estimate[0].value);
      }
    }
    const model = {
      id: id == "" ? 0 : id,
      MainlawId: mainlawId,
      mainlawsubbuid:
        editDetailFlag === true ? mainlawsub_bu_id[0].mainlawsub_buid : 0,
      Seq: seq,
      description: description1,
      descriptionlink: description2,
      estimateNo: estimateValue,
      priority: priority,
      month01: month01,
      month02: month02,
      month03: month03,
      month04: month04,
      month05: month05,
      month06: month06,
      month07: month07,
      month08: month08,
      month09: month09,
      month10: month10,
      month11: month11,
      month12: month12,
      user: userState.id,
      month_list: JSON.stringify({
        month01: month01,
        month02: month02,
        month03: month03,
        month04: month04,
        month05: month05,
        month06: month06,
        month07: month07,
        month08: month08,
        month09: month09,
        month10: month10,
        month11: month11,
        month12: month12,
      }),
      bucode: mainlawId,
      count: 2,
      editflag: editDetailFlag,
      lawGroupSubId: lawGroupSub,
      brcsDate: brcsDate
    };

    if ((code || buCode) && (model.estimateNo == "" || model.priority == "")) {
      Swal.fire({
        icon: "error",
        title: "กรุณากรอกข้อมูลให้ครบ !",
      });
      return false;
    }

    // console.log("monthCounth.length", monthCounth.length);
    // console.log("estimate[0].value", estimate[0].value);
    // console.log("parseInt(estimateNo)", parseInt(estimate[0].value));
    // console.log("estimateValue", estimateValue);
    if (monthCounth.length === estimateValue) {
      if (model.id === 0) {
        saveDataHowTo(model);
        setModal(false);
        seteditDetailFlag(false);
        newDetails();
      } else {
        saveDataHowTo(model);
        setModal(false);
      }
    } else {
      Swal.fire({
        icon: "error",
        title: "กรุณาเลือกจำนวนเดือนให้ตรงกับความถี่ !",
      });
      return false;
    }
  };

  const deleteDetails = (record) => {
    toggleConfirm();
    setDataSelected(record);
    setId(record.id);
  };

  const onDelete = (e) => {
    e.preventDefault();
    const model = {
      id: id,
    };
    deleteDataHowTo(model);
    toggleConfirm();
  };

  // const saveData = async (data) => {
  //   try {
  //       const result = await Api.createMainLawSub(data);
  //       if (result.status === 200) {
  //           const { data } = result.data;
  //           toggle();
  //           window.location.href = "/home/lawinfo/search";
  //       }
  //   } catch (error) {

  //   }
  // }

  // const handleChange = (e) => {
  //     setRecordStatus(e.target.value);
  // }

  function generateNewBtn() {
    if(isView != true){
      if (userState.role == "FQO" && !code) {
        return (
          <CRow className="mt-3">
            <CCol xs="12" lg="12">
              <CButton variant="outline" onClick={newDetails} color={"info"}>
                <CIcon name="cil-plus" />
                <span className="ml-2">สร้างสาระสำคัญ</span>
              </CButton>
            </CCol>
          </CRow>
        );
      }
    }
  }

  function generateTable(){
    return (
      <CDataTable
      items={mainLawsSubDatas}
      fields={(userState.role == "FQO" && !code) ? fieldsFqo : fieldsOther}
      hover
      sorter
      striped
      bordered
      pagination
      scopedSlots={{
        jan: (item) => {
          return (
            <td>
              {item.month01 == true && (
                <div>
                  <CIcon
                    name="cil-check-alt"
                    className={
                      userState.role == "USER" ? "cursor-pointer" : ""
                    }
                    onClick={() =>
                      userState.role == "USER"
                        ? modalDetailOpen(true)
                        : false
                    }
                  />
                  {userState.role == "USER" ? (
                    <CIcon
                      onClick={() => modalCauseOpen(true)}
                      className="ml-1 cursor-pointer"
                      name="cil-chevron-circle-down-alt"
                    />
                  ) : (
                    ""
                  )}
                </div>
              )}
            </td>
          );
        },
        feb: (item) => {
          return (
            <td>
              {item.month02 == true && (
                <div>
                  <CIcon
                    name="cil-check-alt"
                    className={
                      userState.role == "USER" ? "cursor-pointer" : ""
                    }
                    onClick={() =>
                      userState.role == "USER"
                        ? modalDetailOpen(true)
                        : false
                    }
                  />
                  {userState.role == "USER" ? (
                    <CIcon
                      onClick={() => modalCauseOpen(true)}
                      className="ml-1 cursor-pointer"
                      name="cil-chevron-circle-down-alt"
                    />
                  ) : (
                    ""
                  )}
                </div>
              )}
            </td>
          );
        },
        mar: (item) => {
          return (
            <td>
              {item.month03 == true && (
                <div>
                  <CIcon
                    name="cil-check-alt"
                    className={
                      userState.role == "USER" ? "cursor-pointer" : ""
                    }
                    onClick={() =>
                      userState.role == "USER"
                        ? modalDetailOpen(true)
                        : false
                    }
                  />
                  {userState.role == "USER" ? (
                    <CIcon
                      onClick={() => modalCauseOpen(true)}
                      className="ml-1 cursor-pointer"
                      name="cil-chevron-circle-down-alt"
                    />
                  ) : (
                    ""
                  )}
                </div>
              )}
            </td>
          );
        },
        apr: (item) => {
          return (
            <td>
              {item.month04 == true && (
                <div>
                  <CIcon
                    name="cil-check-alt"
                    className={
                      userState.role == "USER" ? "cursor-pointer" : ""
                    }
                    onClick={() =>
                      userState.role == "USER"
                        ? modalDetailOpen(true)
                        : false
                    }
                  />
                  {userState.role == "USER" ? (
                    <CIcon
                      onClick={() => modalCauseOpen(true)}
                      className="ml-1 cursor-pointer"
                      name="cil-chevron-circle-down-alt"
                    />
                  ) : (
                    ""
                  )}
                </div>
              )}
            </td>
          );
        },
        may: (item) => {
          return (
            <td>
              {item.month05 == true && (
                <div>
                  <CIcon
                    name="cil-check-alt"
                    className={
                      userState.role == "USER" ? "cursor-pointer" : ""
                    }
                    onClick={() =>
                      userState.role == "USER"
                        ? modalDetailOpen(true)
                        : false
                    }
                  />
                  {userState.role == "USER" ? (
                    <CIcon
                      onClick={() => modalCauseOpen(true)}
                      className="ml-1 cursor-pointer"
                      name="cil-chevron-circle-down-alt"
                    />
                  ) : (
                    ""
                  )}
                </div>
              )}
            </td>
          );
        },
        jun: (item) => {
          return (
            <td>
              {item.month06 == true && (
                <div>
                  <CIcon
                    name="cil-check-alt"
                    className={
                      userState.role == "USER" ? "cursor-pointer" : ""
                    }
                    onClick={() =>
                      userState.role == "USER"
                        ? modalDetailOpen(true)
                        : false
                    }
                  />
                  {userState.role == "USER" ? (
                    <CIcon
                      onClick={() => modalCauseOpen(true)}
                      className="ml-1 cursor-pointer"
                      name="cil-chevron-circle-down-alt"
                    />
                  ) : (
                    ""
                  )}
                </div>
              )}
            </td>
          );
        },
        jul: (item) => {
          return (
            <td>
              {item.month07 == true && (
                <div>
                  <CIcon
                    name="cil-check-alt"
                    className={
                      userState.role == "USER" ? "cursor-pointer" : ""
                    }
                    onClick={() =>
                      userState.role == "USER"
                        ? modalDetailOpen(true)
                        : false
                    }
                  />
                  {userState.role == "USER" ? (
                    <CIcon
                      onClick={() => modalCauseOpen(true)}
                      className="ml-1 cursor-pointer"
                      name="cil-chevron-circle-down-alt"
                    />
                  ) : (
                    ""
                  )}
                </div>
              )}
            </td>
          );
        },
        aug: (item) => {
          return (
            <td>
              {item.month08 == true && (
                <div>
                  <CIcon
                    name="cil-check-alt"
                    className={
                      userState.role == "USER" ? "cursor-pointer" : ""
                    }
                    onClick={() =>
                      userState.role == "USER"
                        ? modalDetailOpen(true)
                        : false
                    }
                  />
                  {userState.role == "USER" ? (
                    <CIcon
                      onClick={() => modalCauseOpen(true)}
                      className="ml-1 cursor-pointer"
                      name="cil-chevron-circle-down-alt"
                    />
                  ) : (
                    ""
                  )}
                </div>
              )}
            </td>
          );
        },
        sep: (item) => {
          return (
            <td>
              {item.month09 == true && (
                <div>
                  <CIcon
                    name="cil-check-alt"
                    className={
                      userState.role == "USER" ? "cursor-pointer" : ""
                    }
                    onClick={() =>
                      userState.role == "USER"
                        ? modalDetailOpen(true)
                        : false
                    }
                  />
                  {userState.role == "USER" ? (
                    <CIcon
                      onClick={() => modalCauseOpen(true)}
                      className="ml-1 cursor-pointer"
                      name="cil-chevron-circle-down-alt"
                    />
                  ) : (
                    ""
                  )}
                </div>
              )}
            </td>
          );
        },
        oct: (item) => {
          return (
            <td>
              {item.month10 == true && (
                <div>
                  <CIcon
                    name="cil-check-alt"
                    className={
                      userState.role == "USER" ? "cursor-pointer" : ""
                    }
                    onClick={() =>
                      userState.role == "USER"
                        ? modalDetailOpen(true)
                        : false
                    }
                  />
                  {userState.role == "USER" ? (
                    <CIcon
                      onClick={() => modalCauseOpen(true)}
                      className="ml-1 cursor-pointer"
                      name="cil-chevron-circle-down-alt"
                    />
                  ) : (
                    ""
                  )}
                </div>
              )}
            </td>
          );
        },
        nov: (item) => {
          return (
            <td>
              {item.month11 == true && (
                <div>
                  <CIcon
                    name="cil-check-alt"
                    className={
                      userState.role == "USER" ? "cursor-pointer" : ""
                    }
                    onClick={() =>
                      userState.role == "USER"
                        ? modalDetailOpen(true)
                        : false
                    }
                  />
                  {userState.role == "USER" ? (
                    <CIcon
                      onClick={() => modalCauseOpen(true)}
                      className="ml-1 cursor-pointer"
                      name="cil-chevron-circle-down-alt"
                    />
                  ) : (
                    ""
                  )}
                </div>
              )}
            </td>
          );
        },
        dec: (item) => {
          return (
            <td>
              {item.month12 == true && (
                <div>
                  <CIcon
                    name="cil-check-alt"
                    className={
                      userState.role == "USER" ? "cursor-pointer" : ""
                    }
                    onClick={() =>
                      userState.role == "USER"
                        ? modalDetailOpen(true)
                        : false
                    }
                  />
                  {userState.role == "USER" ? (
                    <CIcon
                      onClick={() => modalCauseOpen(true)}
                      className="ml-1 cursor-pointer"
                      name="cil-chevron-circle-down-alt"
                    />
                  ) : (
                    ""
                  )}
                </div>
              )}
            </td>
          );
        },
        option: (item) => {
          return (
            <td className="center">
              {isView === true ? (
                <div></div>
              ) : (
                <CButtonGroup>
                  <CButton
                    color="info"
                    variant="outline"
                    shape="square"
                    size="sm"
                    onClick={() => {
                      editDetails(item);
                    }}
                  >
                    <CIcon name="cilPen" />
                  </CButton>
                  <CButton variant="ghost" size="sm" />
                  {!code && userState.role=="FQO" ? (
                    <CButton
                      color="danger"
                      variant="outline"
                      shape="square"
                      size="sm"
                      onClick={() => {
                        deleteDetails(item);
                      }}
                    >
                      <CIcon name="cilTrash" />
                    </CButton>
                  ) : (
                    <div></div>
                  )}
                </CButtonGroup>
              )}
            </td>
          );
        },
        description: (item) => {
          return (
            <td>
              <div
                className={
                  isView == true && userState.role === "BU"
                    ? "cursor-pointer"
                    : ""
                }
                onClick={() => {
                  if(isView && userState.role === "BU"){
                    toggleDetails(item.id);
                  }
              
                }}
              >
                {item.description}
              </div>
            </td>
          );
        },
        details: (item) => {
          let seq = 0;
          return (
            <CCollapse show={toggles.includes(item.id)}>
              <CRow className="mt-3">
                <CCol xs="12" lg="12">
                  <CCard>
                    <CCardBody>
                      <CDataTable
                        className="font-weight-bold"
                        items={item.assessments}
                        fields={fieldsAssignment}
                        itemsPerPage={5}
                        header={true}
                        bordered
                        hover={false}
                        scopedSlots={{
                          seq: (item) => {
                            seq++;
                            return (
                              <td className="text-center">{seq}</td>
                            );
                          },
                          fullname: (item) => {
                            return (
                              <td>
                                <div>{item.fullname}</div>
                              </td>
                            );
                          },
                          jan: (item) => {
                            return item.valMonth == "01" ? (
                              <td><CBadge color={getBadge(item.systemStatus)}>P</CBadge></td>
                            ) : (
                              <td></td>
                            );
                          },
                          feb: (item) => {
                            return item.valMonth == "02" ? (
                              <td><CBadge color={getBadge(item.systemStatus)}>P</CBadge></td>
                            ) : (
                              <td></td>
                            );
                          },
                          mar: (item) => {
                            return item.valMonth == "03" ? (
                              <td><CBadge color={getBadge(item.systemStatus)}>P</CBadge></td>
                            ) : (
                              <td></td>
                            );
                          },
                          apr: (item) => {
                            return item.valMonth == "04" ? (
                              <td><CBadge color={getBadge(item.systemStatus)}>P</CBadge></td>
                            ) : (
                              <td></td>
                            );
                          },
                          may: (item) => {
                            return item.valMonth == "05" ? (
                              <td><CBadge color={getBadge(item.systemStatus)}>P</CBadge></td>
                            ) : (
                              <td></td>
                            );
                          },
                          jun: (item) => {
                            return item.valMonth == "06" ? (
                              <td><CBadge color={getBadge(item.systemStatus)}>P</CBadge></td>
                            ) : (
                              <td></td>
                            );
                          },
                          jul: (item) => {
                            return item.valMonth == "07" ? (
                              <td><CBadge color={getBadge(item.systemStatus)}>P</CBadge></td>
                            ) : (
                              <td></td>
                            );
                          },
                          aug: (item) => {
                            return item.valMonth == "08" ? (
                              <td><CBadge color={getBadge(item.systemStatus)}>P</CBadge></td>
                            ) : (
                              <td></td>
                            );
                          },
                          sep: (item) => {
                            return item.valMonth == "09" ? (
                              <td><CBadge color={getBadge(item.systemStatus)}>P</CBadge></td>
                            ) : (
                              <td></td>
                            );
                          },
                          oct: (item) => {
                            return item.valMonth == "10" ? (
                              <td><CBadge color={getBadge(item.systemStatus)}>P</CBadge></td>
                            ) : (
                              <td></td>
                            );
                          },
                          nov: (item) => {
                            return item.valMonth == "11" ? (
                              <td><CBadge color={getBadge(item.systemStatus)}>P</CBadge></td>
                            ) : (
                              <td></td>
                            );
                          },
                          dec: (item) => {
                            return item.valMonth == "12" ? (
                              <td><CBadge color={getBadge(item.systemStatus)}>P</CBadge></td>
                            ) : (
                              <td></td>
                            );
                          },
                        }}
                      />
                    </CCardBody>
                    <CCardFooter>
                      <CRow>
                        <label>สถานะเอกสาร</label>
                        <CCol sm="2">
                          <CBadge color="warning">P</CBadge>
                          <label class="ml-2">Send To Evaluate</label>
                        </CCol>
                        <CCol sm="2">
                          <CBadge color="primary">P</CBadge>
                          <label class="ml-2">Send To Verification</label>
                        </CCol>
                        <CCol sm="2">
                          <CBadge color="info">P</CBadge>
                          <label class="ml-2">Send To Approver</label>
                        </CCol>
                        <CCol sm="2">
                          <CBadge color="success">P</CBadge>
                          <label class="ml-2">Approve</label>
                        </CCol>
                        <CCol sm="2">
                          <CBadge color="danger">P</CBadge>
                          <label class="ml-2">Reject To UserDivision</label>
                        </CCol>
                      </CRow>
                    </CCardFooter>
                  </CCard>
                </CCol>
              </CRow>
            </CCollapse>
          );
        },
      }}
    />
    );
  }
  return (
    <>
      {generateNewBtn()}
      <CRow className="mt-3">
        <CCol xs="12" lg="12">
          <CCard>
            <CCardBody>
              {generateTable()}
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>

      <CModal show={modalConfirm} onClose={setModalConfirm} color="danger">
        <CModalHeader closeButton>
          <CModalTitle> Delete Data</CModalTitle>
        </CModalHeader>
        <CModalBody>Are you confirm to delete?</CModalBody>
        <CModalFooter>
          <CButton onClick={onDelete} color={"primary"} variant={"outline"}>
            Confirm
          </CButton>{" "}
          <CButton variant="outline" color="secondary" onClick={() => setModalConfirm(false)}>
            Close
          </CButton>
        </CModalFooter>
      </CModal>

      <CModal show={modal} onClose={toggle} size="lg">
        <CModalHeader closeButton>
          <CModalTitle>รายละเอียดประเภทกฏหมาย</CModalTitle>
        </CModalHeader>
        <CForm onSubmit={onSubmit} action="">
          <CModalBody>
            {/* <CInput type="hidden" value={id} id="id"/> */}
            <CRow>
              <CCol xs="12" sm="12">
                <CRow>
                  <CCol xs="4">
                    <CFormGroup>
                      <CLabel>ลำดับที่</CLabel>
                      <CInput
                        // readOnly={
                        //   id !== null && editDetailFlag === true && true
                        // }
                        // readOnly={userState.role != "FQO"}
                        readOnly
                        // onChange={(e) => setSeq(e.target.value)}
                        value={seq}
                        // name="seq"
                        // id="seq"
                        // required
                        // type="number"
                        // pattern="[0-9]*"
                        // inputMode="numeric"
                        // min="1"
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel>สาระสำคัญกฎหมาย</CLabel>
                      <CTextarea
                        readOnly={userState.role != "FQO"}
                        rows="7"
                        onChange={(e) => setDescription1(e.target.value)}
                        value={description1}
                        name="description1"
                        id="description1"
                        required
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>
                <CRow hidden={!code && !buCode}>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel>กลุ่มของกฎหมายย่อย</CLabel>
                      {/* <CInput value={lawGroupSub}
                      readOnly/> */}
                      <CSelect
                        custom
                        name="lawGroupSub"
                        id="lawGroupSub"
                        value={lawGroupSub}
                        // disabled={id !== null}
                        onChange={(e) => setLawGroupSub(e.target.value)}
                        // required
                      >
                        <option value="">กรุณาเลือก</option>
                        {lawGroupSubData.map((item, index) => {
                          return (
                            <option key={item.id} value={item.id}>
                              {item.name}
                            </option>
                          );
                        })}
                      </CSelect>
                    </CFormGroup>
                  </CCol>
                </CRow>
                <CRow hidden={!code && !buCode}>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel>วันบังคับใช้กฎหมาย</CLabel>
                      {/* <CInput value={brcsDate}
                      readOnly/> */}
                      <CInput
                        // disabled={id != null}
                        type="date"
                        name="brcsDate"
                        value={brcsDate}
                        onChange={(e) => {
                          setBrcsDate(e.target.value);
                        }}
                        // required
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel>งานที่เกี่ยวข้อง</CLabel>
                      <CTextarea
                        readOnly={userState.role != "BU" && userState.role != "FQO"}
                        rows="7"
                        onChange={(e) => setDescription2(e.target.value)}
                        value={description2}
                        name="description2"
                        id="description2"
                        required
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>
                <CRow hidden={!code && !buCode}>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel>ลำดับความสำคัญ</CLabel>
                      <CSelect
                        disabled={
                          userState.role != "FQO"
                        }
                        custom
                        name="priority"
                        id="priority"
                        value={priority}
                        onChange={(e) => setPriority(e.target.value)}
                        required={userState.role == "BU"}
                      >
                        <option value="">กรุณาเลือก</option>
                        {priorityData.map((item, index) => {
                          return (
                            <option key={item.id} value={item.id}>
                              {item.priorityName}
                            </option>
                          );
                        })}
                      </CSelect>
                    </CFormGroup>
                  </CCol>
                </CRow>
                <CRow hidden={!code && !buCode}>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel>จำนวนครั้งที่ประเมิน</CLabel>
                      <Select
                        className="basic-single"
                        classNamePrefix="select"
                        options={options}
                        value={estimate}
                        required={userState.role == "BU"}
                        // defaultValue={destimate[0]}
                        // onChange={(e) => setEstimate(e.value)}
                        onChange={(e) =>
                          setEstimate(
                            options.filter((d) => d.value === e.value)
                          )
                        }
                      />

                      {/* <CSelect
                        disabled={
                          userState.role != "BU" && userState.role != "FQO"
                        }
                        custom
                        selected={"2"}
                        name="estimate"
                        id="estimate"
                        value={estimate}
                        onChange={(e) => setEstimate(e.target.value)}
                        require
                      >
                        <option value="">กรุณาเลือก</option>
                        <option value={"0"}>ไม่กำหนด</option>
                        <option value={"12"}>ทุกเดือน</option>
                        <option value={"2"} selected="selected">
                          2 รอบ/ปี
                        </option>
                        <option value={"3"}>3 รอบ/ปี</option>
                        <option value={"4"}>4 รอบ/ปี</option>
                      </CSelect> */}
                    </CFormGroup>
                  </CCol>
                </CRow>
                <CRow hidden={!code && !buCode}>
                  <CCol xs="12">
                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="chkAll"
                        name="chkAll"
                        value=""
                        checked={allMonth}
                        onClick={() => {
                          allMonthCheck();
                          setflagClear(!flagClear);
                        }}
                      />
                      <CLabel variant="custom-checkbox" htmlFor="chkAll">
                        เลือกทั้งหมด
                      </CLabel>
                    </CFormGroup>
                    <CButton
                      color="danger"
                      variant="outline"
                      size="sm"
                      onClick={() => clearMonth()}
                    >
                      ยกเลิกการเลือก
                    </CButton>
                  </CCol>
                </CRow>
                <CRow hidden={!code && !buCode}>
                  <CCol xs="12">
                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="month01"
                        name="month01"
                        checked={month01}
                        onChange={(e) => setMonth01(e.target.checked)}
                        disabled={
                          userState.role != "BU" && userState.role != "FQO"
                        }
                      />
                      <CLabel variant="custom-checkbox" htmlFor="month01">
                        ม.ค.
                      </CLabel>
                    </CFormGroup>
                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="month02"
                        name="month02"
                        checked={month02}
                        onChange={(e) => setMonth02(e.target.checked)}
                        disabled={
                          userState.role != "BU" && userState.role != "FQO"
                        }
                      />
                      <CLabel variant="custom-checkbox" htmlFor="month02">
                        ก.พ.
                      </CLabel>
                    </CFormGroup>

                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="month03"
                        name="month03"
                        checked={month03}
                        onChange={(e) => setMonth03(e.target.checked)}
                        disabled={
                          userState.role != "BU" && userState.role != "FQO"
                        }
                      />
                      <CLabel variant="custom-checkbox" htmlFor="month03">
                        มี.ค.
                      </CLabel>
                    </CFormGroup>
                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="month04"
                        name="month04"
                        checked={month04}
                        onChange={(e) => setMonth04(e.target.checked)}
                        disabled={
                          userState.role != "BU" && userState.role != "FQO"
                        }
                      />
                      <CLabel variant="custom-checkbox" htmlFor="month04">
                        เม.ย.
                      </CLabel>
                    </CFormGroup>
                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="month05"
                        name="month05"
                        checked={month05}
                        onChange={(e) => setMonth05(e.target.checked)}
                        disabled={
                          userState.role != "BU" && userState.role != "FQO"
                        }
                      />
                      <CLabel variant="custom-checkbox" htmlFor="month05">
                        พ.ค.
                      </CLabel>
                    </CFormGroup>
                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="month06"
                        name="month06"
                        checked={month06}
                        onChange={(e) => setMonth06(e.target.checked)}
                        disabled={
                          userState.role != "BU" && userState.role != "FQO"
                        }
                      />
                      <CLabel variant="custom-checkbox" htmlFor="month06">
                        มิ.ย.
                      </CLabel>
                    </CFormGroup>
                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="month07"
                        name="month07"
                        checked={month07}
                        onChange={(e) => setMonth07(e.target.checked)}
                        disabled={
                          userState.role != "BU" && userState.role != "FQO"
                        }
                      />
                      <CLabel variant="custom-checkbox" htmlFor="month07">
                        ก.ค.
                      </CLabel>
                    </CFormGroup>
                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="month08"
                        name="month08"
                        checked={month08}
                        onChange={(e) => setMonth08(e.target.checked)}
                        disabled={
                          userState.role != "BU" && userState.role != "FQO"
                        }
                      />
                      <CLabel variant="custom-checkbox" htmlFor="month08">
                        ส.ค.
                      </CLabel>
                    </CFormGroup>
                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="month09"
                        name="month09"
                        checked={month09}
                        onChange={(e) => setMonth09(e.target.checked)}
                        disabled={
                          userState.role != "BU" && userState.role != "FQO"
                        }
                      />
                      <CLabel variant="custom-checkbox" htmlFor="month09">
                        ก.ย.
                      </CLabel>
                    </CFormGroup>
                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="month10"
                        name="month10"
                        checked={month10}
                        onChange={(e) => setMonth10(e.target.checked)}
                        disabled={
                          userState.role != "BU" && userState.role != "FQO"
                        }
                      />
                      <CLabel variant="custom-checkbox" htmlFor="month10">
                        ต.ค.
                      </CLabel>
                    </CFormGroup>
                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="month11"
                        name="month11"
                        checked={month11}
                        onChange={(e) => setMonth11(e.target.checked)}
                        disabled={
                          userState.role != "BU" && userState.role != "FQO"
                        }
                      />
                      <CLabel variant="custom-checkbox" htmlFor="month11">
                        พ.ย.
                      </CLabel>
                    </CFormGroup>
                    <CFormGroup variant="custom-checkbox" inline>
                      <CInputCheckbox
                        custom
                        id="month12"
                        name="month12"
                        checked={month12}
                        onChange={(e) => setMonth12(e.target.checked)}
                        disabled={
                          userState.role != "BU" && userState.role != "FQO"
                        }
                      />
                      <CLabel variant="custom-checkbox" htmlFor="month12">
                        ธ.ค.
                      </CLabel>
                    </CFormGroup>
                  </CCol>
                </CRow>
              </CCol>
            </CRow>
          </CModalBody>
          <CModalFooter>
            <CButton variant="outline" type="submit" color="primary">
              ตกลง
            </CButton>
            <CButton variant="outline" color="secondary" onClick={toggle}>
              ยกเลิก
            </CButton>
          </CModalFooter>
        </CForm>
      </CModal>
    </>
  );
};

export default HowTo;
