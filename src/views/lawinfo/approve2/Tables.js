import React, { useState, useEffect } from 'react'
import {
    CCardBody,
    CBadge,
    CButton,
    CCollapse,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CCol,
    CCard,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CSelect,
    CForm,
    CModalTitle,
    CButtonGroup
} from '@coreui/react'

const getStatus = status => {
    switch (status) {
        case "SADR": return 'Save';
        case "NEDO": return 'Submit';
        case "CACE": return 'Delete';
        case 'SEEV': return 'Send To Evaluate'
        case 'REFQ': return 'Reject To FQO'
        case 'EVAU': return 'Send To Approver 1'
        case 'REBU': return 'Reject To BU'
        case 'SEA2': return 'Send To Approver 2'
        case 'REDI': return 'Reject To User Division'
        case 'DOAP': return 'Document Approve'
        case 'REA1': return 'Reject To Approve 1'
        default: return ''
    }
}

const fields = [
    { key: 'docno', label: 'เลขที่เอกสาร' },
    { key: 'revisionNo', label: 'ครั้งที่แก้ไข' },
    { key: 'systemStatus', label: 'สถานะ'},
    { key: 'createDate', label: 'วันที่สร้างเอกสาร'},
    { key: 'createBy', label: 'ผู้สร้างเอกสาร' },
    { key: 'updateDate', label: 'วันที่แก้ไข' },
    { key: 'updateBy', label: 'ผู้แก้ไข' },
    { key: 'option', label: '', _style: { width: '7%' }, filter: false, },
]


const Tables = ({ data = [], refreshData = () => { } }) => {

    return (
        <>
            {/* <CRow>
                <CCol xs="12" lg="12" >
                    <CButton variant="outline" onclick={newDetails} color={'success'}><CIcon name="cil-plus" /><span className="ml-2">สร้างประเภทกฏหมาย</span></CButton>
                </CCol>
            </CRow> */}
            <CRow className="">
                <CCol xs="12" lg="12" >
                    <CDataTable
                        items={data}
                        fields={fields}
                        // tableFilter={{ label: "ค้นหา", placeholder: 'พิมพ์คำที่ต้องการค้นหา' }}
                        // cleaner
                        itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
                        itemsPerPage={10}
                        hover
                        sorter
                        striped
                        bordered
                        pagination
                        scopedSlots={{
                            'systemStatus': (item) => (
                                <td>
                                    <CBadge>
                                        {getStatus(item.systemStatus)}
                                    </CBadge>
                                </td>
                            ),
                            'option': (item) => (
                                <td className="center">
                                    <CButtonGroup>
                                        <CButton
                                            color="success"
                                            variant="outline"
                                            // shape="square"
                                            size="sm"
                                            onClick={() => (window.location.href = `/home/lawinfo/approve2/create?id=${item.id}`)}
                                        >
                                            {/* <CIcon name="cilPen" /> */}
                                            รายละเอียด
                                        </CButton>
                                        {/* <CButton variant="ghost" size="sm" /> */}
                                        {/* <CButton
                                            color="danger"
                                            variant="outline"
                                            shape="square"
                                            size="sm"
                                            // onClick={() => { deleteDetails(item) }}
                                        >
                                            <CIcon name="cilTrash" />
                                        </CButton> */}
                                    </CButtonGroup>
                                </td>
                            ),
                        }}
                    />
                </CCol>
            </CRow>
        </>
    )
}

export default Tables
