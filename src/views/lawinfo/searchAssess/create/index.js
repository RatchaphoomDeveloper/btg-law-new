import React, { useEffect, useRef, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CForm,
  CContainer,
  CLabel,
  CSelect,
  CInput,
  CTabs,
  CTextarea,
} from "@coreui/react";
import Api from '../../../../api/ApiLawInfo';
// import MainLaw from "../../component/mainlawTab";
// import HowTo from "../../component/howtoTab";
import Estimate from "../../component/estimateTab";
import TableFiles from "../../component/TableFilesAssessment";
import EmployeeModal from "../../component/employeeModal";
import FilePreview from "./../../../../assets/ตัวอย่าง ประเมินความสอดคล้อง (FM-SE-49).pdf";
import DetailModal from "../../component/detailModal";
import CauseModal from '../../component/causeModal';
import "./index.scss";
import CreatHeadPanel from "../../component/headpanelAssess";
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2/src/sweetalert2.js";

const ViewAssess = (props) => {
  const userState = useSelector((state) => state.changeState.user);
  var id = new URLSearchParams(props.location.search).get("id");
  var code = new URLSearchParams(props.location.search).get("code");
  var assessId = new URLSearchParams(props.location.search).get("assessId");
  const [tabAddress, setTabAddress] = useState("1");
  const [lawid, setLawId] = useState(0);
  const [mainlawid, setMainLawId] = useState();
  const [modal, setModal] = useState(false);
  const [modalDetail, setModalDetail] = useState(false);
  const [modalCause, setModalCause] = useState(false);
  const [modelSaveHeader, setModalSaveHeader] = useState([]);
  const [isHideCancelRemark, setHideCancelRemark] = useState(true);

  const [mainLawName, setMainLawName] = useState([]);
  const [lawDescription, setLawDescription] = useState([]);
  const [lawGroupSub, setLawGroupSub] = useState("");
  const [brcsDate, setBrcsDate] = useState("");
  const [estimate, setEstimate] = useState([]);
  const [description, setDescription] = useState([]);
  const [rejectReason, setRejectReason] = useState([]);

  const [fileCount, setFileCount] = useState(0);

  const [logTable, setLogTable] = useState([]);
  const [statusData, setStatusData] = useState([]);
  const statusOption = [
    { value: "SADR", label: "Save" },
    { value: "NEDO", label: "Submit" },
    { value: "CACE", label: "Delete" },
    { value: "SEEV", label: "Send To Evaluate" },
    { value: "REFQ", label: "Reject To FQO" },
    { value: 'SEA1', label: 'Send To Verification'},
    // { value: "REBU", label: "Reject To BU" },
    { value: "SEA2", label: "Send To Approver" },
    { value: "REDI", label: "Reject To User Division" },
    { value: "DOAP", label: "Document Approve" },
    // { value: "REA1", label: "Reject To Approver 1" },
  ];

  useEffect(() => {
    setStatusData(statusOption);
    setLawId(id);
    getAssess();
    return () => {};
  }, []);

  const handleMainLawId = (idx) => {
    setMainLawId(idx);
  };

  const handleTabAddress = (idx) => {
    setTabAddress(idx);
  };
  
  const saveDataMainLaw = async (data) => {
    try {
        const result = await Api.createMainLaw(data);
        if (result.status === 200) {
            const { data } = result.data;
            window.location.href = "/home/lawinfo/searchAssess/search";
        }
    } catch (error) {

    }
  }

  const saveDataHowTo = async (data) => {
    try {
        const result = await Api.createMainLawSub(data);
        if (result.status === 200) {
            const { data } = result.data;
            window.location.href = "/home/lawinfo/searchAssess/search";
        }
    } catch (error) {

    }
  }
  
  const validateDataHeader = async (data) => {
    // try {
    //     if (data.SystemStatus == "CACE") {
    //       setHideCancelRemark(true);
    //       // setModal(true);
    //       setModalSaveHeader(data);
    //     }else{
    //       saveDataHeader(data);
    //     }
    // } catch (error) {

    // }
  }
  
  const saveDataHeader = async (data) => {
    // try {
    //     const result = await Api.create(data);
    //     if (result.status === 200) {
    //         const { data } = result.data;
    //         window.location.href = "/home/lawinfo/searchOnly/search";
    //     }
    // } catch (error) {

    // }
  }

  const getAssess = async () => {
    try {
      const model = {
        assessId: assessId,
        user: userState.id,
      };
      const result = await Api.getAssessmentDetail(model);
      if (result.status == 200) {
        var res = result.data[0];
        setMainLawName(res.mainLawName);
        setLawDescription(res.description);
        setEstimate(res.estimateResult);
        setDescription(res.estimateDesc);
        setRejectReason(res.rejectReason);
        setLogTable(res.log);
        setBrcsDate(res.brcsDate);
        setLawGroupSub(res.lawGroupSub);
        // setSystemStatus(res.systemStatus);
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <h2>สร้างข้อมูลกฏหมาย</h2>
      <CCard fluid className="mt-3">
        <CCardHeader>
          <CRow>
            <CCol sm="12">
              ข้อมูลกฏหมาย
            </CCol>
          </CRow>
          <div style={{ fontSize: "12px" }}>
            <CreatHeadPanel id={id} assessId={assessId} validateDataHeader={validateDataHeader} code={code} page={"searchAssess"}/>
          </div>
        </CCardHeader>
        <CCardBody>
          <CTabs
            activeTab={tabAddress}
            onActiveTabChange={(idx) => handleTabAddress(idx)}
          >
            <CNav variant="tabs">
              <CNavItem>
                <CNavLink data-tab="1">
                  <b>บันทึกผลการประเมิน</b>
                </CNavLink>
              </CNavItem>
              {/* <CNavItem>
                <CNavLink data-tab="1">
                  <b>กฎหมายหลัก</b>
                </CNavLink>
              </CNavItem> */}
              {/* <CNavItem>
                <CNavLink data-tab="2" disabled>
                  <b>วิธีการดำเนินงาน/แผนการดำเนินงาน</b>
                </CNavLink>
              </CNavItem> */}
              <CNavItem>
                <CNavLink data-tab="2">
                  <b>แนบไฟล์ ({fileCount})</b>
                </CNavLink>
              </CNavItem>
            </CNav>
            <CTabContent>
              <CTabPane data-tab="1">
                <Estimate
                  assessId={assessId}
                  // onSubmitEstimate={onSubmitEstimate}
                  page={"searchAssess"}
                  description={description}
                  lawGroupSub={lawGroupSub}
                  brcsDate={brcsDate}
                  estimate={estimate}
                  lawDescription={lawDescription}
                  mainLawName={mainLawName}
                  rejectReason={rejectReason}
                  logTable={logTable}
                />
              </CTabPane>
              {/* <CTabPane data-tab="1">
                <MainLaw
                  lawid={lawid}
                  handleMainLawId={handleMainLawId}
                  handleTabAddress={handleTabAddress}
                  saveDataMainLaw={saveDataMainLaw}
                />
              </CTabPane> */}
              {/* <CTabPane data-tab="2">
                <HowTo
                  modalCauseOpen={setModalCause}
                  modalDetailOpen={setModalDetail}
                  mainlawId={mainlawid}
                  saveDataHowTo={saveDataHowTo}
                  buCode={code}
                />
              </CTabPane> */}
              <CTabPane data-tab="2">
                <div>
                  <TableFiles assessId={assessId} page={"searchAssess"} setFileCount={setFileCount} />
                </div>
              </CTabPane>
            </CTabContent>
          </CTabs>
        </CCardBody>
      </CCard>
      <EmployeeModal modal={modal} setModal={setModal} />
      <DetailModal modal={modalDetail} setModal={setModalDetail} />
      <CauseModal modal={modalCause} setModal={setModalCause} />
    </>
  );
};

export default ViewAssess;
