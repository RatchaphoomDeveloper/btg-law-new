import React, { useState, useEffect } from 'react'
import {
    CCardBody,
    CBadge,
    CButton,
    CCollapse,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CCol,
    CCard,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CSelect,
    CForm,
    CModalTitle,
    CButtonGroup
} from '@coreui/react'
import { useDispatch, useSelector } from "react-redux";
import { getBrcsAssessmentDetail } from "../../../redux/getbrcaassessment/getbrcaassessmentAction";

const getEstimate = est => {
    switch (est) {
        case '1': return 'ไม่เกี่ยวข้อง'
        case '2': return 'สอดคล้อง'
        case '3': return 'ไม่สอดคล้อง'
        case '4': return 'สอดคล้อง A'
        default: return ''
    }
}


const getBadge = (status) => {
  switch (status) {
    case 'SEEV': return 'info'
    case 'REFQ': return 'dark'
    case 'SEA1': return 'warning'
    case 'SEA2': return 'danger'
    case 'REDI': return 'dark'
    case 'DOAP': return 'success'
    case 'INPR': return 'primary'
    case 'CLNE': return 'info'
    case 'CLOS': return 'warning'
    case 'CLS2': return 'danger'
    case 'RJCL': return 'dark'
  }
};

const getStatus = status => {
    switch (status) {
        case "SADR": return 'Save';
        case "NEDO": return 'Submit';
        case "CACE": return 'Delete';
        case 'SEEV': return 'Send To Evaluate'
        case 'REFQ': return 'Reject To FQO'
        case 'SEA1': return 'Send To Verification'
        case 'SEA2': return 'Send To Approver'
        case 'REDI': return 'Reject To User Division'
        case 'DOAP': return 'Document Approve'
        case 'INPR': return 'In Process'
        case 'CLNE': return 'Close New'
        case 'CLOS': return 'Complete Send To Verification'
        case 'CLS2': return 'Complete Send To Approver'
        case 'RJCL': return 'Reject Complete To User Division'
        default: return ''
    }
}

const fields = [
    // { key: 'assessmentNo', label: 'ASSESSMENT No' },
    { key: 'docno', label: 'เลขที่เอกสาร', _style: { width: '15%' } },
    // { key: 'docNoBu', label: 'เลขที่เอกสาร BU' },
    { key: 'mainLawName', label: 'ชื่อกฎหมาย', _style: { width: '25%' } },
    { key: 'description', label: 'สาระสำคัญกฎหมาย' },
    // { key: 'valYear', label: 'ปีที่ประเมิน' },
    // { key: 'valMonth', label: 'เดือนที่ประเมิน' },
    // { key: 'userLogin', label: 'ผู้ประเมิน' },
    // { key: 'email', label: 'Email' },
    // { key: 'estimateResult', label: 'ผลการประเมิน' },
    // { key: 'estimateDesc', label: 'รายละเอียดผลการประเมิน' },
    // { key: 'option', label: '', _style: { width: '7%' }, filter: false, },
]

const subfields = [
    { key: 'assessmentNo', label: 'เลขที่ประเมิน' },
    // { key: 'docno', label: 'เลขที่เอกสาร' },
    { key: 'docNoBu', label: 'เลขที่เอกสาร BU' },
    // { key: 'mainLawName', label: 'ชื่อกฎหมาย' },
    // { key: 'description', label: 'สาระสำคัญกฎหมาย' },
    { key: 'valYear', label: 'ปี' },
    { key: 'valMonth', label: 'เดือน' },
    { key: 'userLogin', label: 'ผู้ประเมิน' },
    { key: 'email', label: 'Email' },
    { key: 'estimateResult', label: 'ผลการประเมิน' },
    // { key: 'estimateDesc', label: 'รายละเอียดผลการประเมิน' },
    { key: 'status', label: 'สถานะ' },
    { key: 'option', label: '', _style: { width: '8%' }, filter: false, },
    { key: "fileCount", label: "File", _style: { width: "7%" } },
  ]


const Tables = ({ data = [], refreshData = () => {}, searchData }) => {
    const userState = useSelector((state) => state.changeState.user);
    const [details, setDetails] = useState([]);
  const [lastIndex, setLastIndex] = useState(null);
  const dispatch = useDispatch();
    const tranbuState = useSelector((state) => {
        console.log("state",state.getbrcaassessment);
        return state.getbrcaassessment.data;
    });

    useEffect(() => {
      setDetails([]);
      return () => {};
    }, [searchData]);

    const toggleDetails = (index) => {
      console.log("data", data);
      const position = data.indexOf(index);
      let newDetails = data.slice();
      if (lastIndex == index) {
        newDetails.splice(position, 1);
        setLastIndex(null);
      } else {
        newDetails = [...data, index];
        setLastIndex(index);
      }
      setDetails(newDetails);
      console.log("setDetail", details);
    };

    const getSubTable = (Docno, MainLawName, Desc) => {
      searchData.DetailDocno = Docno;
      searchData.DetailMainLaw = MainLawName;
      searchData.DetailDesc = Desc;
      try {
        dispatch(getBrcsAssessmentDetail(searchData));
      } catch (error) {
        console.log(error.message);
      }
    };

    return (
      <>
          <CRow className="">
              <CCol xs="12" lg="12" >
                  <CDataTable
                      items={data}
                      fields={fields}
                      itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
                      itemsPerPage={10}
                      hover
                      sorter
                      striped
                      bordered
                      pagination
                      scopedSlots={{
                            docno: (item, index) => (
                              <td>
                                <label
                                  style={{ cursor: "pointer", color: "blue" }}
                                  onClick={() => {
                                    toggleDetails(index);
                                    getSubTable(item.docno, item.mainLawName, item.description);
                                  }}
                                >
                                  {details.includes(index) ? item.docno : item.docno}
                                </label>
                              </td>
                            ),
                            details: (item, index) => {
                              return (
                                <CCollapse show={details.includes(index)}>
                                  <CCardBody>
                                    <CDataTable
                                      items={tranbuState}
                                      fields={subfields}
                                      hover
                                      sorter
                                      striped
                                      bordered
                                      pagination
                                      scopedSlots={{
                                        option: (items) => (
                                            <td className="center">
                                            <CButtonGroup>
                                                <CButton
                                                    color="success"
                                                    variant="outline"
                                                    // shape="square"
                                                    size="sm"
                                                    onClick={() => (window.location.href = `/home/lawinfo/searchAssess/create?id=${items.id}&code=${items.brcsBuId}&assessId=${items.assessmentId}`)}
                                                >
                                                    รายละเอียด
                                                </CButton>
                                            </CButtonGroup>
                                        </td>
                                        ),
                                        estimateResult: (item) => (
                                            <td>
                                                <CBadge>
                                                    {getEstimate(item.estimateResult)}
                                                </CBadge>
                                            </td>
                                        ),
                                        status: (items) => (
                                            <td>

                                                <CBadge color={getBadge(items.systemStatus)}>
                                                    {getStatus(items.systemStatus)}
                                                </CBadge>
                                            </td>
                                        ),
                                        fileCount: (items) => (
                                          <td>
                                            File : {items.fileCount}
                                          </td>
                                        ),
                                      }}
                                    ></CDataTable>
                                  </CCardBody>
                                </CCollapse>
                              );
                            },
                      }}
                  />
              </CCol>
          </CRow>
      </>
  )
};

  export default Tables;
