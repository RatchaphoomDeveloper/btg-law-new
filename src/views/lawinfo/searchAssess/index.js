import React, { useEffect, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CNav,
  CForm,
  CContainer,
  CLabel,
  CSelect,
  CInput,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import Api from '../../../api/ApiLawInfo';
import Tables from "./Tables";
import SearchPanel from "../component/searchpanelAssess";
import { useDispatch, useSelector } from "react-redux";

const SearchOnly = () => {
  const userState = useSelector((state) => state.changeState.user);
  const [dataTable, setDataTable] = useState([]);

  const [statusData, setStatusData] = useState([]);
  const [searchData, setSearchData] = useState([]);
  const statusOption = [
    { value: "SADR", label: "Save" },
    { value: "NEDO", label: "Submit" },
    { value: "CACE", label: "Delete" },
    { value: "SEEV", label: "Send To Evaluate" },
    { value: "REFQ", label: "Reject To FQO" },
    { value: 'SEA1', label: 'Send To Verification'},,
    { value: "SEA2", label: "Send To Approver" },
    { value: "REDI", label: "Reject To User Division" },
    { value: "DOAP", label: "Document Approve" },
    // { value: "REA1", label: "Reject To Approver 1" },
  ];

  useEffect(() => {
    setStatusData(statusOption);
    getData();
    return () => {};
  }, []);

  const getData = async (data) => {
    try {
      if (data == undefined) {
        data = {
          "page": "search",
          "userId": userState.id,
          "role": userState.role
        }
      }else{
        data.page = "search";
        data.userId = userState.id;
        data.role = userState.role
      }
      setSearchData(data);
      const result = await Api.getAssessment(data);
      if (result.status == 200) {
          setDataTable(result.data);
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <h2>ค้นหาข้อมูลกฎหมาย</h2>
      <CCard fluid className="mt-3">
        <CCardHeader>
          <div style={{ fontSize: "12px" }}>
            <SearchPanel getData={getData} statusOption={statusData}/>
          </div>
        </CCardHeader>
        <CCardBody>
          <Tables refreshData={getData} data={dataTable} searchData={searchData} />
        </CCardBody>
      </CCard>
    </>
  );
};

export default SearchOnly;
