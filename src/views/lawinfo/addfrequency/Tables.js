import React, { useState, useEffect } from "react";
import {
  CCardBody,
  CBadge,
  CButton,
  CCollapse,
  CDataTable,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CCol,
  CCard,
  CRow,
  CCardHeader,
  CFormGroup,
  CLabel,
  CInput,
  CSelect,
  CForm,
  CModalTitle,
  CButtonGroup,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { statusData } from "./data";
import { useSelector, useDispatch } from "react-redux";
import { getTranBrcsBu } from "../../../redux/gettranbrcsbu/gettranbrcsbuAction";
import { fetchgetMainLaw } from "../../../redux/mainLaw/mainLawAction";
// import { getTranBrcsBu } from "../../redux/gettranbrcsbu/gettranbrcsbuAction";
const getStatus = (status) => {
  switch (status) {
    case "SADR":
      return "Save";
    case "NEDO":
      return "Submit";
    case "CACE":
      return "Delete";
    case "SEEV":
      return "Send To Evaluate";
    case "REFQ":
      return "Reject To FQO";
    case "EVAU":
      return "Send To Approver 1";
    case "REBU":
      return "Reject To BU";
    case "SEA2":
      return "Send To Approver 2";
    case "REDI":
      return "Reject To User Division";
    case "DOAP":
      return "Document Approve";
    case "REA1":
      return "Reject To Approve 1";
    default:
      return "";
  }
};

const fields = [
  { key: "docno", label: "เลขที่เอกสาร", _style: { width: "15%" } },
  { key: "mainLawName", label: "ชื่อกฎหมาย" , _style: { width: "40%" } },
  { key: "revisionNo", label: "ครั้งที่แก้ไข" },
  { key: "systemStatus", label: "สถานะ" },
  { key: "createDate", label: "วันที่สร้างเอกสาร" },
  { key: "createBy", label: "ผู้สร้างเอกสาร" },
  { key: "updateDate", label: "วันที่แก้ไข" },
  { key: "updateBy", label: "ผู้แก้ไข" },
  { key: "option", label: "", _style: { width: "7%" }, filter: false },
];

const subfields = [
  { key: "docno", label: "เลขที่เอกสาร" },
  { key: "code", label: "โครงสร้าง" },
  { key: "revisioN_NO", label: "ครั้งที่แก้ไข" },
  { key: "systeM_STATUS", label: "สถานะ" },
  { key: "creatE_DATE", label: "วันที่สร้างเอกสาร" },
  { key: "creatE_BY", label: "ผู้สร้างเอกสาร" },
  { key: "updatE_DATE", label: "วันที่แก้ไข" },
  { key: "updatE_BY", label: "ผู้แก้ไข" },
  { key: "option", label: "", _style: { width: "7%" }, filter: false },
];
const Tables = ({ data = [], refreshData = () => {} }) => {
  const [details, setDetails] = useState([]);
  const [lastIndex, setLastIndex] = useState(null);
  const dispatch = useDispatch();
  const tranbuState = useSelector((state) => {
    console.log(state.gettranbrcsbu);
    return state.gettranbrcsbu.data;
  });
  const toggleDetails = (index) => {
    const position = data.indexOf(index);
    let newDetails = data.slice();
    if (lastIndex == index) {
      newDetails.splice(position, 1);
      setLastIndex(null);
    } else {
      newDetails = [...data, index];
      setLastIndex(index);
    }
    setDetails(newDetails);
    console.log(details);
  };

  const getSubTable = (tranData) => {
    try {
      dispatch(getTranBrcsBu({ Docno: tranData, page: "frequency" }));
    } catch (error) {
      console.log(error.message);
    }
  };


  return (
    <>
      {/* <CRow>
                <CCol xs="12" lg="12" >
                    <CButton variant="outline" onclick={newDetails} color={'success'}><CIcon name="cil-plus" /><span className="ml-2">สร้างประเภทกฏหมาย</span></CButton>
                </CCol>
            </CRow> */}
      <CRow className="">
        <CCol xs="12" lg="12">
          <CDataTable
            items={data}
            fields={fields}
            // tableFilter={{ label: "ค้นหา", placeholder: 'พิมพ์คำที่ต้องการค้นหา' }}
            // cleaner
            itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
            itemsPerPage={10}
            hover
            sorter
            striped
            bordered
            pagination
            scopedSlots={{
              docno: (item, index) => (
                <td>
                  <label
                    style={{ cursor: "pointer", color: "blue" }}
                    onClick={() => {
                      toggleDetails(index);
                      getSubTable(item.docno);
                    }}
                  >
                    {details.includes(index) ? item.docno : item.docno}
                  </label>
                </td>
              ),
              mainLawName: (item) => (
                <td>
                  {item.mainLawName[0]}
                </td>
              ),
              option: (item, index) => (
                <td>
                  <CButton
                    color="success"
                    variant="outline"
                    // shape="square"
                    size="sm"
                    onClick={() =>
                      (window.location.href = `/home/lawinfo/create?id=${item.id}`)
                    }
                  >
                    {/* <CIcon name="cilPen" /> */}
                    รายละเอียด
                  </CButton>
                </td>
              ),
              details: (item, index) => {
                console.log("details.includes(index)", details.includes(index));
                return (
                  <CCollapse show={details.includes(index)}>
                    <CCardBody>
                      <CDataTable
                        items={tranbuState}
                        fields={subfields}
                        hover
                        sorter
                        striped
                        bordered
                        pagination
                        scopedSlots={{
                          option: (items) => (
                            <td className="center">
                              <CButtonGroup>
                                <CButton
                                  color="success"
                                  variant="outline"
                                  // shape="square"
                                  size="sm"
                                  onClick={() =>
                                    (window.location.href = `/home/lawinfo/addfrequency/create?id=${item.id}`)
                                  }
                                >
                                  {/* <CIcon name="cilPen" /> */}
                                  รายละเอียด
                                </CButton>
                                {/* <CButton variant="ghost" size="sm" /> */}
                                {/* <CButton
                                                          color="danger"
                                                          variant="outline"
                                                          shape="square"
                                                          size="sm"
                                                          // onClick={() => { deleteDetails(item) }}
                                                      >
                                                          <CIcon name="cilTrash" />
                                                      </CButton> */}
                              </CButtonGroup>
                            </td>
                          ),
                          systemStatus: (items) => (
                            <td>
                              <CBadge>{getStatus(items.systeM_STATUS)}</CBadge>
                            </td>
                          ),
                        }}
                      ></CDataTable>
                      {/* {tranbuState.map((subi, i) => {
                        return (
                          <div key={i}>
                            <label>{subi.code}</label>
                          </div>
                        );
                      })} */}
                      {/* <p className="text-muted">
                          User since: {item.registered}
                        </p>
                        <CButton size="sm" color="info">
                          User Settings
                        </CButton>
                        <CButton size="sm" color="danger" className="ml-1">
                          Delete
                        </CButton> */}
                    </CCardBody>
                  </CCollapse>
                );
              },
              systemStatus: (item) => (
                <td>
                  <CBadge>{getStatus(item.systemStatus)}</CBadge>
                </td>
              ),
              // option: (item) => (
              //   <td className="center">
              //     <CButtonGroup>
              //       <CButton
              //         color="success"
              //         variant="outline"
              //         // shape="square"
              //         size="sm"
              //         onClick={() =>
              //           (window.location.href = `/home/lawinfo/create?id=${item.id}`)
              //         }
              //       >
              //         {/* <CIcon name="cilPen" /> */}
              //         รายละเอียด
              //       </CButton>
              //       {/* <CButton variant="ghost" size="sm" /> */}
              //       {/* <CButton
              //                                 color="danger"
              //                                 variant="outline"
              //                                 shape="square"
              //                                 size="sm"
              //                                 // onClick={() => { deleteDetails(item) }}
              //                             >
              //                                 <CIcon name="cilTrash" />
              //                             </CButton> */}
              //     </CButtonGroup>
              //   </td>
              // ),
            }}
          />
        </CCol>
      </CRow>
      {/* <TableToggle modal={modal} toggle={toggle} /> */}
    </>
  );
};

export default Tables;
