import React, { useState, useEffect } from "react";
import {
  CCardBody,
  CBadge,
  CButton,
  CCollapse,
  CDataTable,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CCol,
  CCard,
  CRow,
  CCardHeader,
  CFormGroup,
  CLabel,
  CInput,
  CSelect,
  CForm,
  CModalTitle,
  CButtonGroup,
} from "@coreui/react";
import { useSelector, useDispatch } from "react-redux";
import { getTranBrcsBu } from "../../../redux/gettranbrcsbu/gettranbrcsbuAction";
import { fetchgetMainLaw } from "../../../redux/mainLaw/mainLawAction";
import { fetchgetMainLawSub } from "../../../redux/mainLawSub/mainLawSubAction";

const getStatus = (status) => {
  switch (status) {
    case "SADR":
      return "Save";
    case "NEDO":
      return "Submit";
    case "CACE":
      return "Delete";
    case "SEEV":
      return "Send To Evaluate";
    case "REFQ":
      return "Reject To FQO";
    case "EVAU":
      return "Send To Approver 1";
    case "REBU":
      return "Reject To BU";
    case "SEA2":
      return "Send To Approver 2";
    case "REDI":
      return "Reject To User Division";
    case "DOAP":
      return "Document Approve";
    case "REA1":
      return "Reject To Approve 1";
    default:
      return "";
  }
};

const getBadge = (status) => {
  switch (status) {
    case "SADR":
      return "light";
    case "NEDO":
      return "secondary";
    case "CACE":
      return "dark";
    case "SEEV":
      return "info";
    case "REFQ":
      return "dark";
    case "EVAU":
      return "warning";
    case "REBU":
      return "dark";
    case "SEA2":
      return "danger";
    case "REDI":
      return "dark";
    case "DOAP":
      return "success";
    case "REA1":
      return "dark";
  }
};

const fields = [
  { key: "docno", label: "เลขที่เอกสาร", _style: { width: "15%" } },
  { key: "mainLawName", label: "ชื่อกฎหมาย", _style: { width: "40%" } },
  { key: "revisionNo", label: "ครั้งที่แก้ไข" },
  { key: "systemStatus", label: "สถานะ" },
  { key: "createDate", label: "วันที่สร้างเอกสาร" },
  { key: "createBy", label: "ผู้สร้างเอกสาร" },
  { key: "updateDate", label: "วันที่แก้ไข" },
  { key: "updateBy", label: "ผู้แก้ไข" },
];

const subfields = [
  { key: "docno", label: "เลขที่เอกสาร" },
  { key: "code", label: "โครงสร้าง" },
  { key: "revisioN_NO", label: "ครั้งที่แก้ไข" },
  { key: "systeM_STATUS", label: "สถานะ" },
  { key: "creatE_DATE", label: "วันที่สร้างเอกสาร" },
  { key: "creatE_BY", label: "ผู้สร้างเอกสาร" },
  { key: "updatE_DATE", label: "วันที่แก้ไข" },
  { key: "updatE_BY", label: "ผู้แก้ไข" },
  { key: "option", label: "", _style: { width: "7%" }, filter: false },
];

const Tables = ({ data = [], refreshData = () => {}, page, searchData }) => {
  const [details, setDetails] = useState([]);
  const [lastIndex, setLastIndex] = useState(null);
  const userState = useSelector((state) => state.changeState.user);
  const dispatch = useDispatch();
  const tranbuState = useSelector((state) => {
    console.log(state.gettranbrcsbu);
    return state.gettranbrcsbu.data;
  });
  const [selected, setSelected] = useState([]);

  const toggleDetails = (index) => {
    console.log("data", data);
    const position = data.indexOf(index);
    let newDetails = data.slice();
    if (lastIndex == index) {
      newDetails.splice(position, 1);
      setLastIndex(null);
    } else {
      newDetails = [...data, index];
      setLastIndex(index);
    }
    setDetails(newDetails);
    console.log("setDetail", details);
    // console.log(details);
  };

  const getSubTable = (tranData) => {
    try {
      if (searchData == undefined) {
        searchData = {
          refDocNo: tranData,
          page: page ?? "",
        };
      } else {
        searchData.page = page ?? "";
        searchData.refDocNo = tranData;
      }
      dispatch(getTranBrcsBu(searchData));
    } catch (error) {
      console.log(error.message);
    }
  };

  useEffect(() => {
    dispatch(fetchgetMainLaw({}));
    dispatch(fetchgetMainLawSub({}));
    return () => {};
  }, []);

  return (
    <>
      {/* <CRow>
                  <CCol xs="12" lg="12" >
                      <CButton variant="outline" onclick={newDetails} color={'success'}><CIcon name="cil-plus" /><span className="ml-2">สร้างประเภทกฏหมาย</span></CButton>
                  </CCol>
              </CRow> */}
      <CRow className="">
        <CCol xs="12" lg="12">
          <CDataTable
            items={data}
            fields={fields}
            // tableFilter={{ label: "ค้นหา", placeholder: 'พิมพ์คำที่ต้องการค้นหา' }}
            // cleaner
            itemsPerPageSelect={{
              label: "จำนวนการแสดงผล",
              values: [10, 25, 50, 100],
            }}
            itemsPerPage={10}
            hover
            sorter
            striped
            bordered
            pagination
            scopedSlots={{
              docno: (item, index) => (
                <td>
                  <label
                    style={{ cursor: "pointer", color: "blue" }}
                    onClick={() => {
                      toggleDetails(index);
                      getSubTable(item.docno);
                    }}
                  >
                    {item.docno}
                  </label>
                </td>
              ),
              mainLawName: (item) => (
                <td>
                  {item.mainLawName[0]}
                </td>
              ),
              details: (item, index) => {
                return (
                  <CCollapse show={details.includes(index)}>
                    <CCardBody>
                      <CDataTable
                        items={tranbuState}
                        fields={subfields}
                        hover
                        sorter
                        striped
                        bordered
                        pagination
                        scopedSlots={{
                          systeM_STATUS: (item) => (
                            <td>
                              <CBadge color={getBadge(item.systeM_STATUS)}>
                                {getStatus(item.systeM_STATUS)}
                              </CBadge>
                            </td>
                          ),
                          option: (items) => (
                            <td className="center">
                              <CButtonGroup>
                                <CButton
                                  color="success"
                                  variant="outline"
                                  // shape="square"
                                  size="sm"
                                  onClick={() => {
                                    window.location.href = `/home/lawinfo/searchOnly/create?id=${item.id}&code=${items.id}&buCode=${items.bU_ID}`;
                                  }}
                                >
                                  {/* <CIcon name="cilPen" /> */}
                                  รายละเอียด
                                </CButton>
                                {/* <CButton variant="ghost" size="sm" /> */}
                                {/* <CButton
                                                          color="danger"
                                                          variant="outline"
                                                          shape="square"
                                                          size="sm"
                                                          // onClick={() => { deleteDetails(item) }}
                                                      >
                                                          <CIcon name="cilTrash" />
                                                      </CButton> */}
                              </CButtonGroup>
                            </td>
                          ),
                        }}
                      ></CDataTable>
                      {/* {tranbuState.map((subi, i) => {
                        return (
                          <div key={i}>
                            <label>{subi.code}</label>
                          </div>
                        );
                      })} */}
                      {/* <p className="text-muted">
                          User since: {item.registered}
                        </p>
                        <CButton size="sm" color="info">
                          User Settings
                        </CButton>
                        <CButton size="sm" color="danger" className="ml-1">
                          Delete
                        </CButton> */}
                    </CCardBody>
                  </CCollapse>
                );
              },
              systemStatus: (item) => (
                <td>
                  <CBadge color={getBadge(item.systemStatus)}>
                    {getStatus(item.systemStatus)}
                  </CBadge>
                </td>
              ),
            }}
          />
        </CCol>
      </CRow>
      {/* <TableToggle modal={modal} toggle={toggle} /> */}
    </>
  );
};

export default Tables;
