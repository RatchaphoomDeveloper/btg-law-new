import React, { useEffect, useRef, useState } from "react";
import { useHistory } from "react-router-dom";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CForm,
  CContainer,
  CLabel,
  CSelect,
  CInput,
  CTabs,
  CTextarea,
} from "@coreui/react";
import Api from '../../../../api/ApiLawInfo';
import MainLaw from "../../component/mainlawTab";
import HowTo from "../../component/howtoTab";
import EmployeeModal from "../../component/employeeModal";
import FilePreview from "./../../../../assets/ตัวอย่าง ประเมินความสอดคล้อง (FM-SE-49).pdf";
import DetailModal from "../../component/detailModal";
import CauseModal from '../../component/causeModal';
import "./index.scss";
import CreatHeadPanel from "../../component/headpanelReadOnly";
import Swal from "sweetalert2/dist/sweetalert2.js";
import { useDispatch, useSelector } from "react-redux";
import { fetchgetMainLaw } from "../../../../redux/mainLaw/mainLawAction";
import { fetchgetMainLawSub } from "../../../../redux/mainLawSub/mainLawSubAction";
import { WEB_API } from "../../../../env";
import TableFiles from "../../create/TableFiles";

const ViewOnly = (props) => {
  const dispatch = useDispatch();
  const mainlawsState = useSelector((state) => state.mainLaws.mainlaws);
  const mainlawsSubState = useSelector(
    (state) => state.mainLawsSub.mainlawsSub
  );

  var id = new URLSearchParams(props.location.search).get("id");
  var code = new URLSearchParams(props.location.search).get("code");
  var buCode = new URLSearchParams(props.location.search).get("buCode");
  const lawinfoData = useSelector((state) => state.lawinfoReducer.data);
  const [mainlawState, setmainlawState] = useState([]);
  const [mainlawSubState, setmainlawSubState] = useState([]);
  const [tabAddress, setTabAddress] = useState("1");
  const [lawid, setLawId] = useState(0);
  const [mainlawid, setMainLawId] = useState();
  const [modal, setModal] = useState(false);
  const [modalDetail, setModalDetail] = useState(false);
  const [modalCause, setModalCause] = useState(false);
  const [isHideCancelRemark, setHideCancelRemark] = useState(true);
  const userState = useSelector((state) => state.changeState.user);
  const userStateToken = useSelector((state) => state.changeState.token);
  const [statusData, setStatusData] = useState([]);
  const [maimlawFlag, setmainlawFlag] = useState(false);
  const [maimlawFlagSub, setmainlawFlagSub] = useState(false);
  const [idSub, setidSub] = useState(0);
  const tranbrsBuState = useSelector((state) => state.gettranbrcsbu.data);
  const [mainlawSubData, setmainlawSubData] = useState([]);
  const [headerData, setHeaderData] = useState([]);
  const [fileCount, setFileCount] = useState(0);
  const statusOption = [
    { value: "SADR", label: "Save" },
    { value: "NEDO", label: "Submit" },
    { value: "CACE", label: "Delete" },
    { value: "SEEV", label: "Send To Evaluate" },
    { value: "REFQ", label: "Reject To FQO" },
    { value: "SEA2", label: "Send to Approver 2" },
    { value: "DOAP", label: "Document Approve" },
    { value: "REDI", label: "Reject To User Division" },
  ];

  const buttonOption = [
  ];


  useEffect(() => {
    // getData();
    if (id === null) {
      dispatch(fetchgetMainLaw({}));
    } else {
      returnMainLawAsync(id);
      setmainlawFlag(true);
      // dispatch(fetchgetMainLawSub({}));
    }
    setStatusData(statusOption);
    setLawId(id);
    return () => {};
  }, []);

  useEffect(() => {
    if (maimlawFlag !== false) {
      returnMainLawAsync(id);
    }
    if (maimlawFlagSub !== false) {
      returnMainLawSubAsync(idSub);
    }

    return () => {};
  }, [mainlawState, mainlawSubState]);

  const getMainLawSubPromise = (value, bu_id) => {
    return new Promise((resolve, reject) => {
      setTimeout(async () => {
        let results = await Api.getMainLawSub(value, bu_id);
        if (results.data.status === 200) {
          let prepare = results.data.response;
          let result = [];
          prepare.length !== 0 &&
            prepare.forEach((data) => {
              if (data.monthList !== " ") {
                let monthParse = data && JSON.parse(data["monthList"]);
                result.push({
                  id: data.id,
                  seq: data.seq,
                  description: data.description,
                  descriptionlink: data.descriptionLink,
                  mainlawsub_buid: data.mainlawsubbuid,
                  estimate: data.estimateNo,
                  priority: data.priority,
                  month01: monthParse.month01,
                  month02: monthParse.month02,
                  month03: monthParse.month03,
                  month04: monthParse.month04,
                  month05: monthParse.month05,
                  month06: monthParse.month06,
                  month07: monthParse.month07,
                  month08: monthParse.month08,
                  month09: monthParse.month09,
                  month10: monthParse.month10,
                  month11: monthParse.month11,
                  month12: monthParse.month12,
                  lawGroupSubId: data.lawGroupSubId,
                  brcsDate: data.brcsDate,
                  brcsDateForPicker: data.brcsDateForPicker,
                });
              } else {
                result.push({
                  id: data.id,
                  seq: data.seq,
                  description: data.description,
                  descriptionlink: data.descriptionLink,
                  mainlawsub_buid: data.mainlawsubbuid,
                  estimate: data.estimateNo,
                  priority: data.priority,
                  month01: false,
                  month02: false,
                  month03: false,
                  month04: false,
                  month05: false,
                  month06: false,
                  month07: false,
                  month08: false,
                  month09: false,
                  month10: false,
                  month11: false,
                  month12: false,
                  lawGroupSubId: data.lawGroupSubId,
                  brcsDate: data.brcsDate,
                  brcsDateForPicker: data.brcsDateForPicker,
                });
              }
            });
          resolve(result);
        }
      }, 600);
    });
  };

  const getMainLaw = (value) => {
    return new Promise((resolve, reject) => {
      return setTimeout(() => {
        dispatch(fetchgetMainLaw(value));
        Swal.showLoading();
        resolve(mainlawsState);
      }, 1500);
    }).then(() => {
      Swal.close();
    });
  };

  const returnMainLawAsync = async (value) => {
    let results = await getMainLaw(value);
    setmainlawState(results);
    setmainlawFlag(false);
  };

  const returnMainLawSubAsync = async (value, bu_id) => {
    let results = await getMainLawSubPromise(value, bu_id);
    console.log("getMainLawSubPromise", results);
    setmainlawSubData(results);
    setmainlawFlagSub(false);
    if (results) {
      Swal.close();
    }
  };

  const handleMainLawId = (idx) => {
    setMainLawId(idx);
    setidSub(idx);
    if (code === null && buCode == null) {
      returnMainLawSubAsync(idx, 0);
    } else {
      returnMainLawSubAsync(idx, buCode);
    }
  };

  const handleTabAddress = (idx) => {
    setTabAddress(idx);
  };

  const validateDataHeader = async (data) => {}
  const saveDataMainLaw = async (data) => {}
  const deleteDataMainLaw = async (data) => {}

  return (
    <>
      <h2>สร้างข้อมูลกฏหมาย</h2>
      <CCard fluid className="mt-3">
        <CCardHeader>
          <CRow>
            <CCol sm="12">
              ข้อมูลกฏหมาย
            </CCol>
          </CRow>
          <div style={{ fontSize: "12px" }}>
            <CreatHeadPanel
              statusOption={statusData}
              buttonOption={buttonOption}
              id={id}
              validateDataHeader={validateDataHeader}
              // isHideCancelRemark={isHideCancelRemark}
              code={code}
              setHeaderData={setHeaderData}
            />
          </div>
        </CCardHeader>
        <CCardBody>
          <CTabs
            activeTab={tabAddress}
            onActiveTabChange={(idx) => handleTabAddress(idx)}
          >
            <CNav variant="tabs">
              <CNavItem>
                <CNavLink data-tab="1">
                  <b>กฎหมายหลัก</b>
                </CNavLink>
              </CNavItem>
              <CNavItem>
                <CNavLink data-tab="3">
                  <b>แนบไฟล์ ({fileCount})</b>
                </CNavLink>
              </CNavItem>
            </CNav>
            <CTabContent>
              <CTabPane data-tab="1">
                <MainLaw
                  lawid={lawid}
                  handleMainLawId={handleMainLawId}
                  handleTabAddress={handleTabAddress}
                  saveDataMainLaw={saveDataMainLaw}
                  mainlawsStates={code !== null ? mainlawState : mainlawsState}
                  code={code}
                  buCode={buCode}
                  isView={true}
                  headerData={headerData}
                />
              </CTabPane>
              <CTabPane data-tab="3">
                <div>
                  <TableFiles id={id} code={code} buCode={buCode} setFileCount={setFileCount} />
                </div>
              </CTabPane>
            </CTabContent>
          </CTabs>
        </CCardBody>
      </CCard>
      <EmployeeModal modal={modal} setModal={setModal} />
      <DetailModal modal={modalDetail} setModal={setModalDetail} />
      <CauseModal modal={modalCause} setModal={setModalCause} />
    </>
  );
};

export default ViewOnly;
