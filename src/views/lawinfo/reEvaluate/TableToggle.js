import {
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CButton,
} from "@coreui/react";
import React from "react";
// const [modal, setModal] = useState(false);
const TableToggle = ({ modal, toggle }) => {
  return (
    <div>
      <div
        class="modal fade"
        id="exampleModalCenter"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <CModal show={modal} onClose={toggle} className="modal-90w">
          <CModalHeader closeButton>Modal title</CModalHeader>
          <CModalBody>Lorem ipsum dolor...</CModalBody>
          <CModalFooter>
            <CButton color="primary">Do Something</CButton>{" "}
            <CButton variant="outline" color="secondary" onClick={toggle}>
              Cancel
            </CButton>
          </CModalFooter>
        </CModal>
      </div>
    </div>
  );
};

export default TableToggle;
