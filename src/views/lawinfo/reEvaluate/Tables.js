import React, { useState, useEffect } from "react";
import {
  CCardBody,
  CBadge,
  CButton,
  CCollapse,
  CDataTable,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CCol,
  CCard,
  CRow,
  CCardHeader,
  CFormGroup,
  CLabel,
  CInput,
  CSelect,
  CForm,
  CModalTitle,
  CTextarea,
  CInputCheckbox,
  CButtonGroup,
} from "@coreui/react";
import { useDispatch, useSelector } from "react-redux";
import { getBrcsAssessmentDetail } from "../../../redux/getbrcaassessment/getbrcaassessmentAction";
import Api from "../../../api/ApiLawInfo";
import Swal from "sweetalert2/src/sweetalert2.js";

const getEstimate = (est) => {
  switch (est) {
    case "1":
      return "ไม่เกี่ยวข้อง";
    case "2":
      return "สอดคล้อง";
    case "3":
      return "ไม่สอดคล้อง";
    case "4":
      return "สอดคล้อง A";
    default:
      return "";
  }
};

const getStatus = (status) => {
  switch (status) {
    case "SADR":
      return "Save";
    case "NEDO":
      return "Submit";
    case "CACE":
      return "Delete";
    case "SEEV":
      return "Send To Evaluate";
    case "REFQ":
      return "Reject To FQO";
    case "SEA1":
      return "Send To Verification";
    case "SEA2":
      return "Send To Approver";
    case "REDI":
      return "Reject To User Division";
    case "DOAP":
      return "Document Approve";
    case "INPR":
      return "In Process";
    case "CLNE":
      return "Close New";
    case "CLOS":
      return "Complete Send To Verification";
    case "CLS2":
      return "Complete Send To Approver";
    case "RJCL":
      return "Reject Complete To User Division";
    default:
      return "";
  }
};

const getBadge = (status) => {
  switch (status) {
    case "SEEV":
      return "info";
    case "REFQ":
      return "dark";
    case "SEA1":
      return "warning";
    case "SEA2":
      return "danger";
    case "REDI":
      return "dark";
    case "DOAP":
      return "success";
    case "INPR":
      return "primary";
    case "CLNE":
      return "info";
    case "CLOS":
      return "warning";
    case "CLS2":
      return "danger";
    case "RJCL":
      return "dark";
  }
};

const fields = [
  // { key: 'assessmentNo', label: 'ASSESSMENT No' },
  { key: "docno", label: "เลขที่เอกสาร", _style: { width: "15%" } },
  // { key: 'docNoBu', label: 'เลขที่เอกสาร BU' },
  { key: "mainLawName", label: "ชื่อกฎหมาย", _style: { width: "25%" } },
  { key: "description", label: "สาระสำคัญกฎหมาย" },
  // { key: 'valYear', label: 'ปีที่ประเมิน' },
  // { key: 'valMonth', label: 'เดือนที่ประเมิน' },
  // { key: 'userLogin', label: 'ผู้ประเมิน' },
  // { key: 'email', label: 'Email' },
  // { key: 'estimateResult', label: 'ผลการประเมิน' },
  // { key: 'estimateDesc', label: 'รายละเอียดผลการประเมิน' },
  // { key: 'option', label: '', _style: { width: '7%' }, filter: false, },
];

const subfields = [
  { key: "select", label: "", filter: false },
  { key: "assessmentNo", label: "เลขที่ประเมิน" },
  // { key: 'docno', label: 'เลขที่เอกสาร' },
  { key: "docNoBu", label: "เลขที่เอกสาร BU" },
  // { key: 'mainLawName', label: 'ชื่อกฎหมาย' },
  // { key: 'description', label: 'สาระสำคัญกฎหมาย' },
  { key: "valYear", label: "ปี" },
  { key: "valMonth", label: "เดือน" },
  { key: "userLogin", label: "ผู้ประเมิน" },
  // { key: 'email', label: 'Email' },
  { key: "estimateResult", label: "ผลการประเมิน" },
  // { key: 'estimateDesc', label: 'รายละเอียดผลการประเมิน' },
  { key: "systemStatus", label: "สถานะ" },
  { key: "option", label: "", _style: { width: "8%" }, filter: false },
  { key: "fileCount", label: "File", _style: { width: "7%" } },
];

const Tables = ({ data = [], refreshData = () => {}, searchData }) => {
  const userState = useSelector((state) => state.changeState.user);
  const [selected, setSelected] = useState([]);
  const [details, setDetails] = useState([]);
  const [lastIndex, setLastIndex] = useState(null);
  const dispatch = useDispatch();
  const [cancelRemark, setCancelRemark] = useState("");
  const [modalRemark, setModalRemark] = useState(false);
  const tranbuState = useSelector((state) => {
    console.log("state", state.getbrcaassessment);
    return state.getbrcaassessment.data;
  });

  useEffect(() => {
    setDetails([]);
    return () => {};
  }, [searchData]);

  const toggleDetails = (index) => {
    console.log("data", data);
    const position = data.indexOf(index);
    let newDetails = data.slice();
    if (lastIndex == index) {
      newDetails.splice(position, 1);
      setLastIndex(null);
    } else {
      newDetails = [...data, index];
      setLastIndex(index);
    }
    setDetails(newDetails);
    console.log("setDetail", details);
  };

  const toggleRemark = () => {
    setCancelRemark("");
    
    if (selected.length == 0) {
      Swal.fire({
        icon: "error",
        title: "กรุณาเลือกรายการ",
      });
      return false;
    }
    
    setModalRemark(!modalRemark);
  };

  const getSubTable = (Docno, MainLawName, Desc) => {
    searchData.DetailDocno = Docno;
    searchData.DetailMainLaw = MainLawName;
    searchData.DetailDesc = Desc;

    try {
      dispatch(getBrcsAssessmentDetail(searchData));
    } catch (error) {
      console.log(error.message);
    }
  };

  const check = (e, id) => {
    if (e.target.checked) {
      setSelected([...selected, id]);
    } else {
      setSelected(selected.filter((itemId) => itemId !== id));
    }
  };

  const reEvaluate = () => {
    console.log(selected);
    if (selected.length == 0) {
      Swal.fire({
        icon: "error",
        title: "กรุณาเลือกรายการ",
      });
      return false;
    }
    if (cancelRemark.length == 0) {
      Swal.fire({
        icon: "error",
        title: "กรุณากรอกหมายเหตุ",
      });
      return false;
    }
    const model = {
      assessmentIds: selected.join(","),
      user: userState.id,
      remark: cancelRemark,
    };
    submitReEvaluate(model);
  };

  const submitReEvaluate = async (data) => {
    try {
      Swal.showLoading();
      const result = await Api.saveReEvaluate(data);
      Swal.close();
      if (result.status === 200) {
        const { data } = result.data;
        toggleRemark();
        refreshData();
        Swal.fire({
          icon: "success",
          title: "Save Success",
        });
        window.location.reload();
      }
    } catch (error) {
      if (error.response.data.statusCode && error.response.data.statusCode == 550){
        Swal.fire({
          icon: "error",
          title: "ERROR",
          text: `Send Email Error ${error.response.data.message}`
        });
        window.location.reload();
      }else{
        Swal.fire({
          icon: "error",
          title: "ERROR",
          text: error.response.data
        });
        if(error.response.data == "Send Email Error"){
          window.location.reload();
        }
      }
    }
  };

  const selectAll = () => {
    const selectDataAll = tranbuState.map((x) => x.assessmentId);
    console.log("selectDataAll", selectDataAll);
    setSelected(selectDataAll);
  };
  const unSelectAll = () => {
    setSelected([]);
  };

  return (
    <>
      <CRow>
        <CCol xs="12" lg="12">
          <CButton className="mr-2"onClick={toggleRemark} color={"success"} variant={"outline"}>
            <span className="ml-2">Re Evaluate</span>
          </CButton>
          <CButton className="mr-2" onClick={selectAll} color={"info"} variant={"outline"}>
            <span className="ml-2">เลือกทั้งหมด</span>
          </CButton>
          <CButton className="mr-2" onClick={unSelectAll} color={"danger"} variant={"outline"}>
            <span className="ml-2">ยกเลิก</span>
          </CButton>
        </CCol>
      </CRow>
      <CRow className="">
        <CCol xs="12" lg="12">
          <CDataTable
            items={data}
            fields={fields}
            itemsPerPageSelect={{
              label: "จำนวนการแสดงผล",
              values: [10, 25, 50, 100],
            }}
            itemsPerPage={10}
            hover
            sorter
            striped
            bordered
            pagination
            scopedSlots={{
              docno: (item, index) => (
                <td>
                  <label
                    style={{ cursor: "pointer", color: "blue" }}
                    onClick={() => {
                      toggleDetails(index);
                      getSubTable(
                        item.docno,
                        item.mainLawName,
                        item.description
                      );
                    }}
                  >
                    {details.includes(index) ? item.docno : item.docno}
                  </label>
                </td>
              ),
              details: (item, index) => {
                return (
                  <CCollapse show={details.includes(index)}>
                    <CCardBody>
                      <CDataTable
                        items={tranbuState}
                        fields={subfields}
                        hover
                        sorter
                        striped
                        bordered
                        pagination
                        scopedSlots={{
                          select: (subitem) => {
                            return (
                              <td>
                                <CFormGroup variant="custom-checkbox">
                                  <CInputCheckbox
                                    custom
                                    id={`checkbox-${subitem.assessmentId}`}
                                    checked={
                                      selected.includes(subitem.assessmentId)
                                        ? true
                                        : false
                                    }
                                    onChange={(e) =>
                                      check(e, subitem.assessmentId)
                                    }
                                  />
                                  <CLabel
                                    variant="custom-checkbox"
                                    htmlFor={`checkbox-${subitem.assessmentId}`}
                                  />
                                </CFormGroup>
                              </td>
                            );
                          },
                          option: (items) => (
                            <td className="center">
                              <CButtonGroup>
                                <CButton
                                  color="success"
                                  variant="outline"
                                  // shape="square"
                                  size="sm"
                                  onClick={() =>
                                    (window.location.href = `/home/lawinfo/reEvaluate/create?id=${items.id}&code=${items.brcsBuId}&assessId=${items.assessmentId}`)
                                  }
                                >
                                  รายละเอียด
                                </CButton>
                              </CButtonGroup>
                            </td>
                          ),
                          systemStatus: (item) => (
                            <td>
                              <CBadge color={getBadge(item.systemStatus)}>
                                {getStatus(item.systemStatus)}
                              </CBadge>
                            </td>
                          ),
                          estimateResult: (item) => (
                            <td>
                              <CBadge>
                                {getEstimate(item.estimateResult)}
                              </CBadge>
                            </td>
                          ),
                          fileCount: (items) => (
                            <td>
                              File : {items.fileCount}
                            </td>
                          ),
                        }}
                      ></CDataTable>
                    </CCardBody>
                  </CCollapse>
                );
              },
            }}
          />
        </CCol>
      </CRow>
      <CModal show={modalRemark} onClose={toggleRemark}>
        <CForm onSubmit={reEvaluate} action="">
          <CModalBody>
            <CRow>
              <CCol xs="12" sm="12">
                <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel htmlFor="cancelRemark">หมายเหตุ</CLabel>
                      <CTextarea
                        onChange={(e) => setCancelRemark(e.target.value)}
                        rows={8}
                        value={cancelRemark}
                        id="cancelRemark"
                        placeholder="กรุณากรอก หมายเหตุ"
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>
              </CCol>
            </CRow>
          </CModalBody>
          <CModalFooter>
            <CButton variant="outline" type="submit" color="primary">
              ตกลง
            </CButton>
            <CButton variant="outline" color="secondary" onClick={toggleRemark}>
              ยกเลิก
            </CButton>
          </CModalFooter>
        </CForm>
      </CModal>
    </>
  );
};

export default Tables;
