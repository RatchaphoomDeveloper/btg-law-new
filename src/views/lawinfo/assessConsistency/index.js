import React, { useEffect, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CNav,
  CForm,
  CContainer,
  CLabel,
  CSelect,
  CInput,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import Api from '../../../api/ApiLawInfo';
import Tables from "./Tables";
import SearchPanel from "../component/searchpanel";

const SearchAssessConsistency= () => {
  const [dataTable, setDataTable] = useState([]);

  const [statusData, setStatusData] = useState([]);
  const statusOption = [
    { value: "SEEV", label: "Send To Evaluate" },
    { value: 'REDI', label: 'Reject To User Division'},
  ];
  
  useEffect(() => {
    setStatusData(statusOption);
    getData();
    return () => {};
  }, []);

  const getData = async (data) => {
    try {
      if (data == undefined) {
        data = {
          "page": "estimate"
        }
      }else{
        data.page = "estimate";
      }
      const result = await Api.get(data);
      if (result.status == 200) {
          setDataTable(result.data);
      }
    } catch (error) {
      console.log(error);
    }
  };
  
  return (
    <>
      <h2>ค้นหาข้อมูลกฎหมาย</h2>
      <CCard fluid className="mt-3">
        <CCardHeader>
          <CRow>
            <CCol sm="12">
              ค้นหากฏหมาย
              {/* <CNav className="card-header-actions">
                <CForm inline>
                  <CButton
                    size="sm"
                    variant="outline"
                    color="primary"
                    // disabled={selected.length <= 0}
                    style={{ marginRight: "4px" }}
                    onClick={() =>
                      (window.location.href = "/home/lawinfo/assessConsistency/create")
                    }
                  >
                    <CIcon
                      size="sm"
                      name="cil-check-circle"
                      style={{ marginRight: "2px" }}
                    />
                    <b> สร้าง</b>
                  </CButton>
                </CForm>
              </CNav> */}
            </CCol>
          </CRow>
          <div style={{ fontSize: "12px" }}>
            <SearchPanel getData={getData} statusOption={statusData}/>
          </div>
        </CCardHeader>
        <CCardBody>
          <Tables refreshData={getData} data={dataTable} />
        </CCardBody>
      </CCard>
    </>
  );
};

export default SearchAssessConsistency;
