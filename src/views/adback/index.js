import React, { useRef, useState, useContext, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import Api from "../../api/ApiLogin";
import Swal from "sweetalert2/src/sweetalert2.js";
import { setLocalStorage } from "../../utils/localStorage.js";
import queryString from "query-string";
const AdBack = (props) => {
  //const token = new URLSearchParams(props.location.search).get("access_token");
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    const token = queryString.parse(props.location.hash);
    if (token.access_token) {
      var model = {
        token: token.access_token,
      };
      loginAdfs(model);
    } else {
      history.push("/home");
    }
    return () => {};
  }, []);

  const loginAdfs = async (model) => {
    try {
      const result = await Api.loginAdfs(model);
      if (result.status === 200) {
        const { user, token } = result.data;
        if (user) {
          setLocalStorage("token", token);
          setLocalStorage("role", user.role);
          dispatch({ type: "set_user", user: user, token: token });
          history.push("/home");
        } else {
          Swal.fire({
            icon: "error",
            title: "ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง โปรดลองใหม่อีกครั้ง",
          });
        }
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: error.response.data,
      }).then((result) => {
        history.push("/home");
      });
    }
  };

  return <></>;
};

export default AdBack;
