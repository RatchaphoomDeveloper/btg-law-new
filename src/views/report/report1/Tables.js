import React, { useState, useEffect } from 'react'
import {
    CCardBody,
    CBadge,
    CButton,
    CCollapse,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CCol,
    CCard,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CSelect,
    CForm,
    CModalTitle,
    CButtonGroup
} from '@coreui/react'
// import Api from '../../api/ApiRole';
import CIcon from '@coreui/icons-react'
// import { statusData } from './data';

const getBadge = status => {
    switch (status) {
        case 'Active': return 'success'
        case 'Inactive': return 'secondary'
        case 'Pending': return 'warning'
        case 'Banned': return 'danger'
        default: return 'primary'
    }
}

const fields = [
    // { key: 'spec', label: 'ข้อกำหนดที่ต้องปฏิบัติตาม' },
    // { key: 'activeDate', label: 'วันที่มีผลบังคับใช้' },
    // { key: 'result', label: 'ผลการประเมิน กฎหมาย / ข้อกำหนด' },
    // { key: 'description', label: 'รายละเอียดหลักฐาน' },
    // { key: 'rateDate', label: 'วันที่ประเมิน', _style: { width: '15%' } },
    { key: 'systemType', label: 'ประเภทของระบบ' },
    { key: 'assessmentno', label: 'เลขที่เอกสาร' },
    { key: 'lawGroup', label: 'กลุ่มของกฎหมาย' },
    { key: 'createDate', label: 'วันที่ประกาศ' },
    { key: 'lawAgencies', label: 'หน่วยงานผู้ออกกฎหมาย'},
    { key: 'estimateUser', label: 'ผู้ประเมิน'},
    { key: 'mainlawName', label: 'ชื่อกฎหมาย'},
    { key: 'mainLawSubName', label: 'ข้อกำหนดที่ต้องปฏิบัติตาม'},
    { key: 'department', label: 'แผนก'},
    { key: 'option', label: '', _style: { width: '10%' }, filter: false, },
]


const Tables = ({ data = [], refreshData = () => { }, getDetail }) => {

    // const [modal, setModal] = useState(false);
    // const [id, setId] = useState('');
    // const [sequence, setSequence] = useState('');
    // const [code, setCode] = useState('');
    // const [description, setDescription] = useState('');
    // const [recordstatus, setRecordStatus] = useState('');
    // const [dataSelected, setDataSelected] = useState('');
    // const [modalConfirm, setModalConfirm] = useState(false);

    // const toggle = () => {
    //     setModal(!modal);
    // }

    // const toggleConfirm = () => {
    //   setModalConfirm(!modalConfirm);
    // }

    // const newDetails = (record) => {
    //     toggle();
    //     setId('');
    //     setSequence('');
    //     setCode('');
    //     setDescription('');
    //     setRecordStatus("A");
    // }
    // const deleteDetails = (record) => {
    //     toggleConfirm();
    //     setDataSelected(record);
    //     setId(record.id);
    // }

    // const onDelete = async () => {
    // //   try {
    // //       const result = await Api.delete(dataSelected);
    // //       if (result.status === 200) {
    // //           const { data } = result.data;
    // //           toggleConfirm();
    // //           refreshData();
    // //       }
    // //     } catch (error) {
  
    // //     }
    // }
    
    
    const getPdfDetails = (record) => {
        var model = {
            refDocNo: record.assessmentno
        }
        getDetail(model);
    }

    // const onSubmit = (e) => {
    //   e.preventDefault();
    //   const model = {
    //     "id": id == "" ? 0 : id,
    //     "code": code,
    //     "sequence": sequence,
    //     "description": description,
    //   }
    //   saveData(model);
    // }
  
    // const saveData = async (data) => {
    // //   try {
    // //       const result = await Api.create(data);
    // //       if (result.status === 200) {
    // //           const { data } = result.data;
    // //           toggle();
    // //           refreshData();
    // //       }
    // //   } catch (error) {
  
    // //   }
    // }

    // const handleChange = (e) => {
    //     setRecordStatus(e.target.value);
    // }

    return (
        <>
            <CRow className="mt-3">
                <CCol xs="12" lg="12" >
                    <CCard>
                        <CCardBody>
                            <CDataTable
                                items={data}
                                fields={fields}
                                tableFilter={{ label: "ค้นหา", placeholder: 'พิมพ์คำที่ต้องการค้นหา' }}
                                cleaner
                                itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
                                itemsPerPage={10}
                                hover
                                sorter
                                striped
                                bordered
                                pagination
                                scopedSlots={{
                                    option: (item) => (
                                        <td className="center">
                                            <CButtonGroup>
                                                <CButton
                                                    color="info"
                                                    variant="outline"
                                                    shape="square"
                                                    size="sm"
                                                    onClick={() => { getPdfDetails(item) }}
                                                >
                                                    รายงาน
                                                </CButton>
                                            </CButtonGroup>
                                        </td>
                                    ),
                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>

            </CRow>

            {/* <CModal show={modalConfirm} onClose={setModalConfirm} color="danger" >
                <CModalHeader closeButton>
                    <CModalTitle> Delete Data : {code}</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    Are you confirm to delete {code} ?
                </CModalBody>
                <CModalFooter>
                    <CButton variant="outline" onclick={onDelete} color="primary">Confirm</CButton>{' '}
                    <CButton variant="outline" color="secondary" onClick={() => setModalConfirm(false)}>Close</CButton>
                </CModalFooter>
            </CModal>
            <CModal
                show={modal}
                onClose={toggle}
            >
                <CModalHeader closeButton>
                    <CModalTitle>รายละเอียด Site</CModalTitle>
                </CModalHeader>
                <CForm onSubmit={onSubmit} action="" >
                    <CModalBody>
                        <CInput type="hidden" value={id} id="id"/>
                        <CRow>
                            <CCol xs="12" sm="12">
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="code">รหัส</CLabel>
                                            <CInput onChange={e => setCode(e.target.value)} value={code} id="code" placeholder="กรอกรหัส" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="description">คำอธิบาย</CLabel>
                                            <CInput onChange={e => setDescription(e.target.value)} value={description} id="description" placeholder="กรอกคำอธิบาย" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="sequence">ลำดับ</CLabel>
                                            <CInput onChange={e => setSequence(e.target.value)} value={sequence} id="sequence" placeholder="กรอกลำดับ" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                            </CCol>
                        </CRow >
                        <CRow>
                            <CCol xs="12">
                                <CFormGroup>
                                    <CLabel htmlFor="recordstatus">สถานะการใช้งาน</CLabel>
                                    <CSelect custom name="recordstatus" id="recordstatus" value={recordstatus} onChange={handleChange} required>
                                        <option value={""}>-- สถานะการใช้งาน --</option>
                                        {
                                            statusData.map((item, index) => {
                                                return <option key={item.code} value={item.code}>{item.name}</option>
                                            })
                                        }
                                    </CSelect>
                                </CFormGroup>
                            </CCol>
                        </CRow>
                    </CModalBody>
                    <CModalFooter>
                        <CButton variant="outline" type="submit" color="primary">ตกลง</CButton>
                        <CButton
                            color="secondary"
                            onClick={toggle}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CForm >
            </CModal > */}

        </>
    )
}

export default Tables
