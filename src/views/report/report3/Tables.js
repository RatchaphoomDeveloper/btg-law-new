import React, { useState, useEffect } from 'react'
import {
    CCardBody,
    CBadge,
    CButton,
    CCollapse,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CCol,
    CCard,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CSelect,
    CForm,
    CModalTitle,
    CButtonGroup
} from '@coreui/react'
// import Api from '../../api/ApiRole';
import CIcon from '@coreui/icons-react'
// import { statusData } from './data';

const getBadge = status => {
    switch (status) {
        case 'Active': return 'success'
        case 'Inactive': return 'secondary'
        case 'Pending': return 'warning'
        case 'Banned': return 'danger'
        default: return 'primary'
    }
}

const fields = [
    // { key: 'no', label: 'ลำดับที่' },
    // { key: 'docNo', label: 'เลขที่เอกสาร' },
    // { key: 'spec', label: 'ชื่อกฎหมาย / ข้อกำหนด' },
    // { key: 'activeDate', label: 'วันที่มีผลบังคับใช้' },
    // { key: 'result', label: 'ผลการประเมิน กฎหมาย / ข้อกำหนด' },
    // { key: 'plant', label: 'โรงงาน/แผนก' },
    // { key: 'rateDate', label: 'วันที่ประเมิน', _style: { width: '15%' } },
    // { key: 'description', label: 'รายละเอียด สอดคล้อง / ไม่สอดคล้อง / ไม่เกี่ยวข้อง' },
    { key: 'systemType', label: 'ประเภทของระบบ' },
    { key: 'plant', label: 'โรงงาน' },
    { key: 'lawGroup', label: 'กลุ่มของกฎหมาย' },
    { key: 'createDate', label: 'วันที่ประกาศ' },
    { key: 'lawAgencies', label: 'หน่วยงานผู้ออกกฎหมาย'},
    { key: 'mainlawName', label: 'ชื่อกฎหมาย'},
    { key: 'mainLawSubName', label: 'ข้อกำหนดที่ต้องปฏิบัติตาม'},
    { key: 'option', label: '', _style: { width: '10%' }, filter: false, },
]


const Tables = ({ data = [], refreshData = () => { }, getDetail }) => {
    
    const getPdfDetails = (record) => {
        var model = {
            plant: record.plant,
            pntCode: record.pntCode,
            mainlawName: record.mainlawName,
            mainLawSubName: record.mainLawSubName,
            mlId: record.mlId,
            mlsId: record.mlsId,
        }
        getDetail(model);
    }


    return (
        <>
            <CRow className="mt-3">
                <CCol xs="12" lg="12" >
                    <CCard>
                        <CCardBody>
                            <CDataTable
                                items={data}
                                fields={fields}
                                tableFilter={{ label: "ค้นหา", placeholder: 'พิมพ์คำที่ต้องการค้นหา' }}
                                cleaner
                                itemsPerPageSelect={{ label: "จำนวนการแสดงผล", values: [10, 25, 50, 100]}}
                                itemsPerPage={10}
                                hover
                                sorter
                                striped
                                bordered
                                pagination
                                scopedSlots={{
                                    option: (item) => (
                                        <td className="center">
                                            <CButtonGroup>
                                                <CButton
                                                    color="info"
                                                    variant="outline"
                                                    shape="square"
                                                    size="sm"
                                                    onClick={() => { getPdfDetails(item) }}
                                                >
                                                    รายงาน
                                                </CButton>
                                            </CButtonGroup>
                                        </td>
                                    ),
                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    )
}

export default Tables
