import React, { useEffect, useState } from 'react'
import {
    CBadge,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CModal,
    CModalHeader,
    CModalFooter,
    CButton,
    CNav,
    CForm,
    CLabel,
    CSelect,
    CInput,
} from '@coreui/react'
import CIcon from "@coreui/icons-react";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import Api from '../../../api/ApiReport';
import Tables from './Tables';
import SearchPanel from "../component/searchpanelReport3";
import { REPORT_API, WEB_API } from "../../../env";
import axios from "axios";
import Swal from "sweetalert2/src/sweetalert2.js";
import { useDispatch, useSelector } from "react-redux";

const Report3 = () => {
    const [dataTable, setDataTable] = useState([]);
    const [searchData, setSearchData] = useState([]);
    const userState = useSelector((state) => state.changeState.user);

    useEffect(() => {
        getData();
        return () => {
  
          }
      }, []);
  
    const getData = async (data) => {
        try{
            console.log("data", data);if (data == undefined) {
                data = {
                  page: "frequency",
                  role: userState.role,
                };
              } else {
                data.page = "frequency";
                data.role = userState.role;
              }
            setSearchData(data);
            Swal.showLoading();
            const result = await Api.Report3(data);
            Swal.close();
            if (result.status == 200) {
                setDataTable(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }
    
    const getDetail = async (data) => {
        try{
            data.role = userState.role;
            Swal.showLoading();
            const result = await Api.ExportReport3(data);
            Swal.close();
            if (result.status == 200) {
                window.open(result.data, "_blank")
            }
        }catch(error){
            console.log(error);
        }
    }


    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardHeader>
                            <b> รายงานผลการทบทวนการประเมินระดับโรงงาน ({dataTable.length}) </b>
                            <div style={{ fontSize: "12px" }}>
                                <SearchPanel getData={getData} />
                            </div>
                        </CCardHeader>
                        <CCardBody>
                            <Tables refreshData={getData} data={dataTable} getDetail={getDetail} />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    )
}

export default Report3;