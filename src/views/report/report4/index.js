import React, { useEffect, useState } from 'react'
import {
    CBadge,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CModal,
    CModalHeader,
    CModalFooter,
    CButton,
    CNav,
    CForm,
    CLabel,
    CSelect,
    CInput,
} from '@coreui/react'
import CIcon from "@coreui/icons-react";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import Api from '../../../api/ApiReport';
import Tables from './Tables';
import SearchPanel from "../component/searchpanelReport4";
import Swal from "sweetalert2/src/sweetalert2.js";
import { useDispatch, useSelector } from "react-redux";

const Report4 = () => {
    const [dataTable, setDataTable] = useState([]);
    const [searchData, setSearchData] = useState([]);
    const userState = useSelector((state) => state.changeState.user);

    const [statusData, setStatusData] = useState([]);
    const statusOption = [
      { value: "SADR", label: "Save" },
      { value: "NEDO", label: "Submit" },
      { value: "CACE", label: "Delete" },
      { value: "SEEV", label: "Send To Evaluate" },
      { value: "REFQ", label: "Reject To FQO" },
      { value: 'SEA1', label: 'Send To Verification'},
    //   { value: "REBU", label: "Reject To BU" },
      { value: 'SEA2', label: 'Send To Approver'},
      { value: 'REDI', label: 'Reject To User Division'},
      { value: 'DOAP', label: 'Document Approve'},
    //   { value: 'REA1', label: 'Reject To Approver 1'},
      { value: 'INPR', label: 'In Process'},
      { value: 'CLNE', label: 'Close New'},
      { value: 'CLOS', label: 'Complete Send To Verification'},
      { value: 'CLS2', label: 'Complete Send To Approver'},
      { value: 'RJCL', label: 'Reject Complete To User Division'},
    ];

    useEffect(() => {
        setStatusData(statusOption);
        getData();
        return () => {
  
          }
    }, []);

    const getData = async (data) => {
        try{
            console.log("data", data);if (data == undefined) {
                data = {
                    page: "frequency",
                    role: userState.role,
                };
                } else {
                    data.page = "frequency";
                    data.role = userState.role;
                }
            setSearchData(data);
            Swal.showLoading();
            const result = await Api.Report4(data);
            Swal.close();
            if (result.status == 200) {
                setDataTable(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }
    
    const getDetail = async (data) => {
        try{
            data.role = userState.role;
            Swal.showLoading();
            const result = await Api.ExportReport4(data);
            Swal.close();
            if (result.status == 200) {
                window.open(result.data, "_blank")
            }
        }catch(error){
            console.log(error);
        }
    }


    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardHeader>
                            <b> รายงานผลการแก้ไขป้องกัน ({dataTable.length}) </b>
                            <div style={{ fontSize: "12px" }}>
                                <SearchPanel getData={getData} statusOption={statusData} />     
                            </div>
                        </CCardHeader>
                        <CCardBody>
                            <Tables refreshData={getData} data={dataTable} getDetail={getDetail}/>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    )
}

export default Report4;