import React, { useEffect, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CNav,
  CForm,
  CContainer,
  CLabel,
  CSelect,
  CInput,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import ApiSystemType from '../../../api/ApiSystemType';
import ApiCountryAssign from '../../../api/ApiCountryAssign';
import ApiCountryType from "../../../api/ApiCountryType";
import ApiCountry from '../../../api/ApiCountry';
import ApiLawGroup from '../../../api/ApiLawGroup';
import ApiLawGroupSub from '../../../api/ApiLawGroupSub';
import ApiLawAgencies from '../../../api/ApiLawAgencies';
import ApiLawAgencieSub from "../../../api/ApiLawAgencieSub";
import ApiLawType from '../../../api/ApiLawType';
import ApiPlant from '../../../api/ApiPlant';
import ApiDivision from "../../../api/ApiDeivsion";
// import statusData from '../data';

const SearchPanel = ({getData, statusOption = [], page, getDetail}) => {
    const [systemTypeData, setSystemTypeData] = useState([]);
    const [countryAssignData, setCountryAssignData] = useState([]);
    const [countryTypeData, setCountryTypeData] = useState([]);
    const [countryData, setCountryData] = useState([]);
    const [lawGroupData, setLawGroupData] = useState([]);
    const [lawGroupSubData, setLawGroupSubData] = useState([]);
    const [deptData, setDeptData] = useState([]);
    const [deptSubData, setDeptSubData] = useState([]);
    const [lawTypeData, setLawTypeData] = useState([]);
    const [plantData, setPlantData] = useState([]);
    const [departmentData, setDepartmentData] = useState([]);
    // const [statusData, setStatusData] = useState([]);

    const [systemtype, setSystemType] = useState(0);
    const [countryAssign, setCountryAssign] = useState(0);
    const [countryType, setCountryType] = useState("");
    const [country, setCountry] = useState(0);
    const [lawGroup, setLawGroup] = useState(0);
    const [lawGroupSub, setLawGroupSub] = useState("");
    const [dept, setDept] = useState(0);
    const [deptSub, setDeptSub] = useState(0);
    const [lawType, setLawType] = useState(0);
    const [status, setStatusType] = useState('');
    const [name, setName] = useState('');
    const [docNo, setDocNo] = useState('');
    const [plant, setPlant] = useState("");
    const [department, setDepartment] = useState("");
    
    const [qDate, setQDate] = useState({ startDate: null, endDate: null });
    const [qFocused, setQFocused] = useState();

    const [brcsDate, setBrcsDate] = useState({ startDate: null, endDate: null });
    const [brcsFocused, setBrcsFocused] = useState();

    useEffect(() => {
        getSystemTypeOption();
        getCountryAssignOption();
        getCountryTypeOption();
        getCountryOption();
        getLawGroupOption();
        getLawGroupSubOption();
        getDeptOption();
        getDeptSubOption();
        getLawTypeOption();
        getPlantOption();
        getDepartmentOption();
        // getStatusOption();
          return () => {
  
          }
    }, []);

    const getSystemTypeOption = async () => {
        try{
            const result = await ApiSystemType.get();
            if (result.status == 200) {
              setSystemTypeData(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }

    const getPlantOption = async () => {
        try{
            const result = await ApiPlant.get();
            if (result.status == 200) {
              setPlantData(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }

    const getDepartmentOption = async () => {
      try {
        const result = await ApiDivision.get();
        if (result.status == 200) {
          setDepartmentData(result.data);
        }
      } catch (error) {
        console.log(error);
      }
    }

    const getCountryAssignOption = async () => {
        try{
            const result = await ApiCountryAssign.get();
            if (result.status == 200) {
              setCountryAssignData(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }

    const getCountryTypeOption = async () => {
      try {
        const result = await ApiCountryType.get();
        if (result.status == 200) {
          setCountryTypeData(result.data);
        }
      } catch (error) {
        console.log(error);
      }
    }

    const getCountryOption = async () => {
        try{
            const result = await ApiCountry.get();
            if (result.status == 200) {
              setCountryData(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }

    const getLawGroupOption = async () => {
        try{
            const result = await ApiLawGroup.get('');
            if (result.status == 200) {
              setLawGroupData(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }

    const getLawGroupSubOption = async (val) => {
      try {
        var result;
        // console.log("val", val);
        // console.log("lawGroup", lawGroup);
        if (val){
          result = await ApiLawGroupSub.get(val);
        }else{
          if(val == ""){
            result = await ApiLawGroupSub.get(val);
          }else{
            result = await ApiLawGroupSub.get(lawGroup);
          }
        }
        // const result = await ApiLawGroupSub.get(lawGroup);
        if (result.status == 200) {
          setLawGroupSubData(result.data);
        }
      } catch (error) {
        console.log(error);
      }
    };

    const getDeptOption = async () => {
        try{
            const result = await ApiLawAgencies.get();
            if (result.status == 200) {
              setDeptData(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }

    const getDeptSubOption = async (id) => {
      try {
        const result = await ApiLawAgencieSub.get(id);
        if (result.status == 200) {
          setDeptSubData(result.data);
        }
      } catch (error) {
        console.log(error);
      }
    };

    const getLawTypeOption = async () => {
        try{
            const result = await ApiLawType.get();
            if (result.status == 200) {
              setLawTypeData(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }
    
    const onLawAgencieChange = (val) => {
      setDept(val);
      getDeptSubOption(val);
    }

    const onLawGroupChange = (val) => {
      setLawGroup(val);
      getLawGroupSubOption(val);
    }

    // const getStatusOption = async () => {
    //     try{
    //         // const result = await ApiLawType.get();
    //         // if (result.status == 200) {
    //         //   setLawTypeData(result.data);
    //         // }
    //         setStatusData(statusData);
    //     }catch(error){
    //         console.log(error);
    //     }
    // }

    const resetSearchOption = (e,type) => {
        setSystemType(0);
        setCountryAssign(0);
        setCountry(0);
        setLawGroup(0);
        setLawGroupSub(0);
        setDept(0);
        setDeptSub(0);
        setLawType(0);
        setPlant("");
        setDepartment("");
        setStatusType("");
        setName('');
        setDocNo('');
        setQDate({ startDate: null, endDate: null });
        setBrcsDate({ startDate: null, endDate: null });
        const model = {
          "typeReset": "reset"
        };
        getLawGroupSubOption('');
        getData(model);
    }

    const packData = (e, type) => {
      // console.log("department", department);
      // e.preventDefault();
      const model = {
        "systemtype": systemtype,
        "countryAssign": countryAssign,
        "country": country,
        "lawGroup": lawGroup,
        "lawGroupSub": lawGroupSub,
        "dept": dept,
        "deptSub": deptSub,
        "lawType": lawType,
        "status": status,
        "name": name,
        "docNo": docNo,
        "startDate": qDate.startDate,
        "endDate": qDate.endDate,
        "startBrcsDate": brcsDate.startDate,
        "endBrcsDate": brcsDate.endDate,
        "plantCode": plant,
        "departmentCode": department,
      }
      getData(model);
    }

    const exportData = (e) => {
      // e.preventDefault();
      const model = {
        "systemtype": systemtype,
        "countryAssign": countryAssign,
        "country": country,
        "lawGroup": lawGroup,
        "lawGroupSub": lawGroupSub,
        "dept": dept,
        "deptSub": deptSub,
        "lawType": lawType,
        "status": status,
        "name": name,
        "docNo": docNo,
        "startDate": qDate.startDate,
        "endDate": qDate.endDate,
        "startBrcsDate": brcsDate.startDate,
        "endBrcsDate": brcsDate.endDate,
        "plantCode": plant,
        "departmentCode": department,
      }
      getDetail(model);
    }


    return (
    <>
      <CRow>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>ระบบ</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
              <CSelect
                custom
                name="systemtype"
                id="systemtype"
                value={systemtype}
                onChange={(e)=>setSystemType(e.target.value)}
              >
                <option value="">กรุณาเลือก</option>
                {
                  systemTypeData.map((item, index) => {
                    return <option key={item.id} value={item.id}>{item.name}</option>
                  })
                }
              </CSelect>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>แหล่งที่มากฎหมาย</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
            <CSelect
                custom
                name="countryAssign"
                id="countryAssign"
                value={countryAssign}
                onChange={(e)=>setCountryAssign(e.target.value)}
              >
                <option value="">กรุณาเลือก</option>
                {
                  countryTypeData.map((item, index) => {
                    return <option key={item.id} value={item.id}>{item.name}</option>
                  })
                }
              </CSelect>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>ประเทศที่ออกกฎหมาย</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
            <CSelect
                custom
                name="country"
                id="country"
                value={country}
                onChange={(e)=>setCountry(e.target.value)}
              >
                <option value="">กรุณาเลือก</option>
                {
                  countryAssignData.map((item, index) => {
                    return <option key={item.id} value={item.id}>{item.countryName}</option>
                  })
                }
              </CSelect>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>ประเภทกฎหมาย</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
            <CSelect
                custom
                name="lawType"
                id="lawType"
                value={lawType}
                onChange={(e)=>setLawType(e.target.value)}
              >
                <option value="">กรุณาเลือก</option>
                {
                  lawTypeData.map((item, index) => {
                    return <option key={item.id} value={item.id}>{item.name}</option>
                  })
                }
              </CSelect>
            </CCol>
          </CRow>
        </CCol>
      </CRow>
      <CRow className="mt-3">
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>หน่วยงานผู้ออกกฎหมาย</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
              <CSelect
                custom
                name="dept"
                id="dept"
                value={dept}
                onChange={(e)=>onLawAgencieChange(e.target.value)}
              >
                <option value="">กรุณาเลือก</option>
                {
                  deptData.map((item, index) => {
                    return <option key={item.id} value={item.id}>{item.name}</option>
                  })
                }
              </CSelect>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>หน่วยงานย่อยผู้ออกกฎหมาย</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
              <CSelect
                custom
                name="deptSub"
                id="deptSub"
                value={deptSub}
                onChange={(e)=>setDeptSub(e.target.value)}
              >
                <option value="">กรุณาเลือก</option>
                {
                  deptSubData.map((item, index) => {
                    return <option key={item.id} value={item.id}>{item.name}</option>
                  })
                }
              </CSelect>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>กลุ่มของกฎหมาย</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
            <CSelect
                custom
                name="lawGroup"
                id="lawGroup"
                value={lawGroup}
                onChange={(e)=>onLawGroupChange(e.target.value)}
              >
                <option value="">กรุณาเลือก</option>
                {
                  lawGroupData.map((item, index) => {
                    return <option key={item.id} value={item.id}>{item.name}</option>
                  })
                }
              </CSelect>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>กลุ่มของกฎหมายย่อย</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
            <CSelect
                custom
                name="lawGroupSub"
                id="lawGroupSub"
                value={lawGroupSub}
                onChange={(e)=>setLawGroupSub(e.target.value)}
              >
                <option value="">กรุณาเลือก</option>
                {
                  lawGroupSubData.map((item, index) => {
                    return <option key={item.id} value={item.id}>{item.name}</option>
                  })
                }
              </CSelect>
            </CCol>
          </CRow>
        </CCol>
      </CRow>
      <CRow className="mt-3">
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel>
                {" "}
                <b>หมายเลขเอกสาร</b>{" "}
              </CLabel>
            </CCol>
            <CCol col xs="12" md="12">
              <CInput name="docNo" id="docNo" value={docNo} onChange={(e)=>setDocNo(e.target.value)}/>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel>
                {" "}
                <b>ชื่อกฎหมาย</b>{" "}
              </CLabel>
            </CCol>
            <CCol col xs="12" md="12">
              <CInput name="name" id="name" value={name} onChange={(e)=>setName(e.target.value)}/>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel>
                {" "}
                <b>วันที่สร้างเอกสาร</b>{" "}
              </CLabel>
            </CCol>
            <CCol col xs="12" md="12">
              <DateRangePicker
                displayFormat="DD/MM/YYYY"
                small
                block
                align="center"
                startDatePlaceholderText={"จากวันที่"}
                startDate={qDate.startDate}
                startDateId="startQDate"
                endDate={qDate.endDate}
                endDateId="endQDate"
                endDatePlaceholderText={"ถึงวันที่"}
                onDatesChange={(value) => setQDate(value)}
                focusedInput={qFocused}
                onFocusChange={(focusedInput) =>
                  setQFocused(focusedInput)
                }
                orientation="horizontal"
                openDirection="down"
                minimumNights={0}
                isOutsideRange={() => false}
              />
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel>
                {" "}
                <b>วันที่บังคับใช้กฎหมาย</b>{" "}
              </CLabel>
            </CCol>
            <CCol col xs="12" md="12">
              <DateRangePicker
                displayFormat="DD/MM/YYYY"
                small
                block
                align="center"
                startDatePlaceholderText={"จากวันที่"}
                startDate={brcsDate.startDate}
                startDateId="startBrcsDate"
                endDate={brcsDate.endDate}
                endDateId="endBrcsDate"
                endDatePlaceholderText={"ถึงวันที่"}
                onDatesChange={(value) => setBrcsDate(value)}
                focusedInput={brcsFocused}
                onFocusChange={(focusedInput) =>
                  setBrcsFocused(focusedInput)
                }
                orientation="horizontal"
                openDirection="down"
                minimumNights={0}
                isOutsideRange={() => false}
              />
            </CCol>
          </CRow>
        </CCol>
      </CRow>   
      <CRow className="mt-3">
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>สถานะเอกสาร</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
            <CSelect
                custom
                name="status"
                id="status"
                value={status}
                onChange={(e)=>setStatusType(e.target.value)}
              >
                <option value="">กรุณาเลือก</option>
                {statusOption.map((item, index) => {
                    return (
                      <option key={item.value} value={item.value}>
                        {item.label}
                      </option>
                    );
                  })}
              </CSelect>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>โรงงาน</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
            <CSelect
                custom
                name="plant"
                id="plant"
                value={plant}
                onChange={(e)=>setPlant(e.target.value)}
              >
                <option value="">กรุณาเลือก</option>
                {
                  plantData.map((item, index) => {
                    return <option key={item.plant} value={item.plant}>{item.siteDescription}({item.plant})</option>
                  })
                }
              </CSelect>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>แผนก</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
            <CSelect
                custom
                name="department"
                id="department"
                value={department}
                onChange={(e)=>setDepartment(e.target.value)}
              >
                <option value="">กรุณาเลือก</option>
                {
                  departmentData.map((item, index) => {
                    return <option key={item.code} value={item.code}>{item.description}</option>
                  })
                }
              </CSelect>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3" className="mt-4">
          <CRow className="justify-content-end">
            <CCol sm="12" md="6" className="justify-content-center">
              <CButton
                color="danger"
                variant="outline"
                block
                onClick={() => resetSearchOption()}
              >
                <b> ล้างข้อมูล </b>{" "}
              </CButton>
            </CCol>
        {page == "report5" ? 
            <CCol sm="12" md="6" className="justify-content-center">
                <CButton
                    color="success"
                    variant="outline"
                    block
                    onClick={() => exportData()}
                >
                    <CIcon name="cil-search" style={{ marginRight: "2px" }} />{" "}
                    <b> Export Excel </b>
                </CButton>
            </CCol>
            : 
            <CCol sm="12" md="6" className="justify-content-center">
              <CButton
                color="success"
                variant="outline"
                block
                onClick={() => packData()}
              >
                <CIcon name="cil-search" style={{ marginRight: "2px" }} />{" "}
                <b> ค้นหา </b>
              </CButton>
            </CCol>}
          </CRow>
        </CCol>
      </CRow>   
    </>
  );
};

export default SearchPanel;