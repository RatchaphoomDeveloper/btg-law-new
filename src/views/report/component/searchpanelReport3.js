import React, { useEffect, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CNav,
  CForm,
  CContainer,
  CLabel,
  CSelect,
  CInput,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import ApiSystemType from '../../../api/ApiSystemType';
import ApiLawGroup from '../../../api/ApiLawGroup';
import ApiLawAgencies from '../../../api/ApiLawAgencies';
import ApiPlant from '../../../api/ApiPlant';
import ApiDivision from "../../../api/ApiDeivsion";

const SearchPanel = ({getData,}) => {
    const [systemTypeData, setSystemTypeData] = useState([]);
    const [lawGroupData, setLawGroupData] = useState([]);
    const [deptData, setDeptData] = useState([]);
    const [plantData, setPlantData] = useState([]);
    const [departmentData, setDepartmentData] = useState([]);

    const [systemtype, setSystemType] = useState(0);
    const [lawGroup, setLawGroup] = useState(0);
    const [dept, setDept] = useState(0);
    const [name, setName] = useState('');
    const [plant, setPlant] = useState("");
    const [department, setDepartment] = useState("");
    
    const [qDate, setQDate] = useState({ startDate: null, endDate: null });
    const [qFocused, setQFocused] = useState();

    useEffect(() => {
        getSystemTypeOption();
        getLawGroupOption();
        getDeptOption();
        getPlantOption();
        getDepartmentOption();
        // getStatusOption();
        return () => {

        }
    }, []);

    const getSystemTypeOption = async () => {
        try{
            const result = await ApiSystemType.get();
            if (result.status == 200) {
              setSystemTypeData(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }

    const getPlantOption = async () => {
        try{
            const result = await ApiPlant.get();
            if (result.status == 200) {
              setPlantData(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }

    const getDepartmentOption = async () => {
      try {
        const result = await ApiDivision.get();
        if (result.status == 200) {
          setDepartmentData(result.data);
        }
      } catch (error) {
        console.log(error);
      }
    }

    const getLawGroupOption = async () => {
        try{
            const result = await ApiLawGroup.get('');
            if (result.status == 200) {
              setLawGroupData(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }

    const getDeptOption = async () => {
        try{
            const result = await ApiLawAgencies.get();
            if (result.status == 200) {
              setDeptData(result.data);
            }
        }catch(error){
            console.log(error);
        }
    }

    const resetSearchOption = (e,type) => {
        setSystemType(0);
        setLawGroup(0);
        setDept(0);
        setName('');
        setPlant("");
        setDepartment("");
        setQDate({ startDate: null, endDate: null });
        const model = {
          "typeReset": "reset"
        };
        getData(model);
    }

    const packData = (e) => {
      // e.preventDefault();
      const model = {
        "systemtype": systemtype,
        "lawGroup": lawGroup,
        "dept": dept,
        "name": name,
        "startDate": qDate.startDate,
        "endDate": qDate.endDate,
        "plantCode": plant,
        "departmentCode": department,
      }
      getData(model);
    }


    return (
    <>
      <CRow>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>ระบบ</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
              <CSelect
                custom
                name="systemtype"
                id="systemtype"
                value={systemtype}
                onChange={(e)=>setSystemType(e.target.value)}
              >
                <option value="">กรุณาเลือก</option>
                {
                  systemTypeData.map((item, index) => {
                    return <option key={item.id} value={item.id}>{item.name}</option>
                  })
                }
              </CSelect>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>หน่วยงานผู้ออกกฎหมาย</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
              <CSelect
                custom
                name="dept"
                id="dept"
                value={dept}
                onChange={(e)=>setDept(e.target.value)}
              >
                <option value="">กรุณาเลือก</option>
                {
                  deptData.map((item, index) => {
                    return <option key={item.id} value={item.id}>{item.name}</option>
                  })
                }
              </CSelect>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>กลุ่มของกฎหมาย</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
            <CSelect
                custom
                name="lawGroup"
                id="lawGroup"
                value={lawGroup}
                onChange={(e)=>setLawGroup(e.target.value)}
              >
                <option value="">กรุณาเลือก</option>
                {
                  lawGroupData.map((item, index) => {
                    return <option key={item.id} value={item.id}>{item.name}</option>
                  })
                }
              </CSelect>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel>
                {" "}
                <b>ชื่อกฎหมาย</b>{" "}
              </CLabel>
            </CCol>
            <CCol col xs="12" md="12">
              <CInput name="name" id="name" value={name} onChange={(e)=>setName(e.target.value)}/>
            </CCol>
          </CRow>
        </CCol>
      </CRow>
      {/* <CRow className="mt-3">
      </CRow> */}
      <CRow className="mt-3">
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel>
                {" "}
                <b>วันที่สร้างเอกสาร</b>{" "}
              </CLabel>
            </CCol>
            <CCol col xs="12" md="12">
              <DateRangePicker
                displayFormat="DD/MM/YYYY"
                small
                block
                align="center"
                startDatePlaceholderText={"จากวันที่"}
                startDate={qDate.startDate}
                startDateId="startQDate"
                endDate={qDate.endDate}
                endDateId="endQDate"
                endDatePlaceholderText={"ถึงวันที่"}
                onDatesChange={(value) => setQDate(value)}
                focusedInput={qFocused}
                onFocusChange={(focusedInput) =>
                  setQFocused(focusedInput)
                }
                orientation="horizontal"
                openDirection="down"
                minimumNights={0}
                isOutsideRange={() => false}
              />
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>โรงงาน</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
            <CSelect
                custom
                name="plant"
                id="plant"
                value={plant}
                onChange={(e)=>setPlant(e.target.value)}
              >
                <option value="">กรุณาเลือก</option>
                {
                  plantData.map((item, index) => {
                    return <option key={item.plant} value={item.plant}>{item.siteDescription}({item.plant})</option>
                  })
                }
              </CSelect>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3">
          <CRow>
            <CCol sm="12" md="12">
              <CLabel htmlFor="Priority">
                {" "}
                <b>แผนก</b>{" "}
              </CLabel>
            </CCol>
            <CCol sm="12" md="12">
            <CSelect
                custom
                name="department"
                id="department"
                value={department}
                onChange={(e)=>setDepartment(e.target.value)}
              >
                <option value="">กรุณาเลือก</option>
                {
                  departmentData.map((item, index) => {
                    return <option key={item.code} value={item.code}>{item.description}</option>
                  })
                }
              </CSelect>
            </CCol>
          </CRow>
        </CCol>
        <CCol sm="12" md="3" className="mt-4">
          <CRow className="justify-content-end">
            <CCol sm="12" md="6" className="justify-content-center">
              <CButton
                color="danger"
                variant="outline"
                block
                onClick={() => resetSearchOption()}
              >
                <b> ล้างข้อมูล </b>{" "}
              </CButton>
            </CCol>
            <CCol sm="12" md="6" className="justify-content-center">
              <CButton
                color="success"
                variant="outline"
                block
                onClick={() => packData()}
              >
                <CIcon name="cil-search" style={{ marginRight: "2px" }} />{" "}
                <b> ค้นหา </b>
              </CButton>
            </CCol>
          </CRow>
        </CCol>
      </CRow>   
      {/* <CRow className="mt-3">
      </CRow>    */}
    </>
  );
};

export default SearchPanel;