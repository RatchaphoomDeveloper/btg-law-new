export const tableData = [
    { id: 0, no: 1, docNo: 'MS-MOL-DNT-001-H507-HR-2020-01', spec: '1.ประกาศสำนักงานประกันสังคม เรื่อง การยื่นแบบคำขอทำธุรกรรมทางอิเล็กทรอนิกส์ (ฉบับที่ ๒) พ.ศ. ๒๕๖๒', activeDate: "", plant: "", result: "", description: '', rateDate: ""},
    { id: 1, no: 2, docNo: 'MS-MOL-DNT-001-H507-HR-2020-02', spec: '2.ประกาศสำนักงานประกันสังคม เรื่อง การยื่นแบบคำขอทำธุรกรรมทางอิเล็กทรอนิกส์ (ฉบับที่ ๒) พ.ศ. ๒๕๖๒', activeDate: "", plant: "", result: "", description: '', rateDate: ""},
    { id: 2, no: 3, docNo: 'MS-MOL-DNT-001-H507-HR-2020-03', spec: '3.ประกาศสำนักงานประกันสังคม เรื่อง การยื่นแบบคำขอทำธุรกรรมทางอิเล็กทรอนิกส์ (ฉบับที่ ๒) พ.ศ. ๒๕๖๒', activeDate: "", plant: "", result: "", description: '', rateDate: ""},
]

export const statusData = [
    { id: 0, name: 'Active', code: "A" },
    { id: 1, name: 'In Active', code: "I" },
]
