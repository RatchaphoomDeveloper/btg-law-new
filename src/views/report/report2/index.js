import React, { useEffect, useState } from 'react'
import {
    CBadge,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CModal,
    CModalHeader,
    CModalFooter,
    CButton,
    CNav,
    CForm,
    CLabel,
    CSelect,
    CInput,
} from '@coreui/react'
import CIcon from "@coreui/icons-react";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import Api from '../../../api/ApiReport';
// import ApiSystemType from '../../../api/ApiSystemType';
// import ApiLawGroup from '../../../api/ApiLawGroup';
// import ApiLawAgencies from '../../../api/ApiLawAgencies';
// import ApiPlant from '../../../api/ApiPlant';
import Tables from './Tables';
// import { tableData } from './data';
import SearchPanel from "../component/searchpanel";
import { REPORT_API, WEB_API } from "../../../env";
import axios from "axios";
import Swal from "sweetalert2/src/sweetalert2.js";
import { useDispatch, useSelector } from "react-redux";

const Report2 = () => {
    const [dataTable, setDataTable] = useState([]);
    const [searchData, setSearchData] = useState([]);
    const userState = useSelector((state) => state.changeState.user);
    // const [systemTypeData, setSystemTypeData] = useState([]);
    // const [deptData, setDeptData] = useState([]);
    // const [lawGroupData, setLawGroupData] = useState([]);
    // const [plantData, setPlantData] = useState([]);
    
    // const [cDate, setCDate] = useState({ startDate: null, endDate: null });
    // const [cFocused, setCFocused] = useState();
    
    // const [rDate, setRDate] = useState({ startDate: null, endDate: null });
    // const [rFocused, setRFocused] = useState();

  const [statusData, setStatusData] = useState([]);
  const statusOption = [
    { value: "SADR", label: "Save" },
    { value: "NEDO", label: "Submit" },
    { value: "CACE", label: "Delete" },
    { value: "SEEV", label: "Send To Evaluate" },
    { value: "REFQ", label: "Reject To FQO" },
    { value: 'SEA1', label: 'Send To Verification'},
    // { value: "REBU", label: "Reject To BU" },
    { value: "SEA2", label: "Send To Approver" },
    { value: "REDI", label: "Reject To User Division" },
    { value: "DOAP", label: "Document Approve" },
    // { value: "REA1", label: "Reject To Approver 1" },
  ];

    useEffect(() => {
        setStatusData(statusOption);
        getData();
        return () => {
  
          }
      }, []);
  
      const getData = async (data) => {
          try{
              console.log("data", data);if (data == undefined) {
                  data = {
                    page: "frequency",
                    role: userState.role,
                };
                } else {
                    data.page = "frequency";
                    data.role = userState.role;
                }
              setSearchData(data);
              // axios.defaults.baseURL = REPORT_API;
                Swal.showLoading();
                const result = await Api.Report2(data);
                Swal.close();
            // axios.defaults.baseURL = WEB_API;
              if (result.status == 200) {
                  setDataTable(result.data);
                  // window.open(result.data.responseMessage, "_blank")
              }
          }catch(error){
              console.log(error);
          }
      }
    
      const getDetail = async (data) => {
          try{
              // console.log("data", data);
              // if (data == undefined) {
              //     data = {
              //         page: "frequency",
              //     };
              // } else {
              //     data.page = "frequency";
              // }
              // setSearchData(data);
              // axios.defaults.baseURL = REPORT_API;
                    data.role = userState.role;
                    Swal.showLoading();
                const result = await Api.ExportReport2(data);
                Swal.close();
            // axios.defaults.baseURL = WEB_API;
                if (result.status == 200) {
                  // setDataTable(result.data);
                  window.open(result.data, "_blank")
              }
          }catch(error){
              console.log(error);
          }
      }

    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardHeader>
                            <b> รายงานผลการประเมินระดับโรงงาน ({dataTable.length}) </b>
                            <div style={{ fontSize: "12px" }}>
                                <SearchPanel getData={getData} statusOption={statusData} />     
                            </div>
                        </CCardHeader>
                        <CCardBody>
                            <Tables refreshData={getData} data={dataTable} getDetail={getDetail}/>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    )
}

export default Report2;