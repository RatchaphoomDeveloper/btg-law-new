export const lawData = [
    { id: 0, code: 'M01', name: 'Management', statuscode: 'P', status: 'Pending', editby: "Admin", editdate: "12/05/2020" },
    { id: 1, code: 'Q01', name: 'Quality',  statuscode: 'A', status: 'Active', editby: "Admin", editdate: "12/05/2020" },
    { id: 2, code: 'M01', name: 'Management',  statuscode: 'B', status: 'Banned', editby: "Admin", editdate: "12/05/2020" },
    { id: 3, code: 'Q01', name: 'Quality',  statuscode: 'I', status: 'Inactive', editby: "Admin", editdate: "12/05/2020" },
]

export const statusData = [
    { id: 0, name: 'Active', code: "A" },
    { id: 1, name: 'In Active', code: "I" },
]

