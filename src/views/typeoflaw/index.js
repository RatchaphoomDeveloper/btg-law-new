import React, { useEffect, useState } from 'react'
import {
    CBadge,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CModal,
    CModalHeader,
    CModalFooter,
    CButton
} from '@coreui/react'
import Api from '../../api/ApiTypeOfLaw';
import { lawData } from './data';
import Tables from './Tables';

const TypeofLaw = () => {
    const [dataTable, setDataTable] = useState([]);
    useEffect(() => {
        getData();
          return () => {
  
          }
      }, []);
  
      const getData = async () => {
          try{
              const result = await Api.get();
              if (result.status == 200) {
                  setDataTable(result.data);
              }
          }catch(error){
              console.log(error);
          }
      }
    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardHeader>
                            <b> ประเภทของกฏหมาย </b>
                        </CCardHeader>
                        <CCardBody>
                            <Tables refreshData={getData} data={dataTable} />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    )
}

export default TypeofLaw;