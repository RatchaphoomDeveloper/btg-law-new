import React, { useState, useEffect } from "react";
import {
  CBadge,
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CImg,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { useHistory, useLocation } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2";
import { removeLocalStorage, setLocalStorage } from "../utils/localStorage";
import Api from "../api/ApiLogin";

const logoBetagro = require("./../assets/icons/icon_logo.png");

const TheHeaderDropdown = (props) => {
  const dispatch = useDispatch();
  const userState = useSelector((state) => state.changeState.user);

  const history = useHistory();

  // useEffect(() => {
  //   console.log("userState", userState);

  // }, []);

  const changeRole = (item) => {
    Swal.fire({
      title: "",
      text: `Change Role To ${item}`,
      icon: "question",
      showCancelButton: true,
      cancelButtonText: "Cancel",
      confirmButtonText: "Confirm",
    }).then((result) => {
      if (result.isConfirmed) {
        const user = userState;
        // console.log("user", user);
        user.role = item;
        setLocalStorage("role", item);
        dispatch({ type: "set_user", user: user });
        setTimeout(() => {
          //   window.location.reload(true);
          window.location.href = "/home/dashboard";
          history.push('/home/dashboard')
          history.go(0);
        }, 500);
      }
    });
  };

  const onLogout = async () => {
    const model = {
      user: userState.id,
    };
    Swal.showLoading();
    const result = await Api.logout(model);
    Swal.close();
    removeLocalStorage("token");
    setTimeout(() => {
      dispatch({ type: "set_user", user: {} });
      history.push("/timeout")
    }, 200);
  }

  return (
    <CDropdown inNav className="c-header-nav-items mx-2" direction="down">
      <CDropdownToggle className="c-header-nav-link" caret={false}>
        <span className="mr-2">
          {userState.fullname}, {userState.role}
        </span>
        <div className="c-avatar">
          <CImg
            src={logoBetagro}
            className="c-avatar-img"
            alt="admin@bootstrapmaster.com"
          />
        </div>
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        {/* <CDropdownItem
                    header
                    tag="div"
                    color="light"
                    className="text-center"
                >
                    <strong>ผู้ใช้งาน : {userState.fullname}, {userState.role}</strong>
                </CDropdownItem> */}
        {/* <CDropdownItem>
          <CIcon name="cil-bell" className="mfe-2" />
          Updates
          <CBadge color="info" className="mfs-auto">42</CBadge>
        </CDropdownItem>
        <CDropdownItem>
          <CIcon name="cil-envelope-open" className="mfe-2" />
          Messages
          <CBadge color="success" className="mfs-auto">42</CBadge>
        </CDropdownItem>
        <CDropdownItem>
          <CIcon name="cil-task" className="mfe-2" />
          Tasks
          <CBadge color="danger" className="mfs-auto">42</CBadge>
        </CDropdownItem>
        <CDropdownItem>
          <CIcon name="cil-comment-square" className="mfe-2" />
          Comments
          <CBadge color="warning" className="mfs-auto">42</CBadge>
        </CDropdownItem> */}
        {/* <CDropdownItem divider /> */}
        {userState.roles.map((item, index) => (
          <CDropdownItem
            onClick={() => (item == userState.role ? null : changeRole(item))}
          >
            {item == userState.role && (
              <CIcon
                style={{ color: "#8CC63F" }}
                color="success"
                name="cil-check-alt"
                className="mfe-2"
              />
            )}
            {item}
          </CDropdownItem>
        ))}
        <CDropdownItem onClick={() => onLogout()}>
          <CIcon name="cil-lock-locked" className="mfe-2" />
          ออกจากระบบ
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  );
};

export default TheHeaderDropdown;
