export default [
  {
    _tag: "CSidebarNavTitle",
    mustfilter: "false",
    _children: ["จัดการข้อมูล"],
  },
  {
    _tag: "CSidebarNavDropdown",
    name: "กฎหมาย",
    route: "/home/lawinfo",
    icon: "cil-notes",
    mustfilter: "true",
    _children: [
      {
        _tag: "CSidebarNavItem",
        name: "สร้างเอกสาร",
        content: "สร้างเอกสาร",
        to: "/home/lawinfo/search",
        className: "c-sidebar-nav-item-sub",
        role: ["FQO"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "นำเข้าเอกสาร",
        content: "นำเข้าเอกสาร",
        to: "/home/lawinfo/import",
        className: "c-sidebar-nav-item-sub",
        role: ["FQO"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "Assign More Bu",
        content: "Assign More Bu",
        to: "/home/lawinfo/assignMoreBu/search",
        className: "c-sidebar-nav-item-sub",
        role: ["FQO"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "ใส่ความถี่",
        content: "ใส่ความถี่",
        to: "/home/lawinfo/addfrequency/search",
        className: "c-sidebar-nav-item-sub",
        role: ["BU"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "Assign More Plant",
        content: "Assign More Plant",
        to: "/home/lawinfo/assignMorePlant/search",
        className: "c-sidebar-nav-item-sub",
        role: ["BU"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "ประเมินความสอดคล้อง",
        content: "ประเมินความสอดคล้อง",
        to: "/home/lawinfo/assessConsistency/search",
        className: "c-sidebar-nav-item-sub",
        role: ["USER"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "Re-Evaluate",
        content: "Re-Evaluate",
        to: "/home/lawinfo/reEvaluate/search",
        className: "c-sidebar-nav-item-sub",
        role: ["BU"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "ทวนสอบเอกสาร",
        content: "ทวนสอบเอกสาร",
        to: "/home/lawinfo/approve1/search",
        className: "c-sidebar-nav-item-sub",
        role: ["VERIFICATION"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "อนุมัติ",
        content: "อนุมัติ",
        to: "/home/lawinfo/approve2/search",
        className: "c-sidebar-nav-item-sub",
        role: ["APPROVER"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "การจัดการแผนการแก้ไขป้องกัน",
        content: "การจัดการแผนการแก้ไขป้องกัน",
        to: "/home/lawinfo/manageActionPlan/search",
        className: "c-sidebar-nav-item-sub",
        role: ["USER", "VERIFICATION", "APPROVER"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "ค้นหาเอกสาร",
        content: "ค้นหาเอกสาร",
        to: "/home/lawinfo/searchOnly/search",
        className: "c-sidebar-nav-item-sub",
        role: ["FQO", "VISITOR"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "ค้นหาเอกสาร",
        content: "ค้นหาเอกสาร",
        to: "/home/lawinfo/searchFrequency/search",
        className: "c-sidebar-nav-item-sub",
        role: ["BU"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "ค้นหาเอกสาร (FQO, BU)",
        content: "ค้นหาเอกสาร (FQO, BU)",
        to: "/home/lawinfo/searchOnly/search",
        className: "c-sidebar-nav-item-sub",
        role: ["ADMIN"],
      },
      // {
      //   _tag: "CSidebarNavItem",
      //   name: "ค้นหาเอกสาร (BU)",
      //   content: "ค้นหาเอกสาร (BU)",
      //   to: "/home/lawinfo/searchFrequency/search",
      //   className: "c-sidebar-nav-item-sub",
      //   role: ["ADMIN"],
      // },
      // {
      //   _tag: "CSidebarNavItem",
      //   name: "ค้นหาเอกสาร",
      //   content: "ค้นหาเอกสาร",
      //   to: "/home/lawinfo/searchAssess/search",
      //   className: "c-sidebar-nav-item-sub",
      //   role: ["USER", "VERIFICATION", "APPROVER"],
      // },
      {
        _tag: "CSidebarNavItem",
        name: "ค้นหาเอกสาร (รายการประเมิน)",
        content: "ค้นหาเอกสาร (รายการประเมิน)",
        to: "/home/lawinfo/searchAssess/search",
        className: "c-sidebar-nav-item-sub",
        role: ["ADMIN","USER", "VERIFICATION", "APPROVER","FQO", "BU"],
      },
      // {
      //   _tag: "CSidebarNavItem",
      //   name: "ค้นหาเอกสาร (รายการประเมิน)",
      //   content: "ค้นหาเอกสาร (รายการประเมิน)",
      //   to: "/home/lawinfo/searchAssess/search",
      //   className: "c-sidebar-nav-item-sub",
      //   role: ["ADMIN"],
      // },
      {
        _tag: "CSidebarNavItem",
        name: "ทบทวนเอกสาร",
        content: "ทบทวนเอกสาร",
        to: "/home/lawinfo/reviewDocument/search",
        className: "c-sidebar-nav-item-sub",
        role: ["BU"],
      },
    ],
  },
  {
    _tag: "CSidebarNavTitle",
    mustfilter: "false",
    _children: ["รายงาน"],
  },
  {
    _tag: "CSidebarNavDropdown",
    name: "รายงาน",
    route: "/home/report",
    icon: "cil-notes",
    mustfilter: "true",
    _children: [
      {
        _tag: "CSidebarNavItem",
        name: "ผลการประเมินระดับแผนก",
        content: "ผลการประเมินระดับแผนก",
        to: "/home/report/report1",
        className: "c-sidebar-nav-item-sub",
        role: ["ADMIN", "FQO", "BU", "USER", "VERIFICATION", "APPROVER", "VISITOR"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "ผลการประเมินระดับโรงงาน",
        content: "ผลการประเมินระดับโรงงาน",
        to: "/home/report/report2",
        className: "c-sidebar-nav-item-sub",
        role: ["ADMIN", "FQO", "BU", "USER", "VERIFICATION", "APPROVER", "VISITOR"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "ผลการทบทวนการประเมินระดับโรงงาน",
        content: "ผลการทบทวนการประเมินระดับโรงงาน",
        to: "/home/report/report3",
        className: "c-sidebar-nav-item-sub",
        role: ["ADMIN", "FQO", "BU", "USER", "VERIFICATION", "APPROVER", "VISITOR"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "ผลการแก้ไขป้องกัน",
        content: "ผลการแก้ไขป้องกัน",
        to: "/home/report/report4",
        className: "c-sidebar-nav-item-sub",
        role: ["ADMIN", "FQO", "BU", "USER", "VERIFICATION", "APPROVER", "VISITOR"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "รายงานจัดการข้อมูลกฎหมาย",
        content: "รายงานจัดการข้อมูลกฎหมาย",
        to: "/home/report/report5",
        className: "c-sidebar-nav-item-sub",
        role: ["ADMIN", "FQO", "BU", "USER", "VERIFICATION", "APPROVER", "VISITOR"],
      },
    ],
  },
  {
    _tag: "CSidebarNavDropdown",
    name: "รายงาน รูปแบบกราฟ",
    route: "/home/graph",
    icon: "cil-notes",
    mustfilter: "true",
    _children: [
      {
        _tag: "CSidebarNavItem",
        name: "รายงาน Regulation Compliance",
        content: "รายงาน Regulation Compliance",
        to: "/home/graph/report1",
        className: "c-sidebar-nav-item-sub",
        role: ["ADMIN", "FQO", "BU", "USER", "VERIFICATION", "APPROVER", "VISITOR"],
        // role: ["ADMIN", "FQO"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "รายงาน Regulation Risk",
        content: "รายงาน Regulation Risk",
        to: "/home/graph/report2",
        className: "c-sidebar-nav-item-sub",
        role: ["ADMIN", "FQO", "BU", "USER", "VERIFICATION", "APPROVER", "VISITOR"],
        // role: ["ADMIN", "FQO"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "รายงาน Overdue Action Plan",
        content: "รายงาน Overdue Action Plan",
        to: "/home/graph/report3",
        className: "c-sidebar-nav-item-sub",
        role: ["ADMIN", "FQO", "BU", "USER", "VERIFICATION", "APPROVER", "VISITOR"],
        // role: ["ADMIN", "FQO"],
      },
    ],
  },
  {
    _tag: "CSidebarNavTitle",
    mustfilter: "false",
    _children: ["ตั้งค่า"],
  },
  // {
  //   _tag: 'CSidebarNavDropdown',
  //   name: 'กฎหมาย',
  //   route: '/home/lawsetting',
  //   icon: 'cil-settings',
  //   mustfilter: 'true',
  //   _children: [
  //     // {
  //     //   _tag: 'CSidebarNavItem',
  //     //   name: 'ประเภทกฏหมาย',
  //     //   content: 'ประเภทกฏหมาย',
  //     //   to: '/home/lawsetting/lawtype',
  //     //   className: 'c-sidebar-nav-item-sub',
  //     //   role: ['ADMIN']
  //     // },
  //     // {
  //     //   _tag: 'CSidebarNavItem',
  //     //   name: 'หน่วยงานผู้ออกกฎหมาย',
  //     //   content: 'หน่วยงานผู้ออกกฎหมาย',
  //     //   to: '/home/lawsetting/lawagencies',
  //     //   className: 'c-sidebar-nav-item-sub',
  //     //   role: ['ADMIN']
  //     // },
  //     // {
  //     //   _tag: 'CSidebarNavItem',
  //     //   name: 'หน่วยงานย่อยผู้ออกกฎหมาย',
  //     //   content: 'หน่วยงานย่อยผู้ออกกฎหมาย',
  //     //   to: '/home/lawsetting/lawagenciesub',
  //     //   className: 'c-sidebar-nav-item-sub',
  //     //   role: ['ADMIN']
  //     // },
  //     // {
  //     //   _tag: 'CSidebarNavItem',
  //     //   name: 'ชื่อกลุ่มกฏหมาย',
  //     //   content: 'ชื่อกลุ่มกฏหมาย',
  //     //   to: '/home/master/legalgroupname',
  //     //   className: 'c-sidebar-nav-item-sub',
  //     //   role: ['ADMIN']
  //     // },
  //   ]
  // },
  {
    _tag: "CSidebarNavDropdown",
    name: "Admin",
    route: "/home/master",
    icon: "cil-settings",
    mustfilter: "true",
    _children: [
      {
        _tag: "CSidebarNavItem",
        name: "ประเภทของระบบ",
        content: "systemtype",
        to: "/home/master/systemtype",
        className: "c-sidebar-nav-item-sub",
        role: ["ADMIN"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "กลุ่มของกฎหมาย",
        content: "lawgroup",
        to: "/home/master/lawgroup",
        className: "c-sidebar-nav-item-sub",
        role: ["ADMIN"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "แหล่งที่มาของกฎหมาย",
        content: "แหล่งที่มาของกฎหมาย",
        to: "/home/master/countrytype",
        className: "c-sidebar-nav-item-sub",
        role: ["ADMIN"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "ประเทศที่ออกกฎหมาย",
        content: "ประเทศที่ออกกฎหมาย",
        to: "/home/master/countryassign",
        className: "c-sidebar-nav-item-sub",
        role: ["ADMIN"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "รหัสประเทศ",
        content: "รหัสประเทศ",
        to: "/home/master/country",
        className: "c-sidebar-nav-item-sub",
        role: ["ADMIN"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "หน่วยงานผู้ออกกฎหมาย",
        content: "หน่วยงานผู้ออกกฎหมาย",
        to: "/home/lawsetting/lawagencies",
        className: "c-sidebar-nav-item-sub",
        role: ["ADMIN"],
      },
     
      {
        _tag: "CSidebarNavItem",
        name: "ประเภทกฏหมาย",
        content: "ประเภทกฏหมาย",
        to: "/home/lawsetting/lawtype",
        className: "c-sidebar-nav-item-sub",
        role: ["ADMIN"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "กลุ่มธุรกิจ",
        content: "กลุ่มธุรกิจ",
        to: "/home/master/bu",
        className: "c-sidebar-nav-item-sub",
        role: ["ADMIN"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "โรงงาน",
        content: "โรงงาน",
        to: "/home/master/plant",
        className: "c-sidebar-nav-item-sub",
        role: ["ADMIN"],
      },
      // {
      //   _tag: 'CSidebarNavItem',
      //   name: 'Site',
      //   content: 'site',
      //   to: '/home/master/site',
      //   className: 'c-sidebar-nav-item-sub',
      //   role: ['ADMIN']
      // },
      {
        _tag: "CSidebarNavItem",
        name: "หน่วยงาน",
        content: "หน่วยงาน",
        to: "/home/master/devision",
        className: "c-sidebar-nav-item-sub",
        role: ["ADMIN"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "Email Template",
        content: "Email Template",
        to: "/home/master/emailtemplate",
        className: "c-sidebar-nav-item-sub",
        role: ["ADMIN"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "Plant Assignment",
        content: "Plant Assignment",
        to: "/home/master/buAssign",
        className: "c-sidebar-nav-item-sub",
        role: ["BU", "FQO", "ADMIN"],
      },
    ],
  },
  // {
  //   _tag: 'CSidebarNavDropdown',
  //   name: 'ตั้งค่า BU',
  //   route: '/home/master',
  //   icon: 'cil-settings',
  //   mustfilter: 'true',
  //   _children: [
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'BuAssign',
  //       content: 'BuAssign',
  //       to: '/home/master/buAssign',
  //       className: 'c-sidebar-nav-item-sub',
  //       role: ['BU', 'ADMIN']
  //     },
  //   ]
  // },
  {
    _tag: "CSidebarNavDropdown",
    name: "ข้อมูลพื้นฐานผู้ใช้ระบบ",
    route: "/home/usersetting",
    icon: "cil-settings",
    mustfilter: "true",
    role: ["ADMIN"],
    _children: [
      {
        _tag: "CSidebarNavItem",
        name: "ประเภทผู้ใช้งาน",
        content: "ประเภทผู้ใช้งาน",
        to: "/home/usersetting/role",
        className: "c-sidebar-nav-item-sub",
        role: ["ADMIN"],
      },
      {
        _tag: "CSidebarNavItem",
        name: "จัดการข้อมูลผู้ใช้งาน",
        content: "จัดการข้อมูลผู้ใช้งาน",
        to: "/home/usersetting/usermanagement",
        className: "c-sidebar-nav-item-sub",
        role: ["ADMIN"],
      },
      // {
      //   _tag: 'CSidebarNavItem',
      //   name: 'Standard',
      //   content: 'standart',
      //   to: '/home/master/standart',
      //   className: 'c-sidebar-nav-item-sub'
      // },
    ],
  },
  {
    _tag: "CSidebarNavDropdown",
    name: "การตั้งค่าระบบ",
    route: "/home/masterconfig",
    icon: "cil-settings",
    mustfilter: "true",
    role: ["ADMIN"],
    _children: [
      {
        _tag: "CSidebarNavItem",
        name: "การตั้งค่าระบบ",
        content: "การตั้งค่าระบบ",
        to: "/home/masterconfig",
        className: "c-sidebar-nav-item-sub",
        role: ["ADMIN"],
      },
    ],
  },
];
