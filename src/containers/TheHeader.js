import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useHistory, useLocation } from "react-router-dom";
import {
  CHeader,
  CHeaderBrand,
  CHeaderNav,
  CHeaderNavItem,
  CHeaderNavLink,
  CSubheader,
  CToggler,
  CBreadcrumbRouter,
  CImg,
  CButton,
  CLink
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { adfs_redirect_url, adfs_client_id } from "../env";

// routes config
import routes from '../routes'
import {
  TheHeaderDropdown
} from './index'
const logoBetagro = require('./../assets/icons/icon_logo.png');

const TheHeader = () => {
  const dispatch = useDispatch()
  const userState = useSelector((state) => state.changeState.user);
  const asideShow = useSelector(state => state.changeState.asideShow)
  const darkMode = useSelector(state => state.changeState.darkMode)
  const sidebarShow = useSelector(state => state.changeState.sidebarShow)
  const history = useHistory();

  const toggleSidebar = () => {
    const val = [true, 'responsive'].includes(sidebarShow) ? false : 'responsive'
    dispatch({ type: 'set', sidebarShow: val })
  }

  const toggleSidebarMobile = () => {
    const val = [false, 'responsive'].includes(sidebarShow) ? true : 'responsive'
    dispatch({ type: 'set', sidebarShow: val })
  }


  function renderHeaderMenu(){
    console.log("userState", userState);
    if(Object.keys(userState).length > 0){
      return(
        <TheHeaderDropdown />
      );
    }else{
      return(
        <>
          <CButton color="success" size="sm" variant="outline" className="mr-2" onClick={() => history.push("/login")}>
            <label style={{color: "#fff", fontSize: "14px", cursor: "pointer", marginBottom: "5px"}}>เข้าสู่ระบบแบบเก่า</label>
          </CButton>
          <CLink href={`https://login.microsoftonline.com/eb1f94dc-8d25-4c67-985c-04be74c8f698/oauth2/v2.0/authorize?client_id=${adfs_client_id}&response_type=token&redirect_uri=${adfs_redirect_url}&scope=https%3A%2F%2Fgraph.microsoft.com%2F.default%20&state=12345`}>
            <CButton color="success" size="sm" variant="outline">
              <label style={{color: "#fff", fontSize: "14px", cursor: "pointer", marginBottom: "5px"}}>เข้าสู่ระบบ</label>
            </CButton>
          </CLink>
        </>
      );
    }
  }

  return (
    <CHeader withSubheader>
      <CToggler
        inHeader
        className="ml-md-3 d-lg-none"
        onClick={toggleSidebarMobile}
      />
      <CToggler
        inHeader
        className="ml-3 d-md-down-none"
        onClick={toggleSidebar}
      />
      <CHeaderBrand className="mx-auto d-lg-none" to="/">
        <CImg
          src={logoBetagro}
          className="c-sidebar-brand-full"
        />
      </CHeaderBrand>

      <CHeaderNav className="d-md-down-none mr-auto">
        <CHeaderNavItem className="px-3" >
          <CHeaderNavLink to="/dashboard">Dashboard</CHeaderNavLink>
        </CHeaderNavItem>
      </CHeaderNav>

      <CHeaderNav className="px-3">
        {/* <CToggler
          inHeader
          className="ml-3 d-md-down-none"
          onClick={() => dispatch({ type: 'set', darkMode: !darkMode })}
          title="Toggle Light/Dark Mode"
        >
          <CIcon name="cil-moon" className="c-d-dark-none" alt="CoreUI Icons Moon" />
          <CIcon name="cil-sun" className="c-d-default-none" alt="CoreUI Icons Sun" />
        </CToggler>
        <CToggler
          inHeader
          className="d-md-down-none"
          onClick={() => dispatch({ type: 'set', asideShow: !asideShow })}
TheHeaderDropdown        >
          <CIcon className="mr-2" size="lg" name="cil-applications-settings" />
        </CToggler> */}
        
        {renderHeaderMenu()}
      </CHeaderNav>

      <CSubheader className="px-3 justify-content-between">
        <CBreadcrumbRouter className="border-0 c-subheader-nav m-0 px-0 px-md-3" routes={routes} />
      </CSubheader>
    </CHeader>
  )
}

export default TheHeader
