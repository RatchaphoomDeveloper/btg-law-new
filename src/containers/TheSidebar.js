import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarNavDivider,
  CSidebarNavTitle,
  CSidebarNavDropdown,
  CSidebarNavItem,
  CImg,
  CContainer,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { VERSION_WEB } from "../../src/env";
// import { useDispatch, useSelector } from "react-redux";

// sidebar nav config
import navigation from "./_nav";
const logoBetagro = require("./../assets/icons/icon_logo.png");

const TheSidebar = () => {
  const [nevItem, setNevItem] = useState([]);
  const dispatch = useDispatch();
  const show = useSelector((state) => state.changeState.sidebarShow);
  const userState = useSelector((state) => state.changeState.user);

  useEffect(() => {
    
    if(Object.keys(userState).length > 0){
      handlePermission();
    }
    console.log("show::",show);
    return () => {};
  }, [userState]);

  // const handlePermission = () => {
  //   if (userState !== undefined){
  //     const newNav = []
  //     for (var i = 0; i < navigation.length; i++) {
  //       if (navigation[i].mustfilter === false){
  //         newNav.push(navigation[i])
  //       }else{
  //         let childrenArr = navigation[i]._children
  //         let newArr = []
  //           for (var j = 0; j < childrenArr.length; j++) {
  //             if (childrenArr[j].role !== undefined){
  //               if (childrenArr[j].role.includes(userState.role)){
  //                 newArr.push(childrenArr[j])
  //               }
  //             }else {
  //               newArr.push(childrenArr[j])
  //             }
  //           }
  //         newNav.push({
  //           _tag: navigation[i]._tag,
  //           name: navigation[i].name,
  //           route: navigation[i].route,
  //           icon: navigation[i].icon,
  //           mustfilter: navigation[i].mustfilter,
  //           _children: newArr
  //         })
  //       }
  //     }
  //     setNevItem(newNav)
  //   }
  // }

  const handlePermission = () => {
    if (userState !== undefined) {
      const newNav = [];
      for (var i = 0; i < navigation.length; i++) {
        if (navigation[i].mustfilter == false) {
          newNav.push(navigation[i]);
        } else {
          let childrenArr = navigation[i]._children;

          if (!childrenArr) {
            if (navigation[i].role) {
              console.log("menu::", navigation[i]);
              const found = navigation[i].role.some((r) =>
                userState.role.split(",").includes(r)
              );

              if (found) {
                newNav.push(navigation[i]);
              }
              continue;
            } else {
              continue;
            }
          } else if (childrenArr) {
            if (navigation[i].role) {
              const found = navigation[i].role.some((r) =>
                userState.role.split(",").includes(r)
              );

              if (found) {
                let newArr = [];
                for (var j = 0; j < childrenArr.length; j++) {
                  if (childrenArr[j].role !== undefined) {
                    const found = childrenArr[j].role.some((r) =>
                      userState.role.split(",").includes(r)
                    );

                    if (found) {
                      newArr.push(childrenArr[j]);
                    }
                  } else {
                    newArr.push(childrenArr[j]);
                  }
                }
                newNav.push({
                  _tag: navigation[i]._tag,
                  name: navigation[i].name,
                  route: navigation[i].route,
                  icon: navigation[i].icon,
                  mustfilter: navigation[i].mustfilter,
                  _children: newArr,
                });
              }
              continue;
            } else {
              let newArr = [];
              for (var j = 0; j < childrenArr.length; j++) {
                if (childrenArr[j].role !== undefined) {
                  const found = childrenArr[j].role.some((r) =>
                    userState.role.split(",").includes(r)
                  );

                  if (found) {
                    newArr.push(childrenArr[j]);
                  }
                } else {
                  newArr.push(childrenArr[j]);
                }
              }
              newNav.push({
                _tag: navigation[i]._tag,
                name: navigation[i].name,
                route: navigation[i].route,
                icon: navigation[i].icon,
                mustfilter: navigation[i].mustfilter,
                _children: newArr,
              });
            }
          }
        }
      }
      setNevItem(newNav);
    }
  };

  return (
    <CSidebar
      show={show}
      unfoldable
      onShowChange={(val) => {
      
        dispatch({ type: "set", sidebarShow: val })}
      }
    >
      <CSidebarBrand className="d-md-down-none" to="/">
        <CImg style={{ height: 80, width: 100 }} src={logoBetagro} />
        <CImg src={logoBetagro} className="c-sidebar-brand-minimized" />
      </CSidebarBrand>
      <CSidebarNav>
        <CCreateElement
          items={nevItem}
          components={{
            CSidebarNavDivider,
            CSidebarNavDropdown,
            CSidebarNavItem,
            CSidebarNavTitle,
          }}
        />
      </CSidebarNav>
      <CContainer className="text-center mb-2">
        <span>Version : {VERSION_WEB}</span>
      </CContainer>
      {/* <CSidebarMinimizer className="c-d-md-down-none" /> */}
    </CSidebar>
  );
};

export default React.memo(TheSidebar);
