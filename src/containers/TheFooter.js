import React from 'react'
import { CFooter, CLink } from '@coreui/react'
import {VERSION_WEB} from '../env'

const TheFooter = () => {
  return (
    <CFooter fixed={false}>
      <div>
        Version {VERSION_WEB}
      </div>
      <div className="ml-auto">
        {/* <span className="mr-1">Right text</span> */}
      </div>
    </CFooter>
  )
}

export default React.memo(TheFooter)
