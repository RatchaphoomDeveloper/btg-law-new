import AlertError from "../reusable/AlertError";
export const checkUser = (user) => {
  if (user.username) {
    return true;
  } else {
    AlertError("", "กรุณาเข้าสู่ระบบก่อนเข้าใช้งาน Feature นี้");
    return false;
  }
};

export const checkUserBool = (user) => {
  if (user.username) {
    return true;
  } else {
    return false;
  }
};
