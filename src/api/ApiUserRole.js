import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class Api extends Component {
  static get = async (id) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/GetMasterUserRole?id=${id}`,
      method: "get",
    });
    return result;
  };
  static create = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveMasterUserRole",
      method: "post",
      data: data,
    });
    return result;
  };
  static delete = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/DeleteMasterUserRole`,
      method: "post",
      data: data
    });
    return result;
  };
}

export default Api;
