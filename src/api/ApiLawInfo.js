import axios from "axios";
import { Component } from "react";
import { setHeaderAuth, setHeaderRole } from "../utils";

class Api extends Component {
  // static get = async (page) => {
  //     const result = await axios({
  //         url: `Master/GetTranBrcs?page=${page}`,
  //         method: "get",
  //     });
  //     return result;
  // };
  static delBrcsMainLawSub = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/DelBrcsMainLawSub?mainlawId=${data}`,
      method: "get",
    });
    return result;
  };
  static getAssessment = async (data) => {
    await setHeaderAuth();
    await setHeaderRole();
    const result = await axios({
      url: "Master/GetTranBrcsAssessmentHeader",
      method: "post",
      data: data,
    });
    return result;
  };
  static getAssessmentFile = async (id) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/GetTranBrcsAssessmentFile?id=${id}`,
      method: "get",
    });
    return result;
  };
  static getAssessmentAllFile = async (id) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/GetTranBrcsAssessmentAllFile?id=${id}`,
      method: "get",
    });
    return result;
  };
  static getAssessmentDetail = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/GetTranBrcsAssessmentDetailData",
      method: "post",
      data: data,
    });
    return result;
  };
  static get = async (data) => {
    await setHeaderAuth();
    await setHeaderRole();
    const result = await axios({
      url: "Master/GetTranBrcs",
      method: "post",
      data: data,
    });
    return result;
  };
  static getTranBrcsBu = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/GetTransBrsBuSel",
      method: "post",
      data: data,
    });
    return result;
  };

  static getFrequency = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/GetTranBrcsFrequency",
      method: "post",
      data: data,
    });
    return result;
  };
  static getDetail = async (id, code) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/GetTranBrcsDetail?id=${id}&code=${code}`,
      method: "get",
    });
    return result;
  };
  static create = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveTranBrcs",
      method: "post",
      data: data,
    });
    return result;
  };
  static updateTranBu = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/UpdateTranBrcsBu",
      method: "post",
      data: data,
    });
    return result;
  };
  static reject = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/RejectTranBrcsBu",
      method: "post",
      data: data,
    });
    return result;
  };
  static review = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/ReviewTranBrcs",
      method: "post",
      data: data,
    });
    return result;
  };
  static saveAssessSelected = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveTranBrcsAssesmentStatusSelected",
      method: "post",
      data: data,
    });
    return result;
  };
  static saveReEvaluate = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveTranBrcsReEvaluate",
      method: "post",
      data: data,
    });
    return result;
  };
  static saveAssessment = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveTranBrcsAssessment",
      method: "post",
      data: data,
    });
    return result;
  };
  static saveAssess = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveTranBrcsAssesmentStatus",
      method: "post",
      data: data,
    });
    return result;
  };
  static saveEstimate = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveTranBrcsAssesmentEstimate",
      method: "post",
      data: data,
    });
    return result;
  };
  static saveAssessFileToDB = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveTranBrcsAssesmentFileToDB",
      method: "post",
      data: data,
    });
    return result;
  };
  static saveImportFileToTemp = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveTranBrcsFileImportToTemp",
      method: "post",
      data: data,
    });
    return result;
  };
  static saveImport = async (data) => {
    await setHeaderAuth();
    console.log("saveDataImport", data);
    const result = await axios({
      url: "Master/SaveImport",
      method: "post",
      data: data,
    });
    return result;
  };
  static getTemp = async (user) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/GetTranBrcsFileImportToTemp?user=${user}`,
      method: "get",
    });
    return result;
  };
  static getMainLaw = async (id) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/GetBrcsMainLaw?id=${id}`,
      method: "get",
    });
    return result;
  };
  static createMainLaw = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveBrcsMainLaw",
      method: "post",
      data: data,
    });
    return result;
  };
  static deleteMainLaw = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/DeleteBrcsMainLaw",
      method: "post",
      data: data,
    });
    return result;
  };
  static deleteMainLawSub = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/DeleteBrcsMainLawSub?id=${data.id}`,
      method: "post",
    });
    return result;
  };
  static getMainLawSub = async (mainlawId, bu_id) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/GetBrcsMainLawSub?id=${mainlawId}&bu_id=${
        bu_id ? bu_id : 0
      }`,
      method: "get",
    });
    return result;
  };

  static getAssesmentMonthList = async (mainlaw_id) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/GetAssesmentMonthList?id=${mainlaw_id}`,
      method: "get",
    });
    return result;
  };
  static createMainLawSub = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveBrcsMainLawSub",
      method: "post",
      data: data,
    });
    return result;
  };
  static createMainLawSubList = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveBrcsMainLawSubList",
      method: "post",
      data: data,
    });
    return result;
  };
  static getPriority = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/GetMasterPriority",
      method: "get",
    });
    return result;
  };
  static getTranBrcsFileSel = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/GetTranBrcsFileSel?brcs_id=${data}`,
      method: "get",
    });
    return result;
  };
  static deleteFile = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/DeleteTranBrcsFile?id=${data.id}`,
      method: "post",
    });
    return result;
  };
  static deleteFileAssessment = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/DeleteTranBrcsAssessmentFile?id=${data.id}`,
      method: "post",
    });
    return result;
  };
  static saveMainLawSubBuList = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveBrcsMainLawSubBuList",
      method: "post",
      data: data,
    });
    return result;
  };
}

export default Api;
