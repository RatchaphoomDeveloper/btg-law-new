import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class Api extends Component {
  static get = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/GetMasterConfig",
      method: "get",
    });
    return result;
  };
  static getHistory = async (id) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/GetMasterConfigHistory?id=${id}`,
      method: "get",
    });
    return result;
  };
  static create = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveMasterConfig",
      method: "post",
      data: data,
    });
    return result;
  };
}

export default Api;
