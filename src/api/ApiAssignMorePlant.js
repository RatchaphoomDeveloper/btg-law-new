import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class Api extends Component {
  static getFromOtherUserBu = async (id) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/GetOtherBuAssignFromUser?id=${id}`,
      method: "get",
    });
    return result;
  };
  static create = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveAssignMorePlant",
      method: "post",
      data: data,
    });
    return result;
  };
}

export default Api;
