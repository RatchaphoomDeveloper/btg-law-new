import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class Api extends Component {
  static get = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/GetMasterUser",
      method: "get",
    });
    return result;
  };
  static getDetail = async (id) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/GetMasterUserDetail?id=${id}`,
      method: "get",
    });
    return result;
  };
  static getFromBuAssign = async (id) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/GetMasterUserFromBuAssign?id=${id}`,
      method: "get",
    });
    return result;
  };
  static getBuOption = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/GetMasterBuOption",
      method: "get",
    });
    return result;
  };
  static getSiteOption = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/GetMasterPlantOption",
      method: "get",
    });
    return result;
  };
  static getDivisionOption = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/GetMasterDivisionOption",
      method: "get",
    });
    return result;
  };
  static create = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveMasterUser",
      method: "post",
      data: data,
    });
    return result;
  };
  static delete = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/DeleteMasterUser?id=${data.id}`,
      method: "post",
    });
    return result;
  };
}

export default Api;
