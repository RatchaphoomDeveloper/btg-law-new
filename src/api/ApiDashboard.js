import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class Api extends Component {
  static get = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/GetDashboard",
      method: "get",
    });
    return result;
  };
  static getData = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/GetDashboardData",
      method: "get",
    });
    return result;
  };
  static getTask = async (id) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/GetDashboardTask?user=${id}`,
      method: "get",
    });
    return result;
  };

  static getDataGraphMainlawByBu = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/GetDashboardGraphMainlawByBu",
      method: "get",
    });
    return result;
  };

  static getDataGraphMainlawByMonth = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/GetDashboardGraphMainlawByMonth",
      method: "get",
    });
    return result;
  };
  static getDashboardGraphMainlawByLawGroup = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/GetDashboardGraphMainlawByLawGroup",
      method: "get",
    });
    return result;
  };
}

export default Api;
