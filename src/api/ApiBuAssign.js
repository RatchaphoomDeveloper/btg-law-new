import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class Api extends Component {
  static get = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/GetBuAssign",
      method: "get",
    });
    return result;
  };
  static getFromUserBu = async (id) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/GetBuAssignFromUser?id=${id}`,
      method: "get",
    });
    return result;
  };
  static create = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveBuAssign",
      method: "post",
      data: data,
    });
    return result;
  };
  static delete = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/DeleteBuAssign?id=${data.id}`,
      method: "post",
    });
    return result;
  };
}

export default Api;
