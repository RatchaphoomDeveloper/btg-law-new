import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class Api extends Component {
  static get = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/GetMasterPlant",
      method: "get",
    });
    return result;
  };
  static getByBu = async (bu) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/GetMasterPlantByBu?bu=${bu}`,
      method: "get",
    });
    return result;
  };
  static getBuOption = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/GetMasterBuOption",
      method: "get",
    });
    return result;
  };
  static create = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveMasterPlant",
      method: "post",
      data: data,
    });
    return result;
  };
  static delete = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/DeleteMasterPlant?id=${data.id}`,
      method: "post",
    });
    return result;
  };
}

export default Api;
