import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiReportGraph extends Component {
  static Report1 = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Report/ReportGraph1",
      method: "post",
      data: data,
    });
    return result;
  };
  static Report2 = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Report/ReportGraph2",
      method: "post",
      data: data,
    });
    return result;
  };
  static Report2SubLaw = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Report/ReportGraph2LawGroupSub",
      method: "post",
      data: data,
    });
    return result;
  };
  static Report2SubLawPlant = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Report/ReportGraph2LawGroupSubPlant",
      method: "post",
      data: data,
    });
    return result;
  };
  static Report3 = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Report/ReportGraph3",
      method: "post",
      data: data,
    });
    return result;
  };

  static Report3Detail = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Report/ReportGraph3Detail",
      method: "post",
      data: data,
    });
    return result;
  };

  static Report3Priority = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Report/ReportGraph3Priority",
      method: "post",
      data: data,
    });
    return result;
  };
}

export default ApiReportGraph;
