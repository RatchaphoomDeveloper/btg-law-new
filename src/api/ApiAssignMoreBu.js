import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiAssignMoreBU extends Component {
//   static get = async () => {
//     await setHeaderAuth();
//     const result = await axios({
//       url: "Master/GetMasterBu",
//       method: "get",
//     });
//     return result;
//   };
  static getBuOption = async (id) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/GetMasterOtherBuOption?id=${id}`,
      method: "get",
    });
    return result;
  };
  static create = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveAssignMoreBu",
      method: "post",
      data: data,
    });
    return result;
  };
//   static delete = async (data) => {
//     await setHeaderAuth();
//     const result = await axios({
//       url: `Master/DeleteMasterBu?id=${data.id}`,
//       method: "post",
//     });
//     return result;
//   };
}

export default ApiAssignMoreBU;
