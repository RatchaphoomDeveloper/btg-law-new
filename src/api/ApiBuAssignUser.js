import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class Api extends Component {
  static get = async (id) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/GetBuAssignUser?id=${id}`,
      method: "get",
    });
    return result;
  };
  static create = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveBuAssignUser",
      method: "post",
      data: data,
    });
    return result;
  };
  static delete = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/DeleteBuAssignUser?id=${data.id}`,
      method: "post",
    });
    return result;
  };
}

export default Api;
