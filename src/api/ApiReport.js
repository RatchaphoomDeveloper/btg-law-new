import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiReport extends Component {
  static Report1 = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Report/Report1",
      method: "post",
      data: data,
    });
    return result;
  };
  static ExportReport1 = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Report/ExportReport1",
      method: "post",
      data: data,
    });
    return result;
  };
  static Report2 = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "report/report2",
      method: "post",
      data: data,
    });
    return result;
  };
  static ExportReport2 = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Report/ExportReport2",
      method: "post",
      data: data,
    });
    return result;
  };
  static Report3 = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "report/report3",
      method: "post",
      data: data,
    });
    return result;
  };
  static ExportReport3 = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Report/ExportReport3",
      method: "post",
      data: data,
    });
    return result;
  };
  static Report4 = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "report/report4",
      method: "post",
      data: data,
    });
    return result;
  };
  static ExportReport4 = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Report/ExportReport4",
      method: "post",
      data: data,
    });
    return result;
  };
  static Report5 = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Report/Report5",
      method: "post",
      data: data,
    });
    return result;
  };
}

export default ApiReport;
