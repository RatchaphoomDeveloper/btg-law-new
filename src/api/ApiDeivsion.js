import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class Api extends Component {
  static get = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/GetMasterDivision",
      method: "get",
    });
    return result;
  };
  static create = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveMasterDivision",
      method: "post",
      data: data,
    });
    return result;
  };
  static delete = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/DeleteMasterDivision?id=${data.id}`,
      method: "post",
    });
    return result;
  };
}

export default Api;
