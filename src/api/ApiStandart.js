import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class Api extends Component {
  static get = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/GetMasterStandart",
      method: "get",
    });
    return result;
  };
  static create = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveMasterStandart",
      method: "post",
      data: data,
    });
    return result;
  };
  static delete = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/DeleteMasterStandart?id=${data.id}`,
      method: "post",
    });
    return result;
  };
}

export default Api;
