import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class Api extends Component {
  static getDept = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/GetRegularLawDept",
      method: "get",
    });
    return result;
  };
  static getDeptOption = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/GetMasterLawAgencies",
      method: "get",
    });
    return result;
  };
  static createDept = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveRegularLawDept",
      method: "post",
      data: data,
    });
    return result;
  };
  static deleteDept = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/DeleteRegularLawDept?id=${data.id}`,
      method: "post",
    });
    return result;
  };
  static getType = async (id) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/GetRegularLawType?id=${id}`,
      method: "get",
    });
    return result;
  };
  static getTypeOption = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/GetMasterLawType",
      method: "get",
    });
    return result;
  };
  static createType = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveRegularLawType",
      method: "post",
      data: data,
    });
    return result;
  };
  static deleteType = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/DeleteRegularLawType?id=${data.id}`,
      method: "post",
    });
    return result;
  };
  static get = async (id) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/GetRegularLaw?id=${id}`,
      method: "get",
    });
    return result;
  };
  static create = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveRegularLaw",
      method: "post",
      data: data,
    });
    return result;
  };
  static delete = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/DeleteRegularLaw?id=${data.id}`,
      method: "post",
    });
    return result;
  };
}

export default Api;
