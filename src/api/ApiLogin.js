import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class Api extends Component {
  static login = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Account/Login",
      method: "post",
      data: data,
    });
    return result;
  };
  static loginAdfs = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Account/LoginWithAdfs",
      method: "post",
      data: data,
    });
    return result;
  };
  static logout = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Account/Logout",
      method: "post",
      data: data,
    });
    return result;
  };
}

export default Api;
