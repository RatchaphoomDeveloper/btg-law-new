import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiBU extends Component {
  static getAll = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/GetActionPlanData?id=${data.id}&acpId=${data.acpId}`,
      method: "get",
    });
    return result;
  };
  static getUserFromAssessment = async (id) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/GetUserFromAssessment?id=${id}`,
      method: "get",
    });
    return result;
  };
  static createCause = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveCause",
      method: "post",
      data: data,
    });
    return result;
  };
  static deleteCause = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/DeleteCause?id=${data.id}`,
      method: "post",
    });
    return result;
  };
  static createResolve = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveResolve",
      method: "post",
      data: data,
    });
    return result;
  };
  static deleteResolve = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/DeleteResolve?id=${data.id}`,
      method: "post",
    });
    return result;
  };
  static createPrv = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SavePrevent",
      method: "post",
      data: data,
    });
    return result;
  };
  static deletePrv = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/DeletePrevent?id=${data.id}`,
      method: "post",
    });
    return result;
  };
  static saveActionPlanFileToDB = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "Master/SaveTranBrcsActionPlanFileToDB",
      method: "post",
      data: data,
    });
    return result;
  };

  static getActionPlanFile = async (id, type) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/GetActionPlanFile?id=${id}&type=${type}`,
      method: "get",
    });
    return result;
  };
  static deleteActionPlanFile = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `Master/DeleteTranBrcsActionPlanFile?id=${data.id}`,
      method: "post",
    });
    return result;
  };
}

export default ApiBU;
